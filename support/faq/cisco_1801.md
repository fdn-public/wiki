##### Configuration de routeur Cisco pour l\'ADSL, IPv4 et IPv6

### Préambule

Ce guide montre la configuration basique d\'un routeur cisco en
dual-stack pour l\'ADSL (le mien est un 1801, mais on peut faire la même
chose avec un 877 par exemple). Je n\'ai pas testé cette procédure en
repartant de rien donc il n\'est pas impossible que j\'aie oublié des
éléments, mais il ne manque probablement pas grand chose quand même. Si
un point vous pose problème, contactez-moi à clementATguivyPOINTfr.

On prend les hypothèses suivantes :

 * atm0 est votre interface ADSL
 * eth0 est votre interface ethernet reliée à votre réseau local
 * une IPv4 publique vous est attribuée par votre FAI, 80.67.176.202 pour exemple
 * un range IPv6 vous est attribué par votre FAI, 2001:910:10CA::/48 pour exemple

### Configuration de base, IPv4 et NAT

Fixons tout d\'abord le hostname du routeur et activons le chiffrement
des mots de passe. On peut aussi configurer les messages de login. Tout
cela se fait via le mode configuration, commande \"conf t\" après être
passé en mode enable via la commande éponyme (les caractères \^C sont
des sauts de ligne) :

   hostname mon_routeur
   service password-encryption
   banner login ^C Bonjour. ^C
   banner motd ^C^C

puis on va configurer une adresse ip sur notre interface LAN, et la
définir comme interface intérieure (pour le NAT) :

   interface eth0
   ip address 10.0.0.1 255.0.0.0
   ip nat inside
   no shut

configurons maintenant l\'interface ATM :

   interface atm0
   pvc <vpi/vci> 8/35
    pppoe-client dial-pool-number 1

Nous avons maintenant configuré la partie ATM, la couche du dessus (ppp
et IP) étant à configurer dans une interface séparée, \"dialer\". On va
donc maintenant configurer l\'interface dialer, en la désignant au
passage comme interface extérieure pour le NAT. Le login/password
relatif au ppp étant bien sûr à adapter (ils vous sont fournis par votre
FAI) :

   interface dialer1
   ip address 80.67.176.202 255.255.255.255
   ip nat outside
   encapsulation ppp
   dialer pool 1
   ppp chap hostname prenom.nom@fdn.nerim
   ppp chap password 0 VOTRE_PASSWORD

On positionne la route par défaut vers le Grand Internet :

   ip route 0.0.0.0 0.0.0.0 Dialer1

Terminons de configurer le NAT. En mode configuration (sans être dans
une interface particulière). On commence par créer une access-list pour
préciser quelles IP internes seront NATées :

   access-list 1 permit 10.0.0.0 0.255.255.255

et on active le NAT :

   ip nat inside source list 1 interface Dialer1 overload

On peut aussi vouloir qu\'un serveur du LAN soit joignable de
l\'extérieur. Par exemple pour un serveur web qui écouterait sur le port
tcp 80 à l\'adresse 10.0.0.10, on ferait :

   ip nat inside source static tcp 10.0.0.10 80 interface Dialer1 80

A ce niveau et si je n\'ai rien oublié, on a une connexion IPv4
fonctionnelle, les postes à l\'intérieur du LAN accèdent à internet, et
nos serveurs sont également joignables depuis l\'extérieur en IPv4
suivant les forwardings spécifiés.

### IPv6 et firewalling

Activons maintenant le routage IPv6 et configurons des adresses sur nos
interfaces LAN et WAN, puis fixons la route par défaut :

   ipv6 unicast-routing
   interface eth0
   ipv6 address 2001:910:10CA::1/64
   interface di1
   ipv6 address 2001:910:10CA:1::1/64
   exit
   ipv6 route ::/0 Di1

A ce stade on devrait avoir ipv6 qui fonctionne sur les machines du LAN,
y compris les serveurs qui devraient être directement accessibles de
l\'extérieur. Par contre vous n\'avez peut-être pas envie que votre LAN
soit en mode open-bar pour n\'importe qui sur internet à cause de
l\'IPv6. Nous allons donc remédier à ça, en mettant un peu de
firewalling sur IPv6, pour se rapprocher du compportement qu\'on a en
IPv4 avec le NAT, les désagréments en moins. Grâce à la commande ipv6
inspect on va autoriser le traffic à passer seulement dans un sens, dans
l\'acception TCP/UDP du terme. Dit autrement on va permettre d\'initier
des connexions depuis l\'intérieur vers l\'extérieur, mais pas
l\'inverse.

On commence par créer une access-list qui bloque tout traffic :

   ipv6 access-list block_ipv6
   sequence 100 deny ipv6 any any

Et on l\'applique sur notre interface dialer en entrée :

   interface dialer1
   ipv6 traffic-filter block_ipv6 in

Ainsi tout le traffic IPv6 est bloqué, ce qui offre une grosse sécurité
tout en étant parfaitement inutilisable. On va maintenant définir le
type de traffic à inspecter (en l\'occurence tout) et appliquer cette
inspection sur l\'interface dialer. Le traffic concerné court-circuitera
l\'ACL précédente dans le sens intérieur -\> extérieur, en gros.

   ipv6 inspect name firewall_ipv6 tcp
   ipv6 inspect name firewall_ipv6 udp
   ipv6 inspect name firewall_ipv6 icmp
  
   interface di1
   ipv6 inspect firewall_ipv6 out

Si vous avez suivi jusque là vous avez compris que vos serveurs sur
votre LAN ne sont plus accessibles en IPv6 depuis l\'extérieur, puisque
le traffic entrant est bloqué. Pour régler cela il va donc falloir aller
renseigner notre ACL de tout à l\'heure pour y ajouter les autorisations
nécessaires. Ainsi pour notre serveur web qui a l\'IP 2001:910:10CA::10
:

   ipv6 access-list block_ipv6
   sequence 1 permit tcp any host 2001:910:10CA::10 eq www

Vous pouvez ainsi ajouter les règles d\'autorisation que vous voulez, en
leur mettant un numéro inférieur à 100 bien sûr (puisque la règle 100
bloque tout le traffic).

Et voilà : normalement maintenant vous avez un accès dual-stack avec un
LAN IPv6 protégé.

A propos de protection, vous voudrez peut-être (comme moi) n\'autoriser
l\'accès à votre routeur que depuis le LAN et être directement en mode
enable quand vous vous loggez :

   ipv6 access-list local
    sequence 1 permit ipv6 2001:910:10CA::/64 any
    sequence 100 deny ipv6 any any
   exit
   no aaa new-model
   line vty 0 15
   access-class 1 in
   ipv6 access-class local in
   login
   password 0 votrepassword
   exit

Une petite remarque enfin : sur certains IOS l\'activation du ipv6
inspect ralentit énormément le débit des téléchargements en IPv6 (à
quelques Ko/s max). Un IOS récent de branche M est donc recommandé. Dans
mon cas j\'ai rencontré ce problème de performances avec le
c180x-advipservicesk9-mz.124-24.T5.bin, tandis que cela fonctionne
normalement avec le c180x-advipservicesk9-mz.150-1.M6.bin.

### Bonus : DHCP

Vous pouvez aussi faire jouer le rôle de serveur DHCP (v4) à votre
routeur :

   ip dhcp excluded-address 10.0.0.1 10.0.0.100
   ip dhcp pool pool1
   network 10.0.0.0 255.0.0.0
   defaut-router 10.0.0.1
   dns-server 80.67.169.12 80.67.169.40
   exit

faire une réservation sur l\'adresse MAC d\'un poste :

   ip dhcp pool poste1
      host 10.0.0.40 255.0.0.0
      hardware-address 0050.84ce.ab2c
   exit

(attention pour les postes windows il semble y avoir une subtilité : il
faut utiliser le client-identifier plutôt que l\'adresse MAC pour que ça
fonctionne, je n\'ai pas compris les tenants et aboutissants de cela
mais c\'est ce que j\'ai constaté, je vous laisse chercher).

### Troubleshooting DSL

Enfin, pour consulter les informations sur votre interface ADSL : sh dsl
interface atm0

Les deux colonnes DS et US sont pour le sens descendant et montant
respectivement. Les points intéressants sont surtout le Noise margin
(plus c\'est haut mieux c\'est), l\'attenuation (plus c\'est bas mieux
c\'est), et plus bas, le speed (votre débit).

Enfin pour les bricoleurs il existe une commande cachée pour tweaker
votre interface ADSL pour permettre de grapiller quelques kbps ou même
tout simplement de la faire fonctionner sur une ligne pourrie.

   service internal

et là dans l\'interface atm0 vous pourrez aller régler le paramètre dsl
noise-margin avec une valeur allant de -3 à +3 pour régler la
sensibilité de votre modem, soit pour obtenir un plus haut débit au prix
d\'une stabilité plus faible, soit l\'inverse. Ces commandes ne sont pas
officiellement supportées (d\'ailleurs vous ne pouvez pas trouver le
\"service internal\" avec l\'autocomplétion), donc ça pourrait donner
des résultats aléatoires, vous êtes prévenus.
