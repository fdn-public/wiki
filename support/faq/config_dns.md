# Configuration du client DNS
Les requêtes DNS peuvent se faire sur différents protocoles sécurisés ou non. Historiquement UDP est utilisé conjointement avec TCP sur le port 53 et de façon non sécurisée. Plus récemment sont apparus DoT (DNS over TLS) et DoH (DNS over HTTPS) qui sont plus sécurisés que ce soit en terme de confidentialité ou d'intégrité (nécessite toutefois de faire confiance au résolveur).

## Au niveau applicatif (navigateur web)
Une configuration à ce niveau limitera l'usage du DNS sécurisé à l'application configurée et celle-ci ignorera la configuration système.
Généralement les navigateurs web courants supportent le DNS sécurisé via DoH. Cette configuration se fait dans les paramètres de ce dernier.

> Certains navigateurs sont pré-configurés avec un serveur DoH qui ne correspondra peut-être pas à celui que vous désirez.

### Firefox
Firefox ne permet pas d'activer le DoH sur les versions mobiles (Android et iOS). Il est toutefois possible de l'activer [au niveau du système](#au-niveau-système).

Pour les autres versions, vous pouvez suivre la procédure qui se trouve [ici](https://support.mozilla.org/fr/kb/dns-via-https-firefox#w_activation-et-desactivation-manuelles-du-dns-via-https).

À la fin vous devriez avoir quelque chose de semblable à la capture d'écran (partielle) ci-dessous :

![](images/firefox_doh.png)

### Chrom(ium)
Pour le configurer sur les navigateurs Chrome/Chromium, suivez les instructions sur cette [page](https://support.google.com/chrome/answer/10468685?sjid=17629247259625132869-EU) rubrique « Utiliser une connexion sécurisée pour rechercher les adresses IP des sites »

Voici ce que vous devriez avoir (capture d'écran partielle) :

![](images/chromium_doh.png)

## Au niveau système

### Linux
Glibc n'est pas capable d'utiliser des serveurs DNS autrement qu'avec UDP/TCP. La solution est de passer par un stub resolver. Il s'agit d'un petit logiciel qui sert d'intermédiaire entre vos applications et le serveur DNS que vous souhaitez interroger (serveur DNS récursif).

> glibc n'est pas capable de faire la validation DNSSEC tout seul. Cette validation doit être faite soit par un serveur récursif soit un stub resolver.

#### systemd-resolved

La configuration des serveurs DNS peut se faire globalement ou par lien réseau.

Pour se connecter à un serveur DNS par TLS il faut renseigner l'adresse IP car l'on n'est pas capable de résoudre les noms à ce moment là. Par contre, il faut renseigner le nom de domaine pour pouvoir faire la vérification du certificat et s'assurer que la connexion est bien sécurisée.

Voici un exemple de configuration globale utilisant les serveurs de FDN dans `/etc/systemd/resolved.conf`:

```
[Resolved]
DNS=80.67.169.12#ns0.fdn.fr 2001:910:800::12#ns0.fdn.fr
FallbackDNS=80.67.169.40#ns1.fdn.fr 2001:910:800::40#ns.fdn.fr
DNSOverTLS=yes
```

Les configurations par lien sont faites par les gestionnaires de réseau compatibles comme systemd-networkd ou Network Manager.

Pour plus de renseignement sur les paramètres : [resolved.conf(8)](https://systemd.network/resolved.conf.html), [systemd.network(5)](https://systemd.network/systemd.network.html), [NetworkManager DNS handling](https://wiki.gnome.org/Projects/NetworkManager/DNS)

#### Stubby

Stubby est accessible par IPv4 ou IPv6 en fonction de sa configuration. Pour pouvoir l'utiliser, il faudra configurer sur votre interface l'adresse IP associée à Stubby, généralement `127.0.0.1` pour l'IPv4 et/ou `::1` pour l'IPv6. L'exemple ci-dessous utilise ces dernières.

`/etc/stubby/stubby.yml`:
```yaml
resolution_type: GETDNS_RESOLUTION_STUB
dns_transport_list:
  - GETDNS_TRANSPORT_TLS
tls_authentication: GETDNS_AUTHENTICATION_REQUIRED
tls_query_padding_blocksize: 256
edns_client_subnet_private : 1
idle_timeout: 10000
listen_addresses:
  - 127.0.0.1
  - ::1
round_robin_upstreams: 1
upstream_recursive_servers:
  - address_data: 80.67.169.12
    tls_auth_name: "ns0.fdn.fr"
  - address_data: 2001:910:800::12
    tls_auth_name: "ns0.fdn.fr"
  - address_data: 80.67.169.40
    tls_auth_name: "ns1.fdn.fr"
  - address_data: 2001:910:800::40
    tls_auth_name: "ns1.fdn.fr"
```

Plus d'information sur Stubby et les possibilités de configuration sur cette [page](https://dnsprivacy.org/dns_privacy_daemon_-_stubby/configuring_stubby/).

### Windows

### Android

La configuration peut varier d'un modèle de téléphone à un autre. Voici le [lien](https://support.google.com/android/answer/9654714?hl=fr#zippy=%2Cdns-priv%C3%A9) (rubrique « DNS privé ») de la documentation officielle Android.

## Au niveau du router/serveur DHCP


## Problèmes courants

### La résolution ne se fait pas en DoT (DNS over TLS)
Le DoT utilise le port TCP/853. Ce dernier, n'étant pas très courant, peut être bloqué sur des réseaux plus fermés que la moyenne contrairement au port 53 (DNS classique) ou au TCP/443 (site web sécurisé) qui sont d'usage courant depuis bien plus longtemps.

Malheureusement il est compliqué de contourner cette limitation. Si vous ne pouvez utiliser un réseau plus ouvert, il reste la possibilité de configurer du DoH directement dans votre navigateur.
