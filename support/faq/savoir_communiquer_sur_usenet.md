###### Le Savoir communiquer sur Usenet

Délicatement, vous ouvrez l\'enveloppe que vient de vous envoyer
l\'organisme ou l\'association pour l\'ouverture de votre compte sur
Usenet. Fébrile, vous contemplez cette missive vous proposant vos
sésames.

Mais comment utiliser au mieux Usenet ? A-t-on le droit de tout dire, ou
de tout faire sur ce réseau composé de plusieurs milliers d\'ordinateurs
?

Il y a des règles comme en toutes choses. Je vous en présente ici
quelques unes, en style de dix commandements. Un résumé se trouve en fin
d\'article.

Bien sûr, ce texte n\'a pas la prétention de vous apprendre à
communiquer avec la communauté Usenet. Lisez-le comme un guide pour une
utilisation pratique, rationnelle, agréable et efficace de ce réseau.

##### Le lecteur jamais tu n\'oublieras

A force de tapoter sur un ordinateur, on oublie parfois que nos messages
sont adressés à d\'autres utilisateurs. D\'autres lecteurs comme nous,
qui ne sont pas forcément dans le même bain culturel ou historique. Il
faut faire attention à nos écrits. Ne pas blesser par des mots trop
crus.

Il ne faut jamais oublier qu\'un message écrit sur Usenet est lu dans le
monde entier par d\'autres utilisateurs du réseau. Vos écrits restent.
Prenez le temps de bien rédiger votre article. Si vous utilisez le
réseau pour crier votre colère, ne la laissez pas vous emporter en
écrivant des mots qui pourraient dépasser votre pensée. Une bonne nuit
de sommeil est parfois de bon conseil.

##### L\'administrateur tu n\'agresseras pas

Le responsable du site sur lequel vous vous connectez se nomme
l\'administrateur réseau. Il recoit bien plus d\'informations que
vous-même. En effet, un serveur de Usenet news transporte souvent plus
de dix mega-octets de données par jour.

Trop souvent, malheureusement, l\'administrateur n\'a pas le temps de
lire ces informations. N\'oublions pas également qu\'il est souvent un
bénévole (dans une association) et que la maintenance d\'un serveur de
news nécessite un minimum de temps chaque jour. En fait, il s\'agit
souvent d\'un véritable sacerdoce !

S\'il vous vient donc à l\'esprit d\'écrire directement à
l\'administrateur réseau à propos de son site ou d\'un utilisateur dont
les textes vous déplaisent, n\'oubliez pas de respecter les régles
élémentaires de courtoisie. L\'administrateur ne connaît pas toujours le
sujet de votre message. Essayez d\'être aussi clair, simple et modéré
que possible. Vous aurez ainsi toutes les chances d\'attirer son
attention.

##### La prudence dans les écrits tu emploieras

Plus de 2 500 000 personnes utilisent Usenet dans le monde entier
(chiffres 1991). Il ne faut jamais l\'oublier. Pensez aussi que vos
proches, votre quartier, votre ville font partie de ce monde entier. Et
que parmi les lecteurs se trouve peut-être votre patron ou futur patron,
votre beau-frère ou un ami de vos parents.

La plupart des gens sur Usenet ne vous connaissent que par le fond et la
forme de vos écrits. Faites donc attention à ce que vos écrits ne vous
mettent pas un jour dans l\'embarras.

Avant de poster une information dans un groupe, pensez à vous-même et
aux autres lecteurs. Evitez les fautes d\'orthographe et assurez-vous
que votre texte est facile à lire. Ecrire est un art qui demande de la
pratique. Bien des gens vous jugeront par ces écrits. Et, peut-être même
un jour, pourquoi pas, pourriez-vous en bénéficier.

##### Brièvement tu écriras

Pourquoi écrire en plusieurs lignes ce qui peut tenir en quelques mots ?
Très souvent, plus votre article est long et plus le lecteur le trouvera
ennuyeux. Un message bref aura un meilleur impact.

##### Des titres clairs tu choisiras

Comment voulez-vous qu\'un lecteur pressé lise correctement les news si
le titre manque de précision ? Indiquez clairement dans ce titre le
sujet de votre article. Un titre du style \"Voiture à vendre\" dans le
newsgroup rec.autos (newsgroup consacré aux automobiles) est beaucoup
moins évident à comprendre que \"R 11 GTL à vendre sur Paris\". Mais
restez tout de même concis : certains logiciels, lecteurs de news,
tronquent le titre au 40e caractère.

Lorsque vous répondez à un article, verifiez que le sujet de l\'article
correspond bien encore à son objet. Au fur et à mesure des réponses, il
arrive que, l\'objet de la discussion évoluant, le sujet de l\'article
n\'ait plus rien à voir avec le contenu. Dans ce cas, n\'hésitez pas à
changer le sujet de votre réponse, en placant si possible l\'ancien
titre entre crochet (\"\[\" et \"\]\") à la suite de votre propre titre.

##### A l\'audience toujours tu penseras

Lorsque vous postez un article, pensez aux lecteurs que vous voulez
atteindre. Poser des questions sur UNIX dans le newsgroup fr.rec.cuisine
(newsgroup francophone consacré à la discussion sur la cuisine) sera mal
vu et touchera forcément beaucoup moins de gens que si cette question et
posée dans le newsgroup comp.unix.questions ou fr.comp.os.unix.
Choisissez la meilleure audience possible pour votre message, pas la
plus large.

Si l\'intérêt de votre message est géographiquement limité
(appartements, voiture à vendre, concerts, etc\...) limitez-en sa
distribution à votre région. Le champ \"Distribution\" est disponible
dans ce but.

Si vous voulez faire un test, n\'utilisez pas un groupe international !
Envoyer un message dans le newsgroup misc.misc avec comme titre \"Ceci
est un test\" est le meilleur moyen de voir affluer des messages
caustiques dans votre boîte aux lettres. Utilisez plutôt des newsgroups
locaux plus proches de vous géographiquement, tel que fr.test si vous
résidez en France. Si vous voulez vraiment tester la distribution
internationale, il existe de nombreux groupes de test (alt.test,
misc.test, news.test etc\...) qui bien souvent, grâce à un automate,
vous enverra directement par e-mail un message de confirmation.

Familiarisez-vous avec le newsgroup avant d\'y envoyer vos articles ! Ne
postez pas une news dans un groupe que vous ne lisez jamais, ou peu.
Vous pourriez ignorer ses conventions et ses usages.

Normalement, vous ne vous joindrez pas à une conversation si votre
passage dans ce groupe est exceptionnel. Alors que si vous lisez
fréquemment ce groupe, vous pourrez rejoindre une conversation avec des
remarques pertinentes.

##### A l\'humour et aux sarcasmes tu prendras garde

Sans les intonations vocales de nos conversations habituelles, une
remarque qui se veut drôle peut être très mal interprétée. De même, un
humour trop subtil risque de ne pas être remarqué.

Peu à peu sur le réseau, certains symboles ont vu le jour, appelés
smiley face. Du style :-) terminant votre article ou une section qui se
veut amusante.

Mais soyez également conscient que de vrais articles satiriques sont
souvent postés sans explications explicites. Si un article vous outrage
profondément, demandez-vous s\'il ne faut pas le prendre au second
degré. Certaines personnes l\'avouant parfois publiquement d\'ailleurs
se refusent à l\'utilisation des smiley faces. Donc prenez garde ou vous
pourriez friser l\'apoplexie.

##### Une seule fois ton message tu posteras

A moins que ce ne soit approprié, évitez de poster votre article dans
plusieurs newsgroups à la fois, ceci afin d\'éviter de saturer le réseau
et de laisser les utilisateurs ne pas retrouver systématiquement une
copie votre message dans chacun des groupes qu\'ils lisent.

Surtout ne postez pas votre article plusieurs fois dans differents
groupes. Choisissez plutot le cross-post en indiquant simplement la
liste des groupes destinataires dans la même ligne du champ
\"Newsgroups\". Dans le premier cas, votre article sera integralement
recopié dans chacuns des groupes, alors que dans le second sur les
systèmes appropriés seul l\'original se trouvera dans un des groupes
choisis et un lien sera établi dans les autres groupes destinataires.
Cette derniere solution vous permettra donc d\'économiser de la place
disque et surchargera moins le réseau.

N\'utilisez pas le cross-post (l\'envoi dans plusieurs groupes
simultanément) d\'une facon trop généreuse. Il est souvent inutile par
exemple d\'envoyer votre article dans plusieurs groupes tres proches les
uns des autres. Choisissez le groupe le mieux approprié. En cas de doute
recherchez ce groupe dans la liste du fichier newsgroup. Ce fichier
texte est disponible sur de nombreux serveurs et contient normalement la
liste complète des groupes actifs avec une petite description pour
chaque groupe.

##### Par mail, le plus souvent, tu répondras

Il est désagréable pour tous de lire plusieurs réponses identiques à une
question posée. Ces news traversent plusieurs fois le réseau
inutilement. Envoyez-donc votre réponse par mail tout en suggérant à
l\'auteur de la question de reposter un résumé du tout dans le newsgroup
concerné. Nous pourrons ainsi tous lire une seule fois la réponse sur le
réseau.

Si vous postez une question, rappelez à tous d\'y répondre par mail,
tout en proposant de résumer le tout dans un article que vous
re-posterez à votre tour.

Avant de répondre à un message, vérifiez que personne d\'autre n\'en a
déjà donné la réponse.

##### Un résumé tu posteras

Lorsque vous posez une question sur le réseau, il est de bon ton de
retransmettre les réponses que vous avez recues par messagerie (mail),
pour en faire profiter la communauté. Classez toutes les réponses que
vous avez recues dans un seul article et postez le dans le même
newsgroup ou\ vous avez envoyé votre question. Prenez le temps de
réduire les en-têtes (les champs adresses des messages), de retirer les
réponses redondantes, et n\'hésitez pas à résumer.

Lorsque vous répondez à un article directement en news, dans la mesure
du possible, faites en un résumé, ou reprenez juste entre guillemets la
partie sur laquelle porte votre réponse. Même lorsque vous répondez à
une news complète, résumez-en les principaux points. Les lecteurs
apprécieront d\'autant mieux vos commentaires.

Ne quotez - c\'est a dire placer un \'\>\' devant chaque ligne du texte
des participants - que les phrases importantes. Ne quotez pas l\'article
complet - surtout s\'il est long. Citez les participants au début de
chaque reprise de texte en une seule ligne. Retirez les signatures des
participants si elles n\'apportent rien à l\'objet de l\'article.

N\'oubliez pas que votre article figurera probablement dans une longue
liste d\'articles. Prenez le temps de bien composer votre texte.

##### L\'en-tête tu vérifieras

Pour la réponse à un article, le logiciel de lecture de news peut vous
donner une liste de newsgroups parfois différents des newsgroups ou\ a
été posté l\'article original. Il arrive même que les newsgroups
spécifiés soient inapproppriés : par exemple, quand la discussion a
évolué au fur et à mesure des réponses répétées.

Vous devez donc vérifier la liste des groupes destinataires et corriger
cette liste si besoin. Lorsque vous changez quelque chose dans cette
liste de newsgroups destinataires, indiquez-le en quelques lignes au
début de votre message ; tout le monde ne lit pas les en-têtes.

##### Le Droit d\'auteur tu respecteras

Tout ou partie d\'un article doit faire partie du domaine public; ou
vous en possédez vous-même les droits (peut-être même en êtes vous
l\'auteur).

Naturellement, par sa publication sur Usenet, cette information devient
publique. Vous ne devez donc pas poster ainsi des documents
confidentiels. Par exemple, si vous postez une information ayant un
rapport avec Unix, cette information peut être limitée par la licence
que vous ou votre compagnie a signée avec AT&T. Veillez à ne pas la
violer.

Attention aussi à l\'envoi de critiques de cinéma, paroles de chansons,
ou quoi que ce soit d\'autre publié et qui peut-être protégé par le
droit d\'auteur. Cela pourrait vous valoir, à vous ou à votre compagnie,
voire aux membres de la communauté du réseau d\'être tenus responsables
pour dommages et intérêts. Nous vous recommandons donc fortement de bien
prendre garde à ce type de documents.

##### Les références tu citeras

Vous ne voudriez pas qu\'une autre personne s\'approprie vos idées \...
Alors si vous reprenez celles d\'autrui pour appuyer vos dires, indiquez
vos sources.

##### Les remontrances orthographiques, tu éviteras

Rituellement, une sorte de fléau envahit Usenet : un article corrige une
faute orthographique d\'un autre article. Immédiatement, d\'autres news
suivent, du même acabit, transformant le réseau Usenet en professeur
d\'orthographe patenté. La publication de réponses à ces news se
poursuit souvent durant plusieurs semaines, rendant la lecture de ces
groupes parfois bien pénibles.

Nous faisons tous des erreurs. Reconnaissons-le. Beaucoup pratiquent le
francais ou l\'anglais en seconde langue. Alors, soyez indulgents. Si
vous désirez à tout prix corriger certaines fautes, utilisez la
messagerie (e-mail), mais pas le réseau Usenet.

##### La signature tu n\'exagéreras pas

Une signature peut être ajoutée automatiquement à la fin de votre
article par votre logiciel lecteur de news. Il s\'agit le plus souvent
d\'un fichier nommé \".signature\" dans votre répertoire personnel.

Cette signature a pour but d\'indiquer à tous vos coordonnées
personnelles, voire simplement votre nom complet et votre adresse
e-mail, et non pas de raconter votre vie.

Soyez bref. Ne placez pas ici une tartine de texte. Un message
comportant une signature plus longue que le texte lui-même apparaîtra
comme une preuve de mauvais goût.

La plupart des signatures sont limitées à quatre lignes, ce qui est
amplement suffisant.

##### La longueur des lignes tu limiteras

Eh oui \... le désespoir complet, la planète est équipée de plusieurs
milliers d\'ordinateurs qui ne se comprennent pas toujours. Ainsi,
certains terminaux ne permettent pas d\'afficher à l\'écran, plus de 80
caractères. Parfois, la ligne disparaît au bord de l\'écran, parfois,
elle est coupée au milieu d\'un mot, ne rendant pas toujours facile la
lecture de votre message.

Prenez garde à la longueur de vos lignes. 80 caractéres est un bon
chiffre, mais autant limiter à 76 caractères, ce qui permet en cas de
réponse, de placer un prompt quelconque (\'\>\') en début de chaque
ligne, signifiant ainsi que ce message est une reprise de votre texte.

##### Les caractères de contrôle tu éviteras

Là aussi, attention. Souvent les terminaux interpréteront mal les
caractères de contrôle. Si une telle commande donne sur votre écran la
vidéo inversée, cette même commande peut, sur un autre teminal, bloquer
le clavier d\'un lecteur, ou son affichage graphique.

Evitez donc toute utilisation de ce type de caractére. Aux tabulations,
préférez un nombre défini d\'espaces. Ces tabulations sont parfois mal
interprétés par d\'autres terminaux.

##### A tes annonces publicitaires tu résisteras

N\'utilisez pas Usenet comme un support publicitaire. Les publicités sur
Usenet sont rarement appréciées. En général, plus l\'annonce
publicitaire est tapageuse ou inappropriée, plus elle soulèvera
d\'opposition. L\'article \"Rules for posting to Usenet\" de Mark Horton
postée mensuellement en groupe news.announce.newusers contient plus de
détails sur ce point dans la section intitulée \"Annonces de produits ou
services professionnels\". Essayez donc plutôt les hiérarchies biz.\*.
Vous voici donc au courant des principales rèe;gles. Mais la facon la
plus facile d\'apprendre à utiliser Usenet est encore de bien regarder
comment les autres l\'utilisent. Prenez votre temps. Consultez
tranquillement les groupes.

N\'oubliez pas que cette communauté est composée de plusieurs centaines
de milliers de participants, voire de plusieurs millions. Si vous avez
un doute, fixez vous un délais de quinze jours avant de poster un
article dans un groupe particulier afin de bien comprendre ses
conventions et ses usages. Votre article sera d\'autant plus apprécié
par la communauté.

##### En résumé :

 * Le lecteur jamais tu n'oublieras
 * L'administrateur tu n'agresseras pas
 * La prudence dans les écrits tu emploieras
 * Brièvement tu écriras
 * Des titres clairs tu choisiras
 * A l'audience toujours tu penseras
 * A l'humour et aux sarcasmes tu prendras garde
 * Une seule fois ton message tu posteras
 * Par mail, le plus souvent, tu répondras
 * Un résumé tu posteras
 * L'en-tête tu vérifieras
 * Le Droit d'auteur tu respecteras
 * Les références tu citeras
 * Les remontrances orthographiques, tu éviteras
 * La signature tu n'exagéreras pas
 * La longueur des lignes tu limiteras
 * Les caractéres de contrôle tu éviteras
 * A tes annonces publicitaires tu résisteras

Pour tous commentaires/remarques/corrections sur ce texte, merci
d\'envoyer un e-mail à \<webmaster\@fdn.fr\>.

Ce texte est versé dans le domaine public. N\'hésitez pas à le recopier
et à le transmettre tout autour de vous. Merci de ne pas oublier de
mentionner le nom de l\'auteur.

Webmaster sep 94.
