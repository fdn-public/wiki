###### Code déontologique FDN (1996)

NOTE: Cette page est inclue uniquement pour information, référence et
historique. Elle est issue du site de FDN de 1996. Merci de vous référer
aux documents actuels de l\'association (statuts et règlement intérieur)
pour les textes actuellement en vigueur.

Modalités applicables à l\'espace Web (WWW) alloué à chaque adhérent

##### CODE DEONTOLOGIQUE

 * Il est rappelé à l'adhérent que les informations rendues publiquement accessible par le biais du WWW depuis l'espace qui lui a été alloué sont placées sous sa responsabilité.
 * L'adhérent doit s'assurer que les informations diffusées sont conformes aux lois et règlements en vigueur en France.
 * L'adhérent ne doit diffuser aucune information personnelle et/ou nominative sur une autre personne que lui-même.
 * Les informations diffusées doivent être libres de droits, ou bien les droits doivent appartenir ou avoir été concédés à l'adhérent.
 * L'adhérent devra notamment faire attention lors de la diffusion de logiciels, d'articles publiés dans la presse ou autre publication, sons ou enregistrements, et d'une façon générale: toutes oeuvres protégées par la propriété intellectuelle, sauf exceptions légales prévues.
 * Si l'adhérent diffuse une photographie dont il est l'auteur, il devra s'assurer par tous moyens du respect du droit à l'image des personnes représentées.
 * L'adhérent s'engage à satisfaire aux obligations de la loi No 78-17 du 6 Janvier 1978 dite Informatique et libertés, notamment en informant les personnes qui font l'objet d'un traitement nominatif de leur droit d'accès et de rectification.
 * L'adhérent s'engage à offrir une information honnête et un service loyal.
 * L'adhérent s'engage à ne pas induire en erreur sur le contenu et les possibilités des produits ou services proposés.
 * L'adhérent s'engage à ne pas faire de publicité directe ou indirecte:
   * pour un produit ou service contraire aux présentes règles
   * pour un produit ou service faisant l'objet d'une interdiction légale
   * pour un produit ou service dont la promotion est interdite légalement.
 * L'adhérent s'engage à ne pas faire appel à des sponsors, mécènes, ou toute autre subvention ou aide de nature commerciale ou publicitaire, à l'exclusion des personnes morales de droit public et oeuvres caritatives.
 * L'adhérent s'engage à ne faire figurer aucune offre de prix, de tarifs, ou toute autre indication de valeur commerciale sur les documents de son espace WWW.
 * L'adhérent s'engage à ne pas procéder à des opérations commerciales par le biais des espaces WWW de l'association FDN, notamment prise de commandes: seules les offres/demandes d'informations sont tolérées.
 * Si les relations par messagerie entre l'adhérent et des prospects devaient conduire à des transactions ou opérations commerciales, il est rappelé à l'adhérent que les opérations de télé-achat sont organisées par la loi 93-949 du 26 juillet 1993, et donc des article L121-16 et suivants du Code de la Consommation.
 * Il est rappelé à l'adhérent que les services de Jeux, les services d'information boursière, les services faisant appel à la générosité publique, le tabac et les boissons alcoolisées, les produits médicamenteux, notamment, font l'objet de règles particulières quand à leur diffusion publique.

L\'adhérent veillera à respecter les textes spécifiques concernant les
produits ou services mentionnés par les documents exploités dans son
espace WWW.

 * Il est rappelé à l'adhérent qu'un service WWW est soumis à la loi (modifiée) du 86-1067 du 30 septembre 1986 relative aux services audiovisuels, et qu'il lui appartient d'accomplir les démarches correspondantes, (expliquées sur le site LIJ).
 * Il est rappelé à l'adhérent quelques références d'articles du Nouveau Code Pénal auxquels il devra porter particulièrement attention.

\(c) 1996 FDN - avec l\'aide de Jérôme Rabenou
