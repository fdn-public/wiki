###### Résiliation d'un abonnement VPN

La résiliation du VPN se fait depuis l'[espace adhérent](https://vador.fdn.fr/adherents/) dans la section `Abonnements et services` -> `Abonnements VPN`.

