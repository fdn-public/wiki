###### Emily Postnews répond à vos questions

 * Archive-name: FDN/Doc/Emily-Postnews-repond-a-vos-questions
 * Original-translate: luis(chez)elysia(point)com (Luis Arias)
 * Comment: traduction de l'article "Emily Postnews Answers Your Questions on Netiquette" disponible sur: [http://www.clari.net/brad/emily.html](http://www.clari.net/brad/emily.html) et posté mensuellement en news.announce.newusers. Version du 15 Mars 94 par scs(chez)eskimo(point)com (Steve Summit).

Pour en savoir plus sur l\'auteur, Brad Templeton, voir:
<http://www.clari.net/brad>;

Last-change: Wed Oct 29 19:38:27 CET 1997 blaise(chez)fdn(point)org
(Blaise Thauvin)

##### Note importante

Ce texte est censé être un article satirique. Si vous ne le comprenez
pas ainsi, consultez un médecin ou un comique professionnel. Les
conseils dans cet article doivent être compris pour ce qu\'ils sont :
des exhortations sur ce qu\'il ne faut PAS faire.

##### Chère Emily Postnews

Emily Postnews, première autorité sur l\'étiquette réseau, donne ses
conseils pour se comporter correctement sur Usenet.

Q : Chère Mademoiselle Postnews: Pouvez vous me dire quelle longueur
doit avoir ma signature ? (bavard\@bruyant)

R : Cher Bavard: Essayez d\'avoir la signature la plus longue possible
s\'il vous plaît. Elle est beaucoup plus importante que votre article,
sans aucun doute. Donc essayez d\'avoir plus de lignes dans votre
signature que dans votre texte.

Essayez d\'y inclure un petit dessin composé de caractères ASCII, et
beaucoup de citations mignonnes et de slogans. Les gens ne se lasseront
jamais de lire ces brins de sagesse encore et encore, et vous serez
ainsi personnellement impliqué à la joie que chaque lecteur ressent
lorsqu\'il lira une nouvelle fois votre signature.

N\'oubliez pas d\'y inclure un plan complet de Usenet, afin d\'indiquer
une fois de plus à tous comment vous contacter par e-mail n\'importe oò
dans le monde. N\'oubliez pas, pas la même occasion, d\'y indiquer la
passerelle Internet, cela permettra aux personnes de votre propre site
de savoir comment vous contacter. En plus de votre adresse sur Internet,
donner ici également votre adresse par UUCP et BITNET, meme si elles
sont identiques.

A côté de votre adresse e-mail, incluez vos nom et prénom, société et
organisme. C\'est simplement de la courtoisie - après tout, il y a des
programmes de lecture de news oò il est nécessaire de presser une seule
touche du clavier afin de retourner au début de votre article pour voir
cette information dans son en-tête.

N\'hésitez pas ! Incluez vos coordonnées téléphoniques et votre adresse
postale dans chaque article. Les gens répondent toujours aux articles
Usenet par un coup de fil et par courrier. Quelle idée de s\'embêter à
inclure cette information seulement dans les articles nécessitant une
réponse par un canal conventionnel.

------------------------------------------------------------------------

Q : Chère Emily: Aujourd\'hui j\'ai posté un article en oubliant d\'y
inclure ma signature. Que faire ? (tete\_en\_lair\@monvax)

R : Cher Tête en l\'air: Foncez sur votre terminal immédiatement et
postez un article du style : \"Désolé, j\'ai oublié d\'inclure ma
signature dans mon dernier article. La voici.\"

Comme la plupart de gens auront oublié votre article précédent,
(uniquement parce qu\'il aura osé être si ennuyeux au point de n\'avoir
pas de signature alléchante) ceci va le leur rappeler. De toute facon,
les gens sont beaucoup plus interessés par la signature que par le texte
lui-même. Voir le conseil précédent pour plus de détails importants.

Enfin, n\'oubliez pas d\'inclure votre signature DEUX FOIS dans chaque
article. Vous serez sûr ainsi que les gens la liront.

------------------------------------------------------------------------

Q : Chère Mlle. Postnews: Je n\'ai pas pu faire parvenir un mail à
quelqu\'un sur un autre site. Que dois-je faire ? (turbineur\@zele.max)

R : Cher Turbineur: Pas de problème. Postez simplement votre message
dans un groupe lu par beaucoup de gens. Ecrivez quelque chose du style :
\"Ceci est pour Jean Dupont. Je ne peux pas lui envoyer de mail donc je
poste ici. Pour les autres, prière de ne pas en tenir compte.\"

Ainsi, plusieurs milliers de personnes passeront quelques secondes à
feuilleter le groupe et ignoreront votre article, utilisant jusqu\'à 16
heures-homme de leur temps collectif. Mais cela vous épargnera l\'ennui
majeur de vérifier les cartes Usenet ou de chercher d\'autres solutions.
Réfléchissez, si vous ne pouviez pas distribuer votre message à 30 000
autres ordinateurs, vous seriez peut-être obligé (uh!) d\'appeler les
renseignements téléphoniques pour 3 unités, ou même téléphoner à
l\'individu. Ceci pourrait même vous coûter jusqu\'à quelques FRANCS (!)
pour un appel de 5 minutes!

Et c\'est certainement mieux de dépenser 100 à 200 francs de l\'argent
de quelqu\'un d\'autre en distribuant le message sur le réseau que de
dépenser 150 francs pour un envoi par Chronopost ou même 2,80 francs
pour un simple timbre !

N\'oubliez pas. C\'est la fin du monde si votre message n\'arrive pas,
il faut donc le poster dans le plus grand nombre d\'endroits possibles.

------------------------------------------------------------------------

Q : Comment cela se passe-t-il pour un message de test ?

R : Il est important de faire vos tests sur le réseau mondial. Ne faites
jamais de test dans une petite distribution alors que vous pouvez faire
ce test sur tout la planète. Mettez \"prière d\'ignorer\" dans votre
message, nous savons tous que tout le monde ignore un message ainsi
rédigé. N\'utilisez pas un sujet comme \"Je suis femme, mais je demande
à ce que l\'on me parle comme à un homme.\" parce que ces articles sont
lus en détail par tous les utilisateurs Usenet.

------------------------------------------------------------------------

Q : Quelqu\'un vient de poster que Roman Polanski a réalisé La Guerre
des Etoiles. Qu\'est-ce que je dois faire ?

R : Postez la réponse exacte immédiatement ! Nous ne pouvons pas laisser
les gens croire une telle chose ! C\'est excellent que vous l\'ayez
apercu. Vous serez sans doute le seul à faire la correction, donc postez
votre message dès que possible. Il n\'y a pas de temps à perdre.
N\'attendez certainement pas un jour, ni ne vérifiez si quelqu\'un
d\'autre a déjà fait la correction.

Et ce n\'est pas suffisant de répondre par mail. Comme vous êtes le seul
à savoir que c\'est vraiment Francis Coppola, vous devez en informer le
réseau toute de suite !

------------------------------------------------------------------------

Q : J\'ai lu un article indiquant, \"répondez par mail, je ferai une
compilation.\" Que faire ?

R : Postez votre réponse à tout le réseau. Cette demande ne s\'applique
qu\'à des ignares qui n\'ont rien d\'intéressant à raconter. Vos
messages sont beaucoup plus importants que ceux des autres, donc ce
serait un gachis que de répondre par mail.

------------------------------------------------------------------------

Q : J\'ai regroupé des réponses à un article que j\'ai posté, et
maintenant il faut en faire une compilation. Qu\'est-ce que je dois
faire ?

R : Il suffit de concatener toutes les réponses en un grand fichier et
de le poster. Sur Usenet, on appèle ca une compilation. Cela permet aux
gens de lire toutes les réponses sans être embêté par son programme
lecteur de News. Faites la même chose lorsque vous faites une
compilation d\'un vote.

------------------------------------------------------------------------

Q : J\'ai vu un long article auquel je voudrais répondre en détail,
qu\'estce que je dois faire ?

R : Mettez tout le texte avec votre article, surtout la signature, et
mettez vos commentaires bien serrés entre les lignes. Postez votre
article, ne l\'envoyez pas par mail, même si votre article ressemble à
l\'article original. Tout le monde \*adore\* la lecture de ces débats
point par point, surtout lorsqu\'ils évoluent en insultes et beaucoup
d\'échanges du style \"mais si\" \-- \"mais non\" \-- \"mais si\".

Soyez certain de suivre tout le débat, et ne laissez pas quelqu\'un
d\'autre avoir le dernier mot. Si vous laissez d\'autres gens avoir le
dernier mot, les discussions s\'arrêteront ! Souvenez-vous que les
autres lecteurs du réseau ne sont pas aussi malins que vous et que si
quelqu\'un poste quelque chose d\'inexact, les lecteurs ne pourront
jamais le comprendre correctement sans vos explications. Si quelqu\'un
devient insultant, la meilleure chose à faire est de se mettre à son
niveau et d\'envoyer une réponse. Lorsque je vois qu\'une personne sur
le réseau commence à insulter une autre, je le crois toute de suite sauf
si bien sûr cet article a été réfuté. Mais cela ne me fera pas changer
d\'avis sur l\'insulteur, c\'est donc votre devoir de répondre.

------------------------------------------------------------------------

Q : Comment puis-je choisir dans quels groupes envoyer un article ?

R : Envoyez votre article dans le plus grand nombre de groupes possible,
afin d\'avoir l\'audience la plus large. Après tout, le réseau existe
afin de vous donner une audience. Ignorez ceux qui vous suggèrent
d\'utiliser uniquement les groupes pour lesquels vous pensez que
l\'article est très approprié. Choisissez tous les groupes susceptibles
d\'y avoir quelqu\'un qui pourrait être intéressé.

Soyez certain que la suite d\'une discussion est toujours postée dans
tous les groupes Usenet. Dans le cas rarissime oò vous postez une
réponse contenant quelque chose d\'original, assurez-vous de compléter
la liste des groupes destinataires. Ne mettez jamais une ligne \"Réponse
à:\" dans l\'entête, parce que certains pourraient rater une partie de
la discussion dans les groupes les moins lus.

------------------------------------------------------------------------

Q : Auriez-vous un exemple ?

R : Bien. Admettons que vous voulez reporter le fait que que Gretzky a
été echangé par les Oilers aux Kings. Alors vous pourriez penser que
rec.sport.hockey serait suffisant. ERREUR. Beaucoup plus de gens
pourraient être intéressés. Ceci n\'est pas n\'importe quel échange de
joueurs ! Comme c\'est un article de NEWS, il faut le mettre dans la
hiérarchie news.\* également. Si vous êtes administrateur de news, ou
s\'il y en a sur votre machine, essayez news.admin. Sinon, utilisez
news.misc.

Les Oilers sont sans aucun doute intéressés par la géologie, donc
essayez sci.geo.fluids. C\'est un groupe vedette, donc postez à
sci.astro, et sci.space parce qu\'ils sont également intéressés par les
étoiles. Et bien sûr comp.dcom.telecom parce que ce groupe est né là ou
le téléphone à vu le jour. Et parce qu\'il est canadien, postez votre
article dans soc.culture.Ontario.southwestern. Mais ce groupe de News
n\'existe pas, alors suggérez en news.groups qu\'il devrait être crée.
Avec tous ces groupes d\'intérêt, votre article risque d\'être un peu
bizarre, donc postez à talk.bizarre aussi. (Et postez à comp.std.mumps,
parce qu\'ils n\'ont pratiquement jamais d\'articles là dedans et qu\'un
groupe \"comp\" va propager votre article encore plus loin.)

Vous allez aussi peut être trouver que c\'est plus amusant de poster
votre article dans chaque groupe individuellement. Si vous listez tous
les groupes dans l\'article, il y a des programmes de lecture de news
qui ne montreront l\'article à l\'utilisateur qu\'une seule fois ! Ceci
est intolérable.

------------------------------------------------------------------------

Q : Comment est-ce que je crée un newsgroup ?

R : Le plus simple c\'est de faire quelque chose comme \"inews -C
nouveaugroupe \....\". Et si même si cela peut engendrer de nombreuses
remarques concernant votre nouveau groupe, ceci pourrait ne pas suffire.

D\'abord, postez un message dans news.groups qui décrit le groupe. Ceci
est un \"appel à discussion.\" (Si vous voyez un appel à discussion, il
faut immédiatement poster un message d\'une ligne indiquant si vous
aimez ou si vous n\'aimez pas le groupe.) Lorsque vous proposez le
groupe, choisissez un nom avec un TLA (ATL: acronyme à trois lettres)
que ne sera compris que par les lecteurs \"branchés\" du groupe.

Après l\'appel à discussion, postez l\'appel à flames, suivi par un
appel à débat à propos du nom et un appel à des jeux de mots. A un
moment donné faites un appel au vote. Usenet est une démocratie, donc
maintenant les électeurs peuvent tous poster leurs votes afin d\'être
sûr de toucher toutes les 30 000 machines au lieu de toucher uniquement
la personne qui compte les votes. Postez un long recueil de tous les
votes tous les deux jours afin que les gens puissent se plaindre des
mauvais programmes d\'envoi de mail et des votes en double. Ceci vous
permettra d\'être encore plus populaire et de recevoir beaucoup de
courrier. Après 21 jours, vous pouvez poster les résultats pour que les
gens puissent discuter à propos de toutes les violations techniques du
règlement que vous avez commis. Attribuez-les au
modérateur-de-la-semaine pour news.announce.newgroups. Peut être enfin
votre groupe pourra-t-il être créé.

Afin d\'animer la discussion, choisissez une bonne hiérarchie pour y
insérer votre groupe. Par exemple, comp.race.formula1 ou soc.vlsi.design
seraient des bons noms de groupe. Si vous voulez que l\'on crée votre
groupe rapidement, utilisez des mots intéressants comme \"sex\" ou
\"activism.\" Afin de ne pas limiter la discussion, utilisez un nom
ayant un sens le plus large possible, et n\'oubliez pas ce TLA.

Si possible, comptez les votes sur un site ayant une connexion qui soit
scrutée une fois par semaine sur botswanavax. Essayez de planifier
plutôt le vote pendant une panne de votre site relais.

N\'utilisez en aucun cas la méthode du groupe pilote, parce que ceci
élimine la discussion, les flammes, les jeux de mots, les votes et
violations du règlement, les phases d\'accusation, et donc y élimine ce
qui est amusant. Pour créer un groupe \'alt\', il suffit d\'exécuter la
commande de création. Ensuite exécutez un rmgroup et d\'autres messages
newsgroup pour éviter aux autres utilisateurs de devoir faire cette
partie là.

------------------------------------------------------------------------

Q : Je ne sais pas écrire un mot correctement. J\'espère que vous allez
me dire ce qu\'il faut faire ?

R : Ne vous inquietez pas de la présentation de vos articles. N\'oubliez
pas que c\'est le message qui compte, et non pas sa présentation.
Ignorez le fait qu\'une orthographe peu soignée dans un groupe oò les
gens écrivent proprement envoie le même message silencieux que des
habits malpropres lorsque vous vous adressez à une audience.

------------------------------------------------------------------------

Q : Comment choisir un sujet pour mes articles ?

R : Maintenez le court et sans beaucoup de sens. Les gens seront ainsi
obligés de lire votre article pour savoir ce qu\'il y a dedans. Ceci
implique une plus grande audience pour vous, et nous savons tous que
c\'est pour cela que le réseau existe. Si vous répondez à un article,
soyez certain que de conserver le même sujet, même s\'il n\'a pas de
sens et s\'il ne fait pas partie de la même discussion. Si vous ne le
faites pas, vous ne pourriez pas toucher tous ceux qui cherchent des
infos sur le sujet original, et ceci implique une moindre audience pour
votre article.

------------------------------------------------------------------------

Q : Quel ton dois-je adopter dans mon article ?

R : Soyez aussi provocateur que possible. Si vous ne dites pas de choses
provocantes et remplissez votre article plein d\'insultes sur les gens
du réseau, il se peut que votre texte ne soit pas assez visible dans
l\'avalanche d\'articles, et vous ne recevrez pas de réponse. Plus votre
message est fou, plus il y a de chances que les gens y répondent. Après
tout, le réseau est là afin que l\'on fasse attention à vous.

Si votre article est trop poli, bien argumenté et concis, vous risquez
de ne recevoir que des réponses envoyées par mail. Pouah !

------------------------------------------------------------------------

Q : Mon logiciel de lecture de news m\'a indiqué que ma signature était
trop longue et qu\'il y avait trop de lignes de texte dans mon article.
Qu\'est qu\'il faut faire ?

R : Il n\'y a aucune raison particulière à ce que ce logiciel impose ces
restrictions, alors n\'essayez même pas de savoir pourquoi elles peuvent
s\'appliquer à votre article. La plupart des gens sur le réseau Sont à
la recherche d\'articles généreux composés du texte complet d\'un
article précédent plus quelques lignes.

Afin d\'aider ces gens-là, remplissez votre article avec des lignes
originelles sans intêret afin de contourner les restrictions. Tout le
monde vous en remerciera.

En ce qui concerne la signature, je sais que c\'est difficile, mais vous
allez devoir l\'insérer avec l\'éditeur de texte. Faites-le deux fois
pour être sûr de sa présence. Autre chose, afin d\'illuster votre
support pour la distribution gratuite de l\'information, incluez dans
votre texte un message de copyright interdisant la transmission de votre
article aux sites Usenet dont vous n\'aimez pas la politique.

Maintenant, si vous avez beaucoup de temps libre et que vous voulez
réduire la longueur de votre article, n\'oubliez pas de supprimmer
quelques unes des lignes d\'attribution afin que l\'on pense que
l\'auteur original de \-- disons \-- un appel à la paix dans le monde, a
été écrit suite à l\'appel pour l\'attaque nucléaire de la Corse.

------------------------------------------------------------------------

Q : On vient d\'annoncer à la radio que les Etats-Unis ont envahi
l\'Irak. Dois-je poster un message ?

R : Bien sûr. Le réseau peut toucher des gens en 3 à 5 jours. C\'est le
meilleur moyen de renseigner tout le monde à propos de ce type
d\'évènement longtemps après que la télédiffusion les en a avertis.
Comme vous êtes sans doute la seule personne qui a entendu la nouvelle à
la radio, il vous faut poster votre message le plus rapidement possible.

------------------------------------------------------------------------

Q : J\'ai cette blague géniale. En fait, il y avait trois chaînes de
caractères qui rentrent dans un bar\...

R : S\'il vous plaît. Ne me racontez pas tout. Il faut sousmettre cette
blague à rec.humor. Envoyez là également au modérateur de
rec.humor.funny, je suis sûr qu\'il ne la connait pas.

------------------------------------------------------------------------

Q : Quel ordinateur dois-je acheter ? Un Atari ST ou un Amiga ?

R : Posez la question dans les groupes Atari et Amiga. C\'est une
question originale et intéressante. Je suis sûr qu\'ils vont aimer en
discuter dans ces groupes. En fait, postez votre question immédiatement
à autant de groupes techniques que vous connaissez, en terminant votre
message par : \"Prière de répondre par mail, parce que je ne lis pas ce
groupe.\" ( Personne ne pensera qu\'une telle affirmation est
impertinente. N\'oubliez pas que le réseau est une ressource destinée à
vous aider.)

Il n\'y a pas besoin de lire les groupes à l\'avance ni d\'examiner les
listes de \"frequently asked questions\" pour voir si le sujet a déjà
été traité. Ce genre de précaution est destiné à des gens n\'ayant pas
votre sens inné de la netiquette, et dont leurs questions sans grand
intérêt seront souvent rencontrées. Votre question est certainement
unique; il n\'y a aucune raison de vérifier la liste pour voir si la
réponse y est déjà. Comment pourrait-elle y être, alors que vous venez
de penser à la question il y a quelques instants ?

------------------------------------------------------------------------

Q : Que faire à propos des autres questions importantes ? Comment savoir
quand il faut en poster ?

R : Postez-les toujours. Ce serait gaspiller votre temps que de trouver
un utilisateur averti dans un des groupes et de demander avec le mail si
le sujet a déjà été évoqué. C\'est beaucoup plus facile d\'embêter des
milliers de personnes avec la même question.

------------------------------------------------------------------------

Q : Quelqu\'un vient de poster une requête sur le réseau et je voudrais
avoir la réponse aussi. Que dois-je faire ?

R : Postez une réponse immédiatement, en incluant le texte complet de la
requête. A la fin ajoutez, \"Moi aussi !\" S\'il y a quelqu\'un d\'autre
qui a déjà fait ca, faites une réponse à cet article et rajoutez \"Moi
trois,\" ou tout autre chiffre approprié. N\'oubliez pas votre signature
complète. Après tout, si vous envoyez un mail à l\'auteur original et
que vous demandez une copie des réponses, vous allez simplement
embrouiller la boîte aux lettres de l\'auteur, et vous allez épargner
aux gens qui répondent à la question le devoir agréable de noter tous
les \"moi aussi\" et de renvoyer de multiples copies.

------------------------------------------------------------------------

Q : Qu-est-ce qui permet de juger de la qualité d\'un groupe ?

R : Mais, c\'est le Volume, le Volume, le Volume. N\'importe quel groupe
ayant un volume élevé est forcément bon. N\'oubliez-pas : plus le volume
d\'un groupe est élevé, plus vous trouverez un pourcentage élevé
d\'articles pratiques, sérieux et révélateurs. En fait, si le volume
d\'un groupe est insuffisant, il devrait être supprimé du réseau.

------------------------------------------------------------------------

Q : Emily. Je ne suis pas du tout d\'accord avec quelqu\'un sur le
réseau. J\'ai essayé de me plaindre auprès de son administrateur
système. J\'ai organisé des campagnes d\'envoi de mail. J\'ai appelé
pour qu\'il soit suspendu du réseau et j\'ai téléphoné à son employeur
pour qu\'il se fasse virer. Tout le monde m\'a ri au nez. Qu\'est-ce que
je peux faire ?

R : Allez voir les journaux quotidiens. La plupart de journalistes sont
des experts informatiques de première classe qui vont comprendre le
réseau et vos problèmes, parfaitement. Ils vont éditer des articles
sérieux, bien écrits, sans aucune erreur, et ils vont certainement bien
représenter la situation au public. Le public va également agir
correctement, étant donné qu\'il est complètement averti de la nature
subtile de la société du réseau.

Les journaux n\'utilisent jamais le sensationalisme et ils
n\'introduisent pas de distorsion, alors soyez certain de montrer du
doigt des choses comme le racisme ou le sexisme partout oò elles sont.
Il est évident qu\'ils comprennent que tout ce qui se passe sur le
réseau, en particulier les insultes, doit être compris au premier degré.
Il faut lier ce qui se passe sur le réseau aux origines de
l\'Holocauste, si possible. Si les journaux n\'acceptent pas l\'article,
allez voir un journal populaire \-- ils sont toujours intéressés par les
belles histoires.

Par le fait d\'organiser toute cette publicité gratuite autour du
réseau, vous allez devenir très connu. Les gens sur le réseau vont
attendre avec impatience chacun des vos messages et vont vous citer
constamment. Vous allez recevoir plus de mail que vous en avez jamais
rêvé \-- l\'ultime dans le succès sur le réseau.

------------------------------------------------------------------------

Q : Qu\'est que foobar veut dire ?

R : Mais c\'est vous, cher ami.

------------------------------------------------------------------------

Webmaster Oct 97.
