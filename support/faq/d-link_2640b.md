###### D-Link 2640B

##### Remarques sur le modem

Interface web buguée et traduite via google translate, vérifications des
réglages un peu intrusives qui empêchent par exemple de faire du routage
avec des masques ne finissant pas par \'0\'. ( Il faut donc passer par
la console en SSH ). Les réglages sont parfois appliqués à chaque
rafraîchissement de la page, ce qui peut être embêtant ( 12 fois la même
route par ex\...)

Les réglages sont difficiles à la longue, car les mauvaises cases se
cochent et il est impossible par exemple de changer les DNS une fois la
configuration initiale faite.

Il est possible d\'activer un accès SSH ou Telnet à l\'interface
d\'admin, ce que je conseille vivement pour sortir de cet enfer
javascriptien.

Une documentation détaillée est disponible à cette adresse concernant la
console :
<http://www.usr.com/support/9105/9105-files/cli_reference.html>

##### Configuration de la connexion

Dans l\'onglet *Setup*, cocher la case *Manual setup* puis configurer
comme suit :

 * Username : xxxxxx@fdn.nerim
 * Password : *********
 * Service Name :  Fdn
 * Connection Type : PPPoA Vc-Mux 
 * MTU :   1492
 * Idle Time Out  : 0
 * PPP IP Extension  : Décoché

En bas de page :

 * Vpi: 8
 * Vci: 35
 * Enable NAT : coché
