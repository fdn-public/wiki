# Comment retrouver mes identifiants ?

Pour récupérer vos identifiants de membre de FDN (par défaut, login adhacc-xxx et mot de passe associé) il y a deux possibilités :
- si vous avez un abonnement xDSL, FTTH (sauf via Aquilenet) ou VPN chez FDN, vous pouvez utiliser les identifiants associés pour vous connecter sur votre [espace adhérent·e](https://vador.fdn.fr/adherents/) et y récupérer vos login/mot de passe via l'onglet Informations techniques -> Identifiants de connexion
- si vous n'avez pas d'abonnement FDN ou si vous avez également perdu les identifiants associés (si si, ça arrive... ;-)) : il ne vous reste plus qu'à envoyer un courriel à  buro [at] fdn .fr !
