======= openWRT, installation et configuration sur Netgear DM200
========

L\'objectif de ce tutoriel est de flasher le modem Netgear DM200 afin
d\'y installer OpenWrt. Cette opération a plusieurs intérêts: - le modem
avec le firmware par défaut a des problèmes de déconnexions aléatoires,
OpenWrt semble permettre d\'éviter ce problème - OpenWrt est un outil
plus avancé que le logiciel de Netgear, on peut donc avoir une
configuration plus poussée. En contrepartie, OpenWrt est plus complexe à
appréhender, mais ce tutoriel a justement pour but de vous aider à le
prendre en main pour configurer l\'accès internet.

 **/!\\\*\* ce tutoriel traite du flashage de ce modem avec OpenWrt
        18.06.4, les versions supérieures ont elles besoin de corriger
        uboot ou c\'est fait par défaut? \*\*/!\\\*\* FIXME

##### I) Flash du modem

 **1)\*\* Il faut dans un premier temps récupérer le firmware
        OpenWrt

Aller sur la page
[NetGear:DM200](https://openwrt.org/toh/netgear/dm200) et
télécharger le fichier sous \"Firmware OpenWrt Install\" dans l\'item
\"Installation\" Prenez bien le fichier finissant par
squashfs-factory.img (et non celui qui finit par
squashfs-sysupgrade.bin)

 **2)\*\* Brancher l\'ordinateur au modem

<!-- -->

 **3)\*\* Ouvrir un navigateur web, puis saisissez dans la barre
        d\'adresse :

[www.routerlogin.net](http://www.routerlogin.net) ou
[192.168.5.1](http://192.168.5.1)

Un couple utilisateur/mot de passe vous est demandé, par défaut c\'est -
identifiant : admin - mot de passe : password

La page d\'administration apparaît alors.

{=mediawiki}
{{:support:faq:dm200-etape1.png?400|}}

 **4)\*\* Cliquer sur Avancé (y compris si c\'est le premier accès
        au modem)

{=mediawiki}
{{:support:faq:dm200_image2.png?400|}}

 **5)\*\* Cliquer sur \"assistant de configuration\" puis \"mise à
        jour du micrologiciel\". Cette fenêtre apparaît

{=mediawiki}
{{:support:faq:dm200-image3.png?400|}}

 **6)\*\* Cliquer sur \"Parcourir\", puis choisir le firmware
        OpenWrt téléchargé au 1) et cliquer ensuite sur \"Charger\". Une
        petite fenêtre apparaît qui vous prévient que le firmware
        OpenWrt est plus ancien que celui présent actuellement sur le
        modem. Répondre OK

{=mediawiki}
{{:support:faq:dm200-image4.png?400|}}

 **7)\*\* Le modem redemande de confirmer qu\'on veut bien lancer
        le processus , répondre oui

pas D\'image\... FIXME

Une barre de progression apparaît, il ne reste plus qu\'à attendre.

{=mediawiki}
{{:support:faq:dm200-image6.png?400|}}

{=mediawiki}
{{:support:faq:dm200-image7.png?400|}}

Il est possible d\'avoir \"page internet introuvable\" à la fin du
processus.

Débrancher le câble Ethernet du pc puis le rebrancher afin d\'obtenir
une nouvelle adresse IP locale. Vous devriez pouvoir accéder à la page
d\'administration du modem en saisissant dans la barre d\'adresse d\'un
navigateur web [192.168.1.1](http://192.168.1.1)

##### II) Configuration du modem

#### A. Pour un lien VDSL

Maintenant que le modem est reflashé avec OpenWrt, il faut le configurer
pour avoir accès à internet.

 **1)\*\* Mettre un mot de passe sur le modem comme demandé sur la
        page d\'accueil (configurer un accès ssh n\'est pas obligatoire
        mais conseillé¹)

<!-- -->

 **2)\*\* Aller dans Network -\> Interfaces puis cliquer sur Edit
        de la connection wan

PS: J\'ai du vdsl, je sais pas comment cela se passe avec de l\'adsl

Comme protocole, choisir PPPoE username et password vous ont été
renseignés par FDN par mail.

Puis aller dans Physical settings, et changer d\'interface pour \"Custom
interface\" dans lequel on rentrera dsl0.835 puis entrée pour valider.
Cliquer ensuite sur \"save and apply\", puis attendre quelques minutes.

La connexion devrait se faire, et vous avez maintenant accès à Internet,
et vous avez maintenant un modem avec bien plus de possibilités dans sa
configuration :)

#### B. Pour un lien ADSL

La configuration n\'est pas tout à fait la même en fonction du type de
lien. Voici ce qui change pour configurer le modem-routeur pour l\'ADSL
2 TODO

======= Notes =======

 **¹\*\* Configurer un accès ssh vous permettra d\'accéder au
        modem-routeur en ligne de commande et donc,

vous donnera la possibilité (par exemple) de désactivé l\'interface web
et par là réduire la surface d\'attaque (augmenté la sécurité). Cela
peut se faire comme ceci : ssh root@id.du.modem (demande de mot de
passe si l\'authentification par clé n\'est pas configurée)
/etc/init.d/uhttpd stop && /etc/init.d/uhttpd disable exit

Pour la rendre -momentanément- à nouveau accessible, suivre le même
processus de connexion, puis : /etc/init.d/uhttpd start

Pour revenir à la configuration d\'origine :
/etc/init.d/uhttpd enable && /etc/init.d/uhttpd start

Dans le contexte d\'un réseau local \"familliale\", cette procédure de
désactivation de l\'interface web aura plutôt l\'avantage de libérer des
ressources pour le fonctionnement du modem-routeur. En terme de
sécurité, dans la plus part des cas l\'effet sera plutôt limité puisque
cette interface n\'est pas exposée à l\'extérieur du domicile.

======= Références =======

Ce tutoriel se base sur <https://openwrt.org/toh/netgear/dm200>

[Manuel utilisateur NetGear
DM200](https://www.downloads.netgear.com/files/GDC/DM200/DM200_UM_EN.pdf)

Auteur: Naziel
