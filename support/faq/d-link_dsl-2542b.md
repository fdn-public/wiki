Bon points: - Fonctionnalités assez complètes pour ce prix:

 - Possibilité d'avoir un parefeu sortant (par IP LAN et port),
 - totale liberté pour le choix du LAN (10.0.0.0/8 est accepté par exemple),
 - un "contrôle parental" permettant de filtrer des sites en fonction de l'heure,
 - SNMP, UPnP, QoS réglable manuellement,
 - Gestion des DNS dynamiques "dyndns" et "dlinkdns",
 - "Règles d'application", permettant de déclencher dynamiquement l'ouverture de ports en fonction d'autres ports, avec possibilité de définition manuelle,
 - Envoi des logs par mail,
 - Administration possible depuis le LAN via telnet et SSH, permet donc de se passer de l'interface Web pour automatiser des actions,
 - Supporte le mode bridge, donc permet la connexion en IPv6,
 - Même si c'est une obligation légale, recevoir une copie de la GPL avec un lien vers les sources des logiciels sous GPL utilisés fait toujours plaisir :)

Mauvais points: - A tendance à chauffer, - Il n\'y a pas de manuel en
français, y compris sur le CD-ROM. Les instructions sont au format PDF
en anglais. - Aucune gestion d\'IPv6 en mode routeur, donc choix à faire
entre toutes les fonctions liées au mode routeur, et le support de IPv6.
