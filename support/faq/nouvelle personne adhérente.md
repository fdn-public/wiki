[[_TOC_]]

# Petit guide de la nouvelle personne adhérente à FDN

Le but de cette note est de synthétiser l'information pratique utile à la nouvelle 
personne adhérente pour participer à FDN et utiliser ses
services.

<img src="./images/Sticker_Ubuntu_Party.png" width=20%/>

## Identifiants

Une fois votre adhésion validée, vos identifiants vous sont envoyés par courriel 
sous quelques jours : un identifiant d'adhérent·e de type adhacc-xxx et un mot 
de passe associé à ce dernier.

Gardez-les soigneusement, ce sont eux qui vous permettent d'accéder à votre 
[espace personnel](https://vador.fdn.fr/adherents/) mais aussi au 
[wiki adhérent·es](https://git.fdn.fr/fdn/wiki). Vous pouvez modifier votre mot 
de passe sur votre espace adhérent·e.

Si jamais vous les égarez, cette [page en libre accès](https://www.fdn.fr/le-wiki-de-fdn/) 
sur le site vous explique comment les récupérer :
* soit en vous connectant grâce à vos identifiants ADSL ou VPN (si vous ne les avez pas eux aussi perdus ;-)),
* soit en écrivant un mail à buro@fdn.fr. C'est aussi rappelé dans la 
[FAQ](https://www.fdn.fr/faq/#comment-trouver-ses-identifiants) présente sur le 
site.

## Premiers pas dans FDN

Vous pouvez vous authentifier sur votre 
[espace adhérent·e](https://vador.fdn.fr/adherents/) avec les identifiants 
obtenus lors de votre adhésion.

Le site Vador rappelle des informations administratives et financières relatives 
à votre adhésion. Par exemple la page « Prélèvements » rappelle vos prélèvements 
passés et « Créances dues », à peu près la même chose (mais la ligne « Adhésion FDN 
(1ere année) » correspond en fait au droit d'entrée initial). Le haut du bandeau 
droit du site vador donne aussi votre numéro d'adhérent.

Le site Vador fournit aussi des statistiques du réseau FDN (au sens informatique 
et associatif).

En cas de souci de connexion, vous pouvez aller consulter l'état du réseau de FDN 
via notre [https://status.fdn.fr/](https://status.fdn.fr/) afin de voir si le problème 
vient de notre côté ou non. Pensez également à consulter la 
[FAQ de support FDN](./support/faq) **avant** de nous 
contacter sur support@fdn.fr. ;-)

Si vous souhaitez vous impliquer plus avant dans l'association, n'hésitez pas à 
aller consulter la 
[page du site regroupant nos projets en cours](https://www.fdn.fr/projets/). Vous 
pouvez aussi participer aux [réunions mensuelles](https://git.fdn.fr/fdn/wiki/-/blob/master/pages/bureau/README.md) 
qui ont lieu une fois par mois, tous les 6 du mois.

Le [site de gestion des listes de discussion](https://lists.fdn.fr) va vous 
permettre de souscrire des abonnements à des listes sur des thèmes liés aux 
activités de FDN et/ou gérées par des abonné·es. Il faut vous faire envoyer par 
email un mot de passe avant de pouvoir gérer les abonnements en cliquant sur le 
bouton « Premier login ? ». Quelques listes intéressantes sur la vie de l'association : 
benevoles@fdn.fr, bistro@fdn.fr...

Par défaut, vous êtes abonné·e d'office à la liste ag@fdn.fr sous l'adresse 
email que vous avez fournie dans le formulaire d'adhésion. Vous êtes également, 
en tant que nouvelle personne adhérente, abonné·e à la liste accueil@fdn.fr.

Les amateurices de temps réel pourront utiliser [IRC](https://www.fdn.fr/participer/irc/) 
sur le chan #fdn de geeknode (#fdn[at]irc.geeknode.org) ou via [Matrix](https://matrix.to/#/#fdn:matrix.fdn.fr).

N'hésitez pas non plus à consulter le [site de FDN](https://www.fdn.fr). :-)

## Bonne conduite

L'association dispose depuis septembre 2020 d'un 
[Code de Conduite](https://www.fdn.fr/codedeconduite.pdf) : vous êtes censé·e 
l'avoir lu et y souscrire sans réserve.

La « Nétiquette », en quelque sorte l'éthique du net, maintes fois actualisée a 
désormais sa [page sur Wikipedia](https://fr.wikipedia.org/wiki/N%C3%A9tiquette). 
Sa lecture est une bonne introduction, pour ne pas dire un « must » :

D'autres références, ci-dessous, sont assez anciennes voire dépassées, mais elles 
peuvent donner le ton de ce que sont de bons usages dans FDN et plus généralement, 
sur internet, et non sans humour.

* Le [Code déontologique de FDN (de 1996.md)](./support/faq/old_deontologie.md), un 
peu dépassé mais avec un certain nombre de rappels utiles sur les responsabilités de chacun·e
* Le [Le Savoir communiquer sur Usenet](support/faq/savoir_communiquer_sur_usenet.md), 
ancien aussi (1994) mais toujours utile et en bonne partie valable sur internet
* Au fait, [Qu'est-ce que Usenet](support/faq/qu_est_ce_que_usenet.md) ?
* [Emily Postnews répond à vos questions](support/faq/emily_postnews_repond_a_vos_questions.md), 
grand classique.

Enfin pour être complet dans la récupération d'une vieille archive de documents 
historiques de FDN, mais aussi pour le plaisir, voici les 
[Réponses aux questions fréquemment posées sur Usenet](./support/faq/faq_usenet.md), 
toutes celles bien sûr que vous n'osiez pas poser...

## Les nombreux services de FDN

### Les services habituels

Les services que FDN propose sont listés sur le [site de FDN](https://www.fdn.fr), 
sous l'onglet « services ». Certains sont 
[réservés aux adhérent·es](https://www.fdn.fr/services/services-aux-adherents/), d'autres 
sont [ouverts à toustes](https://www.fdn.fr/services/autres/).

Certains sont très peu utilisés (comme UUCP, malgré sa puissance) voire plus du 
tout (comme les domaines dynamiques), ils peuvent être donc être un peu rouillés 
et l'équipe adminsys ne saura pas forcément répondre facilement à des requêtes 
sur certains de ces services, mais il ne faut pas hésiter à demander.

### Vos propres services

Vous pouvez mettre en œuvre avec votre propre compte sur un serveur de FDN, les 
services dont vous avez vous-même besoin. Pour ce faire vous devrez utiliser un 
accès SSH (demander à un·e Adminsys). Attention tout de même, les services ou 
les scripts que vous mettez en œuvre ne doivent pas être encombrants ou consommateurs 
de ressources au point de gêner les autres utilisateur·ices : il s'agit de 
ressources mutualisées et il convient que chacun·e en fasse un usage mesuré.

### Vers l'auto-hébergement

La tendance actuelle de FDN est de pousser ses abonné·es qui ont plus que des 
besoins « de base » vers l'auto-hébergement, c'est à dire les encourager à monter 
leurs propres services eux-mêmes, voire même leurs propres serveurs (machines 
hébergeant ces services). Par exemple, si vous avez besoin de plusieurs boites 
mail (FDN n'en propose qu'une par adhérent·e), de nombreuses interventions des 
bénévoles d'adminsys, de plusieurs bases de données... Il vous sera recommandé 
d'envisager sérieusement d'apprendre à prendre en charge vos propres besoins.

C'est aussi un encouragement pour devenir indépendant·e et auto-suffisant·e, et 
devenir à part entière un petit bout d'internet en sachant proposer ses propres 
services au reste du monde.

Une étape intermédiaire peut être de souscrire une //machine virtuelle// hébergée 
par FDN pour apprendre à administrer et gérer les services. Ou de monter un 
serveur chez vous, accessible via les adresses IP publiques que FDN pourra vous 
fournir. Enfin en cas de besoins quasi professionnels, FDN, certains de ses membres 
et partenaires, peuvent vous proposer des services plus « dédiés » d'hébergement 
et de raccordement à internet (n'hésitez pas à venir en parler sur les listes).

### Votre adresse email en @fdn.fr

Voir la [documentation sur la page dédiée du wiki adminsys](https://git.fdn.fr/adminsys/new_mail_server/-/tree/master/doc/user) (sur le gitlab).

### Votre site web en .fdn.fr

Voir [cette page](../../../../../fdn/wiki/-/blob/master/pages/travaux/hebergement_web.md) 
et pour les
détails techniques, [la page dédiée sur le wiki adminsys](https://git.fdn.fr/adminsys/wiki/-/blob/master/services_aux_adherents/web-fdn.md) (sur le gitlab).

### Votre accès internet par FDN

Si vous adhérez à FDN en ayant déjà un abonnement ADSL chez un « BOFS » (Bouygues, Orange, Free, SFR), et que 
vous souhaitez transférer votre connexion ADSL chez FDN, une interruption de 
service va se produire lors de cette migration. Pour minimiser ses effets et si 
vous utilisez actuellement le service de mail de votre FAI, il est important 
d'obtenir au préalable une adresse en @fdn.fr et de l'annoncer à vos correspondant·es 
suffisamment à l'avance. Certains fournisseurs conservent vos adresses de courriel 
si vous convertissez votre abonnement en formule « accès gratuit/libre » mais il 
convient de vous assurer de leurs conditions pour en bénéficier.

Lorsque vous migrez depuis votre ancien FAI, si vous étiez en dégroupage total, 
normalement l'ouverture de votre ligne a eu pour effet de résilier votre ancien 
abonnement. Par contre, si vous étiez en dégroupage partiel, sachez que même si 
FDN écrase la ligne téléphonique, 
**nous ne nous occupons __d'aucun__ aspect contractuel concernant l'abonné·e** : 
C'est à vous de résilier votre contrat auprès de votre ancien opérateur.

Ensuite, vous pouvez passer à la connexion à proprement parler. Une [page](modem.md) 
pour aider au choix du modem/routeur. Une [autre](support/faq/passerelle.md) 
pour monter sa passerelle.

À noter : un [guide](openwrt-netgear_dm200.md) est disponible pour installer 
[OpenWrt](https://openwrt.org) sur les modem-routeurs NETGEAR DM200.

## Questions diverses

#### Je vois que l'on m'a attribué le numéro d'adhérent·e 538, mais mon identifiant est adhacc-394. Cette différence vient-elle d'un bogue ? Pourquoi est-on identifié·e par ces deux numéros ?

À chaque formulaire d'inscription rempli dans le système d'information de FDN 
(comme expliqué ci-dessus), un identifiant unique de type adhacc-XXX est généré. 
Le numéro d'adhérent·e est lui attribué à la validation de l'adhésion. Si une 
personne change d'avis et n'envoie finalement pas les papiers pour concrétiser 
son adhésion, ou si elle se trompe et remplit plusieurs fois le formulaire ou 
encore si l'adhésion est refusée, ce sera autant d'identifiants qui ne 
correspondront pas à des numéros d'adhérent·es, d'où cette différence.

Ces deux numéros correspondent donc à des usages différents, l'un servant par 
exemple à s'authentifier sur ce wiki, l'autre servant dans la gestion de 
l'association.

#### Comment prévenir d'un changement de compte bancaire ?

La procédure est décrite dans la page [changement rib](changement_rib.md).

#### FAQ

Une [FAQ](https://www.fdn.fr/faq/) est présente sur notre site, n'hésitez pas à 
la consulter. :-)

## Annexes techniques

Nous traitons ici de quelques sujets qui peuvent poser problème aux adhérent·es 
qui n'ont pas la fibre technique.

Les personnes qui sont à l'aise dans ce domaine noteront qu'un descriptif plus 
technique de l'architecture FDN est disponible sur le [wiki des adminsys](https://git.fdn.fr/adminsys/wiki/) (sur le gitlab).

#### Configuration mail sur votre PC

Configurer la réception de mail par POP ou IMAP est disponible via les menus de 
votre logiciel de courriel préféré, sans options spéciales.
Pour l'envoi, qui est configuré via des options du genre « serveur sortant SMTP », 
il faudra par contre spécifier le port 587 (et non pas le port par défaut 25) ; 
de plus, il faudra activer l'utilisation d'un compte avec identifiant et mot de 
passe, qui sont les mêmes que pour la réception. Vous pourrez revenir au port 25 
quand vous aurez basculé votre accès internet chez FDN.

#### Clefs SSH

SSH est le nom standard d'un protocole/logiciel sous Unix. C'est à la base un telnet sécurisé par 
un algorithme à clefs privés/publiques (cf. par 
[exemple](https://fr.wikipedia.org/wiki/Cryptographie_asymetrique)). La clef 
publique SSH est celle que vous envoyez à votre gentil·le administrateur·rice pour activer vos accès 
(via SSH donc) à solo et yoda comme vu plus haut. N'envoyez surtout pas la clef 
privée. La méthode pour générer les clefs dépend du logiciel utilisé, voyez son 
manuel.
