Après avoir bien paramétré votre
[support/faq/modem.md](support/faq/modem.md) en mode Bridge sur
votre passerelle, il vous faut paramétrer votre firewall pour accepter
de relayer votre trafic sur Internet, et ramener à votre ordinateur le
trafic que vous avez demandé.

Le net dispose de plein de ressources pour régler son firewall et
l\'adapter à ses besoin. Mais pour faciliter la tache de ceux qui n\'ont
pas la possiblité ou le temps de s\'y intéresser, je met à votre
disposition un petit script bash pour faire un réglage qui devrait
correspondre ( à quelques détails près ) aux besoins les plus courants.
De plus, cet exemple dispose d\'une bonne partie de la palette des
règles à savoir utiliser pour une utilisation normale.

Ce code, après avoit été inscrit dans un fichier (par exemple
iptables.sh), doit être rendu exécutable afin de pouvoir etre utilisé.
chmod +x iptables.sh Vous devrez ensuite le placer dans les scripts de
démarrages pour qu\'il soit exécuté lors du boot de votre passerelle, ou
dans le répertoire /etc/ppp/ip-up.d afin d\'être exécuté par pppd dès
l\'établissement de la connection.

En voici le code, documenté, que vous pouvez utiliser moyennant quelques
adaptations. Dans ce script, plusieurs variables sont utilisées :

 * WAN : interface réseau utilisée pour sortir sur Internet, dans mon cas ppp0, notée ppp+ pour correspondre à ppp~0~ / ppp~1~ / ppp~n~ fonction du nombre que j'en ai. Cela évite que tout soit obsolete si un jour, pour une raison ou une autre mon interface se nomme autrement que ppp~0~.
 * LAN : interface réseau utilisée pour le réseau local
 * PPP_LOCAL : Adresse IP publique fournie par FDN. Variable utilisée dans le script, lorsqu'on active la règle pour remplacer l'adresse IP privée des paquets par l'adresse publique routable sur Internet. Vous noterez qu'une deuxieme solution est optée dans le script qui consiste à utiliser une fonction de iptables nommée MASQUERADE. Je laisserai le soin au lecteur d'aller chercher l'information sur les différentes options disponibles pour configurer iptables.

~~~

1.  !/bin/sh
2.  Paramétrage du Firewall

<!-- -->

1.  Définition des variables

WAN=ppp+ LAN=eth0 PPP\_LOCAL=80.67.176.224

1.  REMISE à ZERO des règles de filtrage

iptables -F iptables -X iptables -t nat -F iptables -t nat -X

1.  Configuration par défaut

iptables -P OUTPUT ACCEPT iptables -P INPUT DROP iptables -P FORWARD
ACCEPT

1.  On laisse tous les acces à l\'interface loopback

iptables -A INPUT -i lo -j ACCEPT

1.  On autorise le trafic sur le LAN sans filtrage

iptables -A INPUT -i \$LAN -s 192.168.1.0/24 -j ACCEPT

1.  On laisse le trafic déjà établi afin de ne pas couper
2.  des connections éventuellement établies

iptables -A INPUT -m state \--state RELATED,ESTABLISHED -j ACCEPT

1.  On exclut les paquets qui n\'ont rien à faire en entrant du WAN
2.  afin de ne pas recevoir de paquets non-routables sur internet
3.  Consulter la RFC 1918 pour avoir plus d\'informations.

iptables -A FORWARD -i \$WAN -s 192.168.0.0/16 -j DROP iptables -A
FORWARD -i \$WAN -s 172.16.0.0/12 -j DROP iptables -A FORWARD -i \$WAN
-s 10.0.0.0/8 -j DROP iptables -A INPUT -i \$WAN -s 192.168.0.0/16 -j
DROP iptables -A INPUT -i \$WAN -s 172.16.0.0/12 -j DROP iptables -A
INPUT -i \$WAN -s 10.0.0.0/8 -j DROP

1.  On filtre les paquets sortants pour éviter de polluer Internet
2.  J\'ai désactivé cette règle car elle ne me semblait pas utile.
3.  Je la laisse pour laisser au lecteur le soin de se documenter.
4.  iptables -A OUTPUT -s 192.168.1.0/24 -i \$WAN -j DROP

<!-- -->

1.  Pas de NetBios annoncé sur Internet
2.  Il reste annoncable sur le LAN

iptables -A FORWARD -p tcp \--sport 137:139 -o \$WAN -j DROP iptables -A
FORWARD -p udp \--sport 137:139 -o \$WAN -j DROP iptables -A OUTPUT -p
tcp \--sport 137:139 -o \$WAN -j DROP iptables -A OUTPUT -p udp \--sport
137:139 -o \$WAN -j DROP

1.  Service DNS disponible LAN/WAN
2.  A activer pour avoir un DNS sur son réseau accessible depuis
    l\'extérieur.
3.  Si vous faites du transfert de zone, veillez à laisser tcp ouvert.

iptables -A INPUT -p tcp \--dport 53 -j ACCEPT iptables -A INPUT -p udp
\--dport 53 -j ACCEPT

1.  Service HTTP disponible WAN
2.  A ouvrir pour autohéberger un site web
3.  iptables -A INPUT -p tcp -i \$WAN \--dport 80 -j ACCEPT

<!-- -->

1.  On accepte ICMP et on log

iptables -A INPUT -i \$WAN -p icmp -m limit \--limit 12/hour
\--limit-burst 1 -j LOG \--log-prefix \"ICMP flood: \"

1.  Récupéré sur le wiki FDN -\> règles pour le NAT
2.  La regle mangle sert à corriger les problemes de MTU rencontrés par
    PPPoE.
3.  iptables -t nat -I POSTROUTING -o \$WAN -s 192.168.1.0/24 -j SNAT
    \--to-source \$PPP\_LOCAL

iptables -t mangle -I FORWARD -o \$WAN -p tcp -m tcp \--tcp-flags
SYN,RST SYN -j TCPMSS \--clamp-mss-to-pmtu

1.  Autre méthode pour faire du NAT

iptables -t nat -A POSTROUTING -s 192.168.1.0/24 -o \$WAN -j MASQUERADE
iptables -A FORWARD -i \$LAN -j ACCEPT

1.  APRES AVOIR TOUT CONFIGURE LA REGLE PAR DEFAUT POUR WAN EST REJECT
2.  iptables fonctionne par priorité dans la liste, tout ce qui n\'a pas
    déjà été traité
3.  rencontrera cette regle, et sera rejetté.

iptables -t INPUT -i \$WAN -j REJECT

1.  1.  PORT FORWARDING

2.  Règles utilisées pour rediriger un port particulier vers une machine
    particulière.
3.  Ici, une trame DNS sera redirigée sur le réseau local vers la
    machine qui dispose
4.  du serveur pour traiter ce genre de requetes (ici 192.168.1.2)

iptables -t nat -A PREROUTING -p udp \--dport 53 -i \$WAN -j DNAT
\--to-destination 192.168.1.2:53 iptables -t nat -A PREROUTING -p tcp
\--dport 53 -i \$WAN -j DNAT \--to-destination 192.168.1.2:53 ~~~

N\'hésitez pas à utiliser ce script pour démarrer votre configuration.

Inutile bien sûr d\'insister sur la nécessité de faire des sauvegardes
des configurations qui fonctionnent déjà, et sur la nécessité de se
documenter malgré tout pour éviter de faire des erreurs lourdes de
conséquences.

Ce sujet de wiki évoluera en fonction de la documentation que
j\'arriverai à rassembler.
