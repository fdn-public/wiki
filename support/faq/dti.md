Pour une ligne avec abonnement téléphonique, la DTI est le point de
démarcation entre la ligne France Télécom (paire
cuivre) et votre installation intérieure. Dans certains cas (si vous
avez souscrit un contrat qui couvre votre installation intérieure)
France Télécom peut être responsable de votre installation intérieure;
mais dans la plupart des cas, c'est votre responsabilité (ou celle de
votre propriétaire).

![Photo de DTI](./images/dti.jpeg)

La partie gauche de la DTI est la partie France Télécom; la partie
droite est la partie usager. Dedans vous trouverez un bloc à puncher où
sont normalement reliés les deux fils de votre paire téléphonique
d'usager. Ne jamais toucher à la partie gauche de la DTI (France
Télécom surveillent leur lignes et verront un défaut sur la ligne si
vous la débranchez ou la changez).

Si vous disposez d'une DTI et que vous avez des problèmes de synchro,
il est important que vous fassiez vos tests au niveau de la DTI. Cela
vous permettra de confirmer si les problèmes sont présents sur la prise
de test (auquel cas contactez support@fdn.fr pour la marche à suivre),
ou si les problèmes sont dans votre installation interne (auquel cas
vous devrez localiser le problème et le faire réparer vous-même).
