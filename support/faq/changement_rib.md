###### Pour modifier vos coordonnées bancaires auprès de FDN

Lors de votre adhésion, vous avez transmis à FDN un mandat SEPA. Si vous changez 
de compte bancaire, il faut nous donner à nouveau l'autorisation de prélever des 
sous sur votre compte.

La procédure est décrite dans [la FAQ sur le site Internet](https://www.fdn.fr/faq/#jai-chang%C3%A9-de-compte-en-banque).

À la réception du mandat, le nouveau compte sera validé et il deviendra le compte 
principal, les futurs prélèvements se feront sur le compte. À priori, à partir 
du mois m+1 car il y a souvent un mois d'écart avec le mois effectivement prélevé 
(pensez à continuer d'approvisionner l'ancien compte en conséquence).
