###### Obtenir des adresses IPs supplémentaires

Vous disposez tous, en tant qu\'abonné à Internet par FDN, d\'une
adresse IPv4 publique et d\'un préfixe IPv6. Vous pouvez trouver ces
informations dans votre [espace
adhérent](https://vador.fdn.fr/adherents/), via le lien «
Adresses IP » dans le menu.

Certains auront peut-être besoin de plus d\'adresses IPv4 (en IPv6, vous
avez déjà 2⁸⁰ adresses IP et 2¹⁶ subnets). En général, il s\'agit de
faire tourner différents services sur différentes IPs, ou bien de
distinguer publiquement différentes machines, ou encore d\'avoir chacun
une adresse publique en colocation.

Dans tous les cas, cette demande d\'IPs supplémentaires doit être
justifiée auprès du [RIPE](http://www.ripe.net/), car comme
vous le savez peut-être, les adresses IPv4 se font de plus en plus
rares. Si vous en avez donc réellement besoin et que vous pouvez le
justifier, il faut vous adresser à
services\@fdn.fr.

Techniquement, vous allez demander un \_\_préfixe\_\_ d\'une certaine
longueur, donc un sous-réseau au sens IP, et non pas juste une adresse
en plus. Comme un sous-réseau contient nécessairement l\'adresse du-dit
réseau et l\'adresse de broadcast de ce réseau, attribuer un préfixe /31
(donc qui ne contient que deux adresses, celle du réseau et de
broadcast) est un non-sens, puisqu\'aucune adresse IP n\'est libre dans
ce bloc.

Pour un besoin d\'une ou deux adresses IPv4, il faudra demander un /30 ;
de 3 à 6, un /29 ; etc. Le nombre d\'adresses disponibles dans un
préfixe est déterminé par la formule : 2\^(32 - longueur du préfixe)-2.

Vous ne pouvez pas choisir la plage d\'adresses de laquelle le préfixe
sera extrait. Des plages d\'adresses de FDN sont spécialement dédiées
aux adresses supplémentaires.

Administrativement, du fait que les adresses sont rares, le RIPE est
très regardant sur l\'usage qui va être fait de ces adresses. Il nous
demande donc de remplir un formulaire qui contient les éléments suivants
:

 * Un document officiel (numérisé) qui établisse l'existence du demandeur
   * Pour les particuliers : carte d'identité ou passeport
   * Pour les associations : extrait du JO de la déclaration ou récipissé de la préfecture
   * Pour les entreprises : KBis
 * Nom légal de l'organisation / du particulier
 * Localisation (des machines qui utiliseront les IPs). Exemple : France, Tataouine-les-Bains
 * Site Web (facultatif)
 * Description succinte de l'organisation (en anglais)
 * Espace déjà disponible pouvant convenir à cet usage (si vous avez déjà des blocs en plus, s'assurer qu'ils sont bien utilisés)
 * Espace demandé (suffixe CIDR) (/30, /28, etc.)
 * Découpage de cet espace selon les usages, et usages dans le temps (à court, moyen et long terme), comment allez vous utiliser ces adresses ?
 * Politique d'assignation pour chacun de ces usages (pour ce qui est des attributions à des clients / abonné / « autre qui ne soit pas le demandeur », c'est forcément maximum une IP par personne)
 * Explication en anglais pour chacun des usages ci-dessus
 * Liste des équipements utilisés pour l'infrastructure (nom + fabricant + modèle + commentaire/rôle, mais pas les factures). Quelques infos essentielles suffisent, mais si vous avez prévu qu'une partie des IPs sont utilisées pour votre infra, il faut que ça puisse correspondre
 * Un plan du réseau (si c'est un peu compliqué)

Certains points ne peuvent pas s\'appliquer à tous les cas. Il vous faut
essayer de remplir au mieux, et si vous ne savez pas quoi mettre pour un
cas donné, laissez vide, on vous aiguillera.

Une fois cette demande faite et validée par adminsys, qui va aussi vous
demander éventuellement des informations complémentaires pour remplir
les formulaires du RIPE, vous receverez plus tard l\'adresse du bloc qui
vous a été attribuée. Il ne restera plus qu\'à redémarrer votre modem et
router les nouvelles adresses : à vous de vous débrouiller avec vos
machines.

Si vous êtes à l\'aise avec les demandes du RIPE, vous pouvez
directement joindre à votre e-mail l\'exemple ci-dessous adapté, sans
oubliez votre pièce d\'identité :

<code> % Provider Aggregatable (PA) Assignment Request Form

\% RIPE NCC members can use this form to request a PA assignment. Please
see % ripe-489 for instructions on how to complete this form.

1.  \[GENERAL INFORMATION\]\#

\% % Please add your RegID.

request-type: pa-ipv4 form-version: 1.2 x-ncc-regid: fr.gitoyen

1.  \[ADDRESS SPACE USER\]\#

\% % Who will use the requested address space?

legal-organisation-name: organisation-location: website-if-available:

\% Does this End User already have address space that can be % used for
this assignment? (Yes/No)

space-available:

1.  \[ADDRESSING PLAN\]\#

\% As of 1 January 2010 assignments are for a period of up to 12 months.
% As of 1 July 2010 assignments are for a period of up to 9 months. % As
of 1 January 2011 assignments are for a period of up to 6 months. % As
of 1 July 2011 assignments are for a period of up to 3 months. % % How
will the End User use this address space? % % Subnet Immediate
Intermediate Entire Purpose % size (/nn) Requirement Requirement Period

subnet: subnet: totals:

number-of-subnets:

\% Which netname will you use for this assignment?

netname:

\% Will the End User return any address space?

address-space-returned:

1.  \[EQUIPMENT DESCRIPTION\]\#

\% % What equipment will be used and how will it use the requested %
address space?

equipment-name: manufacturer-name: model-number: other-data:

equipment-name: manufacturer-name: model-number: other-data:

1.  \[NETWORK DESCRIPTION\]\#

\% % Please add more information if you think it will help us understand
% this request.

<add more information>

1.  \[NETWORK DIAGRAM\]\#

\% % Have you attached a network diagram to this request? (Yes/No)

diagram-attached:

1.  \[END of REQUEST\]\#

~~~
