###### Qu\'est-ce que Usenet

 * Archive-name: FDN/Doc/Qu_est-ce_que_Usenet
 * Original-translate: vb@scope.fdn.org (Vincent Borghi)
 * Comment: traduction de l'article "What is Usenet?" de Chip Salzenberg, remanié par Gene Spafford, posté mensuellement en groupes news.announce.newusers.
 * Last-change: Sun Jul 24 1994 by chris@fdn.org (Christian Paulus)

------------------------------------------------------------------------

Le serveur Usenet de fdn est news.fdn.fr

------------------------------------------------------------------------

La première chose à comprendre sur Usenet est, justement, que Usenet est
largement mal compris.

Chaque jour sur Usenet, le phénomène de \"Les aveugles et l\'éléphant\"
apparaît plusieurs fois.

Au sein même des utilisateurs Usenet, la cause la plus fréquente des
querelles (\"flame wars\") est liée à cette méconnaissance. Alors vous
imaginez à quel point Usenet peut être mal compris à l\'extérieur!

Les idées fausses sont monnaie courante parmi les utilisateurs Usenet.
Aussi, cet article commence par balayer ces idées fausses (\"Usenet
n\'est pas\...\"), avant de cerner la réalité (\"Usenet est\...\").

##### Usenet n\'est pas\...

#### Usenet n\'est pas une organisation.

Aucun individu, ni aucun groupe ou association, n\'a autorité sur Usenet
dans son ensemble. Qui accède aux news ? Quels articles sont propagés et
où ? Qui peut poster des articles ? etc. Personne ne le contrôle. Il
n\'existe pas de société \"Usenet Inc.\", ni de \"Usenet User\'s
Group\". Vous dépendez de vous-même.

Certes, diverses activités sont organisées à propos des newsgroups
Usenet. La création de nouveaux newsgroups en est un exemple. Mais ce
serait une erreur de confondre Usenet avec les activités que Usenet rend
possibles. Même si ces activités s\'arrêtaient demain, Usenet
continuerait sans elles.

#### Usenet n\'est pas une démocratie

Puisque rien ni personne ne contrôle Usenet dans son ensemble, il n\'y a
pas de \"gouvernement\" Usenet. Par conséquent, Usenet ne peut pas être
une démocratie, ni une autocratie, ni aucune autre espèce de \"-cratie\"
(mais voir \"Le nez du chameau ?\" plus loin.)

#### Usenet n\'est pas juste

Après tout, qui peut décider de ce qui est juste ou non ? Si quelqu\'un
se comporte de manière injuste ou déloyale, qui l\'arrêtera ? Ni vous,
ni moi, pour sûr.

#### Usenet n\'est pas un droit

Certains considèrent que la liberté d\'expression leur confère le droit
incontestable d\'utiliser les ordinateurs des autres pour propager ce
qu\'ils veulent dire. Ils se trompent. La liberté d\'expression, c\'est
aussi la liberté de rester silencieux. Si je choisis de ne pas mettre
mon ordinateur à disposition pour véhiculer votre discours, c\'est mon
droit. Et oui, la liberté de la presse appartient à ceux qui détiennent
la presse.

#### Usenet n\'est pas un service public

Certains sites Usenet ou certaines de leurs subdivisions, relèvent
d\'organisations publiques ou institutionnelles. Cependant, pour la
plupart des sites, ce n\'est pas le cas. Sur Usenet, il n\'y a aucun
monopole gouvernemental, et peu ou pas de contrôle gouvernemental.

#### Usenet n\'est pas un réseau universitaire

Ce n\'est pas une surprise de voir que de nombreux sites Usenet sont des
universités, des laboratoires de recherche et autres institutions
académiques.

Historiquement, Usenet, tire son origine d\'un lien mis en place entre
deux universités qui souhaitaient simplement s\'échanger des idées et
des informations. Mais, au fil des années, le caractère initial de
Usenet s\'est transformé, si bien qu\'aujourd\'hui, la plupart des sites
Usenet sont des entités commerciales.

#### Usenet n\'est pas un support publicitaire

Du fait de l\'origine universitaire de Usenet, et du fait que Usenet
repose massivement sur la notion de coopération (parfois même entre
concurrents), l\'usage dicte que la publicité soit restreinte à un
niveau minimal. La publicité est tolérée si elle est occasionnelle,
informative et non tapageuse.

Le newsgroup comp.newprod (dédié aux annonces de nouveaux produits dans
le domaine informatique) n\'est PAS une exception à cette règle : les
annonces sont filtrées par une personne qui joue le rôle de modérateur,
dans le but de garantir un niveau informatif raisonnable.

Si vous devez engager une campagne publicitaire, utilisez les newsgroups
de la hiérarchie \"biz\" où la publicité est explicitement acceptée. Les
annonces toucheront ainsi uniquement les sites qui le veulent (du reste,
c\'est le cas pour tous les newsgroups Usenet).

#### Usenet n\'est pas Internet

Internet est un réseau de grande envergure, subdivisé par différents
gouvernements. Internet est le véhicule de nombreux types de trafic,
dont Usenet entre autres. De plus, Usenet est véhiculé au travers de
nombreux réseaux, dont Internet entre autres.

#### Usenet n\'est pas un réseau UUCP

UUCP est un protocole (en réalité, un jeu de plusieurs protocoles, mais
n\'entrons pas dans les détails techniques) qui permet la transmission
de données entre connexions point-à-point, typiquement par
l\'intermédiaire de modems. UUCP est utilisé pour véhiculer de nombreux
types de trafic : Usenet entre autres, mais pas seulement. De plus, UUCP
n\'est qu\'un des véhicules du trafic Usenet, il y en a d\'autres.

#### Usenet n\'est pas un réseau spécifiquement américain

Certes, Usenet est né aux Etats-Unis, et c\'est là, dans un premier
temps, qu\'il s\'est développé le plus rapidement. Mais maintenant,
Usenet s\'étend au monde entier.

Mis à part les Etats-Unis, on trouve les plus grosses concentrations de
sites Usenet au Canada, en Europe, en Australie et au Japon.

Lorsque vous postez des articles, gardez à l\'esprit la portée mondiale
de Usenet. Ceux qui les liront, même s\'ils connaissent votre langue,
peuvent avoir une culture radicalement différente de la vôtre. Et aux
yeux des lecteurs, votre prose risque de prendre un sens qui n\'est pas
celui que vous croyez.

#### Usenet n\'est pas un réseau UNIX

Ne partez pas du principe que tout le monde se sert d\'une machine UNIX
et de \"rn\". Les systèmes utilisés pour poster et lire dans Usenet sont
variés : des VAX sous VMS, des mainframes IBM, des Amigas, des PC sous
MS-DOS, etc.

#### Usenet n\'est pas un réseau ASCII

Le A dans ASCII est le A de \"American\". Les sites d\'autres pays
utilisent souvent des jeux de caractères plus adaptés à leur langue.
Typiquement, mais pas toujours, il s\'agit de surensembles du jeu de
caractères ASCII. En fait, même aux Etats-Unis l\'emploi de l\'ASCII
n\'est pas universel : ainsi, des mainframes IBM recourent à l\'EBCDIC.
Donc, ignorez les sites non-ASCII si vous le voulez, mais ils existent.

#### Usenet n\'est pas un logiciel

Il existe des dizaines de logiciels destinés au transport et à la
lecture des articles Usenet. \"Le logiciel Usenet\", ça n\'existe pas.

Ces différents logiciels peuvent être utilisés (et le sont) pour
d\'autres types de communications, généralement sans risque de mélanger
les deux. De tels réseaux de communication privés sont typiquement
gardés distincts de Usenet, en inventant des noms de newsgroups
différents de ceux qui sont universellement connus.

Bien, cela suffit pour les définitions a contrario du genre \"Usenet
n\'est pas\...\". Maintenant, soyons positifs.

##### Usenet est\...

Usenet est l\'ensemble des personnes qui échangent des articles marqués
par une ou plusieurs étiquettes universellement connues, appelées
\"newsgroups\" (ou simplement \"groupes\").

Note : Le terme \"newsgroup\" est correct, mais ceux-ci ne le sont pas :
\"conference\", \"table ronde\", \"SIG (Special Interest Group)\",
\"board\", \"bboard\", etc. Pour être compris, soyez précis.

#### Diversité

Si la définition ci-dessus paraît vague, c\'est qu\'elle l\'est. Il est
quasiment impossible de généraliser de manière simple, du fait de la
diversité des sites Usenet : universités, grandes écoles, institutions
d\'état, sociétés commerciales de toutes tailles, individuels avec
ordinateurs de tous types, etc.

Note : Il a déjà été écrit qu\'un réseau qui génère des mégabytes et des
mégabytes de trafic par jour, cela ne peut pas être quelque chose de
vague. Je suis d\'accord. Mais aux limites de Usenet, le trafic n\'est
pas si lourd. Dans le monde impalpable des passerelles entre news-groups
et courrier électronique (mailing lists), il est bien difficile de
distinguer la frontière entre Usenet et le reste.

#### Contrôle

Chaque administrateur contrôle son propre site. Personne n\'a de
contrôle réel sur des sites autres que le sien.

L\'administrateur tient sa puissance du système qu\'il administre. Tant
que son travail donne satisfaction, l\'administrateur peut faire ce
qu\'il veut, jusqu\'à et y compris, couper entièrement Usenet.

Cependant, les sites ne sont pas sans influence sur leurs voisins. On
peut discerner, même si cette notion reste floue, des sites
\"alimenteurs\" et des sites \"alimentés\", selon la direction du flot
de news. Les sites \"alimenteurs\" ont une influence sur les sites
voisins, dans la mesure où ils peuvent décider du trafic qu\'ils vont ou
non propager. Mais ce type d\'influence est usuellement facile à
contourner ; et des décisions maladroites, prises sans tact, provoquent
en retour ressentiment et animosité.

#### Articles périodiques

Pour aider à maintenir la cohésion de Usenet, divers articles (dont
celui-ci) sont postés périodiquement dans les groupes de la hiérarchie
\"news\" (et \"fr.news\"). Ces articles sont fournis par différents
volontaires, au titre de service public. Ils sont peu nombreux, mais
très valables: lisez-les.

Parmi ces articles périodiques, on trouve les listes des newsgroups
actifs, qu\'ils soient \"standard\" (à défaut d\'un terme qui
conviendrait mieux) ou \"alternatifs\". Ces listes, tenues par Gene
Spafford, reflètent sa vue personnelle de Usenet, et ne sont en aucune
façon \"officielles\". Cependant, si vous cherchez une description des
sujets discutés sur Usenet, ou si vous démarrez un nouveau site Usenet,
elles constitueront un très raisonnable point de départ.

#### Propagation

Dans le passé, quand UUCP sur lignes à longue distance était le
principal moyen de transmission des articles, quelques sites \"bien
connectés\" avaient une réelle influence, en déterminant quels
news-groups seraient propagés et où. Ces sites se désignaient eux-mêmes
par l\'expression \"the backbone\".

Mais les choses ont changé. Maintenant, même le plus petit site Internet
bénéficie d\'une connectivité comme n\'osaient en rêver les
administrateurs du backbone d\'antan. De plus, aux Etats-Unis, lignes à
longue distance et modems à haute vitesse sont devenus une réalité
abordable. Du coup, des connexions Usenet longue distance sont à la
portée des petites compagnies.

Il n\'y a qu\'un site proéminent pour le transport UUCP de Usenet aux
Etats-Unis : il s\'agit de UUNET. Mais UUNET n\'est pas un protagoniste
de la guerre de la propagation, puisqu\'il ne refuse aucun trafic. UUNET
facture à la minute, après tout; et à côté de cela, des refus de sa part
pourraient compromettre son statut légal de fournisseur de services à
valeur ajoutée.

Ce qui est dit ci-dessus s\'applique aux Etats-Unis. En Europe,
différents coûts de structures ont favorisé la création d\'organisations
hiérarchiques strictement contrôlées, avec une gestion centralisée des
inscriptions. C\'est très différent de la pratique traditionnelle des
sites des Etats-Unis (prenez un nom, installez votre logiciel, connectez
vous à un fournisseur, et voilà). Le petit monopole du modèle européen,
longtemps incontesté, est maintenant mis en compétition avec des
organisations moins rigides, qui fonctionnent selon le modèle américain.

#### Création de Newsgroups

Le document qui décrit la procédure actuelle pour créer un nouveau
newsgroup s\'intitule \"How To Create A New Newsgroup\". Sa dénomination
courante, cependant est \"the guidelines\" (\"lignes directrices\").

Ndt: Un texte en francais, de Christophe Wolfhugel, Règles pour la
création de nouveaux groupes, est disponible en groupes
fr.announce.newgroups, fr.news.groups et fr.news.reponses.

Si vous suivez les guidelines, il est probable que votre groupe sera
créé et largement diffusé. Cependant, du fait de la nature de Usenet, il
n\'y a aucun moyen de forcer la prise en compte du résultat d\'un vote
pour la création d\'un newsgroup. En fait, pour que votre nouveau
newsgroup soit largement propagé, il importe que non seulement vous
suiviez à la lettre les guidelines, mais aussi son esprit. Et vous ne
devez pas laisser planer d\'ombre sur le déroulement du vote.

En d\'autres mots, ne vous moquez pas des administrateurs systèmes, ils
auront leur revanche. Aussi, vous pouvez vous demander : Comment un
nouvel utilisateur est-il supposé connaître quelque chose sur l\'esprit
des guidelines ? Objectivement, il ne peut pas. Ce qui conduit à la
recommandation suivante :

Si vous êtes un nouvel utilisateur, n\'essayez pas de créer un nouveau
newsgroup.

Si vous avez une bonne idée de newsgroup, lisez le groupe news.groups
pendant un moment (au moins six mois), pour voir comment les choses
fonctionnent. Si vous êtes trop impatient pour attendre six mois, alors
vous avez vraiment besoin d\'apprendre : lisez \"news.groups\" pendant
un an ! Si simplement vous ne pouvez pas attendre, trouvez un vieil
habitué de Usenet pour qu\'il prenne en charge le vote à votre place.

Les lecteurs peuvent penser que ce conseil est strictement inutile.
Ignorez-le, à vos risques et périls. Il est embarrassant de parler avant
d\'apprendre. Il est ridicule d\'arriver la bouche ouverte dans une
société que vous ne connaissez pas. Et il est futile d\'essayer
d\'imposer vos désirs à des gens qui peuvent vous faire taire juste en
appuyant sur une touche de leur clavier.

#### Le nez du chameau ?

Comme indiqué sous le titre \"Usenet n\'est pas\...\", Usenet n\'est pas
une démocratie. Cependant, il y a une caractéristique de Usenet qui
présente une forme de démocratie : la création de newsgroups.

Un nouveau newsgroup n\'a aucune chance d\'être diffusé largement si son
initiateur ne suit pas les guidelines; et les guidelines actuelles
considèrent que la création d\'un newsgroup doit faire l\'objet d\'un
vote ouvert.

Il y a ceux qui pensent que la procédure de création de newsgroup
constitue une puissante forme de démocratie, puisque sans force
coercitive, les décisions prises sont presque toujours mises en
application. Pour eux, l\'aspect démocratique de cette procédure de
création de newsgroup est le précurseur d\'un Usenet Du Futur, organisé
et démocratique.

D\'un autre côté, cet aspect démocratique de la procédure de création de
newsgroup est vu par d\'autres personnes comme une imposture hypocrite,
puisque rien ni personne ne peut obliger l\'application des décisions
prises. Selon ce point de vue, le semblant de démocratie n\'est qu\'un
outil pour faire taire les avocats d\'un newsgroup dont la création a
été rejetée lors d\'un vote.

Alors, Usenet est-il sur la voie de la démocratie totale ? Ou bien les
droits de propriété et la suspicion des autorités centrales
l\'emporteront-ils ? Je prends les paris.

#### Si vous êtes mécontent \...

Les droits de propriété étant ce qu\'ils sont, il n\'y a pas plus haute
autorité, sur Usenet, que les personnes qui possèdent les machines sur
lesquelles transitent le trafic Usenet. Si le détenteur d\'une machine
que vous utilisez vous dit \"nous n\'acceptons pas alt.sex sur cette
machine\", et que cela ne vous plaît pas, Usenet ne vous offre aucun
recours.

Cela ne veut pas dire que vous n\'avez pas d\'options. Selon la nature
de votre site, vous pouvez avoir un recours relevant de la politique
interne. Ou bien vous pouvez trouver un soutien extérieur. Ou encore,
avec un investissement minime, vous pouvez vous-même vous procurer les
news en provenenance d\'autres sources. A cet effet, vous pouvez trouver
un ordinateur capable de récupérer les news pour environ 500\$. Un
ordinateur capable de tourner UNIX vous coûtera moins de 2000\$, sachant
qu\'on trouve des systèmes \"UNIX-like\" distribués gratuitement
(NetBSD, FreeBSD, 386BSD et Linux disponibles sur de nombreux sites ftp
dans le monde, complets, avec le code source, et tout ce qu\'il faut
pour se monter son site Usenet) et au moins deux autres systèmes
commerciaux vendus environ 100\$.

Pensez que faire appel à Usenet ne vous aidera pas. Même si ceux qui
liront votre appel sont susceptibles d\'être acquis à votre cause, ils
auront probablement moins d\'influence sur votre site que vous-même
pouvez en avoir.

Dans le même ordre d\'idées, si le comportement de tel ou tel
utilisateur d\'un autre site vous déplaît, seuls les responsables du
site en question peuvent avoir l\'autorité nécessaire pour y faire
quelque chose. Persuadez-les que l\'utilisateur incriminé leur cause du
tort, et peut-être interviendront-ils.

Si l\'utilisateur en cause est lui-même un responsable du site,
oubliez-le, tout simplement : vous ne pourrez rien y faire.
Eventuellement, configurez votre logiciel de lecture des news de sorte
qu\'il ignore les articles provenant de cette personne.

##### Le coin des citations

#### Usenet et Société

Ceux qui n\'ont jamais pratiqué la communication électronique ne se
rendent peut-être pas compte de ce qu\'est une compétence sociale. Une
compétence sociale qui doit être connue est que les autres ont des
points de vue qui non seulement diffèrent du vôtre, mais en plus vont à
son encontre. Et inversement : vos opinions peuvent contredire celles
des autres. Comme dans une conversation en face-à-face, vous n\'avez pas
besoin de cacher vos opinions derrière une façade. Vous n\'avez pas que
des camarades intimes dans le monde, mais malgré cela, vous pouvez mener
une conversation avec tout un chacun. Celui qui ne le peut pas manque de
savoir vivre. \-- Nick Szabo

#### Usenet et Anarchie

L\'anarchie signifie d\'avoir a tolérer les choses qui vous énervent
vraiment. \-- Auteur inconnu

------------------------------------------------------------------------

Webmaster Nov 97.
