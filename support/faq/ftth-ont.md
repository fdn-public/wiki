# Qu'est-ce qu'un ONT ?

Pour la liaison de notre routeur avec le réseau optique appelé PON (Passive Optical Network), on trouve les équipements suivants :  
- un ONT (Optical Network Terminal) ("terminaison de réseau optique" en français)  
- un PTO (Point de Terminaison Optique)  
- un OLT (Optical Line Terminal) ("terminal de ligne optique" en français) 

Le PTO est la prise fibre installée à l'intérieur du domicile, de manière similaire à la prise téléphonique. Il a une identification unique. Si vous n'en avez pas encore un, c'est le livreur qui le posera. En général il suit le même chemin que la ligne téléphonique.

Lors de la finalisation de l'installation d'une ligne FTTH FDN, un·e technicien·ne vient raccorder un boîtier ONT sur le PTO du domicile via une jarretière optique. Cet ONT est un équipement mis à la disposition de l'utilisateur·rice par l'opérateur (et non FDN) le temps du contrat d'abonnement et est à restituer à la fin de celui-ci.  

L'ONT est en charge de :  
- L'établissement de la connexion avec l'OLT situé à l'extérieur du domicile. L'OLT est le point de départ du réseau optique PON. Pour cela le·a technicien·ne procède au paramétrage de l'authentification de l'ONT par introduction d'un mot de passe via son interface web.  
- La conversion du signal optique (protocole FTTH/PON) vers un signal électrique (protocole Ethernet/IP). Le signal converti est disponible via une prise RJ45. La connectique RJ45 (Registered Jack 45) permet de raccorder un routeur par un câble Ethernet (de préférence Gigabit).

Pour résumer, la chaîne d'équipements dans le domicile est :  
*PTO -> ONT -> routeur -> terminaux de l'utilisateur·rice (ordinateurs, ordiphones, etc..).*

Et vous n'aurez donc qu'à brancher votre [routeur](routeur-ftth.md) à l'ONT, avec un câble Ethernet classique.

[Quelques manuels d'ONT](/support/doc/manuels).

# État et signification des voyants

## ONT Huawei EchoLife HG8010H

| Voyant | États et significations |
| ------ | ------ |
| Power |**Éteint (ou orange)** : l'équipement ONT n'est pas raccordé électriquement ou n'est pas allumé (bouton ON/OFF) ou est hors service.<br>**Vert fixe** : l'équipement ONT est bien alimenté électriquement.|
| PON |voir tableau ci-dessous |
| LOS |voir tableau ci-dessous |
| LAN |**Éteint** : il n'y a pas de communication entre l'équipement ONT et votre routeur, via un câble RJ45.<br>**Vert fixe ou clignotant** : fonctionnement normal, des données sont échangées entre l'équipement ONT et le routeur.|

Signification des différents états des voyants PON et LOS :
| No.| Voyant PON | Voyant LOS | Signification|
| ------ | ------ | ------ | ------ |
| 1 | **Éteint** | **Éteint** | L'équipement ONT est désactivé par l'OLT|
| 2 | **Vert clignotant rapidement**<br>(deux fois par seconde)| **Éteint** | Un signal optique est détecté par l'équipement ONT, la synchronisation optique est en cours| 
| 3 | **Vert fixe** | **Éteint** | La synchronisation optique est établie| 
| 4 | **Éteint** | **Orange clignotant lentement**<br>(toutes les deux secondes) | Aucun signal optique n'est détecté par l'équipement ONT| 
| 5 | **Vert clignotant rapidement**<br>(deux fois par seconde)| **Orange clignotant rapidement**<br>(deux fois par seconde)| L'équipement OLT détecte que l'ONT est défectueux ou déconfiguré| 
| 6 | **Vert clignotant rapidement**<br>(deux fois par seconde)| **Orange clignotant lentement**<br>(toutes les deux secondes) | Le signal optique reçu par l'ONT n'est pas dans la plage de fonctionnement nominal (–27 dBm to –8 dBm)| 
| 7| **Vert clignotant lentement**<br>(toutes les deux secondes)| **Orange clignotant lentement**<br>(toutes les deux secondes)| L'équipement ONT est défaillant| 

État des voyants de l'ONT Huawei EchoLife HG8010H en fonctionnement normal : voyants "POWER" et "PON" en vert fixe, voyant "LAN" en vert fixe ou clignotant, voyant "LOS" éteint.  
![ONT Huawei EchoLife HG8010H](images/ftth-ont-huawei-HG8010H.webp)

## ONT Huawei EchoLife HG8010Hv3 fourni par Orange

L'ONT Huawei est appelé "Boitier Fibre" par Orange

| Voyant | États et significations |
| ------ | ------ |
|Alimentation<br>(ou Power) |**Éteint** : l'équipement ONT n'est pas raccordé électriquement ou n'est pas allumé (bouton ON/OFF) ou est hors service.<br>**Vert fixe** : l'équipement ONT est bien alimenté électriquement.|
|Fibre<br>(ou Optical ou PON)|**Rouge fixe ou éteint** : aucun signal optique n'est détecté par l'équipement ONT.<br>**Vert clignotant** : un signal optique est détecté par l'équipement ONT, la synchronisation optique est en cours.<br>**Vert fixe** : la synchronisation optique est établie.|
|État<br>(ou&nbsp;Alarm/Update)	|**Rouge fixe ou clignotant rapidement** (deux fois par seconde) : l'équipement ONT est défaillant.<br>**Rouge clignotant lentement** : l'équipement ONT est en cours de mise à jour.<br>**Éteint** : l'équipement ONT fonctionne correctement.|
|Livebox<br>(ou LAN)|**Éteint** : il n'y a pas de communication entre l'équipement ONT et votre routeur, via un câble RJ45.<br>**Vert fixe ou clignotant** : fonctionnement normal, des données sont échangées entre l'équipement ONT et le routeur.|

État des voyants de l'ONT Huawei EchoLife HG8010Hv3 en fonctionnement normal : voyants "Alimentation" et "Fibre" en vert fixe, voyant "Livebox" en vert fixe ou clignotant, voyant "État" éteint.  
![ONT Huawei EchoLife HG8010Hv3 fourni par Orange](images/ftth-ont-huawei-HG8010Hv3.webp)

## ONT Nokia G-010G-Q fourni par Covage

Référence : Nokia 7368 ISAM ONT G-010G-Q (Intelligent Services Access Manager)

| Voyant | États et significations |
| ------ | ------ |
| Power |**Éteint** : l'équipement ONT n'est pas raccordé électriquement ou n'est pas allumé (bouton ON/OFF) ou est hors service.<br>**Vert fixe** : l'équipement ONT est bien alimenté électriquement.|
| Alarm	|**Rouge fixe** : l'équipement ONT est défaillant.<br>**Éteint** : l'équipement ONT fonctionne correctement.|
| PON|**Rouge fixe ou éteint** : aucun signal optique n'est détecté par l'équipement ONT.<br>**Vert clignotant** : un signal optique est détecté par l'équipement ONT, la synchronisation optique est en cours.<br>**Vert fixe** : la synchronisation optique est établie.|
| LAN |**Éteint** : il n'y a pas de communication entre l'équipement ONT et votre routeur, via un câble RJ45.<br>**Vert fixe ou clignotant** : fonctionnement normal, des données sont échangées entre l'équipement ONT et le routeur.|

État des voyants de l'ONT Nokia G-010G-Q en fonctionnement normal : voyants "Power" et "PON" en vert fixe, voyant "LAN" en vert fixe ou clignotant,  voyant "Alarm" éteint.  
![ONT NOKIA G-010G-Q fourni par Covage](images/ftth-ont-nokia-G-010G-Q.webp)