## Délégation de zone DNS

Vous avez un nom de domaine, et vous souhaitez que FDN gère cette zone (si vous n\'avez pas de nom de domaine propre, vous pouvez aussi en demander un en `chezmoi.fdn.fr`). Vous pouvez autrement vouloir que FDN soit DNS secondaire.

Pour ces deux possibilités :

1. La plus simple, c'est de laisser FDN gérer la zone ; ainsi, ce seront les serveurs de l'association qui travaillent, mais vous n'avez pas de contrôle "direct" dessus. Pour cela, il faut indiquer sur services@fdn.fr les enregistrements que vous souhaitez avoir (noms et adresse IP, voire MX) et configurer chez votre registrar le domaine pour que les serveurs faisant autorité soient `nsa.fdn.fr` et `gchq.fdn.fr`

2. La moins simple, mais plus configurable, puisque ce sera alors vous qui administrerez votre zone, c'est de monter vous-même votre serveur DNS, et d'utiliser ceux de FDN comme secondaires. Pour cela, il faut soit nous indiquer (toujours sur services@fdn.fr) l'adresse IP du serveur DNS primaire, et que celui-ci autorise les transferts depuis les adresses IP de `nsa.fdn.fr` (8**0.67.169.25**, **2001:910:800::25**) et `gchq.fdn.fr` (**80.67.169.26**, **2001:910:800::26**), soit nous fournir une clef TSIG et autoriser les transferts via cette clef. Il faut également ajouter ces serveurs comme faisant autorité chez votre registrar, après le vôtre.


## Délégation d'une zone inverse

En IPv4, vous pouvez demander à changer votre reverse DNS en écrivant à services@fdn.fr.

En IPv6, on peut vous déléguer la zone inverse de votre préfixe, ou simplement changer le reverse de quelques adresses, si vous n'administrez pas votre propre serveur DNS.
Demandez à services@fdn.fr
