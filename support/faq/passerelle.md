#### Intérets de monter sa Passerelle

### IPv6

Vous n\'êtes pas sans savoir que les adresse répondant à la norme IPv4
sont en voie d\'extinction. Il devient de plus en plus difficile d\'en
obtenir de nouvelles, et ça n\'est pas sans poser quelques soucis,
notament pour les nouveaux opérateurs qui inévitablement se verront
refuser l\'octroi de nouvelles plages d\'IPv4 lorsque leur nombre
deviendra vraiment critique.

Il existe depuis maintenant quelques années, une nouvelle norme appelée
IPv6 pour pallier à ce problème. Sa mise en place est longue, et
fastidieuse, notament au niveau du matériel de coeur de réseau, souvent
incompatible avec cette norme. Il en va de même avec la plupart des
équipements \"grand public\" achetés pour fournir de l\'accès ADSL aux
particuliers.

Vous pourrez consulter cette liste de
[support/faq/modem.md](support/faq/modem.md) pour vous rendre
compte que peu prennent en charge l\'IPv6 nativement.

Une des réponses à ce problème d\'incompatibilité du matériel est de
monter sa propre passerelle Internet, qui se chargera d\'établir la
connection Internet, qui recevra (la plupart du temps) son unique
adresse IPv4, sera en mesure d\'accepter ses multiples adresse IPv6, et
pourra les redistribuer sur son réseau local.

### L\'auto-hébergement

Accepteriez-vous que le facteur vous apporte du courrier déjà ouvert
dans votre boite aux lettres ? Moi non. Pour éviter celà,
l\'autohébergement est une des solutions qu\'il est possible de mettre
en place moyennant un peu d\'investissement (humain).

Vous souhaitez diffuser du contenu pour vos amis ? votre famille ? votre
travail ? Votre PME ? C\'est en vous auto-hébergeant que vous pourrez
devenir une partie intégrante d\'Internet, sans avoir à vous reposer sur
des prestataires dont la confiance n\'est pas toujours avérée.

### Souplesse de gestion

Lorsque l\'on dispose de sa propre passerelle, on devient maître de sa
configuration réseau, on décide de ce que l\'on peut faire ou non, de
qui on autorise ou non. En bref, on se réapproprie son acces à Internet,
et on en devient un véritable acteur. Rappelons en effet que sur
Internet, n\'importe quelle machine peut faire office de serveur, ou de
client.

#### Matériel nécessaire

### Un modem

Nécessaire sur les lignes téléphoniques pour établir une connection en
respectant la norme ADSL. Ce [lien](support/faq/modem.md) vous
fournira une liste du matériel que d\'autres ont déjà essayé, avec leurs
avantages, leurs inconvénients.

### Un routeur

Nécessaire pour acheminer le traffic de plusieurs machines connectées à
un réseau local, sur Internet. Et vice-versa. Un ordinateur peut tout à
faire servir de routeur. De même, il existe des modem-routeur qui
cumulent ces deux fonctions.

### Un ordinateur

Comme précisé ci-avant, un ordinateur peut avoir de multiples fonctions.
Il peut s\'occuper des fonctions de routages, tout comme il peut
s\'occuper également de l\'établissement de la connection internet (en
se reposant sur un [support/faq/modem.md](support/faq/modem.md) qui
lui reste indispensable). C\'est le cas pour les ordinateurs auxquels on
branche un modem en mode Bridge. Ces ordinateurs nécessiteront la
plupart du temps deux interfaces réseaux. On parlera dans ce cas de
passerelle internet, ou Gateway.

Au-delà des fonctions citées ci-dessus, un simple ordinateur type PC par
exemple, meme ancien, peut largement tenir les fonctions de passerelle,
et les cumuler avec d\'autres. Il peut servir de serveur de nom (DNS),
de serveur DHCP (attribution automatique des adresses IP pour le réseau
local), de serveur mail (il porte bien son nom, non ?), de serveur web,
et bien d\'autres choses encore !

L\'important est de comprendre que n\'importe quel vieux machin
poussiéreux au fond du grenier peut servir, inutile de craquer son PEL
pour monter une passerelle.

#### Comment monter sa passerelle ?

### Généralités

Une passerelle comme vous l\'avez vu, c\'est un ordinateur sur lequel on
installe un système d\'exploitation, et à qui on confie les taches
d\'établir la connexion à Internet, et d\'aiguiller les ordinateurs de
son réseau propre pour aller sur Internet.

### Choix du matériel

En suivant les liens ci-dessus, et en lisant les items, vous pourrez
aboutir à la configuration de votre choix, et en optant la plupart du
temps pour du matériel bon marché. Bien sûr, il est recommandé
d\'installer des systèmes GNU/Linux ou \*BSD pour tenir ce rôle, et de
ce fait, le choix du matériel devra se faire pour avoir du matériel
compatible avec ces systèmes. Je pense notament aux diverses cartes
réseaux, voire aux modems ADSL intégrés sous forme de cartes PCI.

### Choix du système d\'exploitation

Ce choix sera à faire en fonction de ses propres connaissances. Les
néophytes se renseigneront pour choisir un environnement dans lequel ils
se sentiront à l\'aise.

Bien qu\'il soit tout à fait possible de faire de sa machine de tous les
jours (celle qu\'on utilise pour aller surfer) une passerelle internet,
il est conseillé de faire tenir ce rôle par une machine dédiée. Etant
donné sa nature, hors mis la configuration initiale, peu de choses
seront à faire dessus, ce qui enlève la nécessité d\'un écran, du
clavier et de la souris. De ce fait, ce genre de manipulation demandera
un petit apprentissage des lignes de commandes. Divers tutoriels sont
disponibles sur Internet.

### Choix des logiciels

Il s\'agit là de se poser les questions comme :

 * Quels usages je veux faire de ma passerelle ?
 * Quels services je veux pouvoir rendre sur mon LAN ?
 * Quels services je veux rendre disponibles sur Internet ?

En effet, ces questions vous aiguilleront sur les programmes à installer
pour rendre les services que vous désirez. Citons par exemple quelques
programmes utilisés courament :

 * Serveur DNS : bind9
 * Serveur DHCP : isc-dhcp-server
 * Serveur PPP : pppd
 * MTA : postfix / sendmail / exim4
 * MDA : procmail / exim4
 * Serveur web : apache2 / lighttpd / nginx / cherokee
 * Bases de données : MySQL / PostgreSQL / Firebird
 * Serveurs XMPP : prosody / ejabberd

#### Installation

Plusieurs tutoriels sont disponibles sur Internet, décrivant toutes les
étapes de la configuration d\'une passerelle. Une des plus importante
pour assurer la sécurité de son propre réseau, est la configuration de
son [firewall](support/faq/iptables.md).

Pour rappel, un firewall est une sorte de barrière entre internet et
votre réseau. Cette barrière possède des règles pour accepter ou non le
traffic venant de l\'extérieur, ou sortant de votre réseau. C\'est aussi
lui qui est chargé de rediriger correctement ce qu\'envoient les
ordinateurs de votre réseau vers Internet, ainsi que de retransmettre
les réponses associées.

#### Exploitation

Une fois votre passerelle installée, il ne vous reste plus qu\'à
profiter d\'un Internet où vous etes seul maitre de ce qui s\'y passe.
Envie de monter un serveur de Voix sur IP pour que tout le monde puisse
vous joindre ? Envie de monter votre VPN pour avoir acces aux ressources
de votre réseau local de manière sécurisée ? C\'est en s\'appuyant sur
ce type de passerelle que vous pourrez monter ces projets.

##### Pour terminer

Cette petite page explicative sera remplie de documentation au fur et à
mesure qu\'elle apparaitra sur le wiki.
