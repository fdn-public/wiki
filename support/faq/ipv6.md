#### Avant-propos

Ce paragraphe se destine avant tout aux abonnés moins versés dans la
technique.

Pour l\'instant, IPv4 continue d\'exister et va co-exister longtemps
avec IPv6. \"Passer à IPv6\" n\'est donc :

 - pas très urgent
 - pas risqué (on ne casse pas l'internet habituel)
 - pas très compliqué (pour peu qu'on s'y mette un peu)

 **Attention\*\*

Ici on est en IP publiques ! En v4, le masquerading fait qu\'il y a
nécessairement un firewall ; avec des IP publiques, c\'est optionnel, et
du coup si vous n\'en mettez pas, votre réseau se retrouve \"à poil\"
sur Internet. Commencez donc éventuellement par n\'accepter en entrée
que des choses correspondant à ce qui est sorti:

  ip6tables -A INPUT -i ppp0 -m state --state RELATED,ESTABLISHED -j ACCEPT
  ip6tables -A INPUT -i ppp0 -j DROP

#### Votre préfixe

Pour connaître le préfixe que FDN vous délègue, allez dans votre
[interface adhérent](https://vador.fdn.fr/adherents/) et
cliquez sur « Adresses IP » dans le menu.

#### Configurer IPv6

##### Configuration de PPP (xDSL, ou Fibre via Ielo ou Netwo)
(Actuellement sans doute seulement
possible avec un serveur connecté en PPPd derrière un modem DSL en mode
\"bridge.)

Il faut éventuellement activer le support IPv6 de pppd, en ajoutant la
ligne suivante dans */etc/ppp/options* :

 ipv6 ,

ou

 +ipv6


Il faut aussi créer une route par défaut pour IPv6 (pppd ne le fait pas
le bougre\...).

Mathieu Arnold a dit: sous FreeBSD, je ferais un truc du genre :

 route add -inet6 default -iface ppp0

Stéphane écrit: Sous Debian une configuration */etc/network/interfaces*
ressemble à:

 auto dsl-provider
 iface dsl-provider inet ppp
   provider dsl-provider
   up ip -f inet6 route add default dev ppp0

Une autre manière d\'ajouter la route est d\'utiliser un script ppp:

Ajouter les deux lignes suivantes au fichier de configuration de ppp
(/etc/ppp/peers/fdn chez moi, mais ça dépend de votre configuration):

 ipv6 ,
 ipparam fdn

La première ligne force pppd a négocier une adresse IPv6. La deuxième
ligne est un paramètre qui sera passé en 6e position au script
/etc/ppp/ipv6-up, qui va lui-même le transmettre à ses collègues du
répertoire /etc/ppp/ipv6-up.d sous le doux nom de PPP\_IPPARAM, pour
qu\'il sache que c\'est pour FDN qu\'il est lancé, et donc peut
installer la route par défaut.

Il faut ensuite créer le script qui va bien dans
/etc/ppp/ipv6-up.d/00defaultroute :

~~~

#!/bin/sh
if [ "$PPP_IPPARAM" = "fdn" ];
 then
   /sbin/ip -f inet6 route del ::/0
   /sbin/ip -f inet6 route add ::/0 dev $PPP_IFACE
fi

~~~

Ne pas oublier de le rendre exécutable le fichier (chmod +x
/etc/ppp/ipv6-up.d/00defaultroute).

Une fois l\'ipv6 actif sur ppp0, vous pouvez configurer une adresse de
la plage attribuée (si la machine sert aussi de proxy web ou de serveur
mail par exemple) soit :

  ip -6 addr add 2001:910:1XXX::1/128 dev ppp0

ou

  ifconfig ppp0 inet6 add 2001:910:1XXX::1/128

mais ce n\'est utile que si la machine n\'a pas d\'autre interface
réseau portant une ipv6 publique, configurée comme décrit ci-dessous.

Sur l\'interface lan du routeur, ce coup-ci dans un sous réseau du /48,
le sous réseau 1 dans l\'exemple (Juste après 1XXX: )

  ip -6 addr add  2001:910:1XXX:1::1/64 dev eth1

ou

  ifconfig eth1 inet6 add 2001:910:1XXX:1::1/64

ou sous Debian dans */etc/network/interfaces*

  iface eth1 inet6 static
    address 2001:910:1XXX:1::1
    netmask 64

Répéter l\'opération sur les autres interfaces si vous avez d\'autres
réseaux sur le routeur, en utilisant des sous-réseaux différents
(2001:910:1XXX:2::/64, 2001:910:1XXX:3::/64, etc.)

Si votre routeur demande vraiment une passerelle, indiquez fe80::1

##### Configuration de DHCPv6 (Fibre via Axione)

Il suffit d'activer le support DHCPv6, la configuration se fera automatiquement.

À noter que certains routeurs exigent que le RA (router advertisement) soit reçu avant de lancer la requête DHCPv6, il faut alors désactiver l'utilisation de RA (router advertisement).

##### Diffuser l\'IPv6 dans le réseau « local »

En premier, vérifier que ipv6 forwarding est activé.

  cat /proc/sys/net/ipv6/conf/all/forwarding

Si le résultat est 1 tout va bien sinon il faut le passer à 1 ( sur
debian taper : *echo net.ipv6.conf.all.forwarding = 1 \>\>
/etc/sysctl.conf* puis lancer *sysctl -p* )

On peut configurer les différents IPv6 sur les différentes machines du
réseau local à la main de la même façon que ci-dessus, mais c\'est
relou. On peut le faire faire automatiquement en installant une annonce
sur le routeur (un peu comme dhcp, mais en plus léger):

installer radvd sur le routeur (apt-get install radvd sur debian like)

configurer radvd via le fichier /etc/radvd.conf en y mettant quelque
chose comme ça :

```

 interface eth1
  {
    AdvSendAdvert on;
    AdvOtherConfigFlag on;
    IgnoreIfMissing on;
    prefix 2001:910:1XXX:1::/64
    {
      AdvValidLifetime 2592000;
      AdvPreferredLifetime 604800;
    };
    route ::/0 { };
    RDNSS 2001:910:1XXX:1::1 { };
 };

```

La ligne \" RDNSS 2001:910:1XXX:1::1 \" indique l\'ipv6 du serveur dns
qui tourne sur votre routeur. Si vous n\'en avez pas en ipv6 sur votre
routeur, ne pas mettre cette ligne, ou alors mettre celles de fdn: \"
RDNSS 2001:910:800::12 2001:910:800::40 { }; \"

Redémarrer radvd

Normalement à cette étape vous devriez avoir une ipv6 sur votre pc et
avoir accés à internet en ipv6, sinon bien penser à configurer/vérifier
ip6tables sur votre routeur.

Vous pouvez utiliser aussi dhcpv6 plutôt que radvd. Pour le coup ça se
configure comme dhcp d\'ipv4, avec des IPs v6.

##### Configuration sur CISCO 870

Le cisco en question dispose d\'un switch intégré, de ce fait on ne peut
créer qu\'un seul réseau physique, du coup on configure le /48 complet
dessus. Il faut bien entendu un ios compatible ipv6, être en mode
enable.

remplacer les XX de l\'adresse ipv6 par ce qui vous à été fournis. Pour
d\'autres modèles, il faut modifier VLAN1 par le nom de l\'interface
lan, de plus si vous avez plusieurs sous réseaux il faut modifier
l\'ipv6 par une adresse d\'une des plage en /64 (2001:910:1XXX:1::1/64,
2001:910:1XXX:2::1/64, 2001:910:1XXX:3::1/64 \...\...)

```
 conf t
   ipv6 unicast-routing
   ipv6 icmp error-interval 0
   ipv6 cef
   !
   interface Vlan1
    ipv6 address 2001:910:1XXX::1/48
    ipv6 enable
    ipv6 nd prefix 2001:910:1XXX::/48
    ipv6 nd ra interval 60
    ipv6 nd ra lifetime 180
   !
   interface Dialer0
    ipv6 enable
    ipv6 traffic-filter ipv6-all in
    ipv6 traffic-filter ipv6-all out
   !
   ipv6 route ::/0 Dialer0
   !
   ipv6 access-list ipv6-all
    permit ipv6 any any
   end
```

##### Configuration sur un routeur OpenWRT

1. Sur votre interface PPP IPv4, rajoutez `option ipv6 '1'`
2. Ajoutez une nouvelle interface configurée comme ceci :
```
config interface 'WAN6'
    option device '@WAN' # remplacer par le nom de votre interface IPv4
    option proto 'static'
    list ip6addr 'votreprefixe::1' # ou l'adresse que vous voulez assigner au routeur
    option ip6prefix 'votreprefixe::/48'
    option ip6gw 'fe80::1'
```
3. (Optionel) Si vous voulez que le routeur distribue des IPv6 à vos équipements
(SLAAC / DHCPv6), ajoutez `option ip6assign '64'` sur vos interfaces lan.

#### Configurer Bind pour les enregistrements inverses IPv6

Dans /etc/bind/named.conf.local (sur debian like) ou dans
/etc/bind/named.conf (peut être sur d\'autres distributions ??)

Ajouter ceci (en remplaçant les \*\*X\*\* par ce qui vous a été fournis
(si votre plage est 2001:910:1042 alors il faut remplacer
\*\*X.X\*\*.0\... par 2.4.0\... et le \*\*XX\*\* par 42 ) :

```
 zone "X.X.0.1.0.1.9.0.1.0.0.2.ip6.arpa" {
       type master;
       file "/etc/bind/db.2001.910.1XXX.ip6.arpa";
 };
```

Ensuite éditer /etc/bind/db.2001.910.10\*\*XX\*\*.ip6.arpa (penser à
remplacer \*\*XX\*\* par la bonne valeur).

Le principe est assez simple, on renseigne l\'ipv6 entière mais à
l\'envers, pour éviter de devoir tout \"taper\" à chaque ligne, on
utilise \$ORIGIN que l\'on configure avec la base invariable de l\'ip.

```
 ;
 $ORIGIN X.X.0.1.0.1.9.0.1.0.0.2.ip6.arpa.
 $TTL    604800
 @       IN      SOA     toto.votredomaine.org. root.votredomaine.org. ( 
                         2010070342      ; Serial
                          10800          ; Refresh
                           8640          ; Retry
                         2419200         ; Expire
                          604800 )       ; Negative Cache TTL
 ;
 @                                               10800 IN        NS      toto.votredomaine.org.
 3.f.d.6.4.6.e.f.f.f.e.3.6.1.2.0.0.0.0.0         10800 IN        PTR     yoda.votredomaine.org.
 2.a.c.3.7.6.e.f.f.f.e.3.6.1.2.0.0.0.0.0         10800 IN        PTR     dark-v.votredomaine.org. 
```

Ajouter autant de lignes PTR que nécessaire. Redémarrer bind, et c\'est
bon.

Petite explication des champs (rapide, lire la doc de bind pour plus
d\'informations) :

La ligne SOA indique en premier le nom du serveur dns faisant autorité
sur le domaine en question (ici toto.votredomaine.org) puis l\'adresse
mail de l\'admin mais en mettant un . à la place du @ (ici
root.votredomaine.org).

La ligne Serial indique le numéro de série de la base, il s\'agit de la
date au format annéemoisjour plus deux chiffres (ici 2010-07-03 et 42),
il faut modifier le sérial à chaque modification de la base (d\'où les
deux chiffres après la date, en cas de modifications multiples le même
jour).

Le champ NS indique le serveur dns qui gère le domaine (s\'il y en a
plusieurs, ajouter d\'autres lignes). Les Champs PTR indique les
enregistrements inverses.

Attention, le . est obligatoire à la fin des nom sur les lignes NS et
PTR, il indique qu\'il ne faut pas ajouter votredomaine.org à la suite.

Note : les enregistrements AAAA pour la résolution nom -\> IP vont dans
le même fichier de zone que pour ipv4.
