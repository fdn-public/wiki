# Comment faire pour accéder au wiki adhérent·es de FDN ?

Pour accéder au [wiki des personnes membres de FDN](https://git.fdn.fr/fdn/wiki), vous devez vous connecter au gitlab avec vos identifiants de membre de FDN.

Les login et mot de passe demandés sont ceux qui vous ont été transmis lors de votre adhésion. Il est possible de changer votre mot de passe en vous connectant à votre [espace adhérent·e](https://vador.fdn.fr/adherents/).

Pour récupérer vos identifiants, consulter [cette page](login_adhacc-42.md).

Si vous souhaitez contribuer en écriture au wiki mais que vous n'êtes pas familier avec gitlab, ce [petit guide](https://git.fdn.fr/fdn/wiki/-/blob/master/pages/accueil-communication/doc/tuto%20wiki.md) vous expliquera comment éditer le wiki via l'interface web.