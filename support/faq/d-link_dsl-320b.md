###### d-link\_dsl-320b

Ce modem pur impose d\'utiliser un routeur (ou un ordinateur dédié à la
fonction de passerelle) si vous avez plus d\'une machine à gérer sur
votre réseau local.

##### Configuration DHCP

Avant toute chose, il faus s\'assurer d\'avoir [les bonnes
informations](login_adhacc-42.md) sous la main.

Une fois le modem branché sur l\'arrivée du téléphone, d\'une part, et
sur un ordinateur de l\'autre, il suffit de se connecter sur
l\'interface web du modem.

L\'adresse d\'administration est <http://192.168.1.1>. Pour cela, il
possible de demander une ip au modem via une requête dhcp. Sinon il
suffit de configurer une ip statique, par exemple 192.168.1.2.

 ifconfig eth0 up 192.168.1.2

Par défaut, une fois authentifié (le couple login/mot de passe est
admin/admin en réglage usine) on tombe sur la page de setup (cf
screenshot).

<img src="images/d-link_dsl-320b-setup.png">

Il faut ensuite PPPoE/PPPoA, remplir le username et le password (c\'est
une bonne idée de l\'avoir noté quelque part, en cas de reset du modem,
il est utile d\'avoir accès à ces informations sans avoir accès à
Internet). Le « connexion type » est PPPoE LCC et les VPI et VCI sont
respectivement 8 et 35.

#### Aller sur Internet

Une fois le modem configuré, une nouvelle requête dhcp va configurer le
client automagiquement.

On peut faire la même chose à la main en utilisant ces trois commandes :

 ifconfig eth0 up $ip
 route add 192.168.1.1 eth0
 route add default gw 192.168.1.1

Il faut bien sûr remplacer \$ip par son ip publique (disponible dans
l\'interface du modem).

##### Configuration bridge

Le mode DHCP ci-dessus ne marchera pas si vous n\'avez pas de client
DHCP; il peut provoquer des conflits si un autre serveur DHCP existe sur
le réseau local. De plus il est verbeux: il y a un bail d\'1 minute pour
redonner rapidement la nouvelle adresse publique en cas de décrochage de
la liaison ADSL, ce qui est superflu avec une adresse IP fixe fournie
par FDN.

Il y a plus de contrôle possible en passant en mode bridge. En
particulier, le mode bridge est le seul qui permette d\'[avoir une
adresse publique
IPv6](ipv6.md). Voici
l\'écran associé:

<img src="images/bridge-dsl-320b.png">

Il vaut mieux désactiver le serveur DHCP du modem dans la page LAN pour
éviter un possible conflit.

Il est à noter que la liaison PPPoE devra être gérée par le routeur
placé juste après ce modem. De même, il faudra configurer ce routeur
pour la translation des adresses IPv4 de votre réseau local. Enfin, il
faudra tenir compte de l\'augmentation de la taille des packets dû à la
couche PPPoE. Sur un routeur sous Linux, les commandes suivantes vont
régler les deux derniers points:

 echo 1 > /proc/sys/net/ipv4/ip_forward
 iptables -t nat    -I POSTROUTING -o ppp0 -s 192.168.1.0/24                     -j SNAT --to-source $PPP_LOCAL
 iptables -t mangle -I FORWARD     -o ppp0 -p tcp -m tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

où PPP\_LOCAL contient l\'adresse IP publique allouée par PPPoE.
