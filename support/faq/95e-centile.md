# Quel est le problème ?

Les publicités pour la fibre optique annoncent du 1Gbps pour tout le monde, open-bar, sans limitation, etc. et on parvient effectivement à obtenir une vitesse proche de cela avec speedtest etc.

Mais on me dit par ailleurs que je ne peux pas utiliser ma fibre à 1Gbps ? Pourtant je paye pour 1Gbps, non ?

En vrai, non. Pour un abonnement **vraiment** à 1Gbps, il faut compter des centaines voire milliers d'euros par mois. Les abonnements grand public à quelques dizaines d'euros par mois **partent du principe** que les abonnés ne consomment pas 1Gbps en permanence. Si vous voulez faire perdre de l'argent à votre fournisseur d'accès commercial, téléchargez à fond en permanence :) Il y a cependant de grandes chances que celui-ci se mette automatiquement à brider votre connexion, sans rien vous dire.

Nous on préfère vous expliquer et vous laisser aviser :)

En vrai, les fournisseur d'accès Internet devraient vous facturer à la consommation effective, car à part sur les accès grand public, sur Internet cela se passe toujours ainsi, et donc pour FDN aussi : sa consommation lui est facturée au 95e centile.

# Définition
La mesure du 95e centile est utilisée par les [**opérateurs de transit**](https://git.fdn.fr/fdn-public/wiki/-/blob/master/lexique.md) pour facturer la consommation de **bande passante** globale de **FDN** vers et depuis Internet, autrement appelé le **transit IP**.

Cette mesure correspond au maximum d'usage de la bande passante utilisée pendant 95 % du temps sur la période considérée (en général un mois calendaire). [Plus d'information sur Wikipedia](https://fr.wikipedia.org/wiki/95e_centile).

Les 5 % d'échantillons restant, indiquant des débits utilisés encore plus grands que cela (souvent beaucoup plus grands) sont ignorés dans le calcul du 95e centile et seront négligés pour la facturation; ils correspondent à des consommations ponctuelles, considérées comme non problématiques.

Autrement dit, si la bande passante utilisée a des pics de consommation très grands, mais que l'ensemble de ceux-ci dure moins que 5% du mois (soit < 36h), ils ne sont pas facturés.

Inversement si la bande passante présente des pics très grands dont l'ensemble dure plus que 5% du temps du mois (soit > 36h), ils comptent pour la facturation.

L'idée derrière, c'est que pour les différents clients du fournisseur de transit, les 5% des pics les plus grands des uns et des autres ne "tombent" pas tellement en même temps, et donc ne s'additionnent pas. Les 95% du temps restant, par contre, s'additionnent.

Une autre manière de voir : pour le fournisseur de transit, ce ne sont pas les pics courts qui vont le pousser à dépenser pour poser des tuyaux plus gros, ce sont les pics qui se prolongent pendant plusieurs jours.

# Tarification

La tarification du transit IP se fait au Mbps, en fonction de cette mesure, et sur le trafic global de FDN. Le tarif auquel FDN a actuellement accès est de 1.2€TTC par Mbps au 95e centile.

Pour chaque accès internet, FDN provisionne en gros 5€ par mois pour le transit IP, ce qui correspond donc à une bande passante moyenne d'environ 4Mbps lissés sur le mois. Certains consomment bien moins, certains consomment bien plus, dans l'ensemble cela s'équilibre par péréquation.

Cependant cela peut devenir problématique avec la démocratisation des accès internet FTTH si un abonné pompe en permanence à 1 Gbps. Il suffit par exemple de transférer à quelques centaines de Mbps pendant au moins 36h par mois pour que cela coûte à FDN quelques centaines d'euros par mois !

Notre modèle de tarification n'est pas adapté à une situation aussi particulière, et nous serions obligé d'imputer les frais de transit engendrés.

Rappel de l'article 4 du [Réglement intérieur](https://www.fdn.fr/reglement.pdf) : _"De manière générale, bien que l’Association ait fait le choix de ne pas fixer de seuil limite arbitraire dans l’usage de certains de ses services, il n’en demeure pas moins qu’une personne adhérente dont l’usage immodéré et visiblement hors norme entraîne un coût inhabituel pour l’Association pourra être amenée à en assumer les conséquences financières."_    

Dans l'histoire de FDN, il n'y a eu que deux débordements : une société, qui a payé, et deux particuliers qui ont particulièrement beaucoup consommé le même mois.

# Recommandation

Il est donc recommandé aux abonnés, lorsqu'ils effectuent de grands transferts de données (dans un sens comme dans l'autre, car c'est le maximum des deux qui nous est facturé), de se poser la question de la meilleure façon de les réaliser. Si l'on a par exemple 1To de vidéos à récupérer, à 40Mbps (donc 5Mo/s) cela prend 55h, donc plus que 36h, et donc ce pic de 40Mbps va compter, et coûter à FDN 48€. Si l'on peut se permettre d'attendre une dizaine de jours, on peut brider le transfert à 10Mbps par exemple (donc 1.25Mo/s), le pic va aussi coûter à FDN, mais seulement 12€.

Merci donc aux personnes réalisant des transferts continus, ou très prolongés, d'appliquer une limitation sur ces transferts, de débit maximal de 5-10Mbps (donc environ 1Mo/s) en entrée et sortie, pour lisser leur trafic mensuel.

Vous pouvez aussi essayer de programmer vos transferts plutôt pendant la nuit entre 3h et 7h du matin, où notre trafic est beaucoup plus réduit.

En un sens c'est aussi une question de "consommation responsable" de savoir maîtriser son débit pour éviter de faire poser des tuyaux toujours plus gros par les opérateurs fibre.

# Connaitre sa consommation

La mesure du 95e centile est indiquée par une ligne rouge présente sur toutes les statistiques réseaux dans l'[espace adhérent·es](https://vador.fdn.fr/adherents/)  (Informations techniques → Statistiques réseau → cliquer sur le graphique associé à son accès pour en obtenir le détail), dont voici un exemple ci-dessous :

![Courbes de trafic sur les dernières 24 heures, 7 jours et 4 semaines avec représentation du 95e centile par une ligne rouge](images/trafic-FTTH-95e-centile-abonne.png)

On peut voir (courbe bleue) un trafic sortant vers Internet permanent assez variable d'environ 60-70Mbps de moyenne, mais le 95e centile se retrouve à 133Mbps à cause de quelques pics prolongés. En lissant vraiment le trafic sur le mois, le 95e centile serait beaucoup moins important.

Pour se faire une idée de l'impact relatif, on peut regarder la courbe du total du trafic pour tous les abonnés FTTH de FDN :

![Courbes de trafic sur les dernières 24 heures, 7 jours et 4 semaines avec représentation du 95e centile par une ligne rouge](images/trafic-FTTH-95e-centile.png)

On constate effectivement que cela représente une part importante du trafic total, on retrouve les pics sur la courbe bleue. Le 95e percentile qui nous est facturé se retrouve à 263.56Mbps, même si la moyenne est de 108.98Mbps en entrée et 122.07Mbps en sortie.

# Cas particulier

Si vous échangez des données avec par exemple un autre abonné FDN, cela reste interne à FDN et ne passe pas par transit, donc cela ne coûtera pas à FDN.
