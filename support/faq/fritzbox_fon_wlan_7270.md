###### FritzBox Fon WLAN 7270

Ce modem-routeur ADSL2+ de chez [AVM](http://www.avm.de/)
offre aussi des fonctionnalités de téléphonie sur IP SIP, le support de
l\'IPv6, réseau sans fil privé et invité, serveur NAS et Média, serveur
d\'impression.

##### Configuration IPv6 pour ADSL FDN

Connectez-vous à l\'interface web (IP par défaut : 192.168.0.1). Si ce
n\'est déjà fait, activez le « Mode Avancé ». Allez dans :

```
 Menu : Internet > Données d'accès
 Onglet : IPv6
 Options :
   Prise en charge d'IPv6 :
     Cochez : Prise en charge pour IPv6 active
   Connectivité IPv6 :
     Sélectionnez : Toujours utiliser une connectivité IPv6 native
   Configuration de la connexion :
     Sélectionnez : Utiliser les paramètres statiques
       Préfix LAN : (celui de votre connexion)
         2001:910:***:: /48
       Préfix WAN :
         Cochez : Utiliser le premier réseau /64 du préfixe LAN
       ID d'interface WAN :
         Cochez : Créer automatiquement l'ID d'interface
       Premier serveur DNS : 2001:910:800::12
       Deuxième serveur DNS : 2001:910:800::40
       Autres options de configuration :
         Cochez : Indiquer le serveur DNS également via Router Advertisement (RFC 5006)
       Adresses locales uniques :
         Sélectionnez : Toujours attribuer des adresses locales uniques (unique local adresses, ULA)

Cliquez sur : Valider
```