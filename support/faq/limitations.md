###### Limitations

##### Résolution DNS isc.org

Suite à [une attaque par amplification DNS dans le
passé](https://768kb.wordpress.com/2013/05/27/the-isc-org-dns-attack/),
les [DNS ouverts de FDN](https://www.fdn.fr/actions/dns/)
présentent une limitation : toutes les requêtes DNS provenant de
l\'extérieur du réseau FDN et concernant le domaine isc.org sont
bloquées et résulteront sur un timeout. Cette limitation n\'existe pas
pour les requêtes DNS en provenance de l\'intérieur du réseau FDN
(abonnés DSL, VPN).
