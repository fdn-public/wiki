###### Tester la connexion DSL

Pour les connexions \"dégroupé partiel SFR\" (et probablement en \"non
dégroupé collecté en ACA\")

Si le modem a bien une synchro, mais que la connexion ne s\'établit pas,
tester avec les identifiants :

 login : adsl@mire.ipadsl
 passwd : adsl

Le site <http://mire.ipadsl.net> devrait être accessible (et seulement
avec ces identifiants, ce site n\'est accessible qu\'en interne avec le
login de test, ce n\'est pas un site public).

Tester aussi éventuellement de la même manière:

 login : test@neufpnp
 passwd : adsl

Pour les lignes non-dégroupées ou dégroupage total (FT), les
identifiants sont :

 login : adsl@adsl
 passwd : adsl
