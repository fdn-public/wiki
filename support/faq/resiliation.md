###### Résiliation d\'une ligne xDSL/FTTH (fibre)

La résiliation se fait (sans frais) par courrier électronique à l’association (services@fdn.fr) ou à défaut par courrier postal simple, daté et signé, en précisant votre numéro de ligne, votre adresse et votre numéro d’adhérent·e. Pensez également à nous préciser si vous souhaitez rester adhérent·e de FDN malgré la fin de votre abonnement. Plus vous prévenez tôt, mieux c’est !

En ce qui concerne la FTTH via Ielo, l’abonnement est soumis à une durée d’engagement d’un an avec préavis de trois mois pour la résiliation ; le cas échéant le reliquat sera facturé au prix coûtant (sans le coût de la bande passante). Pour la FTTH via Aquilenet il y a un engagement seulement pour Netwo Covage.
