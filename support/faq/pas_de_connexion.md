[[_TOC_]]

En panne ? Je réalise mon auto-diagnostic **avant** de contacter le support de FDN.

Cela économise du temps à tout le monde: au lieu d'attendre qu'un⋅e bénévole vous fasse un retour pour savoir quoi tester et qu'ensuite la⋅le bénévole attende les résultats de vos tests, on évite cet aller-retour.

Et l'étape -1 résoud très souvent les soucis, commencez par là et ce sera possiblement réglé en 10 minutes !

# Configuration de l'accès internet

## Pour une connexion fibre

Il faut utiliser un routeur disposant d’un port Ethernet et supportant

 * PPPoE pour la FTTH via Kosc ou Netwo ;
 * PPPoE dans un VLAN pour la FTTH via Bouygues ;
 * DHCP (ou "Dynamic IP" ou "Adresse IP Automatique" selon le routeur) pour la FTTH via Axione.

Il n\'y a que très peu de configurations nécessaires à faire. Ce que
vous pourriez bricoler en plus dans votre modem pourrait poser plus de
problèmes qu\'en résoudre :)

* Deux boîtiers sont posés par le technicien lors de la livraison: un [PTO](ftth-ont.md) qui a d'un côté la fibre directement depuis la rue et de l'autre côté une prise fibre, et un [ONT](ftth-ont.md) qui a d'un côté une prise fibre et de l'autre côté une prise Ethernet.
* Il faut brancher le boîtier ONT à la prise WAN de votre routeur, en utilisant un câble Ethernet au moins catégorie 5e voire 6 pour pouvoir profiter d'un débit 1Gbps.
* Si le routeur supporte l'ADSL/VDSL, configurez-le en mode seulement routeur wifi.


###### Pour les lignes Kosc, Bouygues ou Netwo:
  * Il faut utiliser le mode PPPOE.
  * Pour les lignes Kosc ou Netwo (login de la forme `truc.machin@fdn.ilf.kosc` ou `truc.machin@aquilenet.netwo.bidule`), il ne faut pas activer de VLAN (bien que parfois (oui...) pour des lignes Netwo il faut utiliser le vlan ID 835, de manière similaire aux lignes Bouygues ci-dessous).
  * Pour les lignes Bouygues (login de la forme `truc.machin@fdn.fttnet` ou `truc.machin@aquilenet.fibrepro`), il faut activer le VLAN avec le VLAN ID (ou VID) 4001, en mode tagged ou trunk (pas untagged ou access). Il se peut que pour cela il faille aller dans un onglet "IPTV" pour indiquer qu'Internet doit passer par le VID 4001, et cocher une case "802.1Q Tag".
    * Voir [captures suivantes](support/faq/vlan_asus.md) pour configurer le VLAN sur routeur Asus
  * login: `truc.machin@fdn.ilf.kosc`, `truc.machin@fdn.fttnet`, `truc.machine@aquilenet.netwo.bidule` ou `truc.machin@aquilenet.fibrepro`, comme indiqué par mail, et dans votre espace adhérent·e [https://vador.fdn.fr/adherents/](https://vador.fdn.fr/adherents/)
  * mot de passe: comme indiqué par mail, et dans votre espace adhérent·e [https://vador.fdn.fr/adherents/](https://vador.fdn.fr/adherents/)

###### Pour les lignes Axione:
  * Il ne faut pas activer de VLAN.
  * Il faut utiliser le mode DHCP (ou "Dynamic IP" ou "Adresse IP Automatique" selon le routeur), il n'y a pas de login ni de mot de passe à entrer.
  * Pour activer l'[IPv6](ipv6.md), il peut être nécessaire de convaincre votre client DHCPv6 de ne pas avoir besoin de RA (Router Advertisement) (pour dhcpcd: option `noipv6rs` ; pour `networkd`: dans section `DHCPv6`, option `WithoutRA=solicit`)

Dans tous les cas,

* DNS: automatique
* IPv4: automatique
* [IPv6](ipv6.md): automatique / natif, avec DHCPv6-PD



En cas de souci de connexion, voir la section "Résolution des problèmes" plus bas.

## Pour une connexion ADSL/VDSL

Il faut utiliser un modem supportant l'ADSL ou le VDSL. Attention si votre ligne
a été commandée en VDSL (débit jusqu'à 80Mbps), il *faut* que le modem
supporte le VDSL: un modem ne supportant que l'ADSL ne pourra pas fonctionner
sur une ligne VDSL. Par contre un modem VDSL pourra fonctionner sans problème
sur une ligne ADSL.

Il n\'y a que très peu de configurations nécessaires à faire. Ce que
vous pourriez bricoler en plus dans votre modem pourrait poser plus de
problèmes qu\'en résoudre :)

 * login: truc.machin@fdn.nerim ou truc.machin@fdn.dslnet.fr , comme indiqué dans votre espace adhérent·e [https://vador.fdn.fr/adherents/](https://vador.fdn.fr/adherents/)
 * mot de passe: comme indiqué dans votre espace adhérent·e [https://vador.fdn.fr/adherents/](https://vador.fdn.fr/adherents/)
 * Si le modem propose entre ATM ou PTM, pour l'ADSL il faut choisir ATM, et pour le VDSL il faut choisir PTM.
 * pour l'ADSL, le VP (ou VPI) doit être 8 et le VC (ou VCI) doit être 35. PPPoE ou PPPoA vont aussi bien. Parfois l'un ou l'autre peut être plus stable, mais les deux devraient fonctionner.
 * pour le VDSL, le VLAN doit être activé et le VLAN ID (ou VID) configuré à 835, et utiliser PPPoE
 * DNS: automatique
 * IPv4: automatique
 * IPv6: automatique / natif, avec DHCPv6-PD

En cas de souci de connexion, voir la section "Résolution des problèmes plus bas".
# Résolution des problèmes

La résolution du problème ne peut pas se faire sans que l\'on sache où
se situe le problème. Sans plus d\'information, on risque de chercher
longtemps où se situe le souci, alors qu\'il suffit de quelques tests du
côté de l\'abonné pour détecter plus précisément où chercher.

Les étapes suivantes sont constituées de ces tests, **dont il vous
faut nous remonter les résultats à chaque étape, pour que l\'on sache où
se situe le problème.**

 **Ne grillez pas les étapes** . Si le problème est par exemple votre filtre ADSL qui a grillé, aller remonter le problème à Nerim/Ielo ne fera que perdre du temps à tout le monde sans résoudre le problème. Il faut donc vérifier petit à petit tous les éléments de la connexion ADSL, à partir de votre modem. La première chose que Nerim et Ielo vont nous demander sera de toute façon si ces vérifications ont été faites, donc faites-les avant même de nous contacter, vous y gagnerez du temps :)

On ne peut pas non plus juste "demander à un technicien d\'aller
voir", car si le problème est du côté de l\'abonné·e, cette
intervention+déplacement sera facturée, de l\'ordre de la centaine
d\'euros.

Enfin, laissez toujours le matériel branché et allumé, jusqu'à votre
routeur, pour que l'opérateur puisse éventuellement réparer à distance et
constater la réparation effective, ou au moins diagnostiquer précisément où
se situe le problème pour pouvoir le résoudre sur place.

##  Étapes pour la résolution fibre :

### Étape -1

Essayer de débrancher électriquement l'[ONT](ftth-ont.md) et le routeur, attendre 10 minutes (oui, vraiment 10 minutes, pour que de l\'autre côté du fil la déconnexion soit complète), rallumer l'ONT et le routeur.

C\'est ballot, mais la plupart des problèmes sont résolus en relançant ainsi l'ONT.

### Étape 1 (établissement synchro optique)

 - Quand c'est « coupé », est ce que l'ONT voit encore une synchro ? Indiquez-nous [quels voyants (LEDs) sont allumés](ftth-ont.md#état-et-signification-des-voyants) et de quelle couleur.
 - Si oui: passez à l'étape 2.

##  Étapes pour la résolution ADSL/VDSL :

### Étape -1

Essayer de débrancher complètement le modem (à la fois électriquement et de la ligne téléphonique), attendre 10 minutes (oui, vraiment 10 minutes, pour que de l\'autre côté du fil la déconnexion soit complète), rebrancher le modem.

C\'est ballot, mais la plupart des problèmes sont résolus en relançant ainsi le modem. Si cela arrive trop souvent, passer à l\'étape 0.

### Étape 0

Seulement si vous n\'êtes pas en dégroupage total (i.e. seulement si
vous avez encore un abonnement téléphone Orange). 
Si vous êtes en dégroupage total, il faut passer directement à l\'étape 0.5, car le
téléphone ne marchera de toute façon pas.

 - Avant: brancher un téléphone directement sur la ligne téléphonique, sans filtre ADSL entre la ligne et le téléphone.
 - Test: il y a-t-il une tonalité sans aucun grésillement pendant la coupure?

 - Si oui: réessayer avec le filtre, si ça grésille c'est que c'est le filtre ADSL qui est endommagé et il faut le changer, sinon passer à l'étape 1.
 - Si non: la ligne téléphonique est sans doute en dérangement. Vérifiez le câblage à l'intérieur de votre logement, il y a peut-être un fil mal branché et cela produit un faux contact. Sinon, si le service est facturé par Orange, contacter Orange au 3900.

### Étape 0.5

Tout ce qui est à l\'intérieur du local est de votre responsabilité, et
Orange va facturer le déplacement si en fait le problème était là : il
vaut donc mieux corriger un éventuel problème de câblage interne du
local soi-même ou en faisant venir un électricien (ce qui revient moins
cher que ce qu'Orange demande).

 * Si votre local est équipé d'une [DTI](support/faq/dti.md), faites vos tests dessus en branchant le modem dessus pour la suite des opérations, pour éliminer complètement la question de la desserte interne du logement. 
 * Si vous ne trouvez pas de prise DTI, remontez le câble téléphonique dans le local jusqu'à l'endroit où il sort du bâtiment. Faites les tests et branchez le modem sur la dernière prise avant de sortir du bâtiment.
 * Si vous avez un abonnement téléphonique, c'est à cette dernière prise (DTI ou non) que la responsabilité de Orange s'arrête et vous serez facturé·e si FT se déplace et ne trouve pas de problème en aval de cette prise.
 * Si vous êtes en dégroupage total (pas d'abonnement téléphonique), c'est sur la [tête de ligne](support/faq/tdl.md) que la responsabilité de Orange s'arrête.

### Étape 1 (établissement synchro DSL boucle locale)

 - Quand c'est « coupé », est ce que le modem voit encore une synchro (la LED typiquement appelée "Sync" ou "DSL" est-elle allumée sans clignoter) ?
 - Si oui: passez à l'étape 2.
 - Si non: tenter de redémarrer électriquement le modem :
   - Le redémarrage change-t-il quelque chose pour la synchro ?
   - Si oui: passez à l'étape 2.
   - Si non:
     - Il vaut mieux tester avec un autre modem, un autre câble et un autre filtre, connus pour fonctionner. Assurez-vous d'avoir bien un filtre sur chaque prise, s'il y a dans le logement un téléphone branché sur une prise sans filtre, cela ne fonctionnera pas correctement.
     - Envoyez au support les noms des LEDs du modem (ou une photo si ce sont des dessins à côté des LEDs) et lesquelles sont allumées.
     - Si vous avez sur votre modem (ou modem-routeur) des logs ou une page d'état, faites parvenir les logs à l'équipe support: support@fdn.fr . Oui, les informations sont incompréhensibles, mais nous on les comprend :)
     - Indiquez vos jours et heures de disponibilité dans les semaines à venir à support@fdn.fr , avec un numéro de téléphone de contact sur place et le nom du contact sur place.
     - Laissez votre modem allumé pour aider à la vérification de la connectivité.

Note:

 * Pour l'ADSL, vérifier les [Informations sur les VP/VC](support/faq/vpvc.md) (VP 8 / VC 35) et les [Informations sur les protocoles DSL](support/faq/ppp_atm.md)

##  Étapes communes

### Étape 2 (établissement connexion authentifiée)

 - Si vous avez une ligne Axione (pas de login/mot de passe), vous pouvez passer directement à l'étape 3.
 - Est-ce que l'authentification login/mot de passe fonctionne (voir la partie PPP du modem-routeur) ?
 - Si oui: passez à l'étape 3.
 - Si non:
   - Si vous avez une ligne VDSL, vérifiez que le VLAN 802.1q est activé et configuré à 835
   - Si votre routeur a deux configurations séparées pour les login/mot de passe PAP et CHAP, renseignez-les toutes les deux avec le login/mot de passe que l'on a fourni.
   - Essayez la connexion DSL de test [support/faq/tester_la_connexion_dsl.md](support/faq/tester_la_connexion_dsl.md) (en dégroupé partiel, utiliser adsl@mire.ipadsl comme login et adsl comme mot de passe ; en non dégroupé ou dégroupage total, utiliser adsl@adsl et adsl, dans les deux cas, essayer d'accéder à [http://mire.ipadsl.net](http://mire.ipadsl.net), seule page accessible depuis ce compte de test)
   - Si la connexion DSL de test fonctionne, ré-essayez avec les identifiants d'origine et reprenez l'étape 2 (peut-être il y a-t-il une simple faute de frappe dans le login/mot de passe)
   - Si la connexion DSL de test ne fonctionne pas (mais que la synchro est toujours bonne), contactez support@fdn.fr en indiquant le login que vous utilisez pour essayer de vous connecter, et idéalement les logs de routeur parlant de ppp (oui, le contenu est incompréhensible, mais nous on comprend :-) ). Laissez votre modem allumé avec le bon login / mot de passe pour aider à la vérification de la connectivité.

### Étape 3 (établissement configuration IP)

 - La connexion IP fonctionne-t-elle (par exemple, votre navigateur web accède à www.fdn.fr)?
 - Oui: Fini
 - Non:
   - Essayez [http://80.67.169.52/](http://80.67.169.52/) et [http://chewie.fdn.fr/](http://chewie.fdn.fr/), si le premier indique "botbot is among us" mais que le deuxième ne l'indique pas, c'est que la ligne ADSL ou FTTH fonctionne, et c'est juste un problème de configuration DNS: il faut indiquer dans l'interface de récupérer automatiquement le paramétrage DNS.
   - Essayez de débrancher le modem complètement (électricité et téléphone) pendant 10m, pour que le BAS oublie une éventuelle session fantôme restée coincée.
   - Vérifiez sur [https://isengard.fdn.fr/](https://isengard.fdn.fr/) si ce n'est pas un problème général de notre collecte ADSL ou FTTH
   - Contactez support@fdn.fr

### Étape 4 (Test du débit)

 - Avertissement : toujours réaliser les tests de débit depuis son routeur ou depuis un ordinateur directement connecté au routeur avec un cable ethernet, sans emprunter le réseau local filaire, wifi ou CPL qui peuvent être des sources possible de bridage du débit
 - Essayez de télécharger [https://www.fdn.fr/minitel.avi](https://www.fdn.fr/minitel.avi), qui est sur le réseau de FDN, donc devrait être disponible à pleine vitesse.
 - Observez le débit de réception du fichier. Attention 1Mo/s = 8Mbps, donc si par exemple vous téléchargez à un peu plus de 2Mo/s, c'est que vous avez bien 18Mbps.
 - Pour le débit en émission, il n'y a pas vraiment de test simple. Notez tout de même que le A de ADSL signifie Asymétrique, et donc c'est beaucoup plus lent et c'est "normal" (ça a été décidé ainsi au début des années 2000...), typiquement 800Kbps au mieux lorsqu'on a une ligne 18Mbps, donc 100Ko/s au mieux pour par exemple envoyer des photos vers le reste d'Internet.
 - Pour la fibre, s'assurer que le port WAN de son routeur est à minima de type 1 Gbit/s
 - Le site https://testdebit.info/ propose plusieurs serveurs permettant de tester son débit avec http / https

### Étape 5 (fiabilité de la ligne, pour l'ADSL/VDSL seulement)

Si la ligne fonctionne la plupart du temps mais tombe plusieurs fois par jour, ou bien juste plutôt mal:

 - Vérifiez le câblage à l'intérieur de votre logement, il y a peut-être un fil mal branché et cela produit un faux contact.
 - Si possible, testez au niveau de la DTI si votre logement en possède une, et sinon sur la prise la plus proche de l'arrivée de la ligne téléphonique du logement. Cf les détails de l'étape 0.5: si on commande une intervention et que le problème est en fait dans la desserte interne, l'intervention sera facturée.
 - Essayez avec un filtre et un modem connus pour fonctionner sans problème. L'un comme l'autre peuvent vieillir avec le temps ou les orages.
 - Laissez tourner éventuellement `ping 80.67.169.52` pendant quelques minutes pour obtenir les statistiques de pertes de paquets.
