###### Informations sur les protocoles DSL

DSL peut fonctionner sur plusieurs modes, en fonction de ce qui est
configuré au niveau de l\'opérateur.

Il convient de différencier les protocoles utilisés pour
l\'encapsulation ATM (LLC ou VCmux), et la conversion des différents
procotocoles vers cette encapsulation (IP natif, IPX, PPPoE, PPPoA,
\...).

Dans le cas où le PC assure la fonction PPP, l\'interface entre le PC et
le modem (sur Ethernet) utilise PPPoE, qui est défini dans
[RFC2516](http://www.rfc-editor.org/rfc/rfc2516.txt) \-- qui
défini PADI, PADO, etc.

Le tableau de référence pour les différentes combinaison possible est
[TR-037](http://www.broadband-forum.org/technical/download/TR-037.pdf),
table 2 (page 11).

D\'autre part,
[TR-044](http://www.broadband-forum.org/technical/download/TR-044.pdf)
défini les procédures d\'autoconfiguration du modem (qui utilisent
ILMI).

##### Encapsulation ATM

Au niveau de l\'encapsulation ATM, deux modes sont utilisés courramment:
encapsulation LLC ou encapsulation VCmux.

Ces encapsulations sont décrites dans [RFC2364 \-- PPP over
AAL5](http://www.rfc-editor.org/rfc/rfc2364.txt) qui étend
[RFC2684](http://www.rfc-editor.org/rfc/rfc2684.txt).

L\'encapsulation LLC (\"bridged mode\") permet de multiplexer plusieurs
protocoles sur un seul Virtual Circuit (VC) ATM. Le protocole est alors
identifié par un numéro fourni par IEEE 802.2 \"Logicial Link Control\"
(LLC).

L\'encapsulation VCmux (\"routed mode\") utilise un Virtual Circuit ATM
par protocole.

A noter:

 Encore, un comportement étrange de certaines "vielles" versions de dslams:
 si la ligne a été configurée pour accepter les deux modes sur le dslam, et
 que le client précédent était en LLC ou en VCMUX et que le dslam n'a pas
 rebooté, l'encapsulation est bloquée dans ce mode jusqu'au prochain reboot
 (vu chez feu Neuf sur de vieux dslams)
 Source: ML sur support

##### PPPoE et PPPoA

Bien qu\'il existe de nombreuses façons de convertir les différents
protocoles avant de les encapsuler, en pratique PPPoE et PPPoA sont les
deux méthodes utilisées en DSL parce qu\'elles apportent les avantages
de PPP (authentification, auto-configuration, en particulier).

A noter qu\'il s\'agit ici d\'une notion différente du PPPoE entre un
modem et un PC; en pratique ces appelations indiquent un mode de
fonctionnement plutôt qu\'un protocole unique.

\"PPPoA\" désigne le transport de trames PPP sur encapsulation AAl5.
Dans ce cas PPP est directement encapsulé dans ATM en utilisant les
encapsulations décrites dans RFC2364 (TR-012 section 4.1).

\"PPPoE\" désigne le transport de trames PPP sur PPPoE avec
encapsulation RFC2684. Dans ce cas PPP est encaspulé sur Ethernet
(protocole 0x8864) et c\'est Ethernet qui est encapsulé dans ATM (en
utilisant les encapsulations décrites dans RFC2364).

Voir
[Wikipedia](http://en.wikipedia.org/wiki/Point-to-Point_Protocol_over_Ethernet),
et le tableau 2 de TR-037 référencé ci-dessus.

##### En pratique

Un mode standard est d\'utiliser PPPoA avec encapsulation VCmux.

Un mode standard d\'utiliser PPPoE avec encapsulation LLC.

##### Voir aussi

[Spécifications Broadband
Forum](http://www.broadband-forum.org/technical/trlist.php)
en particulier TR-003
