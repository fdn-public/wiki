###### Informations sur les VP/VC (configuration ATM)

ADSL utilise ATM comme technologie de support. ATM utilise des \"Virtual
Paths\" (VP) et des \"Virtual Circuits\" (VC) pour établir des
connections. Au niveau local, les VP et VC sont identifiés par deux
nombres, le numéro de VP/le numéro de VC.

En France, la combinaison VP/VC est 8/35. Normalement ces numéros sont
détectés automatiquement par le modem DSL (en général par un test
loopback ATM \-- le DSLAM répond sur ce VP/VC, qui est alors considéré
valide). Malheureusement le DSLAM peut ne pas être configuré
correctement, auquel cas le VP 8 / VC 35 doit être spécifié manuellement
par l\'utilisateur dans son modem.
