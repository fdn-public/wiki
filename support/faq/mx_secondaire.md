### Comment utiliser les serveurs FDN en MX secondaire ?

Deux cas de figures peuvent se présenter, selon que vous souhaitez un MX
secondaire classique ou bien utiliser UUCP

MX secondaire classique
-----------------------

Les courriels sont transférés entre le MX secondaire et le MX primaire par SMTP.  
Il faut communiquer le ou les noms de domaine à relayer, à l'adresse services@fdn.fr.

1. Les anciennes demandes étaient configurés sur [Solo](https://git.fdn.fr/adminsys/wiki/-/blob/master/services_aux_adherents/solo_email.md)  
Configurer la zone DNS  :

```
IN MX    10 <adresse de votre MX primaire>
IN MX    20 mail.fdn.fr
```

2. Les nouvelles demandes sont configurés sur [Taslin](https://git.fdn.fr/adminsys/new_mail_server/-/blob/master/doc/user/README.md#cr%C3%A9ation-dun-mx-secondaire)

De plus, le serveur mail.fdn.fr est l'ancien serveur MX backup.  
Il est progressivement migré vers mx.fdn.fr.

MX secondaire avec UUCP
-----------------------

- Demander à adminsys\[chez\]fdn.fr (avec copie à
support\[chez\]fdn.fr) de configurer correctement le serveur de courrier
FDN pour vos domaines (ainsi que les champs DNS si vous avez un sous
domaine de fdn.fr)

  => Un identifiant et un mot de passe pour UUCP doivent vous être communiqués en retour.

- Si vous gérez vos domaines, vous devez enregistrer le MX secondaire.

- Configurer UUCP sur votre serveur pour aller chercher chez FDN les
messages qui y sont allés lorsque votre serveur primaire ne répondait
pas :

Éditer le fichier _/etc/uucp/config_ :

```
nodename <VOTRE IDENTIFIANT UUCP>
spool /var/spool/uucp
logfile /var/log/uucp/Log
statfile /var/log/uucp/Stats
debugfile /var/log/uucp/Debug
```


Ainsi que le fichier _/etc/uucp/sys_ :

```
system fdn
# identification
call-login *
call-password *
# dialogue d'identification
chat ogin: \L ssword: \P
chat-timeout 10
# où joindre ce système (ici on fait de l'UUCP au-dessus de TCP)
address uucp.fdn.fr
# quand le contacter (c'est une définition qui peut être très poussée,
# elle ne définit pas quand bidule appellera fdn, mais quand uucico
# sera autorisé à faire cet appel (uucico est lancé à ta discrétion, et
# en général par une crontab ou bien lors de la montée de ta connexion
# vers le grand nain Ternet).
time Any
# protocole (inutile de chercher compliqué)
protocol t
# et par quel mécanisme (PPP, TCP, SSH ou autre...)
port TCP
```


Et enfin le fichier _/etc/uucp/call_ :

`fdn     U<VOTRE IDENTIFIANT UUCP>    <Votre mot de passe>`

:warning: ATTENTION : IL EST IMPERATIF QUE LE FICHIER _/etc/uucp/call_ NE
SOIT LISIBLE **QUE** PAR LES UTILISATEURS ET LE GROUPE _uucp_

USAGE : `/usr/sbin/uucico -S fdn`

Cette commande transfert les courriels du serveur FDN sur votre serveur
de courrier qui les distribue ensuite, généralement en local. Selon la
fréquence d'exécution, il peut être judicieux de l\'inscrire dans un
_cron.d_.

TEST :

 - Se connecter directement en telnet sur mail.fdn.fr ou mx.fdn.fr
 - Dialoguer en SMTP pour lui transmettre un mail minimal à destination d'une adresse de votre domaine et vérifier qu'il accepte.
 - Lancer `/usr/sbin/uucico -S fdn` pour vérifier que le transfert est effectif.
