Pour une ligne sans abonnement téléphonique (dégroupage total), la tête
de ligne est le point de démarcation entre la ligne France Télécom (paire
cuivre) et votre installation intérieure.  Elle se trouve en général à
l'avant du bâtiment ou dans un local technique, vous trouverez un boîtier avec
des réglettes:

![Photo de tête de ligne](./images/tdl.jpg)

Il y a un câble France Télécom qui arrive sur le boîtier, et des paires qui
partent de la façade du boîtier vers votre logement, typiquement jusqu'à une
DTI dans votre logement. Ce lien entre ce boîtier et la DTI est sous votre
responsabilité, vous pouvez faire faire le raccord par un électricien par
exemple. Il ne faut pas câble au pifomètre bien sûr :) On peut vous indiquer
les numéros d'amorce/paire (i.e. ligne/colonne) et numéro de tête.

Si vous disposez d'une DTI et que vous avez des problèmes de synchro, il est
important que vous fassiez vos tests au niveau de la DTI. Cela vous permettra
de confirmer si les problèmes sont présents sur la prise de test, ce qui
élimine la question d'un problème de desserte *dans* votre logement. Il se
peut par contre qu'il y ait un problème de câblage entre la tête de ligne et
la DTI. Un électricien pourra inspecter cela.
