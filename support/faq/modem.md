# Les modems ADSL/VDSL

## Qu'est-ce qu'un modem ?

Afin de se connecter à FDN par ADSL/VDSL, il faut un modem.

Ça se décompose en deux catégories :

 * soit un modem-routeur, style de "box" qui s'occupe de tout avec une interface relativement simple (on configure le type d'accès, le login "ppp", le wifi s'il y a lieu et zou)
 * soit un modem seul, auquel cas la partie login et réseau interne est faite sur un autre routeur ou ordinateur derrière.

Le choix d\'un tout-en-un ou d\'un modem bien séparé du reste est à
l\'appréciation de chacun, avec les avantages suivants.

Pour un modem-routeur:

 * Ça prend moins de place et consomme moins (une seule box qui fait tout) ;
 * Pour une configuration standard, c'est souvent plus simple

Pour un modem séparé:
* Les systèmes d'exploitation pour routeur comme openWRT supportent assez mal les modems qui sont (trop) souvent propriétaires et non compatible avec linux ; séparer le modem permet de prendre la main sur son routeur avec un OS libre ;
* Alternativement, ou de concert, plutôt que de coupler modem+routeur on peut vouloir coupler routeur+serveur, et avoir le choix de la machine pour faire routeur donne plus de possibilités ;
* Toujours dans les possibilités, les routeurs séparés ou un simple linux permettent de faire un plan de réseau interne beaucoup plus évolué que ce que ne permettent les interfaces de réseau grand public. On peut imaginer plusieurs sous-réseaux pour les invités, pour les enfants, pour le chien, pour les voisins et que tout ce beau monde soit isolé pour des raisons de vie privée, ce qu'un modem-routeur ne saura pas faire.

## Description des listes

Les choix de modems sont présentés sous forme de tableaux :

* Nom du modem.
* Support VDSL (plus rapide, mais seulement si la ligne le supporte ; vérifiez bien cette colonne si votre ligne est VDSL à la souscription, ça ne marchera pas en ADSL !)
* Support IPv6.
* Si un membre l'utilise pour faire de l'auto-hébergement (**A-H**) ; en pratique ça donne confirmation que la redirection de port fonctionne et réconforte que le modem est stable 24h/24.
* Le nom d'un utilisateur.
* Les commentaires à propos du matériel et de ses possibilités. 

### Modem-routeurs des abonné·es

| Nom | VDSL | IPv6 | A-H | Utilisateur connu | Commentaires | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| ASUS DSL-AC51 | Non | Oui | Oui | Antoine V. | Aucun problème en PPPoE, le paramétrage est rapide et l'interface admin plutôt bien faite. Seulement 2 ports ethernet Gb, mais un petit switch derrière et on peut diffuser en filaire dans toute la maison. |
|ASUS DSL-AC52U| Non | ? | ? | yost | Fonctionne sans problème en PPPoE, pas testé les autres configurations. L\'interface d\'administration comporte de nombreuses options. La configuration est simple et rapide. Elle comporte des menus dédiés à l\'ipv6, mais pas testé. (16/04/2018)| 
|ASUS DSL-N12E\_C1 | Non | ? | | domi | Impeccable en ADSL2+, testé en PPPoE. L\'interface est complète et permet de facilement avoir les infos de synchro, de connexion PPP, une visualisation du debit utilisé, des utilisateurs en DHCP, etc. L\'ipv6 est mentionné dans l\'interface, mais pas testé. |  
| Asus DSL-N66U | Oui | ? | ? | Jean-Michel | Fonctionne en IPV4 pas testé en IPV6 |  
| Belkin N1 router | Non | ?| ? | Marianne | Par défaut en 192.168.2.1. |  
|Belkin N150 (F6D4230-4) | Non | ? | ? | fab | Fonctionne très bien en PPPoE, PPPoA ou bridge. Seuls bémols : l\'interface d\'administration n\'est vraiment pas ergonomique et pas de \*wrt. | 
| BeWan Combo R2 | Non | ? | ? | Thomas R. | Le gestionnaire de réseau qu\'on trouve sur ubuntu (qu\'on trouvait en tout cas en septembre dernier) est problématique si on cherche à configurer. Heureusement, normalement, avec la détection automatique, ça marche. |
| BT Home Hub 5A | Oui | Oui | Oui | Alexis | Jamais testé avec le firmware d\'origine (acheté en 2017 sur eBay UK avec OpenWrt pré-installé). Fonctionne très bien en VDSL2. 4 ports Ethernet Gigabit parfaitement fonctionnels. Dernière MàJ 19.07.7 toujours supportée et qui s\'est installée sans problème (https://openwrt.org/toh/hwdata/bt/bt\_homehub\_5\_type\_a). Très stable. WiFi non testé. | 
| Buffalo WBMR-HP-G300H | Non | Oui | Oui| sebian | Fonctionne très bien (Giga ethernet, usb, wifi), Flashé sur OpenWRT (http://wiki.openwrt.org/toh/buffalo/wbmr-hp-g300h), Il y a un tutoriel bien fait sur le site de Skhaen (https://www.libwalk.so/2014/03/04/flasher-un-modem-routeur-buffalo-wbmr-hp-g300h-avec-openwrt.html)|
| Buffalo WZR-HP-G300NH | Non | Oui | Oui | Luc M. | Fonctionne très bien (Giga ethernet, usb, wifi), Flashé sur OpenWRT (https://wiki.openwrt.org/toh/buffalo/wzr-hp-g300h) Chaos Calmer 15.05.1. Configuration IPV6 délicate (voir ci-dessous)| 
| Cisco 1721 | Non | ? | Oui | ex-red | Mêmes remarques que pour 1801 mais passif donc pas de bruit et a l\'avantage de coûter peu cher, moins de 50€ sur ebay, carte modem compris. Ladite carte modem (nécessaire pour faire de l\'ADSL) n\'est par contre pas compatible ADSL2. | 
|[Cisco 1801](cisco_1801.md) | Non | Oui | Oui | ex-red | Fonctionnalités avancées, possibilités de configuration largement documentées. Défaut : fait un peu de bruit (très supportable). | 
| Cisco 867VAE-K9 | Oui | Oui | ? | Jean-Michel | |
|  Cisco 887VA-K9 | Oui | Oui | ? | Jean-Michel | | 
| Cisco RV134W | Oui | ? | Oui | Alexis | Interface austère et mal ordonnée, mais nombreux paramétrages possibles. Connexion Ethernet et WiFi très stables. Marche très bien en PTM/PPPoE, autres modes non testés. Supporte le VDSL2. La mise à jour vers le firmware 1.0.0.29 facilite la configuration WAN.| 
| DGN2000 | Non | ? | Oui | Renault | Configuration assez facile. |  
|DGN2200 (v3) | Non | Non | Oui | julienth37 (TDN) | IPv4 OK . Configuration IPv6 disponible, mais pas moyen d\'obtenir quelque chose de fonctionnel |  
|[D-Link 2640B](d-link_2640b.md) | Non | ? | Oui | Arthus | Aucun problème de stabilité de la connexion, interface web un peu buggée pour le routage. Voir la page dédiée pour plus d\'infos|  
|[D-Link DSL-2542B](d-link_dsl-2542b.md) | Non | mouais | ? | Grunt | <http://grunt.fdn.fr/misc/DSL2542-B.txt> ou http://pastebin.com/q34VvTjp | 
| [FritzBox Fon Wlan 7270](fritzbox_fon_wlan_7270.md) | Non | Oui | ? | Léa G | Stable, IPv6 natif, téléphonie SIP, base DECT, 2 FXS, 1 ISDN. <http://www.fritzbox.eu/en/products/FRITZBox_Fon_WLAN_7270/>| 
| [HUAWEI HG655d](HG655d.md) | Oui | ? | ? | Valentin | Fichier [PDF](./images/HUAWEI_GD655_conf.pdf) de la configuration fourni par l'adhérent |  
|Linksys WAG120N | Non | Non | Oui | Mathieu A. | Pas de difficultés, il juste marche. |  
|Linksys WAG200G | Non | Non | Oui | ph | Marche très bien en ADSL1, il ne plante quasiment jamais, 4 ports Ethernet, Wifi b et g. J\'ai dû mettre à jour le firmware (\>=1.01.08 (03)) pour profiter des débits de l\'ADSL2+ (sinon limité à +/-12 Mbits/s). 20 règles de redirection max avec le firmware de base. Il permet d\'envoyer syslog/snmp vers une autre machine du réseau. Il existe également un [firmware open source](http://sourceforge.net/projects/openwag200/?_test=b) avec des fonctionnalités sympas (SSH, Wake-On-Lan, etc.). Je n\'ai pas testé le mode bridge. Je l\'ai utilisé comme routeur pendant quelques années sans aucun souci jusqu\'à ce qu\'un switch Netgear gigabit s\'intercale entre lui et les machines du réseau. Configuré (en vrac) en PPPoA/Multiplexage VC/QS UBR/VPI 8/VCI 35/Modulation multimode.| 
|Linksys X6200 | Oui | Oui | ? | HyP | Interface Web très sommaire, marche bien dans l\'ensemble. Freeze après des périodes de longues connections en ligne \< 1mois (installation mai 2018)|  
|Linksys X6500 | Oui | Oui | ? | le\_jax | Interface web pourrie (problème de css), pas de CLI, mais fait le boulot correctement. ADSL/VDSL, IPv6 (pas tester, je ne connais pas assez). Installation 11/2017 | 
| Netgear DG832 v4 | Non | ? | Oui | LegeoX | Il est paramétré en bridge (menu caché: voir le code source HTML pour le trouver). | 
| Netgear D500 | Non | Oui | ? | Manu | |NETGEAR N150 Wifi DSL Modem Router Model D500: ADSL2+, ipv6. | 
| Netgear DM200 | Oui | Oui | ? | ertheb | NETGEAR DM200 xDSL Modem Router fw:V1.0.0.61 VDSL2, ipv4, ipv6:non testé Pb constaté (très rare): perte du VLANID lors de déconnexion.|  
|Neufbox | Non | Oui | Oui | Julien V. | Repose sur OpenWrt (le firmware Neufbox est libre) et qui intégre un modem(seul le firmware de ce modem n\'est pas libre\...). Le firmware ne supporte pas l\'IPv6, mais ça peut se corriger en le flashant avec le projet Openbox4, ce qui permet aussi de se passer de l\'interface made in SFR. Pour ça, il faut un convertisseur TTL vers série ou usb qui ne servira qu\'une fois, pour ouvrir le telnet. |  
|Olitec SX-202 | Non| ? | ? | Pascal | Matériel \"d\'entrée de gamme\" et donc pas cher. Fonctionne correctement en PPPoE, PPPoA ou Bridge après mise à jour du firmware à la version FwVer:3.5.10.0\_A\_TC3084 HwVer:T14.F7\_0.0. Les SX-202 livrés en 2010 étaient équipés d\'un ancien firmware et refusaient de se synchroniser. Le SX-202 a tendance à négocier un S/N de 6 dB en descente ce qui peut être un peu limite sur certaines lignes. Il est toujours possible de choisir un palier plus fiable avec un S/N plus élevé en contactant en direct la HotLine de Nerim. Les paliers actuellement disponibles sur certains DSLAM sont 2M/256K, 3.5M/320K, 5.3M/384K et 8M/800K. L\'interface en CLI est accessible en Telnet et permet de récupérer pas mal d\'informations sur la liaison. Par contre il m\'a été impossible de modifier certains paramètres avec l\'interface CLI. Il ne m\'a pas été possible d\'obtenir de synchro en utilisant simultanément deux SX-202 sur deux lignes de 3 Km de long provenant du même local, cheminant ensemble et aboutissant au même DSLAM. | 
|Thomson SpeedTouch 536 | Non | ? | Oui | Fernando | \"Je dirais qu\'il fait aussi routeur car on peut configurer du DHCP, du DNS et des routes. Mais il n\'a qu\'un port réseau et je l\'utilise en monde \"pont\" vers une Debian.\" Capture d\'écran des réglages : [Capture-SpeedTouch\_536-FDN.png](http://demotic.fr/photos/albums/captures_ecrans/FDN/Capture-SpeedTouch_536-FDN.png) |
| [TP-Link Archer VR400 v3](https://www.tp-link.com/fr/home-networking/dsl-modem-router/archer-vr400/) | Oui | Oui | ? | Cyril | Paramétrage très simple de la connexion, l'interface n'est par contre pas disponible en français. Par défaut son IP est : 192.168.1.1. Choisir "Other" pour ISP List, cocher "Enable" pour le VLAN Tag et le définir sur 835 si c'est du VDSL, sélectionner "PPPoE" pour Connection Type et entrer le login/mdp fournit par FDN. L'IPv6 n'est en revanche pas activée par défaut, une fois la connexion établie on arrive sur la page principale avec tous les paramètres, cliquer sur "Advanced" (en haut) puis aller sur "Network" (à gauche) -> "Internet" -> puis sur l'icône à droite pour modifier la connexion -> cocher la case "Enable" en face de IPv6 et enfin cliquer sur "Save" en bas à droite. |
| TP-Link TD-W8961N | Non | Oui | ? |  | Choisir le Virtual Circuit "PVC4" dans "ATM VC", "PPPoA/PPPoE" dans "Encapsulation" et remplir les champs username/password en-dessous, bien cocher la case "Yes" pour "Default Route" en-dessous et cocher "Dynamic" pour "Get IP Address" pour la partie IPv4 Address. Si on tente de modifier un autre PVC pour avoir les bonnes valeurs de VPI/VCI au lieu de choisir le PVC4, le modem refusera de mettre à jour la configuration. Uniquement pour les PVC4 et PVC7, l'option "Default Route" est sur "No" par défaut, ne pas oublier de la passer à "Yes". | 
| TP-Link TD-W9970 | Oui | Oui | Oui | Codimp | Aucun soucis. Autres infos, IP par défaut : 192.168.1.1, une fois sur l'interface du modem (il peut demander la création d'un mot de passe) cliquer sur Quick Setup et choisir ensuite l'option auto-detection. La ligne FDN devrait être trouvée automatiquement, il suffira ensuite d'entrer le login et mot de passe fournis. |
| TP-Link TD-W9980 | Oui | Oui | ? | | Meilleur wifi que le TD-W9970 | 
| TrendNet TEW-658BRM | Non | ? | ? | FloJpg | Aucun problème avec ce modem-routeur avec le dernier firmware (déco avec le firmware d\'origine). Pas cher et fonctionnel, il me satisfait en tout point.<http://www.trendnet.com/langfr/products/proddetail.asp?prod=220_TEW-658BRM&cat=166>
|  Trendnet TEW-816DRM | Non | oui | ? | Xavier | Testé et approuvé en ADSL, ipv4, PPPoE. | 
| [U.S.Robotics USR9107](http://www.fdn.fr/~sascoet/monalbum/albums/agglorleans/fleury-les-aubrais/fleury-les-aubraisbustiere/sa-placedeleuropefleury-les-aubrais/sa-212010fleury-les-aubraisdansmasalleenattendantperrineplusque13jours/sa-21201011h53literiefutainejustedeballeefleury-les-aubraisdansmasalleenattendantperrineplusque13joursessayantderendrecetendroitpluschaleureuxetaccueillantenentendantbonnemusiquealorsquetoutestrecouvertdunepoudreneigeusedehors.jpg) | Non | Non? | ? | [Stéphane Ascoët](http://www.fdn.fr/~sascoet), LegeoX | SA : Contient un hub fonctionnel. Marche très bien avec deux machines en réseau qui communiquent entre-elles et partagent la connexion Internet, même si les importants flux de données ont tendance à bouffer toute la bande passante au détriment d\'autres usages au même moment(mais ce n\'est peut-être pas la faute de l\'appareil). Pas de Wi-fi, avantage pour la santé! Dommage que les deux led-témoin de l\'ADSL soient à l\'inverse de l\'ordre logique et peu visible par forte luminosité ambiante. L : Fonctionne mais peu de fonctionnalité (pas de mode bridge). Peut faire serveur d\'impression.| 
| Zyxel P-660R-D1 | Non | Non | Oui | Thomasvo | Pas de problèmes en PPPoA ; la redirection de ports fonctionne à peu près (on peut pas y mettre de plages), pas réussi à faire tomber en marche en bridge. D\'après Alexis B., le Linksys AM200 est préférable.| 
| AirTies Air5010 (Orange) | Non | Oui | Oui | microniko | Ce qui est pratique c\'est qu\'il est vendu chez Orange(c\'est de plus en plus difficile à trouver un modem ces temps-ci) et est utilisable en mode pont (« modem » seul) ou routeur. |


### Modems seuls des abonné·es

| Nom | VDSL | IPv6 | A-H | Utilisateur connu | Commentaires | 
| ------ | ------ | ------ | ------ | ------ | ------ |
|SpeedTouch 516 | Non | Oui | Oui | Romain R. | Configuration peu aisée. \"je \"parle\" en PPtP et qui recrache du PPPoA. J'ai choisi cette solution après moult gueulantes contre PPPoE et son MTU non-standard qui me faisait perdre des paquets IPv6 un peu partout. En routeur, j'ai un bon vieux WRT54GL sous OpenWRT, et je compte le remplacer sous peu par un TP-Link WR1043ND.\" | 
| Netgear DM111P | Non | Partiel | Oui | Grunt | Ne fait pas routeur, juste modem. Un seul port Ethernet. Supporte les modes RFC2684, PPPoE et PPPoA. Ne gère pas IPv6 sauf en RFC2864 (session PPP gérée par l'ordinateur). A fini par merdouiller (déconnexions aléatoires) après 3 ans de bons et loyaux services.| 
| ::: | Non | Partiel | Oui | Julienth37 | Gère IPv6 uniquement en RFC2864 -\> session PPP gérée depuis un équipement derrière le modem, dans mon cas un routeur Calexium CXM Sense dual core 3 ports gigabytes sous pfSense.| 
| Alcatel One Touch Home | Non | Partiel | Oui | Grunt | Ne fait pas routeur sauf en mode \"pro\". Un seul port Ethernet en 10BaseT-HD. Gère le bridge, donc IPv6 possible avec PPP géré par ordinateur. Aucun problème en le configurant d'après ce tutoriel: <http://www.ozcableguy.com/alcatel.asp#atm> Interface Web bien pénible et confusionnante.| 
| SpeedTouch USB | Non | ? | Oui | ? | Driver Linux \speedtch\) derrière une Fonera 2.0g sous OpenWRT (avec un hub USB alimenté entre les deux). De temps en temps le modem se prend un coup de chaud et il faut le débrancher (ou rebooter la Fonera).| 
| [D-Link DSL-320B](d-link_dsl-320b.md)| Non | Oui | Oui | Kycka | Utilisé en mode bridge, la passerelle exécutant pppd derrière pour s'authentifier. Il semblerai que la liaison soit du PPtP entre la passerelle et le modem. Autrement, modem de très bonne facture il me semble. Plutôt stable, après plus d'un an branché en quasi permanence, rien à en redire si ce n'est que je ne peut pas faire de PPPoA. l'IPv6 fonctionne jusqu'ici sans le moindre soucis (mais ce n'est pas le modem qui la gère, mode bridge oblige. | 
| ::: | Non | Oui | Oui | Yonux | Connexion stable. Synchronise au plus haut de ce que me permets la distance de cuivre me séparant du DSLAM (14 et quelques Mbit). Mode bridge. Par contre, pas moyen de mettre un firmware GNU dessus et une diode qui clignote constamment en façade avant. | 
|::: | Non | Oui | Oui | Zergy | Peu de déconnexion, options de configuration convenables. Encore trouvable en neuf pour pas cher. Utilisé en mode bridge. | 
| ::: | Non | Oui | Oui | Fabien Sirjean | Fonctionne très bien en continu depuis deux ans. Utilisé en mode bridge à la maison, le routage est fait par un vieux laptop sous debian. Fun fact : le modem parle telnet, il est \*\*très\*\* configurable (bon, faut avoir un Master en ADSL pour comprendre les paramètres, moi j'ai laissé le défaut :p). | 
| ::: | Non | Oui | Oui | gobelin | Connexion stable. Utilisé en mode bridge, la passerelle exécutant pppd derrière pour s'authentifier. | 
| ::: | Non | ? | Bof | Antoine V. | Utilisé en mode PPP la connexion est très instable et décroche régulièrement. Modem pas cher avec quelques capacités de routage très limitées, il vaut mieux l'utiliser en bridge et lui adjoindre un routeur derrière. | 
| Thompson Speedtouch 510 v5 ou v6 | Non | ? | ? | ex-red | Très stable, aucun problème à signaler. Attention certaines versions qu'on trouve d'occasion sont d'origine Orange et ont un firmware buggé qui fait freezer le modem périodiquement (2-3 fois par jour en usage intensif). | 
| DrayTek Vigor120 | Non | Oui | Oui | [Yannick](yannick@palanque.name.md) | (peut faire aussi routeur) Acheté spécialement pour sa fonction de modem seul avec pont PPPoA vers PPPoE car il était impossible d'utiliser PPPoE avec une ligne dégroupée (par SFR). Configuration obscure pour un néophyte (au moins). Fonction de routage non essayée. Fonctionne très bien. | 
| ::: | Non | Oui | Oui | [Antoine M.](antoine@inaps.org.md) |IPv6 non fonctionnelle en utilisant le bridge PPPoA \<-\> PPPoE (les messages IPV6CP arrivent du LNS mais celui-ci ne reçoit jamais les notres). Fonctionne parfaitement en full PPPoE. | 
| ::: | Non | Oui | Oui | ex-red | Fonctionne sans problème depuis plusieurs années en Bridge, PPPoE. Par défaut le VPI/VCI était configuré en 0/35, il a donc fallu le modifier pour 8/35. | 
| Linksys AM200 | Non | ? | Oui | Benoar | Solide, très peu de déconnexions, beaucoup d'options de config. N'est plus trouvable en neuf, mais se trouve d'occasion facilement et pour pas cher. \[Update\] En fait, il est très bien en bridge, mais en routeur, j'ai constaté qu'il fait de la merde avec le MTU. | 
| ::: | Non | ? | Oui | Alexis B. | Pas de problèmes à signaler. Fonctionne en tant que routeur ou en pont piloté par ppp (par un soekris).| 
| OvisLink EVO-DSL11 | Non | Oui | Oui | Nicolas G. | En mode pont (par défaut), fonctionne sans problème. Un mode routeur est disponible mais non testé. Avec un WRT54G sous OpenWRT, l'IPv6 fonctionne. | 
| TP-link TD-8816 | Non | Oui | Oui | Luc M. | Il a été difficile de le faire fonctionner en mode bridge. La configuration qui marche au final est : Rip Direction = None, Version = Rip-2B, Multicast = IGMP-v2, Encapsulation = RFC 1483, Multiplexing = LLC-based, Bridge mode = Yes, IP address assignment type = Dynamic, SUA = Yes, Multicast = None, Default Route node = Yes (je ne sais pas si tout est utile dans ce qui précède). C'est un routeur Buffalo WZR-HP-G300NH Wireless N sous OpenWRT qui réalise la connexion en PPPoE lorsque le modem est en mode bridge. L'interface web a tendance à rester bloquée et il faut régulièrement forcer le rechargement des pages.| 
| Zyxel AMG1001 | Non | ? | Oui | Amanda H. | Aucun soucis en mode bridge piloté par un OpenWRT, je n'ai pas testé les autres modes. Le modem est relativement simple mais semble contenir pas mal d'options à explorer en non-bridge. IPv6 non testé à ce jour. |


### Modems ou modem-routeurs à problème

| Nom | IPv6 | A-H | Utilisateur connu | Commentaires | 
| ------ | ------ | ------ | ------ | ------ |
| Alcatel Speed Touch Home (Ethernet) | ? | ? | Stun | Semble bien fonctionner mais est incapable de supporter plus d'une dizaine de connexions simultanées, auquel cas les paquets sont simplement perdus. (stun) (il est toutefois possible que cela provienne d'une mauvaise configuration, du à la complexité de celle ci.) | 
| Bewan 700G | ? | ? | Hélène G-B. | La connexion devient inutilisable lorsque deux PC sont connectés. La diode du Wifi ne s'allume pas systématiquement au démarrage, il me faut parfois 3-4 tentatives. | 
| Bewan 900N | ? | ? | Fab | Fonctionne à peu près en PPPoE, mais pas en PPPoA et vraiment pas en bridge. De plus, le firmware est buggé, la ligne tombe dès qu'un des ports ethernet passe de l'état up à down. | 
| Hitachi Tecom AH 4222 | ? | ? | François | (modem fourni autrefois par club-internet puis sfr) Insère la chaîne de caractères \"NEUF\" en préfixe du login au moment de la connexion ppp ; ce comportement semble impossible à empêcher sans changer le firmware | 
| Linksys ADSL MUE2 | ? | ? | Lionel G. | Utilisé avec un firmware GNU RouterTech. Perte régulière de la synchro malgré un boost de la tension et de l'intensité de l'alimentation. | 
| Netgear DG834G  | ? | ? | Benoar | Utilisé avec un firmware OpenWRT. N'a pas marché sur une ligne non-dégroupée alors que ça marchait sur une dégroupée précédemment. Et de manière générale, éviter Netgear [Yannick]([yannick@palanque.name.md) signale cependant que ce modem-routeur fonctionne bien chez lui avec une ligne dégroupée et qu'il ne voit pas de raison particulière de le déconseiller plus qu'un quelconque autre modem-routeur bas de gamme qui ne serait pas mentionné ici, car d'autres pourraient fonctionner encore plus mal ; à condition alors qu'on veuille l'utiliser avec une lignée dégroupée (révision concernée : v5).\] | 
| ::: | non | non | hamster | Dans la partie DHCP, la réservation d'adresse IP privée a une adresse MAC ne fonctionne pas. Il n'est pas possible d'avoir toujours la même IP sur la même machine de façon fiable (fonctionnement en PAT), ce qui rend la redirection de port impossible. C'est rhedibitoire pour faire de l'auto hebergement. | 
| SMC Barricade. | non? | ? | Andrée Ascoët (technicien: [son fils](http://www.fdn.fr/~sascoet)) | Configuration [laborieuse](http://www.fdn.fr/~sascoet/monalbum/albums/bretagne/douarnenez/sa-23nov2010jamesetmongobookdouarnenezkerguimigouanciennechambresousmandrake10etdebianetch/sa-23nov2010jamesetmongobookdouarnenezkerguimigouanciennechambresousmandrake10etdebianetch.jpg),[crispante](http://www.fdn.fr/~sascoet/monalbum/albums/bretagne/douarnenez/sa-23nov2010jamesetmongobookdouarnenezkerguimigouanciennechambresousmandrake10etdebianetch/sa-23nov2010jamesmoietmongobookdouarnenezkerguimigouanciennechambresousmandrake10etdebianetchconfigurantrouteur.jpg),[peu intuitive](http://www.fdn.fr/~sascoet/monalbum/albums/bretagne/douarnenez/sa-23nov2010jamesetmongobookdouarnenezkerguimigouanciennechambresousmandrake10etdebianetch/sa-23nov2010jamesmoietmongobookdouarnenezkerguimigouanciennechambresousmandrake10etdebianetchconfigurantrouteursmcbarricadepourri0.jpg). [Indications frontales fantaisistes](http://www.fdn.fr/~sascoet/monalbum/albums/james/sa-2jan2011mamandouarnenezkerguimigouchambreconsultantvivezcrusurjamessousdebiansqueezefraichementinstalleegigamo4224par2376pixels.jpg). [Comportement aléatoire](http://www.fdn.fr/~sascoet/monalbum/albums/james/sa-30dec2010demarragedefreeduc-cdsurjamesdouarnenezkerguimigouchambregigamogobookbdterresd-horus4224par2376pixels.jpg). Bref, [à fuir](http://www.fdn.fr/~sascoet/monalbum/albums/terriens/chattedemesvoisinsdefleury-les-aubraisbustiere/sa-1003201018h10-44chattedesvoisinsfleury-les-aubraisterrassedmc-lx2au1-40sf4_9iso200foc25_2mm72dpi4224par2376.jpg)\...|

### Documentation technique (constructeur)

[Manuels utilisateur et mode d'emploi](/support/doc/manuels/)

### Documentation et tutos divers :

- [Netgear DGN3500](/support/doc/exemples_config/netgear_dgn3500.md)
- [Netgear WNDR3800](/support/doc/exemples_config/netgear_wndr3800.md)
- [TP-Link TL-WR1043ND v2](/support/doc/exemples_config/tl-wr1043nd_v2.md)
- [TPE-NWIFIROUTER2 (aka "ThinkPenguin Wireless N")](/support/doc/exemples_config/tpe-nwifirouter2.md)
- [ZyXEL AMG1001-T10A](/support/doc/exemples_config/zyxel_amg1001.md)

#### Autres modems

* Triway V2 (Comtrend CT-633.md) : décrit sur le [site officiel FDN](http://www.fdn.fr/Configurer-un-modem-Triway-V2.html) [obsolète ?]

### ADSL2+ PCI card :

<http://linitx.com/product/12181> \\\

**Avis de Léa Gris :\*\*

Cette carte va apparaître comme un périphérique ATM et éventuellement
comme périphérique Ethernet si le pilote prend en charge directement
l'encapsulation EthOA. Quoi qu'il en soit, une connexion FDN, qu'elle
soit dégroupée par Nerim(Alias SFR) ou collecte Orange, on utilise ppp
soit OE soit OA. Avec ce type de carte, autant taper directement PPPoA.

Pendant plusieurs années j'avais une carte similaire (Bewan ADSL
PCI-ST) et je pouvais l'utiliser avec toutes les fantaisies de
protocoles sur ATM. Un beau joujou pour geek.

Je ne vois aucune raison pour que ça ne fonctionne pas avec FDN.

**Avis de Benjamin cama :\*\*

Je pense que non, cette carte n'apparaîtra pas comme un périph ATM, car
il est bien précisé qu'elle est interfacée avec un contrôleur ethernet.
En fait, c'est comme un modem classique qui serait interfacé par PCI :
il contient sa propre gestion de l'ATM, t'oblige à faire du PPPoE
(donc MTU réduite) si tu ne veux pas être routé par lui, et ne gère pas
l'IPv6 en natif, sauf si on bridge, donc. Bref, ça a exactement les
même caractéristiques qu'un modem ADSL Ethernet. Et vu le prix et la
disponibilité du bousin, je ne vois pas de raison de le préférer à un
modem Ethernet \"classique\". À moins que le facteur \"intégré dans le
PC\" soit important. Les cartes du type *Bewan ADSL PCI-ST* n'existent
plus aujourd'hui, à ma connaissance. Je n'ai pas vu de carte qu'on
attaque en ATM direct depuis des lustres (je soupçonne que la pourritude
des drivers n'a pas aidé à les populariser). Mais c'est dommage, en
effet, c'est bien pour faire joujou.


## Faut-il dire modem ou adaptateur ?

### Avis de Stéphane (adminsys) :

Les informations transmises sont numériques, informatiques, notamment en
ATM 8-) Il s\'agit donc d\'un adaptateur vers un support physique de
réseau étendu 8-O

### Avis de Léa Gris (support) :

Un modem est un Modulateur, démodulateur. Un modem ADSL utilise par
exemple (entre autre) la modulation de fréquences orthogonales
multiplexées OFDM ou G.DMT.

Peut-être pourrais-tu expliquer en qui l\'expression modem ne convient
pas à désigner un ATU-R et le documenter dans le Wiki pour
l\'instruction de tous ici à FDN.

De mon avis cependant, quelque puisse-être le degré d\'inexactitude à
l\'emploi de « modem » en ADSL, cela reste un dénomination très commune
et comprise par le plus grand nombre.

## Configurer un routeur OpenWRT Chaos Calmer pour support IPV6

La configuration ci-dessous a été obtenue sur un routeur Buffalo
WZR-HP-G300NH pilotant un routeur Dlink DSL-320B en mode bridge. Elle
semble un peu différente des configurations trouvées sur les tutoriels
OpenWRT et n\'est proposée qu\'à titre d\'exemple, certains éléments
sont peut-être inutiles.

Dans le fichier */etc/sysctl.conf*, mettre les deux lignes suivantes :

 ``net.ipv6.conf.default.forwarding=1

 ``net.ipv6.conf.all.forwarding=1

Dans le fichier */etc/config/network*, mettre les trois sections
suivantes (la partie 2001:910:xxxx des différentes adresses correspond à
la plage d\'IP que FDN vous attribue, la partie yyyy peut être choisie
comme bon vous semble au sein de cette plage):
``
 config 'interface' 'lan'
       option 'ifname' 'eth0'
       option 'type' 'bridge'
       option 'proto' 'static'
       option 'netmask' '255.255.255.0'
       option 'ipaddr' '192.168.163.209'
       option 'ip6addr' '2001:910:xxxx:yyyy::1/64'

 config 'interface' 'wan'
       option 'ifname' 'eth1'
       option '_orig_ifname' 'eth1'
       option '_orig_bridge' 'false'
       option 'proto' 'pppoe'
       option 'username' '<votre identifiant de connexion>'
       option 'password' '<votre mot de passe de connexion>'
       option 'ipv6' 'auto'
       option 'ip6prefix' '2001:910:xxxx::/48'
       option 'mtu'  '1452'

 config 'route6'
       option 'interface' 'wan'
       option 'target'    '::/0'
``
La dernière section, *route6* est la plus litigieuse. J\'ai dû
l\'ajouter car sans elle il n\'y avait pas de route par défaut de
l\'IPV6 vers internet : les connexions IPv6 internes se passaient bien,
mais celles vers l\'extérieur ne franchissait pas le routeur. Un moyen
manuel d\'arriver au même résultat est de lancer dans un shell la
commande:

 ip -6 route add default dev pppoe-wan


La commande précédente implique que vous ayez le paquet *ip* installé,
ce que vous pouvez faire avec un *opkg install ip* si nécessaire.

Vous pouvez vérifier que les routes IPv6 sont correctes avec la commande

 ip -6 route


Voud devriez obtenir un résultat avec au moins une ligne indiquant \'\'
default via fe80::1\'\'.

## Supervision de modems ADSL

Si la stabilité de votre connexion internet laisse parfois à désirer,
vous pouvez éprouver le besoin de connaître, même en votre absence,
quand il y a déconnexion, quand elle reprend, et dans quelles conditions
de débit et rapport signal sur bruit.

Les modems grand public sont trop primitifs pour enregistrer
l\'historique de leur état. Un autre moyen de fabriquer cet historique
est d\'interroger le statut du modem à intervalle régulier, ce qui est
toujours possible au moins par leur interface de configuration Web.

Dans la suite, je vais traiter le cas de deux familles de modems où
l\'on peut faire bien mieux pour les superviser en utilisant une
interface telnet.

## Modems ADSL à base de chipset TrendChip

Le modem Olitex SX202 appartient à cette catégorie, il fait partie de
[ceux conseillés](#modem-routeurs_des_abonnes.md).

Ces modems fonctionnent en général avec des firmwares TrendChip, qui
comprennent le serveur web pour configurer le modem, et qui sont adaptés
pour le fournisseur final, parfois juste au niveau de la charte
graphique. Ces firmwares autorisent des rapports SNMP et un contrôle
plus fin via telnet.

Les principales différences de l\'interface telnet selon le modèle
précis de chipset/firmware utilisé se situent dans la richesse des
commandes disponibles. En particulier, si vous avez un routeur Wifi dans
votre modem, vous aurez des commandes pour traiter cette partie. Mais
dans la suite, nous allons nous contenter de ce qui est commun à tous
les modèles, à savoir la supervision de la connexion ADSL.

Vous vous connectez au modem avec un client telnet sur la même adresse
que celle de l\'interface Web, et le mot de passe administrateur de
cette interface Web (il n\'y a pas de compte utilisateur demandé).

Voici un lien sur la documentation de cette interface telnet, c\'est
succinct et peu explicite:
[BIPAC-5100\_CLI\_Reference\_Manual](http://au.billion.com/downloads/BIPAC-5100_CLI_Reference_Manual.pdf)

On peut en déduire quand même que les commandes suivantes : wan adsl p
wan adsl c wan adsl l n wan adsl l f vont permettre d\'obtenir
l\'uptime ADSL, les débits montants et descendants, les atténuations et
les marges de signal sur bruit associées. C\'est ensuite un petit
exercice d\'écrire un programme qui va régulièrement interroger le modem
par telnet, analyser les réponses et accumuler les résultats. Des
sorties graphiques telles que ci-après peuvent alors être produites :

<img src="images/sx202-up-week.png">

<img src="images/sx202-speed-week.png">

<img src="images/sx202-att-week.png">

<img src="images/sx202-snr-week.png">

Les sources de l\'application de collecte et d\'un script CGI pour une
visualisation Web sont [disponibles ici](images/modem-0.1.tgz).

## Modems ADSL à base de chipset Broadcom

Le modem D-Link DSL-320B appartient à cette catégorie, il fait partie de
[ceux conseillés](#modems_seuls_des_abonnes.md). On notera que
le firmware de ce modem est à base Linux+busybox. La commande qui
fournit des informations équivalentes à celles obtenues pour les
TrendChip est: adsl info --stats

Un lien vers une description des commandes disponibles :
<http://www.usr.com/support/9105/9105-files/cli_reference.html>. Il y a
apparemment de quoi bidouiller assez profondément vos paramètres ADSL
(pas testé, à vos risques et périls).

On se connecte avec le user admin et le mot de passe de l\'interface
Web. Après la validation du mot de passe, il faut taper un second retour
chariot pour obtenir une invite.

## Autres outils

Les allemands semblent assez friands de supervision ADSL et ont donc
développé des outils pour ce faire, qui analysent même la répartition de
la bande passante selon la fréquence, voir par exemple :
<http://www.spida.net/projects/software/dmt-ux/index.en.html>

L\'utilité est limitée si vous sortez des clous de ce qui est vendu sur
le marché ADSL allemand.
