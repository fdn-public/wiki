###### Réponses aux questions fréquemment posées sur Usenet

 * Archive-name: usenet-faq.fr
 * Original-translate: blaise(chez)ptilo(point)fdn(point)org (Blaise Thauvin)
 * Comment: largement inspiré de l'article *Answers to Frequently Asked Questions about Usenet* de Jerry Schwarz posté mensuellement en groupes news.announce.newusers et news.answers.
 * Last-change: Thu Nov 13 10:59:43 MET 1997 (Blaise Thauvin)

Ce document répond aux questions les plus fréquemment posées sur Usenet.
Elles proviennent généralement de nouveaux utilisateurs et ont pour
conséquence de submerger les groupes. Si les réponses à ces questions ne
vous conviennent pas, merci d\'en informer l\'auteur.

Il est a noter que certains groupes ont leur propre FAQ, (Frequently
Asked Questions and Answers). Vous devriez toujours commencer par lire
un groupe pendant quelques temps avant d\'y poster quelque chose car la
réponse s\'y trouve peut-être déjà.

Ces listes de réponses classiques sont appelées FAQ. Si vous êtes un
nouvel utilisateur de Usenet et si vous ne trouvez pas la réponse à vos
questions dans ce document, vous pouvez essayer de les poser dans
news.newusers.questions ou fr.news.divers. Vous pouvez aussi lire
d\'autres FAQ, celles du ou des groupes correspondants à votre question
par exemple. Elles sont presques toutes disponibles dans le groupe
news.answers.

Ce texte est une traduction libre de l\'article de Jerry Schwarz repris
par Gene Spafford. De nombreux passages du texte original ont été
re-écrits, d\'autres réduits. Volontairement, certaines expressions et
abréviations en anglais n\'ont pas été traduites.

Merci d\'envoyer vos remarques et/ou vos commentaires par e-mail à
\<archive-admin\@fdn.org\>.

##### Q01 Que signifie Unix ?

Ce n\'est pas une un sigle, mais un nom dérivé de \"Multics\", un
système d\'exploitation qui était en développement peu de temps avant
que Unix ne créé. La création du nom serait dûe à Brian Kernighan.

##### Q02 Quelle est l\'origine du mot \"foo\"?

La légende la plus répandue est que le mot vient de \"fubar\", lui même
sigle de \"fouled up beyond all recognition\", qui serait une expression
militaire. Plusieurs formes existent, \"fouled\" pouvant être remplacé
par un mot plus fort (Fu..ed, NdT). \"foo\" et \"bar\" ont la même
origine.

##### Q03 La société foo est-elle connectée à Internet ?

Cette question devrait plutôt être posée dans news.config. Vous pouvez
essayer de téléphoner à la société foo. Si vous n\'y connaissez
personne, demandez le service informatique. En dernier recours, essayez
le service d\'annuaire du réseau en faisant un telnet sur
ds.internic.net et utilisez la commande whois.Voyez aussi la question 7.

##### Q04 Que signifie \"rc\" à la fin de certains fichiers comme .newsrc ?

C\'est l\'abréviation de \"run commands\". On trouve ce suffixe au nom
du fichier de commandes utilisé au démarrage d\'un logiciel. Cela vient
du répertoire /etc/rc pour le démarrage en mode multi-utilisateurs de
Unix.

##### Q05 Que signifie :-) ?

C\'est un \"smiley\", petit bonhomme souriant stylisé indiquant au
lecteur que le texte doit être compris au second degré. Sa contrepartie
existe: :-( est un petit bonhomme triste. Pour mieux comprendre, penchez
la tête et vous verrez les yeux, le nez et la bouche.

##### Q06 Comment décrypter les blagues dans rec.humor ?

Elles sont parfois (rarement) codées en rot13. Le rot13 est un simple
décalage alphabetique, \"a\" devient \"m\", \"b\" devient \"n\" etc\...
Certains logiciels de news incluent le decodeur.

##### Q07 Quelqu\'un connaît-il Gérard Dupont ?

Lorsque vous êtes à la recherche de quelqu\'un, le réseau est rarement
le meilleur moyen de le trouver. Le plus efficace est souvent de
téléphoner à sa société, même sur un site différent. La plupart des
sociétés ont des annuaires globaux. En désespoir de cause, ne pas poster
dans misc.misc ou misc.wanted mais plutôt dans soc.net-people.

===== Q08 Démonstration de 1 = 0 =====

On a presque tous vu cela au lycée. Ces démonstrations sont en général
basées sur une division par 0, confondre les racines positive et
négative d\'un nombre, ou autre erreur.

##### Q09 rec.games.\*: Où puis-je trouver les sources de Empire ou Rogue ?

Il n\'est pas possible de se procurer les sources de Rogue, les auteurs
ayant choisi de ne pas les diffuser. Il y a par contre de nombreux
clones disponibles dans comp.sources.games.

Vous pouvez obtenir les sources d\'une version de Empire en envoyant une
bande, une enveloppe timbrée à votre adresse \*et\* une photocopie de
votre licence des sources Unix. Pour plus d\'infos, écrire à
\<mcnc!rti-sel!polyof!john\> ou appeler le 1 516 454 5191 de 9h00 a
21h00, fuseau Est des EU. Les sites ftp peuvent essayer ftp.ms.uky.edu
dans pub/games/empire

##### Q10 comp.unix.questions: Comment supprimer un fichier contenant un caractère non ASCII en tête de nom ?

Vous pouvez essayer de jouer avec les caractères joker (\* et ?). Mais
bien souvent, ca ne fonctionne pas. Ensuite, vous pouvez utiliser rm -r
et rm -i . enfin, vous pouvez utiliser \"find\" et les numéros
d\'i-nodes.

Certains Emacs permettent d\'éditer directement le fichier directory et
donc de changer le nom du fichier.

Pour effacer le fichier - (le signe moins), tapez simplement rm ./-

##### Q11 comp.unix.internals: Il y a un bug dans tel ou tel logiciel standard Unix\.....

Il y a des problèmes dans la façon dont unix gère le suid. Bien que ceci
soit sujet à évolution, pour le moment, il faut vous contenter de ce qui
existe. Dans le cas de bugs plus classiques, renseignez vous toujours
auprès de votre responsable système avant de communiquer sur Usenet.
Votre bug est peut être déjà connu, ou plus probablement, spécifique à
votre site et à sa configuration.

##### Q12 Sujets brûlants: par exemple, dans soc.women, Que pensez vous de l\'avortement ?

Bien que le sujet paraisse adapté au groupe, il en resulte le plus
souvent plus de colère que de débat ou de progres. Une telle discussion
a sa place dans talk.abortion. Si l\'administrateur de votre site a
choisi de ne pas recevoir ce groupe, vous devez le respecter et vous
astreindre à ne pas aborder le sujet. De même, les questions
d\'éducation religieuse doivent être limitées à talk.religion et ne pas
aller dans misc.kids. Les viols dans talk.rape et non dans soc.singles
ou soc.women. Le sionisme, le racisme, le judaïsme, le créationisme\...
doivent être abordés avec la même prudence. D\'une facon générale, les
groupes Usenet, malgré leurs noms très larges, ne peuvent recevoir de
discussions sur tous les sujets. Acceptez le et postez dans les groupes
appropriés.

===== Q13 soc.singles (singles = celibataires): Que veulent dire MOTOS,
MOTSS, MOTAS ? =====

MOTOS: Member of the oposite sex

MOTSS: Member of the same sex

MOTAS: Member of the appropriate sex

SO: Significant other

LJBF: Let\'s just be friends (que l\'on entend surtout lorsqu\'on le
désire le moins)

##### Q14 soc.singles et ailleurs : Que veux dire HASA ?

HASA vient de Heathen and Atheistic SCUM Alliance; the Hedonistic
Asti-Spumante Alliance, Heroes Against Spaghetti Altering, the Society
for Creative Atheism (SCATHE), SASA, SALSA, PASTA, et beaucoup d\'autres
ont suivi.

HASA a commencé dans ce talk.religion.misc et apparaît aussi dans
soc.singles, talk.bizarre, etc\... parce que leur population est la
même.

##### Q15 sci.space.shuttle: Ce groupe ne devrait il pas être fusionné avec sci.space?

Non, l\'un est pour l\'information en direct et l\'autre un forum de
discussion.

##### Q16 Comment utilise-t-on le champ distributions ?

Le champ distribution permet de limiter la propagation de votre article.
Il est par exemple inutile pour une petite annonce de voiture
d\'occasion de faire lire votre message à un habitant du continent
voisin. La distribution par défaut est \"world\", la distribution \"fr\"
limite à la francophonie et \"local\", si elle existe, à votre
organisation. Ceci dépendant de votre administrateur système, il est le
plus a même de vous renseigner sur les distributions reconnues sur son
site.

##### Q17 Pourquoi certaines personnes mettent-elles des lignes spéciales (souvent humoristiques) en tête de leur article ?

Les premières versions des logiciels de news avaient parfois un bug qui
causait la perte des premiers caractères des articles si ceux ci
commençaient par un espace. Pour y remédier, les utilisateurs ont pris
l\'habitude de toujours commencer leur article par un caractère, puis
une phrase.

Cette phrase est maintenant une habitude tenace et sans utilité.

##### Q18 Quelle est l\'adresse de la société \"foo\" ?

Le réseau n\'est pas un service de renseignements gratuit même si
certains le voient ainsi. Plutot que d\'afficher votre flemme en public,
passez quelques minutes à utiliser un minitel, les pages jaunes de
l\'annuaire ou votre téléphone. c\'est souvent plus rapide et plus
efficace.

##### Q19 Quelle est l\'origine du mot \"grep\" ?

L\'editeur historique de Unix: Ed avait une commande g/re/p pour la
recherche globale d\'expression (re:regular expression) et leur
affichage (p:print) sur le terminal. Cette commande était si utilisée
qu\'elle est devenue indépendante sous la forme du grep. D\'aprés Dennis
Ritchie, ceci est la véritable origine du grep.

##### Q20 Comment écrire à un utilisateur UUCP de Bitnet, de Internet à Bitnet, Janet, etc\... ?

Il y a tellement de réseaux et de protocoles qu\'il faudrait un livre
pour tout décrire. Heureusement, il en existe, en voici quelques-uns :

   A Directory of Electronic Mail Addressing & Networks" par Donnalyn Frey and Rick Adams, O'Reilly & Associates, Inc, 2nd edition 1990.
   "The Matrix: Computer Networks and Conferencing Systems Worldwide" by John Quarterman, Digital Press, 1990.

Un autre ouvrage de référence qui peut tenir compagnie aux deux premiers
sur votre étagère \"The User\'s Directory of Computer Networks\" edited
by Tracy LaQuey, Digital Press, 1990.

===== Q21 Est-ce qu-un état des EU a un jour décrété que PI=3 ? =====

L\'histoire se passa aux Etats-Unis, dans l\'Indiana, le 18 janvier
1897. Le texte proposé par un mathématicien excentrique disait \"le
ratio du diamètre à la circonférence, soit de cinq quarts à quatre\",
soit 3,2. Ce décret fut voté une première fois, puis annulé au cours
d\'une seconde assemblée. Pour plus de details, se référer à D.
Singmaster dans \"The Mathematical Intelligencer\" v7 \#2, pp 69-72.

##### Q22 Où trouver un logiciel de courrier intelligent, tenant compte des cartes postées dans comp.mail.maps ?

Les archives du groupe comp.sources.unix sont là pour ca ! Si vous ne
trouvez pas ce que vous cherchez, il est possible d\'écrire au
moderateur du groupe. Les adresses des archives sont postées
régulièrement dans comp.sources.unix et dans comp.sources.d

##### Q23 Que signifie \"food for the NSA line-eater\" ?

NSA: National Security Agency. La légende veut que cette organisation,
ainsi que d\'autres services de renseignement, scrutent les articles à
la recherches d\'informations. Dans l\'espoir de ruiner leurs efforts,
certains ajoutent un dernier paragraphe à leurs articles destiné à ces
agences. Il en resulte les longues signatures parfois humoristiques,
parfois douteusement politiques qui encombrent certains articles.
Conclusion : pour la politique, il y a des groupes spécifiques, pour le
reste, plus c\'est court, mieux c\'est.

##### Q24 Quelqu\'un connait-il le brochage, schéma, interupteurs,.. pour la carte X ?

La plupart du temps, ce genre de question n\'ont pas de raison d\'être
postée, sauf si le fabricant n\'existe plus. Il est en général bien plus
rapide de se renseigner directement chez son fournisseur.

##### Q25 Qu\'est ce que l\'\"anonymous ftp\" (FTP anonyme)?

FTP: File Transfer Protocol. Il s\'agit d\'un protocole de transfert de
fichier par réseau (aussi bien local que via l\'Internet). Souvent,
c\'est aussi le nom du programme qui permet de l\'utiliser (sur Unix
principalement). FTP réclame un login et un mot de passe lorsque l\'on
se connecte. Beaucoup de sites admettent des connections anonymes. Le
login est alors \"Anonymous\" (\"ftp\" peut souvent servir de synonyme
court). Par convention, le mot de passe est libre, mais on doit s\'en
servir pour s\'identifier. L\'adresse e-mail est le meilleur moyen.

N\'oubliez pas que certains sites préfèrent que les connections soient
établies en dehors des heures de travail. Tenir compte de l\'éventuel
décalage horaire. Enfin, les sites principaux ont des mirroirs. Pour des
raisons de coût, et souvent pour gagner de la vitesse, il est préférable
de contacter un site proche de soi plutôt que de traverser inutilement
l\'Atlantique.

##### Q26 Qui est UUNET ?

UUNET est une organisation commerciale fournissant du mail et des news
ainsi que des archives à prix réduit. Pour plus d\'informations, écrire
a info\@uunet.uu.net

##### Q27 N\'y a t il pas un defaut dans le mécanisme des news: si je poste un message dans deux groupes, l\'un modéré, l\'autre non, il est posté uniquement au moderateur et pas dans le groupe libre.

Non ! Le sujet fait l\'objet d\'un débat assez régulieremnt, mais le
système a été concu ainsi. Si le texte etait posté tout de suite à
l\'ensemble des groupes, il faudrait d\'abord enlever le nom du
modérateur et perdre certaines fonctions. Il est de la responsabilité du
modérateur de poster l\'article dans les groupes non modérés. Si vous
voulez que votre article paraisse tout de suite, il faut le poster deux
fois : une fois dans le groupe modéré et une fois dans les groupes non
modérés. N\'oubliez pas que les diffusions dans de nombreux groupes sont
déconseillées.

##### Q28 Que veulent dire FYI, IMHO, ASAP et SASE ?

FYI: For Your Information

IMHO: In My Humble (Honest) Opinion

ASAP: As Soon As Possible

SASE: Self Addressed Stamped Enveloppe

##### Q29 Quelqu\'un pourrait-il reposter \[un grand bout de programme\]

Malheureusement, il arrive qu\'il y ait des pertes dans la propagation
des news. A moins que vous ne vous fassiez l\'écho d\'un grand nombre de
personnes ayant eu le même problème, il faut eviter de demander à des
milliers de sites de dépenser une fortune en ressources de communication
pour diffuser une deuxieme fois un imposant fichier.

N\'oubliez pas que certains utilisateurs paient de leur poche et en
fonction du volume leur lien sur le réseau et n\'ont sans doute pas très
envie de payer deux fois un fichier de quelques Mo qui leur est inutile.
Essayez plutôt de joindre directement l\'auteur par le réseau ou même
par courrier terrestre.

##### Q30 Comment contacter directement le moderateur d\'une mailing-list ?

En général, il y a deux adresse pour chaque liste:

   liste@foo.fr

   liste-request@foo.fr

La première permet d\'écrire à l\'ensemble de la liste tandis que la
seconde permet d\'ecrire à l\'administrateur seulement. Il est de très
mauvais goût, et même parfois inutile, d\'écrire à l\'ensemble de la
liste pour des questions administratives, l\'administrateur n\'étant pas
nécessairement un lecteur.

##### Q31 Que signifient BTW, WRT, RTFM ?

BTW: By The Way

WRT: With Respect To

RTFM: Read The F\*ing Manual (nous vous laissons le choix du mot
central). Cette abreviation est utilisée pour rappeler au lecteur que la
question posée est désespérement bête et que sa réponse se trouve dans
sa documentation (ou celle dont il devrait disposer s\'il n\'avait pas
copié/volé le logiciel). on lui recommande donc de s\'y référer avant de
faire perdre du temps aux autres. Les FAQ, dont celle-ci, font partie de
ces documentations qu\'il est bon de lire avant d\'écrire.

##### Q32 Y a t il des raisons de ne pas reposter un message que j\'ai recu ?

Au dela de la simple politesse qui consiste à demander l\'autorisation à
son auteur, il n\'existe pas encore de jurisprudence concernant les
medias électroniques. Sur papier, l\'auteur d\'une lettre en conserve la
propriété intellectuelle même si aucune marque de copyright n\'apparait.
Le même raisonnement pourrait s\'appliquer à Usenet.

##### Q33 Qu\'est ce qu\'un FQDN ?

FQDN: Fully Qualified Domain Name. C\'est l\'adresse complète d\'un site
internet. Par exemple, la machine uiucuxc a pour adresse
uxc.cso.uiuc.edu sur l\'internet alors que le nom court peut être
utilisé localement.

##### Q34 Comment prononcer \"char\" en C, IOCTL dans Unix, le caractère \# ?

Les opinions varient. Choisissez une prononciation proche de celle
utilisée par vos collègues. De toutes facons, c\'est de celle là dont
vous avez besoin pour communiquer avec eux.

##### Q35 Comment prononcer le mot TEX ?

D\'aprés le créateur de TeX, le X ne se prononce pas comme un x mais
comme un chi Grec. Plus précisement, cela ressemble au \"ch\" des mots
écossais (comme Loch), au \"ach\" allemand, au \"j\" espagnol, ou encore
au \"kh\" russe. Lorsqu\'il est prononcé correctement devant
l\'ordinateur, l\'écran doit devenir humide. \[The TeXbook, 1986,
Addison Wesley, page 1\]

##### Q36 Quelle est la dernière année du 20eme siecle ?

Notre système de numérotation a été créé avant le zéro. Il n\'y a donc
pas d\'année 0 mais on passe directement de 1 avant JC à 1 après JC. Le
premier siècle va donc de 1 a 100, et le 20eme de 1901 a 2000.
Cependant, un siècle représente un intervale de cent ans, quel qu\'il
soit. Vous pouvez donc tout à fait célébrer la fin d\'un siècle le 31
decembre 1999. Simplement, ce ne sera pas la fin du 20 siècle apres JC.

##### Q37 J\'ai entendu parler de cette histoire d\'un enfant atteint d\'une maladie incurrable et voulant recevoir des cartes postales. A qui dois-je ecrire ?

Laissez tomber ! Cette histoire commence à dater et a polué le réseau
depuis suffisamment longtemps. Elle revient regulierement sur la scène
bien que toutes les personnes concernées aient demandé avec insistance
qu\'elle disparaisse.

L\'enfant en question, Craig Shergold, avait effectivement voulu détenir
le record du monde du nombre de cartes postales. Il y en a tellement
aujourd\'hui que personne ne sait quoi en faire. Le Livre des records
n\'accepte plus de nouveaux candidats pour ce record.

Né en 1979, Greg a recu 33 millions de cartes jusqu\'en mai 1991, date à
laquelle sa mère a supplié que cela s\'arrête. Une operation
chirurgicale de 5 heures par le docteur Neal Kassel, à l\'universite de
Virginie, Charlottesville en mars 1991, a beaucoup amelioré son état.

Si vous voulez faire quelque chose d\'utile, donnez l\'équivalent de
cette carte et de son timbre à l\'association charitative de votre
choix. Beaucoup d\'enfants meurent chaque jour et auraient bien besoin
d\'autre chose que d\'une carte postale.

##### Q38 Je viens d\'apprendre que le FCC veut instaurer une taxe sur les modems ! Ou poster un message pour prevenir la population et empêcher cela ?

Pauvres americains chez qui cette rumeur revient regulierement sur la
scene ! Nous avons la chance en France d\'habiter un pays ou nul
n\'aurait l\'idee saugrenue de taxer les derniers gadgets à la mode
(magnétoscopes, modems\...)

Plus sérieusement, se référer à la question 37 et d\'une facon générale,
avant de s\'énerver et de partir en croisade, vérifier l\'origine de
l\'information pour s\'assurer qu\'elle est ni fausse, ni vieille de
plusieurs années.

##### Q39 Y a t il un système Unix public près de chez moi ? Comment me procurer des news et du mail ?

Phil Eschallier poste regulierement une liste de sites Unix ouverts au
public dans comp.misc et alt.bbs sous le nom Nixpub. De plus, d\'autres
sites d\'acces libres mais non unix sont citès dans alt.bbs.list, sous
le nom NetPub listing. Malheureusement, ces listes contiennent peu de
sites hors des Etats-Unis. Pour les sites francais, la FAQ du groupe
soc.culture.french donne des informations utiles sur le moyen de se
connecter en France.

##### Q40 rec.pets: mon animal presente les symptomes suivants, est-ce grave ? sci.med: même question pour un humain

Cela pourrait l\'être. Usenet réunit de grandes compétences, mais rien
ne remplace l\'avis d\'un véritable expert agissant en présence du
malade. Si le problème est serieux, il est important de s\'en occuper
rapidement.

##### Q41 J\'ai une exellente idée pour me faire de l\'argent. Mais une chaîne de courrier électronique ne serait-elle pas une meilleure idée ?

Pour dire les choses simplement : N\'y pensez même pas! Utiliser le
réseau pour faire de l\'argent par un système de chaîne est une très
mauvaise idée. D\'abord, ce n\'est pas une utilisation appropriée des
ressources et cela consomme beaucoup de bande passante. Ensuite, les
réactions très negatives du réseau vont en consommer encore plus.
Principalement dirigées vers vous, ces réactions vont faire exploser
votre boîte aux lettres.

Enfin, dans plusieurs pays, ces pratiques sont illégales et il y a déjà
eu des cas de personnes lourdement condannées pour de telles activités.

##### Q42 Où puis-je trouver les archives des messages postés sur Usenet ?

La plupart des groupes ne sont pas archivés. En effet, le volume est
tellement important et dans certains, le rapport signal/bruit si faible
que l\'archivage n\'aurait pas un grand intérêt, quand bien même il
serait possible. La perception du signal et du bruit variant beaucoup
d\'une personne a l\'autre, il est possible que vous trouviez quelqu\'un
qui stocke les informations que vous recherchez. Les lecteurs habituels
du groupe sont de bons candidats.

Pour les groupes archives, la methode dépend du contenu: Les groupes
\"sources\" et \"binaries\" sont habituellement archives à plusieur
endroits. Pour plus d\'informations, voir la FAQ appellee \"How to find
sources\" dans le groupe comp.sources.wanted.

Pour tout ce qui n\'est pas sources, \"Archie\" peut être une bonne
solution. Se référer à la FAQ mentionne ci dessus, ou au manuel Archie
simplifie et traduit en francais disponible sur ftp.fdn.org.

Pour tous les autres, se référer en premier à la FAQ du groupe si elle
existe, ou sinon, poster directement dans le groupe.

##### Q43 Peut-on envoyer des messages Usenet à partir du courrier (e-mail) ?

Il y a effectivement quelques sites qui proposent une passrelle complete
du courrier vers les forums.

L\'un d\'entre eux est decwrl.dec.com. Pour l\'utiliser, envoyer un
courrier a nom.du.groupe.usenet\@decwrl.dec.com. Par exemple, pour
poster dans soc.culture.french, ecrire a
soc.culture.french.usenet\@decwrl.dec.com. avec comme message le contenu
de votre article.

Les passerelles de ce type sont particulierement surchargees. Essayez de
ne pas les utiliser si vous pouvez faire autrement.

##### Q44 Peut on lire les groupes Usenet par le courrier ?

En général, la réponse est non, mais quelques newsgroups correspondent
aussi à une \"mailing-list\" à laquelle il est possible de s\'abonner.
Pour en connaître la liste, se référer au document \"List of active
newsgroups\" posté dans news.announce.newusers.

Si vous connaissez un adminisatrateur de site, il peut aussi configurer
son site pour vous renvoyer par courrier l\'ensemble d\'un groupe.

En général, ils n\'aiment pas le faire car celà ajoute inutilement
beaucoup de trafic. Ne postez donc pas de message demandant: \"OHE, est
ce que quelqu\'un voudrait me renvoyer tout\....\"

##### Q45 Comment faire pour inclure automatiquement une signature dans mes articles ?

La réponse à cette question est probablement contenue dans le mode
d\'emploi de votre logiciel de news. Si vous travaillez sur une machine
Unix, vous pouvez probablement ajouter une signature à vos messages en
créant un fichier \".signature\" dans votre répertoire racine. Deux
choses importantes à noter:

   Certains logiciels limitent la taille de votre signature, souvent a quatre lignes. Ceci n'est pas innocent. Par courtoisie envers vos lecteurs, n'essayez pas de contourner cette limitation. D'une facon générale, gardez votre signature aussi courte que possible.

   Sur certains systèmes, votre signature ne sera inclue que si les permissions sur ce fichier et sur le repertoire racine le permettent. La signature doit être lisible par tous et le répertoire racine exécutable par tous. Pour cela taper :

   chmod a+/-x $HOME

   chmod a+/-r $HOME/.signature

   Attention aux effets secondaires de cette operation si la confidentialite de vos informations est un sujet important.

##### Q46 Je suis sur Bitnet, puis-je me connecter à Usenet ?

Beaucoup de sites Bitnet ont aussi une connexion sur d\'autres resaux.
Quelques-uns peuvent recevoir Usenet par NNTP ou une autre methode. Les
sites IBM VM/CMS connectés uniquement à Bitnet peuvent malgré tout
recevoir Usenet gra\^ce au logiciel NetNews, disponible gratuitement
aupres de l\'universite de l\'etat de Pennsilvanie. Prenez contact avec
l\'administrateur de votre site pour plus d\'informations. Prenez aussi
contact avec votre \"Listserv\" (voir question 48) favori sur Bitnet. La
liste NETNWS-L contient des informations sur la procedure à suivre pour
recevoir Usenet.

##### Q47 Qu\'est ce qu\'une \"flamme\"?

Il s\'agit d\'un message désagreable en réaction à un premier message.
Pensez à l\'expression francaise \"descendre quelqu\'un en flammes\".
Qu\'une \"flamme\" soit justifiee ou non, elle ne doit pas apparaître
sur le réseau. En effet, ce genre de guerre devie rapidement du sujet
initial et du centre d\'interet du groupe, ne faisant que gacher de la
bande passante echauffer les esprits sans apporter de contenu au debat.
Si vous ressentez le besoin de contredire violemment quelqu\'un, faites
le courtoisement et en argumentant votre opinion sur le réseau, ou alors
directement par message prive à la personne concernée (ce qui n\'empêche
pas à nouveau d\'être poli).

Dans certains cas, une personne s\'amuse à diffuser des messages
ouvertement offensants, injurieux, propagandistes (sectes, reformismes
en tout genre) \... dans des goupes inadaptés. Il est alors inutile de
se plaindre à l\'auteur. La solution consiste à se plaindre au
responsable du site emetteur qui en général coupera l\'acces du mauvais
plaisant si les plaintes sont justifiées (a n\'utiliser qu\'en dernier
ressort)

##### Q48 Qu\'est ce qu\'un \"mail-server/Listserv\" ?

Il s\'agit d\'un serveur de \"mailing-list\". Ces listes sont utilisées
pour la diffusion de messages à un groupe restreint de personnes.
Contrairement à Usenet, chaque personne recoit un message individuel,
résultant en une utilisation moins rationnelle du réseau. Ces listes ne
doivent donc être utilisées que pour de petits groupes ou des groupes
fermés de lecteurs. l\'utilisation est très simple. Pour poster un
message à l\'ensemble de la liste, écrire à :
Nom\_de\_la\_liste\@listserv.foo.bar.

Pour écrire au responsable de la liste, et pour tout message
administratif, utiliser Nom\_de\_la\_liste-request\@listserv.foo.bar. Le
message \"help\" envoyé à la seconde adresse vous donnera les
instructions pour s\'abonner a la liste, se desabonner, etc\...

------------------------------------------------------------------------

Webmaster Nov 97.
