###### Abonnement/désabonnement par courriel :

Pour vous abonner à une liste de diffusion, envoyez un message vide à
l\'adresse *//nom\_de\_la\_liste//-subscribe\@fdn.fr*. Par exemple, pour
la liste //pouet//, l\'adresse est *pouet-subscribe\@fdn.fr*.

Pour se désabonner, le principe est le même, mais l\'adresse est
*//nom\_de\_la\_liste//-unsubscribe\@fdn.fr*.

Cas particulier : pour la liste AG, les inscriptions/désinscriptions
sont automatiques lors de l\'adhésion/fin de l\'adhésion, et les
changements d\'adresses se font en contactant le bureau à
*buro\@fdn.fr*.

###### Accès à l\'interface Web Sympa :

 - Pour gérer vos abonnements par interface web: [https://lists.fdn.fr/wws/lists](https://lists.fdn.fr/wws/lists)
 - Si vous avez oublié votre mot de passe, ou que vous n'en avez jamais eu, (re)demandez-en un dans l'interface, et gardez-le précieusement 8-)
 - **Remarque :** pour la liste AG, que vous tentiez de faire la modification vous-mêmes ou pas, il faut dans tous les cas nous prévenir (à *buro@fdn.fr*) afin que nous changions votre adresse dans notre base interne des utilisateurs
 - En cas de problème ou question, écrire à *services@fdn.fr*
