# Comment bien préparer le rendez-vous pour l'installation de la fibre à son domicile ?

Pour que tout se passe bien le jour J, il est important d'anticiper et de préparer le rendez-vous pour faciliter l'installation de la fibre et éviter tout report.

La préparation sera différente selon que votre domicile est déjà raccordé à la fibre ou non, et que vous habitez en appartement ou en maison.

_(consulter le lexique à la fin pour en savoir plus sur les mots en surbrillance)_

## Préalable : préparer l'intérieur de votre domicile
La configuration de votre domicile et l'emplacement prévu de votre routeur sont à prendre en compte avant même de convenir d'un rendez-vous.

- Votre domicile dispose-t-il d'un [Point de Terminaison Optique (PTO)](ftth-ont.md)? Il s'agit de la prise fibre installée à l'intérieur du domicile, de manière similaire à la prise téléphonique. Il a une identification unique. Si vous n'en avez pas encore un, c'est l'intervenant·e qui le posera. En général en suivant le même chemin que la ligne téléphonique.
- Choisissez un emplacement central pour votre routeur, à proximité d’une prise électrique. Ce choix est primordial pour bénéficier d’une connexion optimale en wifi et profiter pleinement de la fibre dans toutes les pièces de votre logement. Sinon il faudra prévoir la pose de répéteurs wifi.
- N'enfermez pas votre routeur, positionnez-le à au moins un mètre du sol.
- Localisez votre **arrivée télécom intérieure** si votre domicile ne dispose pas de PTO.
- En fonction de l'emplacement prévu pour votre routeur, vérifiez ou posez ou faites poser à l'avance les éléments nécessaires au passage de la fibre (**goulottes, gaines** ...) depuis votre arrivée télécom intérieure.
- Lors de la prise de rendez-vous, indiquez votre souhait d'emplacement et les éventuelles contraintes techniques que pourrait rencontrer l'intervenant·e qui se rendra à votre domicile.

## Votre domicile n'est pas raccordé à la fibre (absence d'un PTO)
### Vous habitez en appartement
L'intervenant·e doit accéder au **local télécom** et passer la fibre optique depuis les parties communes jusqu’à votre appartement.
- Assurez-vous d'avoir l'autorisation du propriétaire ou du syndic
- Localisez votre **arrivée télécom intérieure**
- Renseignez-vous sur les modalités d'accès aux parties communes.
- Assurez-vous que le **local télécom** de votre immeuble soit accessible pour l’intervenant·e. Si nécessaire, vous devez disposer des clés ; renseignez-vous auprès du gestionnaire de votre immeuble (propriétaire, syndic, concierge ou gardien).

### Vous habitez en maison
Localisez votre **arrivée télécom** et préparez l'extérieur de votre maison pour l'arrivée de la fibre. L'intervenant·e doit accéder au **regard** ou aux **fourreaux télécom** pour faire passer la fibre optique jusqu'à votre maison. S'ils ne sont pas dégagés, iel sera dans l’impossibilité d’installer la fibre et vous informera alors sur les travaux que vous devrez réaliser par vous-même. Il faudra alors prendre un nouveau rendez-vous.
- Assurez-vous d'avoir l'autorisation du propriétaire
- Localisez votre **arrivée télécom extérieure**
- Dégagez le **regard** (plaque métallique ou béton rectangulaire) installé en limite de votre terrain. L'intervenant·e  doit accéder au regard situé sur votre propriété. Evacuez les plantes, terres, gravats, etc., de telle façon que le couvercle d'accès à votre regard soit libre.
- Localisez votre **arrivée télécom intérieure** (souvent situés dans le garage)
- Libérez les extrémités du **fourreau télécom ou de la gaine**, du côté du coffret ou du regard puis du côté de l'arrivée intérieure dans votre maison.
- Si nécessaire, remettez en état les infrastructures d'accueil pour le passage de la fibre :
  - Nettoyez votre fourreau télécom ou votre gaine dans votre propriété privée.
  - Localisez un éventuel point de blocage dans votre fourreau télécom ou votre gaine.
  - Réparez ce fourreau télécom ou cette gaine.

_Remarque : si l'arrivée de votre réseau existant (ligne fixe, ADSL, ...) n'est pas souterrain mais aérien, vous n'avez rien à préparer à l'extérieur de votre logement pour votre rendez-vous._

## Votre domicile est déjà raccordé à la fibre (présence d'un PTO)
### Vous habitez en appartement
L'intervenant·e doit accéder au **local télécom** de votre immeuble.
- Renseignez-vous sur les modalités d'accès aux parties communes.
- Assurez-vous que le **local télécom** de votre immeuble soit accessible pour l’intervenant·e. Si nécessaire, vous devez disposer des clés ; renseignez-vous auprès du gestionnaire de votre immeuble (propriétaire, syndic, concierge ou gardien).

### Vous habitez en maison
Pas de préparation spécifique.

## Lexique
### L'arrivée télécom extérieure (coffret ou regard)

L'arrivée télécom matérialise le point de rencontre entre le domaine public et le domaine privé, elle se situe sur votre domaine privé et se présente sous la forme d'un coffret intégré à votre mur de clôture ou d'un regard enterré installé en limite de votre terrain.

- Le coffret est en matière plastique et de dimensions courantes 35 x 25 x 15 cm, il est muni d'une porte verrouillée.
- Le regard est de dimensions courantes 30 x 30 x 30 cm, il est fermé par une plaque en métal ou en béton (carrée ou rectangulaire).

_Remarque : ne confondez pas ce regard télécom avec une plaque d'égout ou une autre arrivée (électricité, gaz, eau). Ce regard ou ce coffret accueille déjà certainement votre ancienne arrivée téléphonique cuivre._

### Le fourreau télécom ou la gaine à l'extérieur de votre maison

Le fourreau télécom permet à l’intervenant·e d'acheminer la fibre jusqu'à l'arrivée intérieure de votre maison.
Le fourreau télécom peut se présenter sous différentes formes : gaine en ciment, gaine annelée souple, tube lisse (diamètre 28 ou 45 mm).

### L’arrivée télécom intérieure

C'est l'arrivée du fourreau télécom ou de la gaine à l'intérieur de votre domicile, cela correspond souvent à votre ancienne arrivée téléphonique cuivre.

Si l'arrivée intérieure actuelle du fourreau télécom ou de la gaine dans votre logement est bien visible et accessible, dans ce cas vous n'avez rien à faire.

Si l'arrivée intérieure actuelle du fourreau télécom ou de la gaine dans votre logement se situe derrière du placoplatre, sous une dalle, etc ... et dans tous les cas où cette arrivée est mal positionnée, vous devez la libérer.

### Les gaines et les goulottes à l'intérieur de votre logement

À l'intérieur de votre logement, l'intervenant·e fait passer la fibre optique depuis votre arrivée intérieure (maison) ou le local télécom (immeuble) jusqu’à votre prise fibre.

### Le local télécom (si vous habitez en appartement)

Le local télécom dit aussi local technique ou "opérateurs" est destiné à accueillir l’ensemble des réseaux de communications électroniques de votre l’immeuble, c'est le point de jonction entre l'arrivée du réseau des opérateurs fibre et la distribution de la fibre dans les appartements. Ce local est situé dans les caves / parking ou au rez-de-chaussée.

### L'autorisation du propriétaire ou du syndic

Vous habitez en appartement :
- l'autorisation donnée dans le cadre de l’accord syndic concerne l’installation de la fibre jusque dans votre logement

Vous habitez en maison individuelle :
- lorsque les infrastructures d'accueil (regard, fourreaux, gaines, goulottes) existent et sont utilisables, l’intervenant·e peut réaliser votre raccordement à la fibre.
- Lorsqu’il n'existe pas d'infrastructures d'accueil (regard, fourreaux, gaines, goulottes) ou qu'elles sont inutilisables, l’intervenant·e ne pourra pas procéder à votre raccordement. Rapprochez-vous de votre propriétaire pour la réalisation des travaux nécessaires.
