###### Informer FDN de sa nouvelle adresse postale

##### Pourquoi?

Les convocations aux assemblées générales sont également envoyées par
courrier papier. Quand elles reviennent à l\'expéditeur, c\'est du
boulot en plus.

##### Comment?

Directement depuis votre espace adhérent (lien //modifier// en bas du
bloc //adresse//).

##### Quand?

Un mois avant l\'AG c\'est un bon //timing// pour recevoir la
convocation à sa nouvelle adresse.
