# FAQ de support FDN

« _Aide toi et le support t'aidera_ »

Vous trouverez ici les réponses apportées par le groupe support aux questions récurrentes des abonné·es FDN. Merci de vous référer à cette page **avant** de nous contacter sur *support@fdn.fr* si vous n'y trouvez pas solution à votre problème.

## Connexion, identification\...

 - [Comment faire pour accéder au wiki adhérent·e de FDN ?](faq/acces_wiki.md) 
 - [Où trouver mes identifiants de connexion ?](faq/login_adhacc-42.md) 
 - [Comment m'abonner ou me désabonner à une liste de diffusion ?](faq/listes_diffusion.md) 
 - [Mon accès ADSL ou Fibre ne fonctionne pas / "troubleshooting" des différents problèmes de connexion](faq/pas_de_connexion.md) 

## Howto techniques

 - [Comment monter sa passerelle Internet ?](faq/passerelle.md) 
 - [Comment utiliser les serveurs FDN en MX secondaire ?](faq/mx_secondaire.md) 
 - [IPv6](faq/ipv6.md) 
 - [Comment utiliser les DNS de FDN ?](faq/config_dns.md) 
 - [Comment déléguer sa gestion d'une zone DNS, quand on a un nom de domaine ?](faq/delegation_dns.md) 
 - [Demander des IPs supplémentaires](faq/ip_supplementaires.md) 
 - [Quel modem choisir pour se connecter à FDN ?](faq/modem.md) 
 - [Quel routeur choisir pour se connecter à FDN par la fibre (FTTH) ?](support/faq/routeur-ftth.md) 
 - [Comment bien préparer le rendez-vous pour l'installation de la fibre à son domicile ?](/support/faq/installation-fibre.md)
 - [Petit script pour bien configurer son Firewall](faq/iptables.md) 
 - [Généralités concernant PPPoE et PPPoA](faq/ppp_atm.md) 
 - [Configuration VPN](vpn/README.md) 
 - [Limitations](faq/limitations.md) 

## Adhésion, etc.

 - [Petit guide de la nouvelle personne adhérente](faq/nouvelle%20personne%20adhérente.md) 
 - [Changement de coordonnées bancaires](faq/changement_rib.md) 
 - [Déménagement](faq/changement-adresse-postale.md) 
 - [Changement d'adresse mail](faq/changement-adresse-email.md)

## Résiliation ADSL/VDSL/FTTH

 - [Résiliation d'une ligne xDSL/FTTH (fibre)](faq/resiliation.md) 

## Résiliation VPN

 - [Résiliation d'un abonnement VPN](faq/resiliation_vpn.md)
