# Howto fibre FDN

Attention, ce tutorial est un brouillon, et par conséquent il peut contenir des erreurs et/ou être incomplet. Merci 
d'aider à le compléter, on enlèvera ce warning quand suffisamment de personnes auront réussi à installer la fibre 
grâce à ce tutorial ;-)

## Comment commander la fibre

C'est désormais comme pour l'xDSL :)

## Livraison de la fibre

Une fois la commande effectuée, la personne demandeuse est normalement contactée assez rapidement (dans la journée) 
par la société SOGETREL qui est mandatée par Ielo.  

Une personne vient normalement vous installer la fibre à la date indiquée ;-)
(pour le premier test on a eu un rdv pour dès la semaine suivante, technicien sympa et à l'heure)

Il faut réfléchir à l'avance à l'endroit où vous souhaitez mettre les deux boîtier fibres. Il y aura un petit 
boîtier blanc, le [PTO](https://blog.ariase.com/box/faq/fibre-point-terminaison-optique) (8cmx8cmx3cm de profondeur) 
qui sera _a priori_ collé au mur. C'est la "prise fibre" où arrive la ligne dans votre logement, comparable à la prise
en T du cuivre. On ne doit normalement pas toucher à ce boîtier,
et si vous déménagez, il doit rester dans l'habitation que vous occupiez. Le numéro apposé sur le PTO est unique et 
permet d’identifier le logement et ainsi de mettre en service un abonnement fibre après une (nouvelle) souscription.     

Le câble de fibre de l'immeuble (ou autre) est raccordé à ce boîtier et un second câble de fibre part de ce boîtier 
vers le deuxième boîtier, l'[ONT](https://fr.wikipedia.org/wiki/Optical_Network_Termination). Celui-ci sert à
différencier votre trafic de celui de vos voisins en chiffrant et déchiffrant les communications vous étant destinées
et à convertir le signal optique en (le plus souvent) Ethernet RJ45.

## Où part la fibre?

La fibre va du PTO jusqu'à une armoire ou un boîtier technique qui peut se situer à votre étage si vous êtes dans un 
immeuble. 

Ce boîtier technique peut être relié à une armoire technique plus générale, qui peut se situer dans la cave de votre 
immeuble par exemple. Il faut donc prévoir de pouvoir donner accès à cet endroit à la personne qui installe la fibre.

Une fois votre PTO relié au boîtier de votre étage, la personne qui installe la fibre branche un câble dans 
l'armoire générale pour router le trafic jusqu'au boîtier de l'étage. Pour ce faire elle connecte un équipement de 
test au boîtier blanc qui fait clignoter le bon câble de l'autre côté (dans l'armoire générale).

## Installation logicielle

Pour se connecter, il faut se connecter en PPPOE avec vos identifiant et mot de passe.

La plupart des distributions GNU/Linux ont un logiciel qui s'appelle rp-pppoe.

Par exemple pour l'installer sous Arch Linux et dérivés on peut utiliser
```console
sudo pacman -S rp-pppoe
```

Pour debian on peut utiliser:
```console
sudo apt install pppoe
```

Ensuite, pour Arch et dérivés, il nous faudra modifier le fichier de configuration /etc/ppp/pppoe.conf avec 
l'interface Ethernet:
```
ETH=eth0
```
Et le nom d'utilisatrice ou d'utilisateur:
```
USER=angeladavis@fdn.ilf.kosc
```

Pour Debian par contre on va devoir créer ce fichier à partir de l'exemple ci dessous (le fichier à été crée à 
partir du fichier par défault de Arch):
```config
#***********************************************************************
#
# pppoe.conf
#
# Configuration file for rp-pppoe.  Edit as appropriate and install in
# /etc/ppp/pppoe.conf
#
# NOTE: This file is used by the pppoe-start, pppoe-stop, pppoe-connect and
#       pppoe-status shell scripts.  It is *not* used in any way by the
#       "pppoe" executable.
#
# Copyright (C) 2000 Roaring Penguin Software Inc.
# Copyright (C) 2018-2021 Dianne Skoll
#
# This file may be distributed under the terms of the GNU General
# Public License.
#
# LIC: GPL
# $Id$
#***********************************************************************

# When you configure a variable, DO NOT leave spaces around the "=" sign.

# Ethernet card connected to DSL modem
ETH=eth0

# PPPoE user name.  You may have to supply "@provider.com"  Sympatico
# users in Canada do need to include "@sympatico.ca"
# Sympatico uses PAP authentication.  Make sure /etc/ppp/pap-secrets
# contains the right username/password combination.
# For Magma, use xxyyzz@magma.ca
USER=angeladavis@fdn.dlsnet.fr

DEMAND=no

# DNS type: SERVER=obtain from server; SPECIFY=use DNS1 and DNS2;
# NOCHANGE=do not adjust.
DNSTYPE=SERVER

# Obtain DNS server addresses from the peer (recent versions of pppd only)
# In old config files, this used to be called USEPEERDNS.  Changed to
# PEERDNS for better Red Hat compatibility
PEERDNS=yes

DNS1=
DNS2=

# Make the PPPoE connection your default route.  Set to
# DEFAULTROUTE=no if you don't want this.
DEFAULTROUTE=yes

### ONLY TOUCH THE FOLLOWING SETTINGS IF YOU'RE AN EXPERT

# How long pppoe-start waits for a new PPP interface to appear before
# concluding something went wrong.  If you use 0, then pppoe-start
# exits immediately with a successful status and does not wait for the
# link to come up.  Time is in seconds.
#
# WARNING WARNING WARNING:
#
# If you are using rp-pppoe on a physically-inaccessible host, set
# CONNECT_TIMEOUT to 0.  This makes SURE that the machine keeps trying
# to connect forever after pppoe-start is called.  Otherwise, it will
# give out after CONNECT_TIMEOUT seconds and will not attempt to
# connect again, making it impossible to reach.
CONNECT_TIMEOUT=30

# How often in seconds pppoe-start polls to check if link is up
CONNECT_POLL=2

# Specific desired AC Name
ACNAME=

# Specific desired service name
SERVICENAME=

# Character to echo at each poll.  Use PING="" if you don't want
# anything echoed
PING="."

# File where the pppoe-connect script writes its process-ID.
# Three files are actually used:
#   $PIDFILE       contains PID of pppoe-connect script
#   $PIDFILE.pppoe contains PID of pppoe process
#   $PIDFILE.pppd  contains PID of pppd process
CF_BASE=`basename $CONFIG`
PIDFILE="/var/run/$CF_BASE-pppoe.pid"

# Do you want to use synchronous PPP?  "yes" or "no".  "yes" is much
# easier on CPU usage, but may not work for you.  It is safer to use
# "no", but you may want to experiment with "yes".  "yes" is generally
# safe on Linux machines with the n_hdlc line discipline; unsafe on others.
SYNCHRONOUS=no

# Do you want to clamp the MSS?  Here's how to decide:
# - If you have only a SINGLE computer connected to the DSL modem, choose
#   "no".
# - If you have a computer acting as a gateway for a LAN, choose "1412".
#   The setting of 1412 is safe for either setup, but uses slightly more
#   CPU power.
CLAMPMSS=1412
#CLAMPMSS=no

# LCP echo interval and failure count.
LCP_INTERVAL=20
LCP_FAILURE=3

# PPPOE_TIMEOUT should be about 4*LCP_INTERVAL
PPPOE_TIMEOUT=80

# Firewalling: One of NONE, STANDALONE or MASQUERADE
FIREWALL=NONE

# Linux kernel-mode plugin for pppd.  If you want to try the kernel-mode
# plugin, use LINUX_PLUGIN=/etc/ppp/plugins/rp-pppoe.so
LINUX_PLUGIN=

# Any extra arguments to pass to pppoe.  Normally, use a blank string
# like this:
PPPOE_EXTRA=""

# Rumour has it that "Citizen's Communications" with a 3Com
# HomeConnect DSL Modem DualLink requires these extra options:
# PPPOE_EXTRA="-f 3c12:3c13 -S ISP"

# Any extra arguments to pass to pppd.  Normally, use a blank string
# like this:
PPPD_EXTRA=""

########## DON'T CHANGE BELOW UNLESS YOU KNOW WHAT YOU ARE DOING
# If you wish to COMPLETELY overrride the pppd invocation:
# Example:
# OVERRIDE_PPPD_COMMAND="pppd call dsl"

# If you want pppoe-connect to exit when connection drops:
# RETRY_ON_FAILURE=no
``` 

Ensuite on aura à configurer le mot de passe dans les fichiers /etc/ppp/pap-secrets et /etc/ppp/chap-secrets.

Voici un exemple (à modifier):
```
# Secrets for authentication
# client	server	secret			IP addresses
angeladavis@fdn.ilf.kosc * theverysecretpassword
```

On peut utiliser exactement le même contenu pour /etc/ppp/pap-secrets et /etc/ppp/chap-secrets.

On peut ensuite tester à la main comme ça:
```console
DEBUG=1 pppoe-start
```

Ça va créer un fichier de log dans /var/log avec les détails du trafic réseau mais normalement ça contient pas le 
mot de passe mais j'ai vérifié avec grep uniquement, je sait pas s'il y'a moyen de le récupérer à partir des trames 
réseaux.

Si on veut faire des tests de connection en permanence, on peut copier le contenu suivant dans 
/etc/systemd/system/tests-fibre.service pour Arch et dérivés et Debian (avec systemd): 
```systemd
[Unit]
Description=Tests de connection pour la fibre
After=network.target

[Service]
Type=forking
ExecStart=/usr/sbin/pppoe-start
# Environment=DEBUG=1 # Would fill up /tmp/
Restart=always
RestartSec=1

[Install]
WantedBy=multi-user.target
```

Puis on peut démarrer les tests avec:
```console
systemctl start tests-fibre.service
```

Et pour que les tests ne s'arretent pas au prochain redémarrage de la machine:
```console
systemctl enable tests-fibre.service
```
