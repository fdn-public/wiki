###### ZyXEL AMG1001-T10A

Modem ADSL2+ très compact (9 * 8 * 2.9 cm), aux fonctionnalités limitées (pas d'IPv6 visiblement, pas d'https...). Je l'utilise en mode bridge avec un routeur sous LibreCMC.

###### Configuration en mode bridge

Obtenir une adresse IP via DHCP et se connecter à l'interface d'admin,
par défaut: http://192.168.1.1/

```
AMG1001-T10A > Interface Setup > Internet
  Section: 'ATM VC'
    Normalement les valeurs par défaut devraient être les bonnes. Au cas où:
      'VPI: 8'
      'VCI: 35'
  Section: 'Encapsulation'
    Selectionner: 'ISP: Bridge Mode'
  Section: 'Bridge Mode'
    Selectionner: '1483 Bridged IP VC-Mux'
```

Il ne reste plus qu'à lui brancher un routeur au cul et configurer celui-ci avec les paramètres de connexion de FDN.
