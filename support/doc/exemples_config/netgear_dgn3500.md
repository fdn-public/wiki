###### Netgear DGN3500

Un des rares combos modem-routeur supporté par OpenWRT. On peut
l'utiliser en mode modem-routeur ou en simple bridge avec un routeur
derrière. Cette documentation couvre les deux configurations.

###### Installation

Obtenir une adresse IP via DHCP et se connecter à
l'interface d'admin, par défaut : http://192.168.0.1

Télécharger le fichier de firmware via le site de openwrt, par exemple
cette page :

https://downloads.openwrt.org/releases/21.02.5/targets/lantiq/xway/

Ou https://firmware-selector.openwrt.org/ pour trouver la version la
plus récente.

Si on flashe depuis le firmware d'origine, le fichier doit se terminer
en `*-factory.img`, dans mon cas :

https://downloads.openwrt.org/releases/21.02.5/targets/lantiq/xway/openwrt-21.02.5-lantiq-xway-netgear_dgn3500-squashfs-factory.img

Dans l'interface d'administration du routeur, aller dans la section
"Upgrade Firmware" et uploadez le fichier. Le routeur va installer
OpenWRT tout seul.

###### Configuration PPP

* ADSL

Récupérez une adresse IP via DHCP et connectez-vous à l'interface
d'admin, par défaut : https://192.168.1.1

[OpenWRT > Network > Interfaces](https://192.168.1.1/cgi-bin/luci/admin/network/network)

[WAN > Edit](https://192.168.1.1/cgi-bin/luci/admin/network/network/wan)

```
Protocol: PPPoATM
[...]
  PPPoA Encapsulation: VC-Mux
  ATM device number: 0
  ATM Virtual Channel Identifier (VCI): 35
  ATM Virtual Path Identifier (VPI): 8
  PAP/CHAP username: (votre login ADSL)
  PAP/CHAP password: (votre mot de passe ADSL)
[...]
Save & Apply
```

Et c'est tout. Pour la connexion IPv6, OpenWRT crée automatiquement
une interface 'wan_6' sur 'ppoa-wan' (différente de l'interface 'wan6'
- 'Alias of "wan"' déjà présente), cela peut prendre quelques minutes.

* IPv6

Cependant, par défaut OpenWRT fournit 2 IPv6 aux machines connectées
au routeur : une issue du range du FAI, et une issue d'un range local
généré lors du reset usine, visible dans le paramètre 'IPv6
ULA-Prefix'. Cela peut parfois empêcher IPv6 de fonctionner
correctement (le traffic IPv6 cherchera à passer par l'IP locale au
lieu de passer par l'IP du FAI). Il faut donc éditer la configuration
comme suit :

Se connecter en SSH :

```
ssh -p 22 root@192.168.1.1
```

L'environnement est un Busybox. Éditez le fichier de configuration
réseau :

```
root@OpenWrt:~# vi /etc/config/network
```

Dans l'interface `lan`, remplacez le

```
        option ip6assign '60'
```

par une adresse issue de votre range attribué par le FAI :

```
config interface 'lan'
        [...]
	option ip6addr '2001:910:****::1/64'
```

Rechargez la configuration :

```
root@OpenWrt:~# /etc/init.d/network restart
```

* Récapitulatif

Voici à quoi ressemble mon fichier de configuration :

```

config interface 'loopback'
	option device 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fdb9:54ad:4dfb::/48'

config atm-bridge 'atm'
	option vpi '1'
	option vci '32'
	option encaps 'llc'
	option payload 'bridged'
	option nameprefix 'dsl'

config dsl 'dsl'
	option annex 'a'
	option firmware '/lib/firmware/adsl.bin'

config device
	option name 'br-lan'
	option type 'bridge'
	list ports 'eth0.1'

config device
	option name 'eth0.1'
	option macaddr '00:26:f2:57:b5:96'

config interface 'lan'
	option device 'br-lan'
	option proto 'static'
	option ipaddr '192.168.1.1'
	option netmask '255.255.255.0'
	option ip6addr '2001:910:107e::1234/64'

config device
	option name 'dsl0'
	option macaddr '00:26:f2:57:b5:97'

config interface 'wan'
	option proto 'pppoa'
	option encaps 'vc'
	option atmdev '0'
	option vci '35'
	option vpi '8'
	option username '************@fdn.dslnet.fr'
	option password '************'
	option ipv6 'auto'

config interface 'wan6'
	option device '@wan'
	option proto 'dhcpv6'

config switch
	option name 'switch0'
	option reset '1'
	option enable_vlan '1'

config switch_vlan
	option device 'switch0'
	option vlan '1'
	option ports '0 1 2 3 5t'
```

###### Configuration Bridge

* Bridge

Récupérez une adresse IP via DHCP et connectez-vous à l'interface
d'admin, par défaut : https://192.168.1.1

[OpenWRT > Network > ATM Bridges](https://192.168.1.1/cgi-bin/luci/admin/network/network)

```
General Setup
  ATM Virtual Channel Identifier (VCI): 35
  ATM Virtual Path Identifier (VPI): 8
  Encapsulation mode: VC-Mux
Save & Apply
```

[OpenWRT > Network > Interfaces](https://192.168.1.1/cgi-bin/luci/admin/network/network)

[WAN > Edit](https://192.168.1.1/cgi-bin/luci/admin/network/network/wan)

Passer l'interface WAN en `Protocol: Unmanaged`

Se connecter en SSH :

```
ssh -p 22 root@192.168.1.1
```

Éditer le fichier de configuration réseau :

```
root@OpenWrt:~# vi /etc/config/network
```

Rajouter le device 'dsl0' (ou '@wan') au bridge :

```
config device
	option name 'br-lan'
	option type 'bridge'
	[...]
        list ports '@wan'
```

Désactivez les services `firewall` et `dnsmasq` :

```
root@OpenWrt:~# /etc/init.d/firewall stop
root@OpenWrt:~# /etc/init.d/dnsmasq stop
root@OpenWrt:~# /etc/init.d/firewall disable
root@OpenWrt:~# /etc/init.d/dnsmasq disable
```

Rechargez la configuration :

```
root@OpenWrt:~# /etc/init.d/network restart
```

Puis brancher un routeur au cul du DGN3500 et entrer les informations
de connexion (voir les docs de configuration de routeurs pour plus de
détails).

* Récapitulatif

Voici à quoi ressemble mon fichier de configuration :

```
config interface 'loopback'
	option device 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd89:3183:27ba::/48'

config atm-bridge 'atm'
	option vpi '8'
	option vci '35'
	option encaps 'vc'
	option payload 'bridged'
	option nameprefix 'dsl'

config dsl 'dsl'
	option annex 'a'
	option firmware '/lib/firmware/adsl.bin'

config device
	option name 'br-lan'
	option type 'bridge'
	list ports 'eth0.1'
        list ports '@wan'

config device
	option name 'eth0.1'
	option macaddr '00:26:f2:57:b5:96'

config interface 'lan'
	option device 'br-lan'
	option proto 'static'
	option ipaddr '192.168.1.1'
	option netmask '255.255.255.0'
	option ip6assign '60'

config device
	option name 'dsl0'
	option macaddr '00:26:f2:57:b5:97'

config interface 'wan'
	option device 'dsl0'
	option proto 'none'

config interface 'wan6'
	option device '@wan'
	option proto 'dhcpv6'

config switch
	option name 'switch0'
	option reset '1'
	option enable_vlan '1'

config switch_vlan
	option device 'switch0'
	option vlan '1'
	option ports '0 1 2 3 5t'
```
