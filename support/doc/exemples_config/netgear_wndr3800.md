###### Netgear WNDR3800

Un routeur supporté par le projet LibreCMC. Dispose d'une grosse
mémoire flash (16MB) et d'une très grosse RAM (128MB), donc ne sera
pas obsolète tout de suite. :)

###### Installation

Après reset usine, obtenir une adresse IP via DHCP et se connecter à
l'interface d'admin, par défaut : http://192.168.1.1 ou http://10.0.0.1

Télécharger le fichier de firmware via le site de librecmc : il doit
se terminer en `*-factory.bin`, dans mon cas :

https://librecmc.org/librecmc/downloads/snapshots/current/main/targets/ath79/generic/librecmc-ath79-generic-netgear_wndr3800-squashfs-factory.img

Dans l'interface d'administration du routeur, aller dans la section
"Upgrade Firmware" et uploader le fichier. Le routeur va installer
LibreCMC tout seul.

###### Configuration

* ADSL

Récupérez une adresse IP via DHCP et connectez-vous à l'interface
d'admin, par défaut : https://192.168.10.1

[libreCMC > Network > Interfaces](https://192.168.10.1/cgi-bin/luci/admin/network/network)

[WAN > Edit](https://192.168.10.1/cgi-bin/luci/admin/network/network/wan)

```
Protocol: PPPoE
[...]
  PAP/CHAP username: (votre login ADSL)
  PAP/CHAP password: (votre mot de passe ADSL)
[...]
Save & Apply
```

Et c'est tout. Il suffit d'attendre un peu, dans les versions récentes
de LibreCMC la connexion IPv6 se met en place toute seule.

* IPv6

Cependant, par défaut LibreCMC fournit 2 IPv6 aux machines connectées
au routeur : une issue du range du FAI, et une issue d'un range local
généré lors du reset usine, visible dans le paramètre 'IPv6
ULA-Prefix'. Cela peut parfois empêcher IPv6 de fonctionner
correctement (le traffic IPv6 cherchera à passer par l'IP locale au
lieu de passer par l'IP du FAI). Il faut donc éditer la configuration
comme suit :

Commencez par activer l'accès SSH :

[libreCMC > System > Administration > SSH Access](https://192.168.10.1/cgi-bin/luci/admin/system/admin/dropbear)

Configurez selon vos préférences. Ensuite, pour se connecter :

```
ssh -p 22 -oHostKeyAlgorithms=+ssh-rsa root@192.168.10.1
```

L'environnement est un Busybox. Éditez le fichier de configuration
réseau :

```
root@libreCMC:~# vi /etc/config/network
```

Dans l'interface `lan`, remplacez le

```
        option ip6assign '60'
```

par une adresse issue de votre range attribué par le FAI :

```
config interface 'lan'
        [...]
	option ip6addr '2001:910:****::1/64'
```

Rajoutez une route :

```
config route6
	option interface 'wan'
	option target '::/0'
	option gateway 'fe80::1'
```

Rechargez la configuration :

```
root@libreCMC:~# /etc/init.d/network restart
```

* Accès au modem depuis le LAN, via le routeur

Si comme moi vous ne pouvez plus accéder à l'interface
d'administration de votre modem (adresse: 192.168.0.1) sans vous
brancher physiquement dessus rajoutez cette interface :

```
root@libreCMC:~# vi /etc/config/network
```

```
config interface 'lan2'
	option ifname 'eth0'
	option proto 'static'
	option ipaddr '192.168.1.2'
	option netmask '255.255.255.0'
```

Rechargez la configuration :

```
root@libreCMC:~# /etc/init.d/network restart
```

* Récapitulatif

Voici à quoi ressemble mon fichier de configuration :

```
config interface 'loopback'
	option ifname 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fdcc:71b9:8d05::/48'

config interface 'lan'
	option type 'bridge'
	option ifname 'eth0'
	option proto 'static'
	option ipaddr '192.168.10.1'
	option netmask '255.255.255.0'
	option ip6addr '2001:910:107e::1234/64'

config device 'lan_eth0_dev'
	option name 'eth0'
	option macaddr '2e:b0:5d:7f:57:c3'

config interface 'wan'
	option ifname 'eth1'
	option proto 'pppoe'
	option username '************@fdn.dslnet.fr'
	option password '************'
	option ipv6 'auto'
	option keepalive '0'

config interface 'wan6'
	option ifname 'eth1'
	option proto 'dhcpv6'

config interface 'lan2'
	option ifname 'eth1'
	option proto 'static'
	option ipaddr '192.168.1.2'
	option netmask '255.255.255.0'

config route6
	option interface 'wan'
	option target '::/0'
	option gateway 'fe80::1'

config switch
	option name 'switch0'
	option reset '1'
	option enable_vlan '1'
	option blinkrate '2'

config switch_vlan
	option device 'switch0'
	option vlan '1'
	option ports '0 1 2 3 5'

config switch_port
	option device 'switch0'
	option port '1'
	option led '6'

config switch_port
	option device 'switch0'
	option port '2'
	option led '9'

config switch_port
	option device 'switch0'
	option port '5'
	option led '2'
```
