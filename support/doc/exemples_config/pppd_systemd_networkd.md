# Config router PPPoE avec pppd + systemd-networkd

Testée sur:
- Nanopi R4S w/ Archlinux ARM

## Paquets

- systemd-resolveconf
- ppp

## Files

=> /etc/ppp/peers/extern0
```
name <id fourni par fdn>
```

=> /etc/ppp/pap-secrets
```
# Secrets for authentication using PAP
# client	server	secret			IP addresses
<id fdn> * <mot de passe fdn>
```

=> /etc/systemd/system/pppoe@.service
```systemd
[Unit]
Description=PPPoE connection over %I
Documentation=man:pppd(8)
BindsTo=sys-subsystem-net-devices-%i.device
After=sys-subsystem-net-devices-%i.device

[Service]
ExecStart=/usr/sbin/pppd plugin %p.so %I call %I linkname %I nodetach persist noauth defaultroute usepeerdns
# Si la version de pppd le supporte (compile avec le support systemd ):
#Type=notify
ExecStart=/usr/sbin/pppd plugin %p.so %I call %I linkname %I up_sdnotify persist noauth defaultroute usepeerdns
ExecStop=/bin/kill $MAINPID
ExecReload=/bin/kill -HUP $MAINPID
SuccessExitStatus=5 12 13 14
Restart=on-failure
StandardOutput=null
# Hardening

AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_BROADCAST CAP_NET_RAW
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE CAP_NET_BROADCAST CAP_NET_RAW
DeviceAllow=/dev/ppp rw
FileDescriptorStoreMax=512
LockPersonality=yes
MemoryDenyWriteExecute=yes
NoNewPrivileges=yes
ProtectProc=invisible
ProtectClock=yes
ProtectControlGroups=yes
ProtectHome=yes
ProtectKernelLogs=yes
ProtectKernelModules=yes
ProtectSystem=strict
RuntimeDirectory=%N
RestrictRealtime=yes
ProtectKernelTunables=yes
ProtectControlGroups=yes
SystemCallArchitectures=native
SystemCallErrorNumber=EPERM
SystemCallFilter=@system-service

[Install]
WantedBy=multi-user.target
```

=> Fichiers .network et .link pour systemd-networkd (/etc/systemd/network/)

Les fichiers `.link` sont spécifiques à la plateforme, l'idée est de renommer les interfaces avec des noms plus explicites.

```systemd
# /etc/systemd/network/10-intern0.link 
[Match]
Path=platform-f8000000.pcie-pci-0000:01:00.0

[Link]
Name=intern0
```
```systemd
# /etc/systemd/network/10-extern0.link
[Match]
Path=platform-fe300000.ethernet

[Link]
Name=extern0
```
```systemd
# /etc/systemd/network/10-extern0.network
[Match]
Name=extern0

[Link]
RequiredForOnline=degraded
```
```systemd
# /etc/systemd/network/ppp.network
[Match]
Kind=ppp

[Network]
DHCP=yes
KeepConfiguration=yes

[DHCPv6]
UseAddress=no

[IPv6AcceptRA]
UseAutonomousPrefix=no
```
```systemd
# /etc/systemd/network/10-router.network
[Match]
Name=intern0

[Network]
Address=192.168.72.1/24
DHCPServer=true
IPMasquerade=ipv4
IPForward=yes
IPv6AcceptRA=no
IPv6SendRA=yes
DHCPPrefixDelegation=yes
MulticastDNS=resolve

[DHCPServer]
PoolOffset=100
PoolSize=100
DNS=_server_address # only if you have a local DNS resolver on the router

# Only used for a local DNS resolver
[IPv6Prefix]
Prefix=fd07::/64
Assign=true

[IPv6SendRA]
DNS=fd07::xx:xx:xx:xx # Router address for a local ipv6 DNS server

[Link]
RequiredForOnline=no
```

## Commandes

```sh
# Empêche pppd d'activer l'implementation noyau de l'ipv6 (on utilise exclusivement systemd-network pour cela) / (specifique au paquet pppd Archlinux)
chmod -x /etc/ppp/ipv6-up.d/00-iface-config.sh
systemctl enable --now pppoe@extern0 systemd-networkd
```
