###### TPE-NWIFIROUTER2 (aka "ThinkPenguin Wireless N")

Routeur vendu avec LibreCMC 1.3.3. Je l'utilise avec un ZyXEL
AMG1001-T10A configuré en mode bridge.

Malheureusement les versions de LibreCMC antérieures à la 1.5.4 ne
sont plus en ligne à cause d'un problème de licence. Et le
TPE-NWIFIROUTER2 n'ayant de 4M de mémoire flash, les versions
actuelles de LibreCMC sont trop volumineuses pour y être installées.

Utiliser un TPE-NWIFIROUTER2 aujourd'hui veut donc dire avoir un
machin pas à jour, avec tous les soucis de sécurité que cela peut
engendrer.

Je documente quand même ici la configuration du routeur, qui est loin
d'être simple (surtout pour la partie IPv6) !

###### Configuration

* ADSL IPv4

Obtenir une adresse IP via DHCP et se connecter à l'interface d'admin,
par défaut : https://192.168.10.1

[libreCMC > Network > Interfaces](https://192.168.10.1/cgi-bin/luci/admin/network/network)

[WAN > Edit](https://192.168.10.1/cgi-bin/luci/admin/network/network/wan)

```
Protocol: PPPoE
[...]
  PAP/CHAP username: (votre login ADSL)
  PAP/CHAP password: (votre mot de passe ADSL)
[...]
Save & Apply
```

* IPv6

Pour l'IPv6, c'est un peu plus compliqué, il faut éditer la
configuration à la main. Commencez par activer l'accès SSH :

[libreCMC > System > Administration > SSH Access](https://192.168.10.1/cgi-bin/luci/admin/system/admin/dropbear)

Configurez selon vos préférences. Ensuite, pour se connecter :

```
ssh -p 22 -oKexAlgorithms=diffie-hellman-group14-sha1 -oHostKeyAlgorithms=+ssh-rsa root@192.168.10.1
```

L'environnement est un vieux Busybox. Éditez le fichier de
configuration réseau :

```
root@libreCMC:~# vi /etc/config/network
```
Dans l'interface `wan`, activez l'option ipv6 :

```
config interface 'wan'
	option ifname 'eth0'
	option _orig_ifname 'eth0'
	option _orig_bridge 'false'
	option proto 'pppoe'
	option username '************@fdn.dslnet.fr'
	option password '************'
	option ipv6 '1'
```

Dans l'interface `wan6`, spécifiez l'interface ethernet utilisée sur
`wan` :

```
config interface 'wan6'
	option ifname 'eth0'
```

Dans l'interface `lan`, donnez à votre routeur une adresse IPv6 issue
de votre range attribué par le FAI :

```
config interface 'lan'
	option ifname 'eth1'
	option force_link '1'
	option type 'bridge'
	option proto 'static'
	option ipaddr '192.168.10.1'
	option netmask '255.255.255.0'
	option ip6addr '2001:910:****::1/64'
```

Rajoutez une route :

```
config route6
	option interface 'wan'
	option target '::/0'
	option gateway 'fe80::1'
```

Rechargez la configuration :

```
root@libreCMC:~# /etc/init.d/network restart
```

* Accès au modem depuis le LAN, via le routeur

Par défaut, je ne peux plus accéder à l'interface d'administration de
mon modem (adresse: 192.168.0.1) sans me brancher physiquement
dessus. On peut remédier à cela en rajoutant une interface :

```
root@libreCMC:~# vi /etc/config/network
```

```
config interface 'lan2'
	option ifname 'eth0'
	option proto 'static'
	option ipaddr '192.168.1.2'
	option netmask '255.255.255.0'
```

Rechargez la configuration :

```
root@libreCMC:~# /etc/init.d/network restart
```

* Récapitulatif

Voici à quoi ressemble mon fichier de configuration :

```
config interface 'loopback'
	option ifname 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd71:062e:47e9::/48'

config interface 'lan'
	option ifname 'eth1'
	option force_link '1'
	option type 'bridge'
	option proto 'static'
	option ipaddr '192.168.10.1'
	option netmask '255.255.255.0'
	option ip6addr '2001:910:****::1234/64'

config interface 'wan'
	option ifname 'eth0'
	option _orig_ifname 'eth0'
	option _orig_bridge 'false'
	option proto 'pppoe'
	option username '************@fdn.dslnet.fr'
	option password '************'
	option ipv6 '1'

config interface 'wan6'
	option ifname 'eth0'

config interface 'lan2'
	option ifname 'eth0'
	option proto 'static'
	option ipaddr '192.168.1.2'
	option netmask '255.255.255.0'

config route6
	option interface 'wan'
	option target '::/0'
	option gateway 'fe80::1'

config switch
	option name 'switch0'
	option reset '1'
	option enable_vlan '1'

config switch_vlan
	option device 'switch0'
	option vlan '1'
	option ports '0 1 2 3 4'
```
