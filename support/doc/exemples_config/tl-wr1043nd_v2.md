###### TP-Link TL-WR1043ND v2

Un routeur supporté par le projet LibreCMC. Les firmwares de type
OpenWRT et LibreCMC ayant tendance à augmenter en taille à peu près
aussi vite que les mémoires flash des routeurs, le mieux est de
prendre un TL-WR1043ND v4 ou v5, qui dispose de 16M de flash et pas
seulement 8M comme les v1, v2 et v3. Ainsi il sera encore supporté
quelques années quand les autres seront déjà désuets. Autre conseil :
ne vous fiez pas à la couleur ! Seul le modèle v1 a une coque blanche,
les autres ont tous la même coque bleue. Demandez à voir l'étiquette
sur la partie inférieure de la coque avant d'acheter.

###### Installation

Après reset usine, obtenir une adresse IP via DHCP et se connecter à
l'interface d'admin, par défaut : http://192.168.0.1

Télécharger le fichier de firmware via le site de librecmc : il doit
se terminer en `*-factory.bin`, dans mon cas :

https://librecmc.org/librecmc/downloads/snapshots/current/main/targets/ath79/generic/librecmc-ath79-generic-tplink_tl-wr1043nd-v2-squashfs-factory.bin

Renommer le fichier en `wr1043nv2_tp_recovery.bin` (ou simplement
faire un lien symbolique).

Dans l'interface d'administration du routeur, aller dans la section
"Upgrade Firmware" et uploader le fichier. Le routeur va installer
LibreCMC tout seul.

###### Configuration

* ADSL

Récupérez une adresse IP via DHCP et connectez-vous à l'interface
d'admin, par défaut : https://192.168.10.1

[libreCMC > Network > Interfaces](https://192.168.10.1/cgi-bin/luci/admin/network/network)

[WAN > Edit](https://192.168.10.1/cgi-bin/luci/admin/network/network/wan)

```
Protocol: PPPoE
[...]
  PAP/CHAP username: (votre login ADSL)
  PAP/CHAP password: (votre mot de passe ADSL)
[...]
Save & Apply
```

Et c'est tout. Il suffit d'attendre un peu, dans les versions récentes
de LibreCMC la connexion IPv6 se met en place toute seule.

* IPv6

Cependant, par défaut LibreCMC fournit 2 IPv6 aux machines connectées
au routeur : une issue du range du FAI, et une issue d'un range local
généré lors du reset usine, visible dans le paramètre 'IPv6
ULA-Prefix'. Cela peut parfois empêcher IPv6 de fonctionner
correctement (le traffic IPv6 cherchera à passer par l'IP locale au
lieu de passer par l'IP du FAI). Il faut donc éditer la configuration
comme suit :

Commencez par activer l'accès SSH :

[libreCMC > System > Administration > SSH Access](https://192.168.10.1/cgi-bin/luci/admin/system/admin/dropbear)

Configurez selon vos préférences. Ensuite, pour se connecter :

```
ssh -p 22 -oHostKeyAlgorithms=+ssh-rsa root@192.168.10.1
```

L'environnement est un Busybox. Éditez le fichier de configuration
réseau :

```
root@libreCMC:~# vi /etc/config/network
```

Dans l'interface `lan`, remplacez le

```
        option ip6assign '60'
```

par une adresse issue de votre range attribué par le FAI :

```
config interface 'lan'
        [...]
	option ip6addr '2001:910:****::1/64'
```

Rajoutez une route :

```
config route6
	option interface 'wan'
	option target '::/0'
	option gateway 'fe80::1'
```

Rechargez la configuration :

```
root@libreCMC:~# /etc/init.d/network restart
```

* Accès au modem depuis le LAN, via le routeur

Si comme moi vous ne pouvez plus accéder à l'interface
d'administration de votre modem (adresse: 192.168.0.1) sans vous
brancher physiquement dessus rajoutez cette interface :

```
root@libreCMC:~# vi /etc/config/network
```

```
config interface 'lan2'
	option ifname 'eth0'
	option proto 'static'
	option ipaddr '192.168.1.2'
	option netmask '255.255.255.0'
```

Rechargez la configuration :

```
root@libreCMC:~# /etc/init.d/network restart
```

* Récapitulatif

Voici à quoi ressemble mon fichier de configuration :

```
config interface 'loopback'
	option ifname 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd1d:b721:1d7d::/48'

config interface 'lan'
	option type 'bridge'
	option ifname 'eth1.1'
	option proto 'static'
	option ipaddr '192.168.10.1'
	option netmask '255.255.255.0'
	option ip6addr '2001:910:****::1234/64'

config interface 'wan'
	option ifname 'eth0.2'
	option proto 'pppoe'
	option username '************@fdn.dslnet.fr'
	option password '************'
	option ipv6 'auto'
	option keepalive '0'

config interface 'wan6'
	option ifname 'eth0.2'
	option proto 'dhcpv6'

config interface 'lan2'
	option ifname 'eth0.2'
	option proto 'static'
	option ipaddr '192.168.1.2'
	option netmask '255.255.255.0'

config route6
	option interface 'wan'
	option target '::/0'
	option gateway 'fe80::1'

config switch
	option name 'switch0'
	option reset '1'
	option enable_vlan '1'

config switch_vlan
	option device 'switch0'
	option vlan '1'
	option ports '1 2 3 4 0t'

config switch_vlan
	option device 'switch0'
	option vlan '2'
	option ports '5 6t'
```
