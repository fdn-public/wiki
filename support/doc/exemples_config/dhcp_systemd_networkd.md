# Config router avec systemd-networkd seulement

Configuration de `systemd-networkd` pour une connexion Aquilenet via Axione (client dhcp pour ipv4 et ipv6 avec IPV6
Prefix Delegation).

Testée sur :
- Minisforum MS-01 w/ Archlinux

La suite de cette documentation considère que l'interface branchée à l'ONT est nommée `wan0` et que celle branchée au
réseau local est nommée `lan0`.

Cette documentation ne documente pas la configuration d'un serveur dhcpv4 pour le réseau local, bien que cette
configuration soit possible à l'aide de `systemd-networkd`. Voir la section
[`[DHCPServer]`](https://man.archlinux.org/man/systemd.network.5#%5BDHCPSERVER%5D_SECTION_OPTIONS) dans le man.

## Dépendances

Toutes les dépendances sont déjà présentes dans l'installation de base d'archlinux (`systemd-networkd`,
`systemd-resolved`, etc.).

## Configuration sysctl

```
# /etc/sysctl.d/40-router.conf 
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1
```

Il est nécessaire de charger la configuration précédente pour qu'elle soit prise en compte sans qu'il n'y ait besoin
de redémarrer la machine :
```console
sysctl --load=/etc/sysctl.d/40-router.conf 
```

## Configuration `iptables`

```
# /etc/iptables/ip6tables.rules
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A INPUT -m conntrack --ctstate INVALID -j DROP

-A INPUT -p icmpv6 -m comment --comment "*: icmp6 (nécessaire pour établir une connexion)" -j ACCEPT
-A INPUT -d fe80::/10 -p udp -m udp --dport 546 -m conntrack --ctstate NEW -m comment --comment "*: dhcpc6 (nécessaire pour établir une connexion)" -j ACCEPT

# Connexion via ssh depuis le réseau local
-A INPUT -i lan0 -p tcp -m tcp --dport 22 -m comment --comment "lan: ssh" -j ACCEPT

-A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i lan0 -o wan0 -m comment --comment "lan > wan" -j ACCEPT

COMMIT
```

```
# /etc/iptables/iptables.rules
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# Facultatif : authorise le ping pour toutes les interfaces 
-A INPUT -p icmp -m icmp --icmp-type 8 -m comment --comment "*: icmp" -j ACCEPT

# Connexion via ssh depuis le réseau local
-A INPUT -i enp87s0 -p tcp -m tcp --dport 22 -m comment --comment "lan: ssh" -j ACCEPT

-A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i lan0 -o wan0 -m comment --comment "lan > wan" -j ACCEPT

COMMIT

*nat
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
-A POSTROUTING -o wan0 -j MASQUERADE

COMMIT
```

Il est nécessaire d'activer et de démarrer les services iptables et ip6tables s'ils ne le sont pas déjà :
```console
systemctl enable --now ip6tables.service iptables.service
```

Sinon, restaurer la configuration avec :
```console
ip6tables-restore /etc/iptables/ip6tables.rules
iptables-restore /etc/iptables/iptables.rules
```

Où combiner les règles ci-dessus avec une configuration existante puis sauvegarder à l'aide de `ip6tables-save` et
de `iptables-save`.

## Configuration `systemd-networkd`

```systemd
# /etc/systemd/network/10-wan.network
[Match]
Name=wan0

[Network]
Description=WAN interface
DHCP=yes

[DHCPv6]
WithoutRA=solicit
```

```systemd
# /etc/systemd/network/10-lan.network
[Match]
Name=lan0

[Network]
Description=Main LAN interface
Address=192.168.1.254/24

IPv6SendRA=yes

DHCPPrefixDelegation=yes

[DHCPPrefixDelegation]
UplinkInterface=wan0
SubnetId=0
Announce=yes
```

Si ce n'est déjà le cas, activer et démarrer les services suivants :
```console
systemctl enable --now systemd-networkd.service systemd-resolved.service systemd-timesyncd.service
```

`systemd-resolved` nécessite que le fichier `/etc/resolv.conf` soit un lien symbolique vers un fichier qu'il gère.
```console
ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

Si les services étaient déjà démarrés, il faut recharger la configuration de `systemd-networkd` puis reconfigurer
les interfaces dont la configuration a été modifiée :
```console
networkctl reload
networkctl reconfigure wan0 lan0
```
