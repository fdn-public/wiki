# :warning: Premier jet, à corriger/peaufiner/compléter si besoin :warning:

# Lexique

Destiné aux débutant·e·s pour le jargon utilisé au sein de l'association, de la Fédération FDN ou plus largement en informatique et en réseaux.  
Classé par ordre alphabétique, les termes en gras ont leur propre définition dans la liste.

En complément, voir aussi la [liste d'abréviations en informatique sur Wikipédia](https://fr.wikipedia.org/wiki/Liste_d%27abr%C3%A9viations_en_informatique).

**95e centile (ou 95 percentile)** : [voir la page dédiée](support/faq/95e-centile.md)

**Admincore** : **Adminsys** qui ont le plus de droits informatiques dans FDN et donc les plus grandes responsabilités en ce qui concerne nos services

**Adminsys** : Contraction d'Administrateur·rice système, personnes qui s'occupent de la gestion des serveurs informatiques (mise à jour, sauvegarde, optimisations, mises en places de services...), c'est grâce à elles que #çamarche :)  
Plus d'infos sur la page de [fonctionnement](https://git.fdn.fr/adminsys/wiki/-/blob/master/equipe/fonctionnement.md) et [équipe](https://git.fdn.fr/adminsys/wiki/-/blob/master/equipe/equipe_adminsys.md) sur le wiki **adminsys**

**ADSL** (_Asymmetric Digital Subscriber Line_): Voir **xDSL** 

**ADSL 2+** : amélioration de l'**ADSL**

**Aquilenet** : **FAI** membre de la Fédération FDN (**FFDN**) tout comme FDN, qui nous partage leur réseau ce qui nous permet d'avoir plus de choix dans nos abonnements **FTTH**, à savoir :
- via Covage, Netwo
- via Orange, Netwo
- via Axione 

**Axione** : Opérateur d'Infrastructure (**OI**) sur lequel se basent certains de nos abonnements Fibre (**FTTH**) par le biais d'**Aquilenet**

**Backend** (ou _back-end_) : Terme anglais désignant la partie d'un site web ou un service que l’utilisateur ne voit pas et qui en permet le fonctionnement

**Backbone** (_dorsale_) : aussi appelé "cœur de réseau", désigne l'interconnexion haut débit entre sous-réseaux qui permet le transit des informations au sein d'un réseau informatique étendu.

**bande passante** : on utilise le terme bande passante pour désigner le débit binaire maximal d'une voie de transmission dans le domaine des réseaux informatiques, spécialement les accès à internet à haut débit. Le débit binaire est une mesure de la quantité de données numériques transmises par unité de temps. Selon ses définitions normatives, il s'exprime en bits par seconde (bit/s, b/s ou bps) ou un de ses multiples en employant les préfixes du Système international (SI) : kb/s (kilobits par seconde), Mb/s (mégabits par seconde) et ainsi de suite.
[Plus d'information sur Wikipedia](https://fr.wikipedia.org/wiki/D%C3%A9bit_binaire)

**BAS** (_Broadband Access Server_) ou **BRAS** (_Broadband Remote Access Server_) : équipement réseau spécialisé dans l'agrégation et le traitement des sessions clients qui assure l'agrégation ainsi que la terminaison des flux des DSLAM (ou des OLT) d'un réseau xDSL (ou d'un réseau PON) ou leur tunnelisation entre le réseau de collecte ou de transport, et le réseau du Fournisseur d'accès à Internet (FAI).

**BBB** (_BigBlueButton_) : Logiciel libre de visioconférence, utilisé pour nos réunions mensuelles, service mis à disposition par FDN [ici](https://bbb.fdn.fr/)

**BOFS** : Acronyme inventé à la Fédération FDN pour désigner les FAI commerciaux "Bouygues, Orange, Free et SFR"

**Boucle locale** : ce qui relie un utilisateur d'un réseau au premier niveau d'équipement du réseau auquel il est abonné. Physiquement, il s'agit de tous les câbles urbains que l'on peut voir dans les rues, des câbles souterrains, ainsi que de la paire de fils arrivant chez l'usager.

**Box** : box internet, boitier multiservice, voir **RGW**

**CLI** (_Command Line Interface_): interface de ligne de commande permettant à l'utilisateur d'émettre des commandes sous la forme de lignes de texte ou de lignes de commande successives pour effectuer les tâches.

**core network** : voir **backbone**

**Debian** : Distribution basée sur le **système d'exploitation** libre GNU/Linux, qui est utilisée au sein de FDN

**DNS** (_Domain Name Service_) : Service de nom de domaine, qui sert à associer un nom de domaine (fdn.fr par exemple) à une adresse IP, c'est un peu l'annuaire des sites Internet, et donc "l'annuaire" peut mentir en disant que tel site n'existe pas pour des raisons de censure etc., ce que ne font pas ceux de FDN, ils sont utilisables par tout le monde : [voir la page sur le site](https://www.fdn.fr/actions/dns/) ainsi que sa [page de documentation](support/faq/config_dns.md) sur le wiki public

**DSL** (_Digital Subscriber Line_) : ligne d'accès numérique, voir **xDSL**

**DSLAM** (_Digital Subscriber Line Access Multiplexer_) : « multiplexeur d'accès DSL », equipement de multiplexage qui permet d'assurer un service de type **DSL** sur les lignes téléphoniques.

**FAI** : Fournisseur d'Accès à Internet, ce qu'est FDN ou les **BOFS**

**FDN** (_French Data Network_) : fournisseur d'accès à Internet associatif.  
Créé en juin 1992, c'est le plus ancien FAI de France encore en activité. Fonctionnant de manière totalement bénévole et désintéressée, l'association fournit aujourd'hui plusieurs centaines de lignes ADSL, VDSL, FTTH et de VPN à travers le pays.

**FFDN** : Fédération des Fournisseurs d'Accès à Internet associatifs, dite Fédération FDN, à ne pas confondre avec **FDN**  
Plus d'infos sur le [wiki interne](https://git.fdn.fr/fdn/wiki#la-f%C3%A9d%C3%A9ration-fdn)

**Frontend** (ou _front-end_) : Terme anglais désignant la partie d'un site web ou un service avec laquelle les utilisateurs interagissent.  
Dans le cas de FDN cela concerne Nitter et Invidious qui sont respectivement des intermédiaires à Twitter (X) et Youtube, permettant d'y naviguer sans pub par exemple, selon le schéma suivant :  
vous -> Nitter/Invidious de FDN -> Twitter/Youtube  
Voir l'[annonce sur le site](https://www.fdn.fr/fdn-lance-ses-instances-invidious-et-nitter/)

**FTTH** (_Fiber To The Home_) : Fibre jusqu'au domicile, c'est ce qui est utilisé pour les abonnements fournis par FDN

**Gitlab** : Logiciel qui fait fonctionner notre **wiki**

**GPON** (_Gigabit Passive Optical Network_) : voir **PON**

**GUI** (_Graphical User Interface_) : interface utilisateur graphique permettant à l'utilisateur d'interagir avec le système à l'aide d'éléments graphiques tels que des fenêtres, des icônes, des menus.

**ICC** : Interface chaise-clavier, utilisateur humain à l’origine d’un problème informatique

**Ielo** : Opérateur de collecte, dont le rôle est d'en gros de louer le réseau des **OI** pour le fournir ensuite à des **FAI** par exemple, ça nous permet d'avoir accès aux abonnements suivants :
- **xDSL**
- via Bouygues
- via Kosc

**IHM** : Interface homme-machine (ou interactions homme-machine)

**Internet** : réseau informatique mondial accessible au public

**Internet backbone** (_dorsale Internet_) : désigne communément l'ensemble des réseaux informatiques de très haut débit (**backbone**) des différents opérateurs composant l'**internet**

**IRC** (_Internet Relay Chat_) : protocole de messagerie instantanée, compatible avec les autres protocoles (**XMPP** et **Matrix**) sur les salons de FDN, plus d'infos [sur le site](https://www.fdn.fr/participer/irc/)

**Jitsi** : Logiciel libre de visioconférence, service mis à disposition par FDN [ici](https://talk.fdn.fr/)

**L2TP** (_Layer 2 Tunneling Protocol_) : protocole de tunnellisation de niveau 2; protocole réseau utilisé pour créer des réseaux privés virtuels (VPN), le plus souvent entre un opérateur de collecte de trafic (dégroupeur ADSL ou opérateur de téléphonie pour les accès RTC) et les fournisseurs d'accès à Internet.

**LNS** (_L2TP Network Server_) : équipement qui termine un tunnel **L2TP**

**Matrix** : protocole de messagerie instantanée, compatible avec les autres protocoles (**IRC** et **XMPP**) sur les salons de FDN, en tant qu'adhérent·e vous pouvez nous demander à vous créer un compte, voir la page [sur le site](https://www.fdn.fr/fdn-ouvre-son-instance-synapse-matrix-et-la-bridge-a-irc-geeknode/)

**Modem** : [voir la page dédiée](support/faq/modem.md#quest-ce-quun-modem-)

**Modem-Routeur** : **Routeur** intégrant un **Modem**, voir **RGW**

**Modèle OSI** (_Open Systems Interconnection_) : norme de communication de tous les systèmes informatiques en réseau.
Le modèle comporte [sept couches](https://fr.wikipedia.org/wiki/Mod%C3%A8le_OSI#Architecture_en_couches), parfois réparties en deux groupes :
- Les trois couches inférieures sont plutôt orientées communication et sont souvent fournies par un système d'exploitation et par le matériel.
- Les quatre couches supérieures sont plutôt orientées application et plutôt réalisées par des bibliothèques ou un programme spécifique. Dans le monde IP, ces quatre couches sont rarement distinguées. Dans ce cas, toutes les fonctions de ces couches sont considérées comme faisant partie intégrante du protocole applicatif. 

**NRA** : Le Nœud de Raccordement d'Abonnés est un local technique sécurisé où toutes les lignes téléphoniques de la même **boucle locale** sont connectées.

**NRO** (_Nœud de Raccordement Optique_) : local technique qui reçoit les infrastructures d'un opérateur fibre. Un NRO abrite un **OLT** (Optical Line Terminal) dont le rôle est de faire la lien entre les réseaux optiques des **OI** et le réseau de distribution local qui va vers les habitations desservies.

**NTP** (_Network Time Protocol_) : protocole qui permet de synchroniser, via un réseau informatique, l'horloge locale d'ordinateurs sur une référence d'heure. 

**OI** : Opérateur d'Infrastructure, c'est lui qui établit et exploite le réseau Fibre, concrètement il a l'obligation de permettre à d'autres (**FAI** par exemple) d'utiliser le réseau ensuite

**ONT** (_Optical Network Terminal_) : [voir la page dédiée](support/faq/ftth-ont.md)

**OLT** (_Optical Line Terminal_) : [voir la page dédiée](support/faq/ftth-ont.md)

**Opérateurs de transit (ou transitaires)** : gestionnaires de réseaux qui font office d’intermédiaires pour acheminer le trafic entre les différents acteurs de l'internet

**Pad** : Éditeur de texte collaboratif en ligne et en temps réel, service mis à disposition par FDN [ici](https://pad.fdn.fr/)

**PADI / PADO / PADR / PADS / PADT** : les différentes étapes dans l'établissement d'une connexion **PPPoE**
1) Client to server: initiation (PADI) _PPPoE Active Discovery Initiation_.
2) Server to client: offer (PADO) _PPPoE Active Discovery Offer_.
3) Client to server: request (PADR) _PPPoE active discovery request_.
4) Server to client: session-confirmation (PADS) _PPPoE Active Discovery Session-confirmation_.
5) Either end to other end: termination (PADT) _PPPoE Active Discovery Termination_. 

**PB ou PBO** (_Point de Branchement Optique_) : Boîtier situé juste avant le câblage allant chez l'utilisateur·rice. Cet équipement permet de relier le réseau optique du Point de mutualisation (**PM**) à l’intérieur du logement. Il est installé sur un poteau ou sur une façade. Le PBO se présente sous la forme d'un boîtier qui permet de faire transiter la fibre vers plusieurs logements à la fois (entre 3 et 12)

**PM** (_Point de mutualisation_) : endroit où sont connectées les fibres optiques provenant des **NRO** des différents opérateurs d'infrastructures et les fibres optiques allant vers les habitations desservies. A partir de là, le réseau est mutualisé. Il existe plusieurs types de PM selon que le logement est en **zone très dense** ou non : voir **PMI**, **PMR**, **PMZ** et **PRDM**.

**PMI** (_Point de Mutualisation d'immeuble_) : **PM** installé au plus proche des logements dans une **zone très dense** et dessert les immeubles de plus de 12 logements. Il se situe en pied d'immeuble, voire même à l'intérieur de celui-ci dans certains cas.

**PMR** (_Point de Mutualisation de Rue_) : **PM** installé au plus proche des logements dans une **zone très dense** et desservant les immeubles de moins de 12 logements et les pavillons. Le PMR est installé dans la rue et comprend jusqu'à 100 lignes.

**PMZ** (_Point de Mutualisation de Zone_) Dans les **zones moins denses**, le PMZ va desservir un ou plusieurs quartiers. L'installation permet de raccorder entre 300 et 1000 lignes ou plus.

**PON** (_Passive Optical Network_) : réseau optique passif, désigne une architecture fibre point-multipoint passive caractérisée par l’absence d’équipements actifs entre le central et les habitations desservies, et par un partage de la fibre entre plusieurs utilisateurs, dans la partie amont du réseau de desserte.

**PPP** (_Point-to-Point Protocol_) : protocole point à point. Protocole de transmission pour internet qui permet d'établir une connexion entre deux hôtes sur une liaison point à point. Il fait partie de la couche liaison de données (couche 2) du **modèle OSI**.

**PPPoE** (_point-to-point protocol over Ethernet_) : protocole d'encapsulation de **PPP** sur Ethernet

**PRDM** (_Point de Raccordement Distant de Mutualisation_). Ce **PM** est présent surtout en zone rurale quand il y a moins de 1000 lignes à raccorder. Dans ce cas, le PRDM peut même se situer dans le **NRO** situé en amont.

**Proxy Radius** (_Remote Authentication Dial-In User Service_) : Equipement permettant d'authentifier les utilisateurs distants.

**PTO** : Point de Terminaison Optique (ou Prise Terminale Optique), [voir la page dédiée](support/faq/ftth-ont.md)

**Puppet** : outil libre de gestion de la configuration de serveurs, il permet le télédéploiement de configuration sur un ensemble de serveurs en quelques minutes. 

**Réseau cuivre (ou cuivré)** : Le réseau cuivre permet d’accéder à l’internet **DSL**, de passer des appels téléphoniques via le réseau téléphonique commuté (**RTC**) ou encore de connecter certains appareils tels que les téléalarmes ou les téléassistances.  
L’opérateur Orange est propriétaire du réseau cuivre et fermera progressivement celui-ci sur tout le territoire d’ici à 2030 ([carte et calendrier de l'arrêt du cuivre et fin de l'ADSL](https://reseaux.orange.fr/nos-reseaux/modernisation-des-reseaux/evolution-de-la-telephonie-fixe-et-interneturl)).  
Le réseau en fibre optique a vocation à se substituer au réseau cuivre.

**RGW (ou RG ou RGU)** (_residential gateway unit_) : passerelle domestique; équipement informatique servant d'interface entre Internet et le réseau domestique. Le terme "passerelle domestique" désigne plus couramment le **modem-routeur** ou la **box** qui permet de relier un réseau local au réseau Internet. FDN ne fourni pas ces équipements.
 
**Routeur** : [voir la page dédiée](support/faq/routeur-ftth.md#quest-ce-quun-routeur-)

**RT** (_Request Tracker_) : logiciel libre de référence en matière de gestion d’incidents et de suivi d’actions

**RTC** : réseau téléphonique commuté, voir **réseau cuivre**. FDN n'offre pas ce service.

**RTFM** (_Read the fucking manual_) : littéralement « lis le putain de manuel », injonction signifiant que la réponse à une question sur le fonctionnement d'un appareil est à chercher dans son mode d'emploi.

**Système d'exploitation** (_operating system_) : Pour faire simple le logiciel qui est la base de tout sur votre machine, fait le lien entre le matériel, les autres logiciels et vous, abrégé généralement en _OS_ dans le milieu  
Quelques exemples : GNU/Linux, Windows, macOS, FreeBSD, Android, iOS, ...

**SRO** (_Sous-répartiteur Optique_) : voir **PM**

**transit IP** : fait référence à la **bande passante** vendue par les **opérateurs de transit** à **FDN** pour accéder à la totalité d'Internet dans le cadre d'un service contractuel et payant. La tarification est réalisée suivant la méthode du **95e centile** (en fonction du débit maximum consommé lors d'un mois calendaire - mesuré toutes les 5 minutes -, moins les 5% des mesures les plus élevés).

**UI** : interface utilisateur. L’interface utilisateur est la présentation graphique d’une application. Il comprend les boutons sur lesquels les utilisateurs cliquent, le texte, les images, les curseurs, les champs de saisie de texte et tous les autres éléments avec lesquels l’utilisateur interagit.

**UUCP** (_Unix to Unix Copy_) : protocole permettant de transférer des fichiers, du courrier électronique ou bien des news.

**UX** : expérience utilisateur. L’expérience utilisateur est déterminée par la facilité ou la difficulté d’interaction avec les éléments d’interface utilisateur créés par les **UI** designers.

**VDSL** (_Very high speed Digital Subscriber Line_) : voir **xDSL**

**VDSL2** : amélioration du **VDSL**

**VPN** (_Virtual Private Network_) : [Voir la page dédiée](https://git.fdn.fr/fdn-public/wiki/-/blob/master/vpn/README.md#quest-ce-quun-vpn-quelle-est-son-utilit%C3%A9-)

**Wiki** : application web qui permet la création, la modification et l'illustration collaboratives de pages à l'intérieur d'un site web.

**Wordpress (ou WP)**: Logiciel libre qui permet d'alimenter notre [site web](https://www.fdn.fr/)

**xDSL** : ensemble des techniques mises en place pour un transport numérique de l'information sur une ligne de raccordement filaire téléphonique. Terme qui désigne soit ADSL, soit VDSL, les deux technologies de transport utilisant le **réseau cuivré** et permettant d’atteindre des débits importants, mais asymétriques, la différence majeure étant que le VDSL est plus rapide

**XMPP** (_Extensible Messaging and Presence Protocol_) : protocole de messagerie instantanée, compatible avec les autres protocoles (**IRC** et **Matrix**) sur les salons de FDN, en tant qu'adhérent·e vous pouvez nous demander à vous créer un compte

**zone moins dense** (ZMD) : Aussi appelée "hors zone très dense" qui concernent les communes qui n'appartiennent pas aux **ZTD**

**zone très dense** (ZTD) : Les zones très denses sont les communes à forte concentration de population, pour lesquelles, sur une partie significative de leur territoire, il est économiquement viable pour plusieurs opérateurs de déployer leurs réseaux de fibre optique, au plus près des logements.