VPN FDN
===

FDN propose un service VPN qui permettra de faire passer le trafic Internet par le réseau FDN.

[[_TOC_]]

## Qu'est-ce qu'un VPN ? Quelle est son utilité ?

VPN est l'acronyme de Virtual Private Network, soit réseau privé virtuel.

Un VPN permet de réaliser une connexion sécurisée entre des ordinateurs distants au travers d'une connexion non sécurisée (Internet), comme s'ils étaient sur le même réseau local.

Cela revient à changer de FAI. Ainsi, cette configuration

<img src="vpn/IMGS/VPN.png" width="80%">

est de fait équivalente à celle-ci

<img src="vpn/IMGS/VPNb.png" width="80%">

(NB : BOFS = Bouygues-Orange-Free-SFR ;-)

Deux précisions importantes :

- les paquets envoyés sont chiffrés entre l'abonné·e et le FAI fournissant le VPN mais ce dernier a accès aux paquets "en clair" - d'où l'intérêt de choisir un FAI de confiance, respectant la neutralité du net

- un VPN n'anonymise absolument pas - c'est même plutôt le contraire en ce qui concerne les services VPN attribuant des IP fixes via un abonnement.

Voir [wikipedia](https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9_virtuel) pour plus de détails.

## Configurer sa machine pour utiliser l'offre VPN de FDN

La mise en place de l'offre VPN nécessite l'installation d'un serveur VPN du côté FDN et d'un client VPN chez l'abonné. L'équipe VPN de FDN a choisi le serveur [OpenVPN](https://openvpn.net/), il faut donc installer le [client OpenVPN Community Edition](https://openvpn.net/community/) sur le poste à connecter.

Il sera aussi nécessaire pour l'abonné d'indiquer au client VPN comment se connecter au serveur VPN, ce qui se fait via un fichier de configuration.

Une bonne base pour ces fichiers de configuration sont ceux proposés au paragraphe suivant, il n'y a en principe pas besoin de les modifier quelque soit l'OS utilisé: GNU/Linux, Windows, Android, iOS, etc. En tout état de cause, il faut commencer par tester le fonctionnement du VPN avec ces fichiers, avant de vouloir personnaliser certains points, qui seront détaillés plus loin et/ou sont commentés dans ces fichiers de configuration.

Un certificat permettant de s'assurer que c'est bien au serveur VPN de FDN qu'on se connecte est intégré à la configuration, mais vous voudrez éventuellement le récupérer à part.

Quelques ressources utiles (en anglais) :
- [OpenVPN Community Documentation](https://openvpn.net/community-resources/)
- [Installing OpenVPN Community](https://openvpn.net/community-resources/installing-openvpn/)
- [OpenVPN Community Wiki](https://community.openvpn.net/)

### Les fichiers de configuration

#### Si vous avez un compte VPN (VPN avec abonnement)

Utilisez les fichiers suivants :
* le fichier de configuration [fdn-vpn.ovpn](openvpn/client/config-fdn-vpn.md).
* le certificat du VPN de FDN [ca-vpn-fdn.crt](https://www.fdn.fr/assets/files/ca-vpn-fdn.crt) (clic droit, enregistrer sous).

OpenVpn vous demandera votre login et mot de passe attribués à l'abonné VPN, vous pouvez modifier le fichier de configuration pour ne pas avoir à le taper à chaque fois, voir plus loin.

Il n'est pas possible d'utiliser le client VPN en même temps sur plusieurs machines. Si ces machines font partie de votre LAN, une solution est d'installer le client VPN sur votre routeur, voir explications [plus bas](#configuration-pour-un-routeur).

#### Si vous n'avez pas de compte VPN (VPN public)

Vous pouvez utiliser le [VPN public de FDN](https://vpn-public.fdn.fr/) en accès libre, qui vous donnera une IP publique dynamique. Le débit maximal que peuvent atteindre les clients VPN connectés simultanément a été monté à 100Mbps. N'hésitez pas à contribuer à la pérennité de ce service en [faisant un don à l'association](https://www.fdn.fr/dons/) !

Utilisez les fichiers suivants :
* le fichier de configuration [fdn-vpn-public.ovpn](openvpn/client/config-fdn-vpn-public.md)
* le certificat du VPN public de FDN [ca-vpn-public-fdn.crt](https://www.fdn.fr/assets/files/ca-vpn-public-fdn.crt) (clic droit, enregistrer sous)

OpenVpn vous demandera un login et un mot de passe, vous pouvez mettre n'importe quoi, ça passera. Vous pouvez modifier le fichier de configuration pour ne pas avoir à le refaire à chaque fois.

Exemple : rajouter au fichier de configuration

``` conf
auth-user-pass /etc/openvpn/client/open.fdn.fr.conf
```

avec le fichier /etc/openvpn/client/open.fdn.fr.conf contenant en 1ère ligne
le login et en 2ème ligne le mot de passe :

``` conf
vpn@open.fdn.fr
fdnopen
```

Pensez à ne pas laisser ce fichier public en lecture à tous si vous êtes abonné VPN :

``` bash
sudo chown root:root /etc/openvpn/client/open.fdn.fr.conf
sudo chmod 600 /etc/openvpn/client/open.fdn.fr.conf
```

### GNU/Linux

#### Installation du client OpenVPN

Avec Debian, Ubuntu, etc. :

``` bash
sudo apt-get update && apt-get install openvpn
```

#### Configuration du client OpenVPN

* Télécharger le fichier de configuration par exemple dans le fichier `/etc/vpn/fdn-vpn.ovpn`.

* Usage avancé: éventuellement éditer ce fichier, en particulier
  rajouter la directive auth-user-pass vue plus haut

#### Utilisation du VPN FDN en ligne de commande

* Utiliser cette commande avec comme argument le nom du fichier de
  configuration :

``` bash
sudo openvpn /etc/vpn/fdn-vpn.ovpn
```

* Entrer les identifiants de connexion (demandé si pas de directive auth-user-pass)

* La connexion est initialisée. Après un certain temps, une ligne "Initialization Sequence Completed" doit apparaître dans la bash.

#### Utilisation du VPN FDN dès le boot (pour Debian)

Mettre le fichier de configuration VPN dans /etc/openvpn avec le suffixe .conf,
par exemple `/etc/openvpn/fdn.conf`.

Puis démarrer le client (sous systemd) :

``` bash
sudo systemctl enable openvpn@fdn
sudo systemctl start openvpn@fdn
```

Vérification du bon démarrage par l'existence d'une ligne "Initialization Sequence Completed" :

``` bash
sudo systemctl status openvpn@fdn
```

#### Points à surveiller
##### Passage par un proxy HTTP

Il se peut que la connexion dont on a disposition ne donne en fait accès qu'à un proxy HTTP. OpenVpn fournit une option pour passer par cela :

``` conf
http-proxy www.example.com 1234
```

Si ça ne fonctionne pas non plus, il est possible d'utiliser directement l'IP alternative, vpn-rw.fdn.fr, comme serveur vpn en udp ou tcp sur les ports généralement non filtrés (443...)

##### Configuration DNS

Si vous utilisez un VPN total (option `redirect-gateway def1`, activée
par défaut), la gestion du traffic DNS par le VPN peut nécessiter des
réglages sur la machine cliente VPN.

* Soit vous continuez à utiliser le DNS du Fournisseur d'Accès Internet local. Dans ce cas, vous ne souhaiterez probablement pas faire passer le traffic DNS dans le tunnel VPN. Si l'adresse IP des serveurs DNS est dans le préfixe de votre interface réseau, i.e. directement accessible sans passer par la passerelle, il n'y a rien de plus à faire. Si leur adresse IP n'est pas dans ce préfixe, i.e. il faut passer par la passerelle, il vous faut ajouter une route dans le fichier de configuration VPN. Par exemple si le serveur DNS est 192.0.1.1 et la passerelle 192.0.0.1, ajouter:

``` conf
route 192.0.1.1 255.255.255.255 192.0.0.1
```

* Soit vous voulez utiliser les DNS de FDN (80.67.169.12 et 80.67.169.40), vous pouvez les mettre dans `/etc/resolv.conf` , un par ligne avec *nameserver* devant leur adresse. Il vous faudra par contre faire attention à la compétition entre cette modification manuelle (ou automatique par openvpn selon les scripts utilisés), et votre client dhcp usuel qui voudra remettre le DNS du Fournisseur d'Accès Internet local. Il n'est pas possible ici de donner une méthode générale de résolution de ce conflit, surtout pour Linux, vu le nombre important d'outils existants qui veulent contrôler `/etc/resolv.conf` : NetworkManager, resolvconf, openresolv, dnsmasq, systemd-networkd... Voir par exemple https://wiki.debian.org/fr/DHCP_Client et https://wiki.debian.org/fr/NetworkConfiguration#D.2BAOk-finir_les_serveurs_de_noms_de_domaine_.28DNS.29

##### Configuration IPv6

Tout un préfixe /48 est routé sur l'interface réseau virtuelle `tun0`
créée par le client VPN, on peut le voir dans la partie "Abonnements
VPN" de l'[espace adhérent·e](https://vador.fdn.fr/adherents).

Pour utiliser ces adresses ipv6, il faut rajouter dans le fichier de configuration VPN:

``` conf
ifconfig-ipv6 X 2001:910:1301::1
```

où X est une adresse de votre /48.

Les commandes :

``` bash
ip -6 address
ip -6 route
```

permettent de vérifier la présence d'adresses et de routes ipv6 relatives à votre /48.

Ultime test: se connecter à http://ipv6.test-ipv6.com/
Tout doit être au vert, sauf éventuellement les tests DNS.

##### Configuration pour un routeur

On peut vouloir faire passer dans le tunnel VPN le traffic de toutes
les machines d'un LAN. Pour simplifier la configuration des postes de
ce LAN, on peut installer le client VPN sur le routeur.

On a vu [plus haut](#configuration-dns) comment choisir les serveurs DNS (FAI ou FDN) et par
où le traffic DNS doit passer (tunnel ou pas). Une solution pour ne
pas avoir à configurer chaque poste est d'utiliser un serveur DNS
récursif sur le routeur, et qui sera celui utilisé par tous les postes
du LAN, VPN actif ou pas.

###### Cas d'ipv4 

Il suffit (sauf pare-feu très fasciste) de rajouter cette régle :

``` bash
sudo iptables -t nat -I POSTROUTING -o tun0 -j MASQUERADE
```

###### Cas d'ipv6 

Outre la configuration ipv6 déjà vue plus haut, il faut :

1. donner des adresses ipv6 de votre /48 aux machines de votre LAN; il
 a plusieurs façon de faire, celle de l'autoconfiguration avec le
  démon radvd sera décrite, elle couvre aussi le point suivant

2. indiquer à ces machines l'adresse de la passerelle ipv6 (i.e. le routeur)

3. permettre au pare-feu du routeur de laisser passer le traffic ipv6
entre le tunnel et votre LAN

Voyons maintenant ces 3 points un par un.

Point 1 et 2 :

* Installation du serveur `radvd` :

``` bash
sudo apt-get install radvd
```

On va supposer dans la suite que votre /48 est 2001:910:abcd::/48
(à modifier en fonction de votre /48 réel) et que vous avez choisi dans
le fichier de configuration VPN:

``` conf
ifconfig-ipv6 2001:910:abcd::1234 2001:910:1301::1
```

2001:910:abcd::1234/64 sera alors l'adresse ipv6 de l'interface `tun0`.

* Création d'un fichier de configuration `/etc/radvd.conf` :

``` conf
interface br-lan
{
  IgnoreIfMissing on;
  AdvSendAdvert on;
  prefix 2001:910:abcd:1::/64
  {
    AdvOnLink on;
    AdvAutonomous on;
    AdvPreferredLifetime 3600;
    AdvValidLifetime 7200;
   };
};
```

où `br-lan` est l'interface bridge de toutes les interfaces LAN du
routeur; s'il n'y a pas de tel bridge, il faut une directive par
interface LAN.

_Notez bien le 1 après abcd!_ Toute autre valeur non nulle
convient. Ceci est indispensable pour distinguer entre le traffic ipv6
pour le LAN et celui pour `tun0` et assurer le routage.

* Création de 2 scripts (permissions 755) :

/etc/openvpn/up-fdn.sh :

``` shell
#! /bin/sh
systemctl start radvd
ip address add 2001:910:abcd:1::1/64 dev br-lan
```

/etc/openvpn/down-fdn.sh :

``` shell
#! /bin/sh
systemctl stop radvd
ip address del 2001:910:abcd:1::1/64 dev br-lan
```

* Utilisation de ces 2 scripts, indiqué en rajoutant les lignes
  suivantes dans le fichier de configuration VPN :

``` conf
script-security 2
up /etc/openvpn/up-fdn.sh
down /etc/openvpn/down-fdn.sh
```

et au redémarrage du client VPN vos postes sur le LAN devraient voir
apparaître des adresses et routes ipv6, si le point 3 suivant est réglé.

Point 3 :

* S'assurer que le traffic ipv6 pourra être routé entre interfaces
réseau. Pour cela, rajouter dans /etc/sysctl.conf :

``` conf
net.ipv6.conf.all.forwarding=1
```

Puis exécuter :

``` bash
sudo sysctl -p /etc/sysctl.conf
```

* Autoriser le "forwarding" du traffic de l'interface réseau virtuelle
`tun0` vers le LAN. Votre pare-feu ne l'autorisera sans doute pas
automatiquement à la création de `tun0`, il faut donc le lui dire,
typiquement:

``` bash
ip6tables -I FORWARD -i tun0 -j ACCEPT
```

* Vérifier que votre pare-feu accepte le traffic icmpv6 en entrée et
sortie sur les interfaces réseau LAN, ceci pour que `radvd`
fonctionne.

##### Configuration fine du routage

Si vous n'activez pas la route par défaut via le tunnel, vous voudrez sans doute router finement selon la source des paquets, sinon différents effets bizarres surviendront. Pour cela, utiliser :

``` bash
ip route add default dev tun0 table 200
ip rule add from 80.67.160.0/18 table 200
ip -6 route add default dev tun0 table 200
ip -6 rule add from 2001:910:1000::/38 table 200
ip -6 rule add from 2001:910:0802::/48 table 200
```

Ce qui va bien faire rentrer dans le tunnel tout ce qui utilise votre IP VPN.

##### Lancer le VPN pour certaines applications uniquement

Les noyaux récents supportent les Net Namespaces (CONFIG\NET\NS=Y). Cela permet de définir des interfaces et règles réseaux disponibles exclusivement pour un groupe de processus que l'on définit.

On peut décider, par exemple, de lancer un navigateur/client mail/client IRC passant par le VPN FDN.

Quelques commandes commentées ci-après, à exécuter dans cet ordre

``` bash
ip netns add vpnfdn # crée un net namespace vpnfdn
ip link add veth0 type veth peer name veth1 # crée une paire veth (virtual ethernet)
ip link set veth0 netns vpnfdn # on assigne le côté veth0 de la paire au netns vpnfdn. L'autre côté reste dans le netns principal
```

Ici, on a créé un lien entre les deux netns : les paquets envoyés sur `veth1` sortent sur `veth0` et vice versa. Maintenant, comme on veut accéder aux internets pas-forcément-neutres de notre FAI historique pour pouvoir contacter le serveur VPN de FDN, on va permettre une connexion. Ceci se fait avec un bridge. On bridge ensemble veth1 et notre interface réseau physique. Ainsi, veth1 verra les paquets passant sur l'interface réseau physique et les transmettra. Comment cela est obtenu varie selon les distributions GNU/Linux. Je le laisse en exercice au lecteur (debian: `brctl addbr br0; brctl addif br0 eth0; brctl addif br0 veth0; ifconfig eth0 0.0.0.0; dhclient br0;`)

Une fois le bridge établi, on va pouvoir avoir du réseau dans le netns vpnfdn... quand il aura des routes. On va ouvrir un shell dedans

``` bash
ip netns exec vpnfdn bash
```

On peut lancer un multiplexeur de terminaux pour permettre de récupérer ce contexte réseau ensuite.

Les commandes suivantes se lancent dans le shell qu'on vient de lancer. On va monter l'interface lo qui est par défaut éteinte dans les netns puis faire une requête DHCP sur la veth0.

``` bash
ifconfig lo up
dhclient veth0
```

Une fois cela fait, on peut lancer openvpn avec notre fichier de configuration pour FDN

``` bash
openvpn fdn.conf
```

On peut ensuite vérifier sa configuration réseau

``` bash
ip ro ls
wget -qO - http://ifconfig.me/ip
```

...et lancer un navigateur ou toute autre application.

Lancer un multiplexeur de terminal type tmux ou screen permet de récupérer la session rapidement et de lancer plusieurs processus sans avoir à jongler avec fg, bg et les redirections de sortie.

#### Utilisation du VPN en mode graphique : avec Network Manager

Network Manager est le logiciel de gestion de connexion (réseaux filaires, wifi) par défaut sous Gnome et peut être utilisé sous d'autres environnements de bureau. Il gère aussi le montage d'un tunnel VPN. C'est l'un des clients les plus populaires.

##### Installation du greffon NetworkManager pour OpenVPN

Le greffon pour openvpn de Network Manager n'est peut-être pas installé, cherchez les paquets "network-manager-openvpn" et "network-manager-openvpn-gnome" ou ayant un nom similaire, et installez-le. Ensuite, redémarrez le démon pour charger le greffon.

Par exemple, avec Debian ou Ubuntu :

``` bash
sudo apt-get install network-manager-openvpn network-manager-openvpn-gnome
sudo systemctl restart NetworkManager
```

##### Configuration du client OpenVPN via Network Manager

Voici maintenant la procédure pour installer la configuration.

* Enregistrer sur votre disque le [fichier de configuration fdn-vpn-networkmanager.ovpn (VPN avec abonnement)](openvpn/client/config-fdn-vpn-networkmanager.md) ou le [fichier de configuration fdn-vpn-public-networkmanager.ovpn (VPN public)](openvpn/client/config-fdn-vpn-public-networkmanager.md) selon ce que vous souhaitez utiliser.

* Clic droit sur l'icône Network Manager -> "Modifications des connexions", puis onglet "VPN" -> "Importer une configuration VPN enregistrée" et retrouver le chemin du fichier de configuration VPN ci-dessus.

* Le fichier de configuration proposé est un peu différent de celui en
  ligne de commande vu plus haut, car ne contenant pas les sections
  délimitées par les tags `<connection>` et `/connection>`. Ces tags ne
  sont pas reconnus, et provoquent un message d'erreur contenant
  "unsupported blob/xml element".

* En haut de la fenêtre, vous pouvez renommer la connexion (au hasard : "Connexion VPN FDN").
* Insérer les identifiants comme suit :

    - le nom d'utilisateur : de la forme xxx@vpn.fdn.fr si vous avez un abonnement, ce que vous voulez si vous utilisez le VPN public en libre accès

    - le mot de passe, à mémoriser parfaitement et ne pas écrire sur un post-it en dessous du clavier, ce que vous voulez si vous utilisez le VPN en libre accès, le serveur ne vérifie rien.


* Une fois l'ordinateur connecté à son réseau physique classique et habituel (en Ethernet filaire sur le réseau de la fac, en wifi, en fibre optique, en mobile via une clé 3G, via n'importe quel réseau habituel en fait), il n'y a plus qu'à activer le VPN d'un simple clic sur "Connexions VPN" -> "Connexion VPN FDN".

* Network Manager établit alors le tunnel sécurisé entre la machine et le réseau de FDN. Au niveau visuel l'apparition d'un cadenas au-dessus de l'icône habituelle de Network Manager indique que la connexion débouche sur un VPN.

* Il est possible de le vérifier en ligne de commande également : la commande "ip address" (nécessite d'être root) montre une nouvelle interface réseau "**tun0**" (pour tunnel 0) ayant pour adresse attribuée une IP du bloc **80.67.179.xxx**, qui est le bloc d'adresses réservées pour le service VPN de FDN
* Si la connexion ne s'établie pas et qu'il y a dans /var/log/syslog une ligne de la forme  <nm-openvpn[xxx]: VERIFY ERROR: depth=0, error=CA signature digest algorithm too weak>, il faut modifier le fichier /etc/NetworkManager/system-connections/Connexion\ VPN\ FDN.nmconnection et rajouter dans le bloc [vpn], juste après la ligne ca=/chemin/vers/ca-vpn-public-fdn.crt la ligne suivante, tls-cipher=DEFAULT:@SECLEVEL=0 puis relancer le service avec la commande sudo systemctl restart NetworkManager.

Voici une [version pas à pas pour MATE](openvpn/client/mate.md).

Voici des versions pas à pas qui semblent être obsolètes  :

* [Version pas-à-pas kde avec captures d'écran](openvpn/client/kde.md)
* [Version pas-à-pas gnome avec captures d'écran](openvpn/client/gnome.md)


### Windows (7/8/10 et 11)

#### Installation et configuration du client OpenVPN Community Edition

1. Téléchargez le fichier MSI correspondant à votre système du [client OpenVPN Community Edition](https://openvpn.net/community-downloads/).
4. Ouvrez et démarrez l'assistant de configuration
5. Accordez les autorisations d’installation sur votre système d’exploitation Windows.
6. Complétez l'assistant de configuration d'OpenVPN GUI.
7. Le logo OpenVPN s'affiche dans votre barre d'état (en bas à droite) avec le statut DÉCONNECTÉ.
8. Cliquez sur l'icône pour démarrer la visite d'intégration.
9. Découvrez comment importer un profil à partir d'un serveur en saisissant le nom d'hôte et les informations d'identification du serveur d'accès ou en téléchargeant un profil depuis votre ordinateur.
10. Acceptez les politiques d'utilisation et de conservation de la collecte de données après les avoir examinées.
11. Importez un profil, soit depuis le serveur, soit depuis un fichier.
12. Choisissez « Importer à partir d'un fichier ».
13. Faites glisser et déposez le [fichier de configuration fdn-vpn.ovpn](openvpn/client/config-fdn-vpn.md) si vous avez un compte VPN avec abonnement ou le [fichier de configuration fdn-vpn-public.ovpn](openvpn/client/config-fdn-vpn-public.md) si vous utilisez le VPN public, ou cliquez sur Parcourir pour accéder à l'emplacement sur votre ordinateur.
14. Le message indique que le profil a été importé avec succès et affiche le nom d'hôte et le titre. Vous pouvez modifier le titre si vous le souhaitez.
15. Cliquez sur Ajouter pour terminer l'importation.

Lors de la connexion, un login et un mot de passe vous sont demandés :
- Si vous n'avez pas de compte @vpn.fdn.fr, vous pouvez compléter avec ce que vous voulez, par exemple `Edward` et `Snowden`. 
- Si vous avez compte @vpn.fdn.fr, vos identifiants sont disponibles dans votre [espace adhérent](https://vador.fdn.fr/adherents/) (Abonnements et services - Abonnements VPN)

### Téléphone

Votre téléphone peut éventuellement prendre un fichier .ovpn, vous pouvez utiliser [ce fichier fdn-vpn-public.ovpn](vpn/openvpn/client/config-fdn-vpn-public.md) pour le VPN public de FDN en libre accès, ou [ce fichier fdn-vpn.ovpn](vpn/openvpn/client/config-fdn-vpn.md) pour votre VPN avec abonnement.

> TODO: tester avec différents OS

#### Android

Sur le dépôt [fdroid](https://f-droid.org/), vous pourrez trouver comme client VPN l'application **OpenVpn for android**. Une fois installée, il suffit de l'ouvrir et d'y importer [le fichier fdn-vpn-public.ovpn](vpn/openvpn/client/config-fdn-vpn-public.md) pour le VPN public de FDN, ou [le fichier fdn-vpn.ovpn](vpn/openvpn/client/config-fdn-vpn.md) pour votre VPN avec abonnement. Le VPN de FDN est alors configuré.

En cliquant sur ce VPN, vos identifiant et mot de passe sont demandés : le VPN s'active une fois qu'ils sont renseignés.

Lire la FAQ et la documentation pour plus d'informations.

#### iOS

Testé et Validé sur iOS 8.1

On peut utiliser l'application OpenVPN pour configurer openvpn. Pour la télécharger: <https://itunes.apple.com/fr/app/openvpn-connect/id590379981?mt=8> ou via l'App Store.

Il suffit de récupérer le fichier de configuration sur votre ordinateur : [fdn-vpn-public.ovpn](vpn/openvpn/client/config-fdn-vpn-public.md) pour le VPN public de FDN en accès libre ou [fdn-vpn.ovpn](vpn/openvpn/client/config-fdn-vpn.md) pour le VPN avec abonnement, d'ouvrir l'éditeur de texte (TextEdit) et de bien penser à sélectionner "Convertir au Format Texte" dans le menu Format. Puis de copier les lignes de configuration (attention de bien copier jusqu'au "\# 8←---------------------" sinon le fichier sera coupé au niveau du certificat et la configuration échouera.

Puis enregistrer le fichier avec l'extension .ovpn Ouvrir iTunes, naviguer dans votre téléphone puis dans App et en sélectionnant OpenVPN il est possible d'importer un fichier, il suffit de sélectionner le fichier .ovpn fraîchement créé.

Il n'est pas nécessaire de copier le certificat car celui-ci est déjà inclus dans le fichier de configuration.

Il ne reste plus qu'à lancer l'application OpenVPN, il trouvera le fichier de configuration.

### MacOS X

Nota : le client OpenVPN Community Edition n'est pas disponible pour les ordinateurs macOS X, vous pouvez utiliser à la place le client Tunnelblick

#### Installation et configuration du client Tunnelblick

3. Téléchargez le fichier DMG du [client Tunnelblick](https://tunnelblick.net/downloads.html#releases) 
4. Ouvrez le fichier et double-cliquez sur l'icône de l'application pour commencer l'installation.
5. Accordez les autorisations d'installation sur votre Mac en saisissant vos informations d'identification lorsque vous y êtes invité.
6. Cliquez sur Fermer lorsque vous recevez le message "L'installation a réussi".
7. Une fois l'installation terminée, lancez Tunnelblick. La page de bienvenue de Tunnelblick s'affiche et demande si vous avez des fichiers de configuration. Cliquez sur le bouton "I have configuration file".
8. Faites glisser et déposez dans l'onglet Configuration le [fichier de configuration fdn-vpn.ovpn](openvpn/client/config-fdn-vpn.md) si vous avez un compte VPN avec abonnement ou le [fichier de configuration fdn-vpn-public.ovpn](openvpn/client/config-fdn-vpn-public.md) si vous utilisez le VPN public.
9. Une fenêtre contextuelle s'affiche, vous demandant si vous souhaitez installer la configuration pour tous les utilisateurs ou seulement pour vous. Sélectionnez l'option souhaitée.
10. Accordez les autorisations d'installation sur votre Mac en saisissant vos informations d'identification lorsque vous y êtes invité.
11. Après cela, sélectionnez le fichier de configuration et cliquez sur Connecter.

Lors de la connexion, un login et un mot de passe vous sont demandés :
- Si vous n'avez pas de compte @vpn.fdn.fr, vous pouvez compléter avec ce que vous voulez, par exemple `Edward` et `Snowden`. 
- Si vous avez compte @vpn.fdn.fr, vos identifiants sont disponibles dans votre [espace adhérent](https://vador.fdn.fr/adherents/) (Abonnements et services - Abonnements VPN)

#### Précédent tutoriel (possiblement obsolète)

[Tutoriel pas à pas avec captures](vpn/openvpn/client/macos.md)

Testé et validé sur MacOS 10.9.5, 10.10.4, 10.11 et Mac OS X High Sierra 10.13.6.

On peut utiliser Tunnelblick pour configurer openvpn, son téléchargement s'effectue depuis [le site officiel](https://tunnelblick.net/).

La configuration prendra moins de 3 minutes, cependant voici quelques petits pièges à éviter :

* Il suffit de récupérer le fichier de configuration [fdn-vpn-public.ovpn](openvpn/client/config-fdn-vpn-public.md) pour le VPN public en libre accès, ou [fdn-vpn.ovpn](openvpn/client/config-fdn-vpn.md) pour le VPN avec abonnement, d'ouvrir l'éditeur de texte (TextEdit) et de bien penser à sélectionner "Convertir au Format Texte" dans le menu Format //avant// de coller le contenu du presse-papier. Puis de copier les lignes de configuration (attention de bien copier jusqu'au *# 8<----------------------* sinon le fichier sera coupé au niveau du certificat et la configuration échouera), et d’enregistrer le fichier avec l'extention .ovpn. Il ne reste plus qu'à double-cliquer sur le fichier .ovpn fraîchement créé et celui-ci sera ajouté automatiquement dans les configurations de Tunnelblick.

* Il n'est pas nécessaire de copier le certificat car celui-ci est déjà inclus dans le fichier de configuration.

### Raspberry lan

[Version pas-à-pas avec captures d'écran](openvpn/client/raspberry_lan.md)
[La page originale de l'auteur](https://git.fdn.fr/mlrx/fdn-vpn_abo___Rpi_lan_memo/-/blob/main/fdn-vpn_abo___rpi_lan_memo.md). Merci à lui.

### Yunohost et la brique internet

Préalable : vous devez disposer d'une [brique internet](https://www.fdn.fr/brique-internet/) et d'un compte [VPN avec abonnement](https://www.fdn.fr/services/vpn/).

Le fichier .cube requis pour l’[installation](https://yunohost.org/fr/install/hardware:internetcube) de la brique internet est accessible dans l’[espace adhérent·e](https://vador.fdn.fr/adherents/).

## Amélioration de la configuration

La configuration fournie par défaut devrait fonctionner dans la grande majorité des situations. Il y a certaines
choses qu'un utilisateur pourrait vouloir changer :

* Les clients OpenVPN récents vont tenter si possible une connexion
  ipv6 au serveur; ceci peut échouer si l'ipv6 de votre FAI est en
  fait inutilisable (cas d'Orange). Il faut alors forcer le protocole
  ipv4 en rajoutant dans le fichier de configuration la ligne:

``` conf
proto udp4
```

* Par défaut, une fois le tunnel lancé tout le trafic Internet passe par le VPN. On peut vouloir ne faire passer que certaines destinations, en commentant `redirect-gateway def1`, et en décommentant et corrigeant la ligne `route`.



## Un souci ?

En cas de souci, n'hésitez pas à envoyer un mail à support[at]fdn.fr, en incluant dans votre mail la sortie des commandes

``` bash
ip route
ip -6 route
ip address
```

Si dans le log d'openvpn vous voyez à répétition

`SENT CONTROL [_.fdn.fr]: 'PUSH_REQUEST' (status=1)`

peut-être que vous venez d'être déconnecté·e violemment de la précédente session, et il faut attendre quelques minutes, pour que la précédente meure vraiment avant de pouvoir en établir une nouvelle. Si le problème persiste, contacter support[at]fdn.fr pour que l'on voie à tuer la session fantôme.

## Accéder à Internet dans les situations les plus difficiles

[FDN renforce son VPN](https://www.fdn.fr/fdn-renforce-son-vpn), méthode de connexion pour tenter de contourner des réseaux interdisant l'usage du VPN ou passer outre la censure.

## Connaitre la configuration du serveur OpenVPN

Si vous souhaitez connaître la configuration du serveur OpenVPN de FDN, [c'est par ici](openvpn/serveur/README.md).
