Serveur VPN FDN
===

La config `/etc/openvpn/serveur-tcp.conf` :

```conf
# On veut écouter en v6 aussi
proto tcp6-server
# On fait du L3, pas du L2
dev tun
# On active l'ipv6
tun-ipv6
push tun-ipv6

# Les clés pour que le client soit sûr de se connecter au bon serveur
ca /etc/ssl/private/star.fdn.fr.chain
cert /etc/ssl/private/star.fdn.fr.crt
key /etc/ssl/private/star.fdn.fr.key
dh dh2048.pem

# Pas de gestion de pool: les ips viennent toujours de radius
server 80.67.179.0 255.255.255.0 nopool
ifconfig-ipv6 2001:910:802:0:ffff::1/64 2001:910:802:0:ffff::1
topology subnet
# Les options par client (ip, routes, etc.) venant de radius sont stockées ici
client-config-dir ccd

# On fournit du DNS 
push "dhcp-option DNS 80.67.169.12"
push "dhcp-option DNS 80.67.169.40"

# On veut détecter les clients morts (surtout en udp)
ping 5
ping-exit 60
push "ping 5"
push "ping-restart 60"

# Garder la plupart des choses actives
persist-key
persist-tun

status /var/log/openvpn-tcp-status.log 1
verb 1

# authentification radius
client-cert-not-required
username-as-common-name
plugin /usr/lib/openvpn/radiusplugin.so /etc/openvpn/radiusplugin.cnf

# scripts pour ajouter les routes /32 et l'accounting
script-security 2
client-connect /etc/openvpn/client-connect
client-disconnect /etc/openvpn/client-disconnect
up /etc/openvpn/up
learn-address /etc/openvpn/client-learn-address

# Pour pouvoir tuer un client:
# socat /var/run/openvpn.management.serveur-tcp EXEC:"echo kill foo@vpn.fdn.fr"
management-client-user root
management /var/run/openvpn.management.serveur-tcp unix
```

Pour udp, c'est tout pareil, sauf qu'on remplace `proto tcp6-server` par :

```conf
proto udp6
push "explicit-exit-nofity"
```

Et on change le chemin du log et la socket de gestion :

```conf
status /var/log/openvpn-udp-status.log 1
management /var/run/openvpn.management.serveur-udp unix
```

On utilise le plugin openvpn-auth-radius, avec des patchs pour supporter que l'ipv6 remonte par radius :
* {{:travaux:vpn_misc:framed-ipv6-address.patch.txt|Add Framed-IPv6-Address support.}}{=mediawiki}
* {{:travaux:vpn_misc:framed-ipv6-route.patch.txt|Add Framed-IPv6-Route support.}}{=mediawiki}


Et quelques correctifs :
* {{:travaux:vpn_misc:fix-fd-leak.patch.txt|There is a file descriptor leak in an errors path in AccountingProcess.cpp.}}{=mediawiki}
* {{:travaux:vpn_misc:disable-acct.patch.txt|Disable sending start/stop accounting events, to avoid suspending trafic when first radius server is off.}}{=mediawiki}
* {{:travaux:vpn_misc:iroute_mask.patch.txt|Fix iroute netmask computation}}{=mediawiki}

Pour `radiusplugin.cnf` :

```conf
NAS-Identifier=vpn
Service-Type=5
Framed-Protocol=
NAS-Port-Type=5
NAS-IP-Address=80.67.169.45
OpenVPNConfig=/etc/openvpn/serveur-udp.conf
subnet=255.255.252.0
p2p6=2001:910:1301::1
overwriteccfiles=true
useauthcontrolfile=true
server
{
  acctport=1813
  authport=1812
  name=80.67.169.41
  retry=1
  wait=1
  sharedsecret=çateregardepas
}
server
{
  acctport=1813
  authport=1812
  name=80.67.169.42
  retry=1
  wait=1
  sharedsecret=çateregardetoujourspas
}
```

`/etc/openvpn/up` :

```sh
#!/bin/sh
dev="$1"
ip -6 addr add 2001:910:1301::1/64 dev $dev
true
```

`/etc/openvpn/client-connect` :

```sh
#!/bin/sh
set -e
[ -z "$ifconfig_pool_remote_ip" ] || ip route add $ifconfig_pool_remote_ip/32 dev $dev proto static
[ -z "$ifconfig_ipv6_pool_remote_ip" ] || ip route add $ifconfig_ipv6_pool_remote_ip/128 dev $dev proto static metric 1024
[ -z "$ifconfig_pool_remote_ip6" ] || ip route add $ifconfig_pool_remote_ip6/128 dev $dev proto static metric 1024
```

`/etc/openvpn/client-disconnect` :

```sh
#!/bin/sh
set -e
[ -z "$ifconfig_pool_remote_ip" ] || ip route del $ifconfig_pool_remote_ip/32 dev $dev proto static || true
[ -z "$ifconfig_ipv6_pool_remote_ip" ] || ip route del $ifconfig_ipv6_pool_remote_ip/128 dev $dev proto static metric 1024 || true
[ -z "$ifconfig_pool_remote_ip6" ] || ip route del $ifconfig_pool_remote_ip6/128 dev $dev proto static metric 1024
```

Pour avoir openvpn exposé sur tous les ports, on utilise un bête :

```bash
iptables -t nat -A PREROUTING -p tcp -d $CATCHALL4 -j DNAT --to-destination $VPN4:1194
iptables -t nat -A PREROUTING -p udp -d $CATCHALL4 -j DNAT --to-destination $VPN4:1194
```

Et aussi, pour que les gens puissent utiliser l'option openvpn `remote-random-hostname` pour être sûr d'avoir un équilibrage de charge, on met une entrée DNS wildcard :

```
*.vpn IN CNAME vpn
```