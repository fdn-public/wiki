VPN FDN sous KDE
===

Cette page fait partie de la [documentationVPN](../../README.md).

> Cette page a été récemment écrite et nécessite sans doute une relecture.

[[_TOC_]]

## Configuration du client OpenVPN

### Via Network Manager

* Clic gauche sur le gestionnaire de réseau (soit comme sur l'image une icône de prise réseau avec un câble, soit encore un symbole wifi par exemple, ceci dépandant de la façon dont vous êtes connectés au réseau), puis en bas à droite, cliquer sur "Gérer les connexions".

![todo description image](image/kde_network_manager_etape_1.png)

* Dans la fenêtre de gestion des connexions, dans l'onglet "VPN", cliquer sur "Ajouter", et sélectionner dans le menu déroulant "OpenVPN" (que ce soit pour le VPN public de FDN ou pour le VPN avec abonnement)

![todo description image](image/kde_network_manager_etape_2.png)

* Saisir ensuite les informations suivantes :
  * Modifier si besoin le nom de la connexion, par exemple "Connexion VPN FDN",
  * Passerelle : si vous avez un compte VPN, vpn.fdn.fr , si vous utilisez le VPN public de FDN, open.fdn.fr ,
  * Type de connexion : sélectionner "**Mot de passe**" dans le menu déroulant,
  * Fichier d'AC : indiquer le fichier dans lequel on aura préalablement copié le contenu du [certificat de FDN](travaux/vpn_misc/doc/openvpn/certificat.md) si vous avez un compte VPN, ou le [certificat openbar de FDN](travaux:vpn_misc:doc:openvpn-open:certificat) si vous utilisez le VPN public de FDN ,
  * Nom d'utilisateur et mode passe : si vouez utilisez un compte VPN, le login et mot de passe disponibles sur votre page de compte adhérent FDN, rubrique "abonnement VPN".

La configuration est terminée ; cliquer sur OK pour fermer les deux
fenêtres ouvertes.

![todo description image](image/kde_network_manager_etape_3.png)

* Cliquer à nouveau sur l'icône du gestionnaire réseau, comme lors de la première étape. La connexion au VPN FDN doit apparaître. Cliquer sur cette connexion (dans la partie droite) pour établir la connexion. Sur la partie gauche, la connexion VPN FDN doit indiquer "connectée". On peut également démarrer Konsole et taper la commande //ifconfig// ; L'interface réseau //tun0// doit apparaître avec l'adresse IP attribuée par FDN, du type 80.67.179.xxx.

![todo description image](image/kde_network_manager_etape_4.png)
