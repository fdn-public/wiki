/!\ Attention : fichier obsolète, voir https://git.fdn.fr/fdn-public/wiki/-/blob/master/vpn/README.md /!\  


fdn-rsf.ovpn

```conf

# Copier-coller à partir d'ici, et modifiez la partie auth-user-pass si vous ne voulez pas 
# avoir à taper votre mot de passe à chaque fois.
# 8<----------------------

# C'est nous qui prenons l'initiative de nous connecter au serveur.
client

# On route de l'IP, on ne fait pas de l'ethernet.
dev tun

# Il est préférable d'utiliser udp, le résultat fonctionne mieux. Il est
# cependant notable que les restrictions d'accès Internet laissent souvent
# plus facilement passer tcp. Essayez donc udp, et seulement s'il ne fonctionne
# pas, essayez tcp.
#proto udp
proto tcp

# Certains réseaux ont en fait une MTU bien inférieure à 1450. Dire aux connexions
# TCP d'être très conservatives, pour que ça marche plus ou moins partout.
mssfix 1300

# En UDP, on peut s'assurer que ça passe de toutes façons en fragmentant au besoin
# quand ça dépasse.
# fragment 1300

# Idéalement, ça devrait être détecté tout seul, mais c'est loin de toujours fonctionner...
# mtu-disc yes

# En udp, Prévenir le serveur quand on termine, permet de relancer
# immédiatement sans attendre que le serveur se rende compte de la
# déconnexion par timeout.
#explicit-exit-notify

# L'adresse du serveur. Normalement le port devrait être 1194, mais 443 passe plus facilement partout.
remote vpn-rsf.fdn.fr 443

# Éventuellement, on peut avoir besoin de passer par un proxy http, décommenter cette ligne en mettant l'adresse et le port du proxy.
#http-proxy 192.0.2.1 8080

# Pour windows: utiliser route.exe.
route-method exe

# Attendre un peu avant d'ajouter les routes.
route-delay 2

# Ne pas utiliser un port local statique, on est client de toutes façons.
nobind

# Garder la clé en mémoire, pour ne pas avoir besoin de la relire lors d'un
# redémarrage.
persist-key

# Ne pas tuer l'interface du tunnel lors d'un redémarrage.
persist-tun

# Décommenter cette ligne pour faire passer tout le trafic via le VPN:
redirect-gateway def1

# On peut aussi vouloir plutôt router seulement quelques destinations, par
# exemple ici tout Gitoyen:
#route 80.67.160.0 255.255.224.0

# Envoyer un login et un mot de passe. Pour éviter de taper à la main login
# et mot de passe, vous pouvez ajouter à droite de "auth-user-pass" le nom d'un
# fichier contenant ces deux informations, une par ligne.
auth-user-pass

# Un minimum de debug, c'est toujours bien.
verb 3

# Certificat permettant de vérifier que c'est bien à FDN que
# l'on se connecte et donc à qui on donne notre mot de passe.
<ca>
-----BEGIN CERTIFICATE-----
MIIFbTCCA1WgAwIBAgIJAJvb3YREm3Y9MA0GCSqGSIb3DQEBBQUAME0xCzAJBgNV
BAYTAkZSMQ8wDQYDVQQIDAZGcmFuY2UxETAPBgNVBAcMCEJvcmRlYXV4MQwwCgYD
VQQKDANSU0YxDDAKBgNVBAMMA3JzZjAeFw0xMzA5MDkxMjM5NDZaFw00MTAxMjUx
MjM5NDZaME0xCzAJBgNVBAYTAkZSMQ8wDQYDVQQIDAZGcmFuY2UxETAPBgNVBAcM
CEJvcmRlYXV4MQwwCgYDVQQKDANSU0YxDDAKBgNVBAMMA3JzZjCCAiIwDQYJKoZI
hvcNAQEBBQADggIPADCCAgoCggIBALbC8FPL8Od4jkE5gu7gAfqE2XrbrbF6pWRv
8vQiYWKyPBto3Xpo9tI5ymaRoXPcKEXFqpqgjC2Ai5wOnZWGzmZHbnmzd9fmq+dj
4aM6R8HeAr3U0dSsl76mzSn0dKjLdORbOFgzda0a+f6ctuj+X4/oI1i8djHiv3zF
r/Sx/4XCHpzqIRzSTnMX6r29iIWWtm2wlEdbQg81zsPQLOZdrUqpjF95yoo7noY+
dLqaCa7m3g5qFGfft58WlmLopcfTPAUEShUcmSjXV+ulObwi3tbtv9dBlXIemZpY
oji8y9788TCTZI3FuG0DNce9GcNhSKvR/bojRlxQrndgSfn1NxeRrbu8rdc89kIv
1ACdD7ohqarOE1dSvIDsxDVUYv0KlDWSyVnf7RDhCm1srWrruklJBzUCpOYoStc0
Z46I7eC/V3HLZaAwYJ2mFvfCM17zk4vTvnmcMhvDU2VFUcmxCnhwuwwTtp52RSck
wJ6XPh03PoWknU2lmGL4gziicXiRfmvZaveLB+nv8szpNwiWTl/hVyz+eDd4Ir9d
kWvXw5xOK+ePwtZMoQwA0fh6j4i35wJnLlbXA8wQisUEhlGD4cBnZ76+Ng9zSMTQ
d87aq0xjUy5SLnJH+GjRs8gS+5AXbBDdVcPz0k8L6SMZ1n66gk62NlHg/JaPsEJT
W17bVjyHAgMBAAGjUDBOMB0GA1UdDgQWBBTHCHVYLhBV9n5mLSxjKgcGnpsVyzAf
BgNVHSMEGDAWgBTHCHVYLhBV9n5mLSxjKgcGnpsVyzAMBgNVHRMEBTADAQH/MA0G
CSqGSIb3DQEBBQUAA4ICAQApCoq7h8efk+QLVo3SgfhHwEJNtdH5WgqOpSr5lpUT
90JJGVrVYOnKAx5v/DMNE+aADGKiy5F7KIeh4avR3h4E+KEBlzA8Y8BVUV+Wt2CL
zIeHrFWy6iBDVSWjEhNGGOlXatKzKuKo8/heotcg1zLBQRo1azRixmQFfKpH8pFp
LryXJSGN1tuhZ6a8os6Loqj2ztFEwM9VdPyxpyHCtaRXSPVrpM4Ga/BLz5GSY04u
pMnTFN5XDL/jKVOWL6NiMRQ9ixtSsonNcj/p2bPNF7YT85iZJCK//fkbMM+8FoT+
TPRIu+mKtjZtoYUgaZHzBgRdeTGpS5KHy88gF1sEgZrN00D45PxFsHty+91qX6RX
hKFw7EGLXLrWllOndGsszUWv4Fyx1FKLJkEqGr3UN6xQx2Df9/sEUP+wK6sbYese
mrblhOLEJY+uMY6nnwNJbK8FoWPL6xbpDFxn9Piuzx1Kg2OtJbpG1BzXhSCwvIe7
Gff3A38a4tDJgj49ZKX1CL92FcTnE3IwtQNn6VkrJtgjq2jon39AeqBgC40w2oB4
pd7BWsUnK5x2lIAw/WcaDTMquNY4VuNGXYSyUpAuHmtsGF7C11VkFjnD0muXh9W4
egBnqJ6iFjjvk/DETYCgJ4IoDKjCcowjz/0vvzPnSddNROmgFDhFaUs6UmwAModI
rQ==
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFhDCCA2ygAwIBAgIBADANBgkqhkiG9w0BAQUFADBNMQswCQYDVQQGEwJGUjEP
MA0GA1UECAwGRnJhbmNlMREwDwYDVQQHDAhCb3JkZWF1eDEMMAoGA1UECgwDUlNG
MQwwCgYDVQQDDANyc2YwHhcNMTMwOTA5MTI0MDU1WhcNNDEwMTI1MTI0MDU1WjBB
MQswCQYDVQQGEwJGUjEPMA0GA1UECAwGRnJhbmNlMQwwCgYDVQQKDANSU0YxEzAR
BgNVBAMMCnJzZi5mZG4uZnIwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoIC
AQDJ0pLqMkKvRVnOaVvnY76LBJXv/ZhG3NinJo9LVoQ+idp19i2X9kd+EOyUbp90
Mr7Y8gyADnrqMsUmS16HmSbWwmiwHNtLkGLvsKiSzc8wrmv3fAwXrIZGBAOqnpyT
iowvcIm8MvNiu0ckttG8l9X/c2v4naTg+Lavf+jxIOYT4VKrJjQOMlnFop2nMCaH
EHouPHwsg/3xM1au4R8+0+4p6ruIkJQ3WODXEvqmtge0z0xBdz5RDOKplAN5T+00
S8RuvjG10ZlwP0+4I0x8cBq1Lzm9ofhsVfLD/zrUcqG+lCRexl4pe7gBqDSlzPy8
YBSX38LLcWchqLZEylY0Ob6DYVBtrMPDM6V+jHEAGywSFC1sFTK7WozWyCeiNw3h
ZxzeRkDSooN4LAKThApET/5yXOXkiIN7KURayBTuvvjCXcjeD+zJtlDxdXsZ69KC
aSXYrYA9K26CV8mwRHxOX9rEzwGkEPuKYfs+DRmmq7B37Kd1ZDpAqx5cr7VqiUfJ
Jsnb7a51PkT5DNPP7qTFa5p6s76RMoPKR13E265iZGoujE8REJ53QxDtgsTkpASe
UC5klJIZE1RnLe6V53bva8qlwkUMmBXcZc06RslXY5Ts2YJlgMbeC1MAatRSXIM1
GOmpmZTH1dzOy6IxjRBhuX36B+3t2X9uasWfUZUT9HGofwIDAQABo3sweTAJBgNV
HRMEAjAAMCwGCWCGSAGG+EIBDQQfFh1PcGVuU1NMIEdlbmVyYXRlZCBDZXJ0aWZp
Y2F0ZTAdBgNVHQ4EFgQUM8Kr2r+il3CmzGy8x+sdRF8dg08wHwYDVR0jBBgwFoAU
xwh1WC4QVfZ+Zi0sYyoHBp6bFcswDQYJKoZIhvcNAQEFBQADggIBAGMN/HKYYiKX
DMBFLtmOuq8bR2ViTT7c86b2UYikHi5s0wOLa1tlljmjZQZhaqxlAdQNegnbxHFF
o/q/QlVsBTxFlIrv7MwjlCsaHoPYbaW+/r/x2QOyfMBryh/aubb7TLwIEePYxUWS
e6Ysc1hbGmPAXa57D+Y8rBQ+nptg8t5QIE/OWO3ioCL9/9TRUETYPGK3cfGCZRjI
fQD+XUkUxrfsR7O1LElWmQMRoVnvdKhSjggUkWup012sFAh62cH7l2774s4Z1V/M
3vNEWACBt5FBpInBN3gRDlpRH1GLAmzmASqSGcsUBoC+Frx+HFX6InIi/f81jVlX
8jgkvDG/aKs93pGIbwo0rkEq16mBF6HHoKnsZkmIV/knz6DB9JVNc0zmAdxEu2ZO
KCXtEbWR79Pgkx8xgTzxUxJnpzDkR25q5fpRHloYWWwgDhQsib4QsFo0F4iKgJ+C
k1OoD9wqQhqkrXRAQHNxeazL4lGQnLD65ld1Ko8rZr/mwJYmCwboTbL+t+mAax4s
vI2yk7uoKePs1PfDKkpGAmXSLnjCfxvLy7EB1+SuNsyAEqonlxN1k2bQO+89lRNk
/44zVVsb8SkJw1vYvSmGRh2PjM/o8fOtEditYIRhyTSxUMy4npQSXC5aDaeBXEx0
pyu1ShyQRl3VK1glyMVNZzGgnUQ8RRWW
-----END CERTIFICATE-----
</ca>

# 8<----------------------
# Copier-coller jusqu'ici.

```
