/!\ ********************************************************************************/!\  
/!\ Attention : fichier obsolète depuis le 23 mars 2024, voir [fdn-vpn-public.ovpn](config-fdn-vpn-public.md) /!\  
/!\ ********************************************************************************/!\  


Lien pour télécharger directement le fichier [fdn-open.ovpn](https://git.fdn.fr/fdn-public/wiki/-/raw/master/vpn/openvpn/client/configuration/fdn-open.ovpn?inline=false)

Ou recopier la configuration ci-après dans un éditeur de texte et l'enregistrer sous "fdn-open.ovpn"


**ATTENTION: ceci est le fichier de configuration pour le serveur VPN public de FDN, dont le débit est limité selon les dons qui sont faits pour ce serveur. Si vous avez un compte @vpn.fdn.fr, c'est l'autre fichier de configuration qu'il faut [utiliser](./vpn/openvpn/client/config-fdn-vpn.md)**

```conf

# Copier-coller à partir d'ici, et modifiez la partie auth-user-pass si vous ne voulez pas 
# avoir à taper votre mot de passe à chaque fois.
# 8<----------------------

# C'est nous qui prenons l'initiative de nous connecter au serveur.
client

# On route de l'IP, on ne fait pas de l'ethernet.
dev tun

# Pour contournement si besoin l'erreur "VERIFY ERROR: depth=0, error=CA signature digest algorithm too weak"
# Décommenter la ligne suivante.
#tls-cipher "DEFAULT:@SECLEVEL=0"

# Certains réseaux ont en fait une MTU bien inférieure à 1450. Dire aux connexions
# TCP d'être très conservatives, pour que ça marche plus ou moins partout.
mssfix 1300

# En UDP, on peut s'assurer que ça passe de toutes façons en fragmentant au besoin
# quand ça dépasse.
# fragment 1300

# Idéalement, ça devrait être détecté tout seul, mais c'est loin de toujours fonctionner...
# mtu-disc yes

# En udp, Prévenir le serveur quand on termine, permet de relancer
# immédiatement sans attendre que le serveur se rende compte de la
# déconnexion par timeout.
#explicit-exit-notify

# Il est préférable d'utiliser udp, le résultat fonctionne mieux. Il est
# cependant notable que les restrictions d'accès Internet laissent souvent
# plus facilement passer tcp. Essayer donc udp, et seulement s'il ne fonctionne
# pas, essayer tcp.

# L'adresse du serveur
<connection>
remote open.fdn.fr 1194
explicit-exit-notify
</connection>

<connection>
remote open.fdn.fr 53
explicit-exit-notify
</connection>

<connection>
remote open.fdn.fr 123
explicit-exit-notify
</connection>

<connection>
remote open.fdn.fr 443 tcp
</connection>

<connection>
remote open.fdn.fr 993 tcp
</connection>

<connection>
remote open.fdn.fr 22 tcp
</connection>

<connection>
remote open.fdn.fr 80 tcp
</connection>

remote-random

# Éventuellement, on peut avoir besoin de passer par un proxy http, décommenter cette ligne en mettant l'adresse et le port du proxy.
#http-proxy 192.0.2.1 8080

# Pour windows: utiliser route.exe.
route-method exe

# Attendre un peu avant d'ajouter les routes.
route-delay 2

# Ne pas utiliser un port local statique, on est client de toutes façons.
nobind

# Garder la clé en mémoire, pour ne pas avoir besoin de la relire lors d'un
# redémarrage.
persist-key

# On peut éventuellement ne pas tuer l'interface du tunnel lors d'un redémarrage, mais cela pose problème si au redémarrage on change de serveur.
# persist-tun

# Faire passer tout le trafic via le VPN:
redirect-gateway def1

# Mais pas le trafic local:
route 10.0.0.0 255.0.0.0 net_gateway
route 172.16.0.0 255.240.0.0 net_gateway
route 192.168.0.0 255.255.0.0 net_gateway

# Activer IPv6
tun-ipv6

# Faire passer tout le trafic IPv6 via le VPN:
route-ipv6 ::/1
route-ipv6 8000::/1

# On peut aussi vouloir plutôt router seulement quelques destinations, par
# exemple ici tout Gitoyen:
#route 80.67.160.0 255.255.224.0

# Envoyer un login et un mot de passe. Vous pouvez mettre n'importe quoi, le
# serveur openbar accepte tout. Pour éviter de taper à la main login
# et mot de passe, vous pouvez ajouter à droite de "auth-user-pass" le nom d'un
# fichier contenant ces deux informations, une par ligne.
auth-user-pass

# Un minimum de debug, c'est toujours bien.
verb 3

# Certificat permettant de vérifier que c'est bien à FDN que
# l'on se connecte et donc à qui on confie notre trafic.
verify-x509-name open.fdn.fr name
<ca>
-----BEGIN CERTIFICATE-----
MIIFsTCCA5mgAwIBAgIJAOmDDYaNduY/MA0GCSqGSIb3DQEBBQUAMG8xCzAJBgNV
BAYTAkZSMQ8wDQYDVQQIDAZGcmFuY2UxDDAKBgNVBAoMA0ZETjEUMBIGA1UEAwwL
b3Blbi5mZG4uZnIxKzApBgkqhkiG9w0BCQEWHHNhbXVlbC50aGliYXVsdEBlbnMt
bHlvbi5vcmcwHhcNMTQwNTEzMDAzOTE3WhcNNDEwOTI4MDAzOTE3WjBvMQswCQYD
VQQGEwJGUjEPMA0GA1UECAwGRnJhbmNlMQwwCgYDVQQKDANGRE4xFDASBgNVBAMM
C29wZW4uZmRuLmZyMSswKQYJKoZIhvcNAQkBFhxzYW11ZWwudGhpYmF1bHRAZW5z
LWx5b24ub3JnMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAsozvz3HE
s7h9/rePsLxQ8GL4PtC+ye3KB7ShSTn94nFe9tsGnJTvgjthvL10/4lHu1msDk/f
QXyg8q5n/YYz9njrvjc/mZBzx7cXNg/1VnXHDB09cIWDvsQM23Gu213QAGZ4Lqc1
lgEDijmG19LBE09i2L+jUh1pjGiPcUFE3KKCleTFRqvFAkhs6yTGcz2r8NqJL1yP
GuGQfjhdhnLGeI+/M2mtiDe4F5KQsHvLFFvyFoeiCjY3ZzoAwT8aGopf/Dkt+ZgA
JkbLMnaaMDK3WpVcE3g2E5xDnDrghvYaEvZAoAH3jyXOLJRutYuCqKsTFl7kpqvE
rLXlJXsxx1eJiVYcIHf9JS06YP059OZ8hCilVmEa/O9E5p670DdTjjDvRW7GRMdJ
HKHNdATmh0JR4VIdhy0Tc1pmcvECBR6ny0uCE91L3w6lPotfhpZnv1IJTrjCWIBy
C0RiH64WYC7NR9kQBmoXW4XFdUE+a4m6YlhItPhXmfgmDaKf2RN8icTiYEeqjRmk
Y3M+AaUAVYUeDjyx/031+i5a9TgtJ5+i08XxZdPrTf7kGWf4i6osSyA3FyGdvla3
X0YMui5YKh/3h6r7Br6S3TP+VftQlCkXb2cSvDAfGMIMszemOm/vrlbvq0dm7oGo
Cx8fJN9BpTYfXZ8TwhbK/U2zKndMXW4BwX0CAwEAAaNQME4wHQYDVR0OBBYEFL/K
cBB39RuGEGhbOOO7NsXfVKvQMB8GA1UdIwQYMBaAFL/KcBB39RuGEGhbOOO7NsXf
VKvQMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggIBAGCiMsRAcEGf8VJ7
blSeBL/mD7S49tqBVNv7DiPGDYbmVubJmMZvDD9ryBvBJa/nd/RRHUrmDlZjYY50
VHy1TEdBiaRKblTQ8/+lSHyX4ktbS7D0tFk6DCjdSHAHBRgyoRFE1JVoO8EdK1mb
lPq/ProY/SN7G9ptiiugT9F+stm9Yk4YOpoz4bTdpfxSBFDiWiOhfYkCDVuXkn2K
85GDw0upLVGwrX8U45jXzFGR5yBsHCz/sOE0b+7jHrXpYnhSSZT3j1SsmSPC7/r2
GFwfpX3OME/JDQYU7g+nfxdFlxazSwdTfQp3Xx/pmZ04q0GwaCNxFZ/9rgXo51q1
Q7wZpMR7QTtYWlSh70d0DKo2ZKJqPSmAbGR/vtuQoPUTMnmnNAxHHOx5UVnF2Oa4
BU/PkotNnV+9PvfxmnbRq8af7Zcgdr+dt+dHYjctSkfu5NaOU54bn+N4IFIq4Dbe
KgA3PTN3QiLsDXiAEhl4sHTHZTXLKgiCjRnc0aiSoGDLCSlZDCWHrQ6IIePej6ou
sUVaNWJkXzJRXDBcFm4ZY7PaXwkcLp6vexzyfWXBrnMvdH943kzH5tMKGRr8CxQU
N0IwrlK4G/Or1DMfta2SLoRNGnKBJGuf3b/nw9ze5MTiKkeDj2DrlD0q9I2+Sd8O
GFKEMIKZzqL7QdEF+ulN9niw1cOf
-----END CERTIFICATE-----
</ca>

# 8<----------------------
# Copier-coller jusqu'ici.

```
