**Documentation obsolète en 2024**

VPN FDN sous Windows 10
===

Cette page fait partie de la [documentation VPN](../../README.md).

[[_TOC_]]

## Configuration du client OpenVPN Connect

### 1. Installer le client "OpenVPN Connect v3" (Plus simple mais propriétaire)
* depuis le site de l'éditeur : [Télécharger OpenVPN Connect](https://openvpn.net/client/client-connect-vpn-for-windows/)
* avec Chocolatey : `choco install openvpn-connect`

### 2. Importer le fichier de configuration
- Vous n'avez pas de [compte @vpn.fdn.fr](config-fdn-open.md)
- Vous avez [compte @vpn.fdn.fr](config-fdn-vpn.md)

![](image/windows10-ovpn_conf1.png) ![](image/windows10-ovpn_conf2.png)

### 3. Ajouter des identifiants
- Vous n'avez pas de compte @vpn.fdn.fr, n'importe lesquels, aucune importance
- Vous avez compte @vpn.fdn.fr, vos identifiants sont disponibles dans votre [espace adhérent](https://vador.fdn.fr/adherents/) (Abonnements et services - Abonnements VPN)
* cliquer sur "add" ou "ajouter"

![](image/windows10-ovpn_conf3.png)

### 4. cliquer sur le switch de connexion

![](image/windows10-ovpn1.png)

### 5. le client demande l'ajout d'un certificat, il faut lui dire de continuer sans
  
![](image/windows10-ovpn2.png)

### 6. là, ça devrait être ok…

![](image/windows10-ovpn3.png) ![](image/windows10-ovpn4.png)

## Références
[Télécharger OpenVPN Connect pour Windows](https://openvpn.net/client/client-connect-vpn-for-windows/)
