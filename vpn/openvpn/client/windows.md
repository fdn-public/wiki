**Documentation obsolète en 2024**

Installation du client OpenVPN sous Windows
===

Cette page fait partie de la [documentation VPN](../../README.md).

Commencer par télécharger OpenVPN depuis le [site officiel](http://openvpn.net/index.php/download.html).  
Note : descendre jusqu'à la section "[Community downloads" et choisir le "Windows Installer"](https://openvpn.net/index.php/download/community-downloads.html).

![sa-2806201713h19lientelechargementopenvpn.png](./image/sa-2806201713h19lientelechargementopenvpn.png)

![winvista_telechargement_site_openvpn.png](./image/winvista_telechargement_site_openvpn.png)

Une fois téléchargé, exécuter le fichier. Un avertissement comme ceux-ci
peut apparaître :

![02-winvistaopenvpninstalluac.png](./image/02-winvistaopenvpninstalluac.png)
![sa-2806201713h26avertissementsecuriteuacwin7.png](./images/sa-2806201713h26avertissementsecuriteuacwin7.png)
![02-win8openvpninstalluac.png](./image/02-win8openvpninstalluac.png)

Dans ce cas, répondre par l\'affirmative (cliquer sur \"Continuer\" (\"Continue\"), \"Exécuter\" ou \"Oui\" (\"Yes\") selon le cas).

L\'assistant d\'installation doit apparaître. Cliquer sur \"Suivant\" (\"Next\") :

![03-winvistaopenvpninstallwizard1.png](./image/03-winvistaopenvpninstallwizard1.png)

Lire la licence, cliquer sur \"J\'accepte\" (\"I Agree\") :

![04-winvistaopenvpninstallwizard2.png](./image/04-winvistaopenvpninstallwizard2.png)

Sélectionner tous les composants (état par défaut de l\'assistant), cliquer sur \"Suivant\" (\"Next\") :

![05-winvistaopenvpninstallwizard3.png](./image/05-winvistaopenvpninstallwizard3.png)

Laisser le chemin d\'installation par défaut, cliquer sur \"Installer\" (\"Install\") :

![06-winvistaopenvpninstallwizard4.png](./image/06-winvistaopenvpninstallwizard4.png)

L\'installation est en cours :

![07-winvistaopenvpninstallwizard5.png](./image/07-winvistaopenvpninstallwizard5.png)

Durant l\'installation, si une fenêtre comme celle-ci apparaît :

![08-winvistaopenvpninstallwizard6.png](./image/08-winvistaopenvpninstallwizard6.png)

![sa-2806201713h28installationperiphtapsouswin7.png](./image/sa-2806201713h28installationperiphtapsouswin7.png)

Cocher la case \"Toujours faire confiance aux logiciels provenant de «OpenVPN Technologies, Inc. »\" puis cliquer sur \"Installer\".

L\'installation continue. Une fois le processus d\'installation terminé, la fenêtre de l\'assistant doit être dans un état semblable à celui-ci :

![09-winvistaopenvpninstallwizard7.png](./image/09-winvistaopenvpninstallwizard7.png)

Cliquer sur \"Terminer\" (\"Finish\") :

![10-winvistaopenvpninstallwizard8.png](./image/10-winvistaopenvpninstallwizard8.png)
