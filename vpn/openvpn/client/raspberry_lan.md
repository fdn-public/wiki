VPN FDN avec la Raspberry Pi
===

Cette page fait partie de la [documentationVPN](../../README.md).

[[_TOC_]]

## Comment router le traffic de son LAN vers un VPN FDN ?

Si vous utilisez la box de votre "grozopérateur", peu de chance d'y arriver.La solution : ajouter un petit routeur/pare-feu entre votre PC et la box.

Voyons comment réaliser ce routeur pour pas trop cher.

### Topologie (exemple)

```mermaid
graph LR
    classDef agrume fill:#ffbf00;
    classDef green  fill:#9fd94c;
    classDef fdnl   fill:#bac2e4;
    style wrt       fill:#9fd94c,stroke:#333,stroke-width:2px,stroke-dasharray:3
    style internet  fill:#df0101,stroke:#b40404,stroke-width:15px,stroke-dasharray:1

    P1("#128421;"):::green --- wrt(openWRT) == VPN === box == VPN === internet == VPN === fdn(FDN):::fdnl <-.-> internet((Internet))
    box{{"Box Grozopérateur #127818;"}}:::agrume %%-.-> internet((Internet))
```

**ou**

```mermaid
graph LR
    classDef agrume fill:#ffbf00;
    classDef green  fill:#9fd94c;
    classDef fdnl   fill:#bac2e4;
    style wrt       fill:#9fd94c,stroke:#333,stroke-width:2px,stroke-dasharray:3
    style internet  fill:#df0101,stroke:#b40404,stroke-width:15px,stroke-dasharray:1

    P1("#128421;"):::green --- switch(switch):::green --- wrt(openWRT) == VPN === box == VPN === internet == VPN === fdn(FDN):::fdnl <-.-> internet((Internet))
    P4("#128187;"):::green --- switch(switch):::green
    P5("#128187;"):::green -.- paw(point d'accès Wi-Fi):::green --- switch(switch):::green
    P7("#128241;"):::green -.- paw(point d'accès Wi-Fi):::green
    box{{"Box Grozopérateur #127818;"}}:::agrume %%-.-> internet((Internet))
```

Pour les deux exemples ci-dessus, la configuration d'openWRT sera la même.

### Besoins matériel

- [x] Une carte Raspberry Pi 3B (env. **40€**) (un Rpi 4B 2 Go RAM, c'est mieux mais env. **50€**)  
Pourquoi le 4 est mieux que le 3 ? Au moins parce qu'il dispose de l'USB 3.0 et du Gigabit nativement (cf: fibre…)
    - [x] Éventuellement, un boîtier pour le Rpi
- [x] L'alimentation électrique du Rpi ; prendre l'officiel ou équivalent si vous trouver la même qualité (env. **10€**)
- [x] Un adaptateur USB/Ethernet - le pilote du cheap ax88179 est packagé et disponible pour openWRT  (env. **15€**)  
[kmod-usb-net-asix-ax88179](https://openwrt.org/packages/pkgdata/kmod-usb-net-asix-ax88179)
- [x] Une carte microSD + de quoi l'utiliser sur votre PC (env. **10€**)
- Total approximatif : **80€**

### Liens vers le matériel

| | Liens |
| :--- | :--- |
| Rpi 4B | [carte](https://www.kubii.fr/cartes-raspberry-pi/2771-nouveau-raspberry-pi-4-modele-b-2gb-0765756931175.html) - [alim](https://www.kubii.fr/14-chargeurs-alimentations-raspberry/2678-alimentation-officielle-usb-type-c-raspberry-pi-3272496300002.html) - [boîtier](https://www.kubii.fr/boitiers-et-supports/2681-boitier-officiel-pour-raspberry-pi-4-kubii-3272496298583.html?search_query=boitier+rpi+4&results=147)
| Rpi 3B | [carte](https://www.kubii.fr/home/1628-raspberry-pi-3-modele-b-1-gb-kubii-5060214370264.html) - [alim](https://www.kubii.fr/accessoires-officiels-raspberry-pi/2593-alimentation-officielle-raspberry-pi-3-eu-micro-usb-51v-25a-kubii-3272496297586.html) - [boîtier](https://www.kubii.fr/boitiers-et-supports/1632-boitier-officiel-pour-raspberry-pi-3b-3-2-kubii-3272496005051.html) |
| Carte SD | [16Go](https://www.ldlc.com/fiche/PB00237901.html) (8 peuvent suffire *largement*) |


### Besoins logiciel

- [x] L'image d'installation
    - [Rpi 4 B](https://downloads.openwrt.org/releases/21.02.1/targets/bcm27xx/bcm2711/openwrt-21.02.1-bcm27xx-bcm2711-rpi-4-squashfs-factory.img.gz)
    - [Rpi 3 B](https://downloads.openwrt.org/releases/21.02.1/targets/bcm27xx/bcm2710/openwrt-21.02.1-bcm27xx-bcm2710-rpi-3-ext4-factory.img.gz)
- [x] de quoi "flasher" la carte microSD (`dd` sur GNU/Linux ou pour Win/Mac [Rpi OS Imager](https://www.raspberrypi.com/news/raspberry-pi-imager-imaging-utility/))


### Installation

#### A. Installer openWRT

1. Télécharger l'archive de l'image d'installation
2. Décompresser l'archive

ex pour Linux :

```bash
bzip -d openwrt-21.02.1-bcm27xx-bcm2710-rpi-3-ext4-factory.img.gz
```

2. Insérer et monter la carte sur votre PC
3. Repérer où est monter la carte (ex: /dev/sdh. Le lecteur, pas une partition)
4. Écrire le système sur la carte

ex pour Linux :

```bash
dd if=openwrt-21.02.1-bcm27xx-bcm2710-rpi-3-ext4-factory.img of=/dev/sdh bs=2M conv=fsync && sync
```

5. Ôter la carte et l'insérer dans le raspberry_pi
6. Brancher le Rpi (réseaux + alim)

#### B. Connexion à openWRT

1. Vérifier que la carte réseau de votre pc est bien sur la tranche `192.168.1.0/24`
2. Si un périphérique utilise déjà l'adresse `192.168.1.1`, le débrancher temporairement  
Si c'est votre PC, changer la pour `192.168.1.2`
3. Ouvrer votre navigateur et aller sur la page d'administration d'openWRT : [http://192.168.1.1](http://192.168.1.1)
4. Cliquez simplement sur "Login" puis ajouter un mot de passe

#### C. Installer le nécessaire

1. Vérifier qu'Internet est accessible
2. Aller dans System / Software
3. Cliquer sur "Update lists…"
4. installer les paquets suivants :
    - `kmod-usb-net-asix-ax88179` (ou celui qui correspond à votre adaptateur usb/eth)
    - `openvpn-openssl`
    - `luci-app-openvpn` (gestion graphique du client openVPN)
5. reboot


### Configuration

#### Réseaux

- eth0 (carte interne)
    - Le lien avec la box
    - 192.168.1.x 
    - attribution de l'adresse IP par le serveur DHCP de la box
- eth1 (usb/eth)
    - Le lien avec notre LAN
    - adresse fixe 10.0.0.254
    - c'est openWRT qui deviendra le serveur DHCP sur notre LAN
- Pare-feu - zones - règles de base
    - LAN -> (forward vers) -> VPN
      * input :   accept
      * output :  accept
      * forward : accept
    - WAN
      * input :   reject
      * output :  accept
      * forward : reject
    - VPN
      * input :   reject
      * output :  accept
      * forward : reject

#### openVPN
Section "OVPN config" :

```conf
client
dev tun
proto udp
mssfix 1300
nobind
server-poll-timeout 3
<connection>
remote vpn.fdn.fr 1194
explicit-exit-notify
</connection>
route-delay 2
persist-key
redirect-gateway def1
route 10.0.0.0 255.0.0.0 net_gateway
route 172.16.0.0 255.240.0.0 net_gateway
route 192.168.0.0 255.255.0.0 net_gateway
tun-ipv6
route-ipv6 ::/1
route-ipv6 8000::/1
auth-user-pass /etc/openvpn/FDN_vpn.auth
verb 3
verify-x509-name *.fdn.fr name
<ca>
-----BEGIN CERTIFICATE-----
MIIFGTCCAwGgAwIBAgIEWgh7mjANBgkqhkiG9w0BAQsFADAsMQswCQYDVQQGEwJG
UjEMMAoGA1UEChMDRkROMQ8wDQYDVQQDEwZDQSBGRE4wHhcNMTcxMTEyMTY0OTMx
WhcNMzcwMTExMTY0OTQzWjAsMQswCQYDVQQGEwJGUjEMMAoGA1UEChMDRkROMQ8w
DQYDVQQDEwZDQSBGRE4wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC/
kZzJVsN4vpK7phHW7sX4UpJ1bEd1BveKBATiMTDIOY8ioVv7tAmNOSTABBi8KYzS
LmflAVgsMGh1JI4+b5O4ZN1DKjKp9WAkJZvotsmHnCYsKBhoYc4JqkZQgG2s7zOm
b7eigEWZQf0F5PIaNUzT2nZZlIjxnv7DiAI+lu46qWQfu09IAca4DyN3ViFmlv03
PD4QpTqdungSWCr2gv3VOVF3yX1+b/P4kX7oWae+U2XFL9hYDUuWaFFdWCTzSRvv
JV7QMSflicb7fCRKC6E2r8x7igxyzr5NT6NAkYWvazgyNc7NOsy2hJ9EkN4IWs/0
GORkzYKBcA0MMFdt5CgbAPBFXleLwoaFpZ4BVkFIhREJHNgK6ZFfK60U4O+F552R
QZPbgD+5geJOi6XbrBD3lQ/yb3qaNoejo1g39D7h571rPRYorDlTj6BZ925D+A+7
Mb6DOZMxYUfQ6SYqZSnWf7aivdLpNNsN8K0un8Z2BB98eK6cIhUv298FxF0+tSZI
ok9T5SxF8URU2VfI6wVcSVRh8Q5aeKf2NINIxN6wrBYSwAls3gkwDEsAny+tCwwL
3hy3Y7SEvg+ItFS+d2RYdqav72Av5H2o6Uxr9025ZPKo89/Czd6XPID96znK2x/N
l851UCjHfvNG2xzRqJa0HhUl2pLyEMpC62g31wKv+wIDAQABo0MwQTAPBgNVHRMB
Af8EBTADAQH/MA8GA1UdDwEB/wQFAwMHBAAwHQYDVR0OBBYEFCtQ0M1liMFOkprT
3G5JCpfc/pNAMA0GCSqGSIb3DQEBCwUAA4ICAQAscgi/f2oJIRwHHR+Yt/nW2Z43
hBVLTf0/u/Doa2m7Ae7Bv138ofaFwwF2q7iwnrb2F6L5deD0ZZoLtL0cNtNz7ajw
46SurhoftZh98ZaEmga6UtdNBDz8EO6aJtcwH4nXmzsfQFJ6WHdoKsWTC2L8u3Q8
nbxVF8x/J5QZKOiNp7hlxGEaFABmfaPvRXa4Fm/KLuITL74pEZ3K0+ufnrsT2S4+
8RcgFYkRsKBkXPbhaGp10XDKHC4PPq26fZYVKMb4WzoeDMVMcfotGmdOrehah0mu
0fC9qElVoKtuEEvKtzAsnAX/jRPRMYqqtD90fqL6txoVKzVQcP8cyY0L6eZhIdYe
nt0NfGhmxo6sRAnVmjA5yIriHOE70Zcd2ebeBcUITe7MReIynuygd85BhYyIegBB
WGsj3iSp2Gg5CBNOe8JBLV6UU7iexThlEfWwbSpgigpdICaAaqjTATsO9PWeIM+v
TsH51AC2wh63U5o6OCp3H18/bVJ3oX2F9fba8pPY5r7T7ou0Sq5Jy6i2US03vtDA
NT2/q5MXAHy7kdMCHzT4KQp81pUTY3bNtujUyGC9Nhgf0CMQMLOmwL7lF9aKWk8J
tG1ixRwplTEHEuJARpKp+MebiyfI87OoCSRJP+LygnkKeYNGxV0fhQnIW3+44bnw
NH0QlNNxLH0iV4UJQA==
-----END CERTIFICATE-----
</ca>
```

Section "auth-user-pass" :
```conf
prénom.nom@vpn.fdn.fr
mot_de_passe

```

### Sources

- [openWRT sur Rpi](https://openwrt.org/toh/raspberry_pi_foundation/raspberry_pi)
- [opemWRT + openVPN client via LuCi (WebUI)](https://openwrt.org/docs/guide-user/services/vpn/openvpn/client-luci)
- [openVPN for HH5A pdf dropbox](https://www.dropbox.com/sh/c8cqmpc6cacs5n8/AAA2f8htk1uMitBckDW8Jq88a?dl=0&preview=4-OpenVPN+Client+for+HH5A+v1.2.pdf)
