VPN FDN sous Gnome
===

Cette page fait partie de la [documentation VPN](../../README.md).

[[_TOC_]]

## Configuration du client OpenVPN

### Via Network Manager

* Clic droit sur l'icône Network Manager -> "Modifications des connexions", puis onglet "VPN" -> "ajouter un VPN" :  
* Si ce choix n'est pas présent (cas de Debian 8 Jessie), assurez-vous d'avoir installé les paquets suivants : `easy-rsa`, 
`network-manager-openvpn-gnome`, `gadmin-openvpn-client`.  
* Allez ensuite dans « Paramètres » puis « Réseau » . Cliquez sur « + » puis « VPN » puis « OpenVpn » (que ce soit pour le VPN public de FDN ou pour le VPN avec abonnement)

![network_manager_etape_1.png](image/network_manager_etape_1.png)

![network_manager_etape_2.png](image/network_manager_etape_2.png)

* Nommer la connexion (au hasard : "Connexion VPN FDN"), puis compléter les informations requises ainsi :
1. la passerelle : si vous avez un compte VPN, vpn.fdn.fr , si vous utilisez le VPN public de FDN, open.fdn.fr ;
2. l'authentification : se fera par mot de passe ;
3. le nom d'utilisateur : de la forme xxx@vpn.fdn.fr ;
4. le mot de passe, à mémoriser parfaitement et ne pas écrire sur un post-it en dessous du clavier ;
5. et enfin le certificat permettant de s'assurer que l'on est bien tombé sur le réseau de FDN et qu'un malfaisant ne détourne pas les données en cours de route. Copiez sur votre machine le [certificat de FDN](https://www.fdn.fr/assets/files/ca-vpn-fdn.crt) (clic droit, enregistrer sous) si vous avez un compte VPN, ou le [certificat du VPN public de FDN](http://www.fdn.fr/ca-vpn-fdn-openbar.crt) (clic droit, enregistrer sous) si vous utilisez le VPN public de FDN, puis pointez vers ce fichier dans l'interface de configuration de network manager.

![network_manager_etape_3.png](image/network_manager_etape_3.png)

* Une fois l'ordinateur connecté à son réseau physique classique et habituel (en Ethernet filaire sur le réseau de la fac, en wifi, en fibre optique, en mobile via une clé 3G, via n'importe quel réseau habituel en fait), il n'y a plus qu'à activer le VPN d'un simple clic sur "Connexions VPN" -> "Connexion VPN FDN" :

![network_manager_etape_4.png](image/network_manager_etape_4.png)

* Network Manager établit alors le tunnel sécurisé entre votre machine et le réseau de FDN. Au niveau visuel l'apparition d'un cadenas au-dessus de l'icône habituelle de Network Manager indique que votre connexion débouche sur un VPN.

* Il est possible de le vérifier en ligne de commande également : la commande "ifconfig" (nécessite d'être root) montre une nouvelle interface réseau "**tun0**" (pour tunnel 0) ayant pour adresse attribuée une IP du bloc **80.67.17x.xxx**, qui est le bloc d'adresses réservées pour le service VPN de FDN :

![if_config_avec_vpn.png](image/if_config_avec_vpn.png)
