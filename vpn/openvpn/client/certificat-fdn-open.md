/!\ ********************************************************************************/!\  
/!\ Attention : fichier obsolète depuis le 23 mars 2024, voir [ca-vpn-public-fdn.crt](certificat-vpn-public-fdn.md) /!\
/!\ ********************************************************************************/!\  


* le [certificat du VPN public de FDN](https://www.fdn.fr/ca-vpn-fdn-openbar.crt) (clic droit, enregistrer sous)