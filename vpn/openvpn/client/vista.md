**Documentation obsolète en 2024**

VPN FDN sous Windows Vista
===

Cette page fait partie de la [documentation VPN](../../README.md).

[[_TOC_]]

## Configuration du client OpenVPN

### Localisation du dossier config d'OpenVPN

Effectuer un clic droit sur l'icône "*OpenVPN GUI*" affichée sur le bureau Windows et choisir "*Ouvrir l'emplacement du fichier*" ("*Open file location*") :

![todo image description](image/guide_windows_vista/11-winvistaopenvpngetfolderpath.png)

Une fenêtre d'explorateur Windows doit apparaître et doit pointer vers le dossier contenant l'exécutable de l'interface graphique d'OpenVPN.

Dans la barre d'adresse, cliquer sur "*OpenVPN*" :

![todo image description](image/guide_windows_vista/12-winvistaopenvpnuppath.png)

Faire un double-clic sur le dossier "*config*" :

![todo image description](image/guide_windows_vista/13-winvistaopenvpnconfigfolder.png)

Effectuer un clic droit sur la barre d'adresse puis sélectionner "*Copier l'adresse en tant que texte*" ("*Copy address as text*") :

![todo image description](image/guide_windows_vista/14-winvistaopenvpngetconfigfolderpath.png)

Cette adresse correspond au chemin du dossier "*config*" sur le système de fichiers. Il est nécessaire de conserver ce chemin pour les étapes utilisant notepad.

### Création des fichiers de configuration

Il faut maintenant lancer notepad avec les droits d'administration.

Pour cela, ouvrir le menu "*Démarrer*" (touche <kbd>Windows</kbd> du clavier) et taper "*notepad*". L'application notepad doit apparaître dans le menu démarrer. Effectuer la combinaison <kbd>Ctrl</kbd>+<kbd>Maj</kbd>+<kbd>Entrée</kbd> (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>Enter</kbd>) :

![todo image description](image/guide_windows_vista/15-winvistaopenvpnstartmenusearchnotepad.png)

Il se peut qu'un message comme ci-dessous apparaisse. Cliquer sur "*Continuer*" ("*Continue*") :

![todo image description](image/guide_windows_vista/16-winvistaopenvpnstartnotepaduac.png)

Une fois l'application Notepad lancée, aller dans le menu "*Fichier*" ("*File*"), puis "*Enregistrer sous*" ("*Save as*"). Il se peut que la fenêtre soit réduite comme ceci :

![todo image description](image/guide_windows_vista/17-winvistaopenvpnnotepadsavehasreduced.png)

Dans ce cas, un simple clic sur "*Parcourir les dossier*" ("*Browse folders*") doit afficher les fichier du répertoire courant.

Dans le champ "*Nom du fichier*" ("*File name*"), coller l'adresse du chemin du répertoire "*config*" de OpenVPN et appuyer sur <kdb>Entrée</kdb> (<kdb>Enter</kdb>) :

![todo image description](image/guide_windows_vista/18-winvistaopenvpnnotepadsavehasrestoredpastepath.png)

Puis entrer le `fdn-vpn.ovpn` comme nom de fichier. Cliquer sur "*Enregistrer*" ("*Save*") :

![todo image description](image/guide_windows_vista/19-winvistaopenvpnnotepadsavefdnovpn.png)

Coller la configuration du VPN de FDN depuis le [fichier de configuration fdn-vpn.ovpn](configuration/fdn-vpn.ovpn) si vous avez un compte VPN, ou depuis le [fichier de configuration fdn-open.ovpn](configuration/fdn-open.ovpn) si vous utilisez le VPN public de FDN.

![todo image description](image/guide_windows_vista/20-winvistaopenvpnnotepadpastefdnovpncontent.png)

Sauvegarder le fichier en allant dans le menu "*Fichier*" ("*File*") puis en sélectionnant "*Enregistrer*" ("*Save*").

La configuration est maintenant terminée.

## Utilisation du VPN de FDN (Windows Vista)

Lancer l'interface graphique d'OpenVPN en faisant un clic droit sur l'icône "*OpenVPN GUI*" puis en sélectionnant "*Exécuter en tant qu'administrateur*" ("*Run as administrator*") :

![todo image description](image/guide_windows_vista/23-winvistaopenvpnstarthasadmin.png)

Le message suivant peut apparaître :

![todo image description](image/guide_windows_vista/24-winvistaopenvpnstarthasadminuac.png)

Cliquer sur "*Continuer*" ("*Continue*").

Une icône doit apparaître dans la barre des tâches (à coté de l'horloge). Effectuer un clic droit sur celle-ci, puis sélectionner "*Connecter*" ("*Connect*") :

![todo image description](image/guide_windows_vista/25-winvistaopenvpnlaunchconnection.png)

Entrer les identifiants de connexion et cliquer sur "*OK*" :

![todo image description](image/guide_windows_vista/26-winvistaopenvpnlogin.png)

La connexion est initialisée :

![todo image description](image/guide_windows_vista/27-winvistaopenvpnconnecting.png)

Après un certain temps, une ligne "*Initialization Sequence Completed*" doit apparaître :

![todo image description](image/guide_windows_vista/28-winvistaopenvpnsuccessfulconnection.png)

Peu après, la fenêtre de connexion disparaît et plusieurs indices montrent que l'on est correctement connecté.

Premier indice : une infobulle apparaît au dessus de l'icône de OpenVPN dans la barre des tâches :

![todo image description](image/guide_windows_vista/29-winvistaopenvpnconnectedtooltip.png)

Second indice : l'icône de OpenVPN passe à la couleur verte :

![todo image description](image/guide_windows_vista/30-winvistaopenvpnconnectedsystray.png)

Troisième indice : en lançant un terminal (avec le programme `cmd`) et en tapant `ipconfig`, on doit retrouver l'IP affectée à l'interface créée par OpenVPN :

![todo image description](image/guide_windows_vista/31-winvistaopenvpnipconfig.png)

