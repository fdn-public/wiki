Lien pour télécharger directement le fichier [fdn-vpn.ovpn](https://git.fdn.fr/fdn-public/wiki/-/raw/master/vpn/openvpn/client/configuration/fdn-vpn.ovpn?inline=false)

Ou recopier la configuration ci-après dans un éditeur de texte et l'enregistrer sous "fdn-vpn.ovpn"

```conf

# Copier-coller à partir d'ici, et modifiez la partie auth-user-pass si vous ne voulez pas 
# avoir à taper votre mot de passe à chaque fois.
# 8<----------------------

# fdn-vpn.ovpn
# fichier de configuration du VPN avec abonnement de FDN
# version 2.0

# C'est nous qui prenons l'initiative de nous connecter au serveur.
client

# On route de l'IP, on ne fait pas de l'ethernet.
dev tun

# Certains réseaux ont en fait une MTU bien inférieure à 1450. Dire aux connexions
# TCP d'être très conservatives, pour que ça marche plus ou moins partout.
mssfix 1300

# En UDP, on peut s'assurer que ça passe de toutes façons en fragmentant au besoin
# quand ça dépasse.
# fragment 1300

# Idéalement, ça devrait être détecté tout seul, mais c'est loin de toujours fonctionner...
# mtu-disc yes

# Ne pas utiliser un port local statique, on est client de toutes façons.
nobind

# Il est préférable d'utiliser udp, le résultat fonctionne mieux. Il est
# cependant notable que les restrictions d'accès Internet laissent souvent
# plus facilement passer tcp. Essayer donc udp, et seulement s'il ne fonctionne
# pas, essayer tcp.
server-poll-timeout 3

# Les adresses des serveurs.
<connection>
remote vpn.fdn.fr 1194
explicit-exit-notify
</connection>

<connection>
remote vpn-rw.fdn.fr 53
explicit-exit-notify
</connection>

<connection>
remote vpn.fdn.fr 1194 tcp
</connection>

<connection>
remote vpn-rw.fdn.fr 80 tcp
</connection>

<connection>
remote vpn-rw.fdn.fr 443 tcp
</connection>

<connection>
remote vpn-rw.fdn.fr 993 tcp
</connection>

<connection>
remote vpn-rw.fdn.fr 22 tcp
</connection>

# Cette dernière tentative implique la mise en place de stunnel 
# https://git.fdn.fr/fdn/wiki/-/blob/master/pages/travaux/stunnel.md
# et vous permet de passer à travers de nombreux réseau.
# Néanmoins, cela doit rester une solution de dernier recours car:
# 1- le débit se trouve fortement réduit
# 2- cela mobilise beaucoup plus de ressources serveur
<connection>
remote 127.0.0.1 11944 tcp
</connection>

# Éventuellement, on peut avoir besoin de passer par un proxy http, décommenter cette ligne en mettant l'adresse et le port du proxy.
#http-proxy 192.0.2.1 8080

# Pour windows: utiliser route.exe.
route-method exe

# Attendre un peu avant d'ajouter les routes.
route-delay 2

# Garder la clé en mémoire, pour ne pas avoir besoin de la relire lors d'un
# redémarrage.
persist-key

# On peut éventuellement ne pas tuer l'interface du tunnel lors d'un redémarrage, mais cela pose problème pour
# résoudre le nom de domaine du serveur si les résolutions DNS passent par le VPN, et aussi si au redémarrage
# on change de serveur (changement de passerelle).
#persist-tun

# Faire passer tout le trafic via le VPN:
redirect-gateway def1

# Mais pas le trafic local:
route 10.0.0.0 255.0.0.0 net_gateway
route 172.16.0.0 255.240.0.0 net_gateway
route 192.168.0.0 255.255.0.0 net_gateway
# Ajouter éventuellement d'autres routes locales, par exemple vers votre cache DNS

# Activer IPv6
# tun-ipv6

# et faire passer tout le trafic IPv6 via le VPN:
route-ipv6 ::/1
route-ipv6 8000::/1

# On peut aussi vouloir plutôt router seulement quelques destinations, par
# exemple ici tout Gitoyen:
#route 80.67.160.0 255.255.224.0

# Envoyer un login et un mot de passe. Pour éviter de taper à la main login
# et mot de passe, vous pouvez ajouter à droite de "auth-user-pass" le nom d'un
# fichier contenant ces deux informations, une par ligne.
auth-user-pass

# Un minimum de debug, c'est toujours bien.
verb 3
<ca>
-----BEGIN CERTIFICATE-----
MIIFGTCCAwGgAwIBAgIEWgh7mjANBgkqhkiG9w0BAQsFADAsMQswCQYDVQQGEwJG
UjEMMAoGA1UEChMDRkROMQ8wDQYDVQQDEwZDQSBGRE4wHhcNMTcxMTEyMTY0OTMx
WhcNMzcwMTExMTY0OTQzWjAsMQswCQYDVQQGEwJGUjEMMAoGA1UEChMDRkROMQ8w
DQYDVQQDEwZDQSBGRE4wggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC/
kZzJVsN4vpK7phHW7sX4UpJ1bEd1BveKBATiMTDIOY8ioVv7tAmNOSTABBi8KYzS
LmflAVgsMGh1JI4+b5O4ZN1DKjKp9WAkJZvotsmHnCYsKBhoYc4JqkZQgG2s7zOm
b7eigEWZQf0F5PIaNUzT2nZZlIjxnv7DiAI+lu46qWQfu09IAca4DyN3ViFmlv03
PD4QpTqdungSWCr2gv3VOVF3yX1+b/P4kX7oWae+U2XFL9hYDUuWaFFdWCTzSRvv
JV7QMSflicb7fCRKC6E2r8x7igxyzr5NT6NAkYWvazgyNc7NOsy2hJ9EkN4IWs/0
GORkzYKBcA0MMFdt5CgbAPBFXleLwoaFpZ4BVkFIhREJHNgK6ZFfK60U4O+F552R
QZPbgD+5geJOi6XbrBD3lQ/yb3qaNoejo1g39D7h571rPRYorDlTj6BZ925D+A+7
Mb6DOZMxYUfQ6SYqZSnWf7aivdLpNNsN8K0un8Z2BB98eK6cIhUv298FxF0+tSZI
ok9T5SxF8URU2VfI6wVcSVRh8Q5aeKf2NINIxN6wrBYSwAls3gkwDEsAny+tCwwL
3hy3Y7SEvg+ItFS+d2RYdqav72Av5H2o6Uxr9025ZPKo89/Czd6XPID96znK2x/N
l851UCjHfvNG2xzRqJa0HhUl2pLyEMpC62g31wKv+wIDAQABo0MwQTAPBgNVHRMB
Af8EBTADAQH/MA8GA1UdDwEB/wQFAwMHBAAwHQYDVR0OBBYEFCtQ0M1liMFOkprT
3G5JCpfc/pNAMA0GCSqGSIb3DQEBCwUAA4ICAQAscgi/f2oJIRwHHR+Yt/nW2Z43
hBVLTf0/u/Doa2m7Ae7Bv138ofaFwwF2q7iwnrb2F6L5deD0ZZoLtL0cNtNz7ajw
46SurhoftZh98ZaEmga6UtdNBDz8EO6aJtcwH4nXmzsfQFJ6WHdoKsWTC2L8u3Q8
nbxVF8x/J5QZKOiNp7hlxGEaFABmfaPvRXa4Fm/KLuITL74pEZ3K0+ufnrsT2S4+
8RcgFYkRsKBkXPbhaGp10XDKHC4PPq26fZYVKMb4WzoeDMVMcfotGmdOrehah0mu
0fC9qElVoKtuEEvKtzAsnAX/jRPRMYqqtD90fqL6txoVKzVQcP8cyY0L6eZhIdYe
nt0NfGhmxo6sRAnVmjA5yIriHOE70Zcd2ebeBcUITe7MReIynuygd85BhYyIegBB
WGsj3iSp2Gg5CBNOe8JBLV6UU7iexThlEfWwbSpgigpdICaAaqjTATsO9PWeIM+v
TsH51AC2wh63U5o6OCp3H18/bVJ3oX2F9fba8pPY5r7T7ou0Sq5Jy6i2US03vtDA
NT2/q5MXAHy7kdMCHzT4KQp81pUTY3bNtujUyGC9Nhgf0CMQMLOmwL7lF9aKWk8J
tG1ixRwplTEHEuJARpKp+MebiyfI87OoCSRJP+LygnkKeYNGxV0fhQnIW3+44bnw
NH0QlNNxLH0iV4UJQA==
-----END CERTIFICATE-----
</ca>

# 8<----------------------
# Copier-coller jusqu'ici.

```
