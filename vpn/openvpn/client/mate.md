VPN FDN sous Debian Mate
===

[[_TOC_]]

## Installation

### 1. Installation de Network Manager

Installez les 4 paquets figurant sur l’image suivante :

![todo description image](image/guide_debian_mate/01.webp)

### 2. Ouverture de Network Manager

Au départ, cliquez sur l’icône du gestionnaire de connexion (network-
manager) qui se trouve près de l’horloge sur le tableau de bord.

Clic _droit_ : Edit connections


![todo description image](image/guide_debian_mate/02.webp)

Une fenêtre s’ouvre alors !

### 3. Importer la configuration du VPN FDN
Cliquez sur *Add* (Ajouter) et sélectionnez soit *OpenVPN* soit (en-dessous) *Import a saved VPN configuration* si vous avez téléchargé la config sur la page wiki du service VPN de fdn.fr

![todo description image](image/guide_debian_mate/03.webp)

Si vous avez téléchargé la configuration, il vous suffit (sans doute, car je procède autrement) de naviguer jusqu’à celle-ci et de confirmer.

Si vous optez pour OpenVPN, voir la suite.

### 4. Afficher la configuration du VPN

Dans la fenêtre qui vient de s’ouvrir, nommer la connexion VPN.

À l’onglet *Général*, vous pouvez autoriser tous les utilisateurs à se connecter.

![todo description image](image/guide_debian_mate/04.webp)

Allez ensuite à l’onglet IPv6 si vous êtes en IPv6 et souhaitez passer par les DNS de FDN.

### 5. Configurer les serveurs DNS pour l'IPv6

Cette étape est optionnelle.

Renseignez les champs DNS supplémentaires avec les nombres ci-dessous.

![todo description image](image/guide_debian_mate/05.webp)

### 6. Configurer les paramètres du VPN

À l’onglet *VPN*, nommez la connexion si ce n’est pas encore fait.  
Indiquez la passerelle comme suit.  
Choisissez *Mot de passe* pour l’authentification.  
Entrez votre identifiant VPN de FDN et votre mot de passe personnel qui va avec.  
Puis naviguez dans vos documents jusqu’au certificat VPN de FDN que vous avez, bien sûr, préalablement téléchargé sur la page VPN de FDN.  
Enregistrez tout cela (bouton en bas à droite) et on est bon.

![todo description image](image/guide_debian_mate/06.webp)

### 7. Se connecter au VPN

Un clic gauche sur l’icône de *network-manager* permet maintenant de se
connecter au VPN. L’icône s’anime quand la connexion est tentée.

![todo description image](image/guide_debian_mate/07.webp)

Si ça marche, vous recevez l’information suivante :

![todo description image](image/guide_debian_mate/08.webp)

Avec votre navigateur, vous surfez maintenant en VPN. Le cadenas associé à l’icône du gestionnaire de connexion en témoigne.

### 8. Se déconnecter du VPN

Pour éteindre le VPN, c’est la même manœuvre que pour l’allumer. Le message qui apparaît est le suivant :

![todo description image](image/guide_debian_mate/09.webp)

Bon surf !
