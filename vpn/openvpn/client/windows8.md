**Documentation obsolète en 2024**

VPN FDN sous Windows 8
===

Cette page fait partie de la [documentation VPN](../../README.md).

[[_TOC_]]

## Configuration du client OpenVPN

### Localisation du dossier config d'OpenVPN

Effectuer un clic droit sur l'icône "*OpenVPN GUI*"affichée sur le bureau Windows et choisir "*Ouvrir l'emplacement du fichier*"("*Open file location"*) :

![todo image description](image/guide_windows_8/10-win8openvpngetfolderpath.png)

Une fenêtre d'explorateur Windows doit apparaître et doit pointer vers le dossier contenant l'exécutable de l'interface graphique d'OpenVPN. Dans la barre d'adresse, cliquer sur "*OpenVPN*":

![todo image description](image/guide_windows_8/11-win8openvpnuppath.png)

Faire un double-clic sur le dossier "*config*":

![todo image description](image/guide_windows_8/12-win8openvpnconfigfolder.png)

Effectuer un clic droit sur la barre d'adresse puis sélectionner "*Copier l'adresse en tant que texte*"("*Copy address as text"*) :

![todo image description](image/guide_windows_8/13-win8openvpngetconfigfolderpath.png)

Cette adresse correspond au chemin du dossier "*config*"sur le système de fichiers. Il est nécessaire de conserver ce chemin pour les étapes utilisant notepad.

### Création des fichiers de configuration

Aller sur l'écran d'accueil de Windows 8 ("*Start Screen"*) en utilisant la touche "*Windows*"du clavier :

![todo image description](image/guide_windows_8/14-win8openvpnstartscreen.png)

Taper "*notepad"*. L'écran d'accueil affiche alors l'application "*Notepad*":

![todo image description](image/guide_windows_8/15-win8openvpnlaunchnotepad.png)

Effectuer le raccourci clavier suivant: Ctrl+Maj+Entrée (Ctrl+Shift+Enter) pour lancer l'application Notepad en tant qu'administrateur. Le message suivant peut apparaître. Cliquer sur "*Oui*"("*Yes"*) :

![todo image description](image/guide_windows_8/16-win8openvpnnotepaduac.png)

Aller dans le menu "*Fichier*"("*File"*) et sélectionner "*Enregistrer sous*"("*Save as"*). Dans le nom du fichier, coller l'adresse du dossier "*config*"dans le champ "*Nom du fichier*"("*File name"*) et appuyer sur "*Entrée*":

![todo image description](image/guide_windows_8/17-win8openvpnnotepadpastconfigfolderpath.png)

Puis entrer le "*fdn-vpn.ovpn*"comme nom de fichier. Cliquer sur "*Enregistrer*"("*Save"*) :

![todo image description](image/guide_windows_8/18-win8openvpnnotepadcreatefdnconfig.png)

Coller la configuration du VPN de FDN depuis le [fichier de configuration fdn-vpn.ovpn](configuration/fdn-vpn.ovpn) si vous avez un compte VPN, ou depuis le [fichier de configuration fdn-open.ovpn](travaux:vpn_misc:doc:openvpn-open:config) si vous utilisez le VPN public de FDN .

![todo image description](image/guide_windows_8/19-win8openvpnnotepadfdnconfigfilecontent.png)

Sauvegarder le fichier en allant dans le menu "*Fichier*"("*File"*) puis en sélectionnant "*Enregistrer*"("*Save"*). Fermer notepad.

Note: ce qui suit ne devrait plus être utile, maintenant que le certificat est intégré au fichier de configuration, vous pouvez passer à la section "*utilisation"*.

Ouvrer à nouveau Notepad en tant qu'administrateur (Écran d'accueil, taper "*notepad"*, puis Ctrl+Maj+Entrée) afin de créer le certificat.

Coller le chemin du dossier "*config*"dans le champ "*Nom du fichier*"("*File name"*) :

![todo image description](image/guide_windows_8/17-win8openvpnnotepadpastconfigfolderpath.png)

Taper "*netmatch-ca.crt*"dans le champ "*Nom du fichier*"("*File name"*) :

![todo image description](image/guide_windows_8/20-win8openvpnnotepadcreatefdncert.png)

Note : le nom du certificat est défini par la ligne commençant par "*ca*"dans le fichier de configuration "*fdn-vpn.ovpn"*.

Coller le contenu du certificat de FDN depuis le [certificat de FDN](travaux:vpn_misc:doc:openvpn:certificat) si vous avez un compte VPN, ou le [certificat openbar de FDN](travaux:vpn_misc:doc:openvpn-open:certificat) si vous utilisez le VPN public de FDN

![todo image description](image/guide_windows_8/21-win8openvpnnotepadfdncertificatecontent.png)

Sauvegarder le fichier en allant dans le menu "*Fichier*"("*File"*) puis en sélectionnant "*Enregistrer*"("*Save"*). Fermer notepad.

La configuration est maintenant terminée.

## Utilisation du VPN de FDN (Windows 8)

Lancer l'interface graphique d'OpenVPN en faisant un clic droit sur l'icône "*OpenVPN GUI*"puis en sélectionnant "*Exécuter en tant qu'administrateur*"("*Run as administrator"*) :

![todo image description](image/guide_windows_8/22-win8openvpniconebureau.png)

Le message suivant peut apparaître. Cliquer sur "*Oui*"("*Yes"*) :

![todo image description](image/guide_windows_8/23-win8adminrunopenvpn.png)

Une icône doit apparaître dans la barre des tâches (à côté de l'horloge). Effectuer un clic droit sur celle-ci, puis sélectionner "*Connecter*"("*Connect"*) :

![todo image description](image/guide_windows_8/24-win8openvpnlaunchconnection.png)

Entrer les identifiants de connexion et cliquer sur "*OK*":

![todo image description](image/guide_windows_8/25-win8openvpnlogin.png)

La connexion est initialisée :

![todo image description](image/guide_windows_8/26-win8openvpnconnecting.png)

Après un certain temps, une ligne "*Initialization Sequence Completed*"doit apparaître :

![todo image description](image/guide_windows_8/27-win8connected.png)

Peu après, la fenêtre de connexion disparaît et plusieurs indices montrent que l'on est correctement connecté.

Premier indice : une infobulle apparaît au-dessus de l'icône de OpenVPN dans la barre des tâches :

![todo image description](image/guide_windows_8/28-win8openvpnconnectedtooltip.png)

Second indice : l'icône de OpenVPN passe à la couleur verte :

![todo image description](image/guide_windows_8/29-win8connectedtray.png)

Troisième indice : en lançant un terminal (avec le programme "*cmd"*) et en tapant "*ipconfig"*, on doit retrouver l'IP affectée à l'interface créée par OpenVPN :

![todo image description](image/guide_windows_8/30-win8openvpnipconfig.png)
