[[_TOC_]]

Cette page fait partie de la [documentation VPN](../../README.md).

Le fichier de configuration OpenVpn n'est malheureusement pas pris en charge nativement par [Mac Os X](https://apple.stackexchange.com/questions/62318/does-osx-support-importing-openvpn-profiles), il vous faudra donc télécharger et installer un logiciel, ce que nous détaillons ci-dessous.  
Nous avons fait le choix de [Tunnelblick](https://tunnelblick.net/), qui est un logiciel libre (GNU General Public License, version 2).

# Installation du client OpenVPN avec Mac OS X

_Ce tutoriel a été testé avec Mac OS X Yosémite 10.10.4, des différences mineures peuvent apparaître avec d'autres version. Il est fonctionnel sur Mac OS X High Sierra 10.13.6._ 

Téléchargez la dernière version (Latest stable relase) de Tunnelblick depuis leur [site officiel](https://tunnelblick.net/).  

![macos_telechargement_site_tunnelblick.png](./image/macos_telechargement_site_tunnelblick.png)  

Une fois téléchargé, ouvrez le fichier puis double-cliquez sur `Tunnelblick`. Un avertissement comme celui-ci peut apparaître :

![macos_installation_tunnelblick.png](./image/macos_installation_tunnelblick.png)  

Cliquer sur `Ouvrir` et saisissez votre mot de passe administrateur ([Configurer des utilisateurs, des invités et des groupes sur Mac](https://support.apple.com/fr-fr/guide/mac-help/mtusr001/mac), [Gérer des mots de passe à l’aide de trousseaux sur Mac](https://support.apple.com/fr-fr/guide/mac-help/mchlf375f392/mac)).

En cas d'échec de l'installation ou pour toute assistance, reportez-vous au [manuel de tunnelblick](https://tunnelblick.net/cInstall.html) en Anglais traduit par [Google](http://translate.google.com/translate?hl=fr&sl=en&tl=fr&u=https%3A%2F%2Fwww.tunnelblick.net%2FcInstall.html&sandbox=1).

# Utilisation du client OpenVPN avec Mac OS X

Lancez le logiciel : à la question de savoir si vous avez des fichiers de configuration, répondez oui.  
Les autres options (vérifier les changements, rechercher les mises à jour, etc.) sont à votre goût, vous pouvez laissez les choix par défaut.

## Créer le fichier fdn-vpn.ovpn

Rendez-vous à
1. [config-fdn-vpn.md](config-fdn-vpn.md) si vous disposez d’un compte VPN avec abonnement,
2. [config-fdn-vpn-public.md](config-fdn-vpn-public.md) si vous souhaitez utiliser le VPN public.

Sélectionnez le texte de « \# Copier-coller à partir d\'ici, » à « \#
Copier-coller jusqu\'ici. », copiez-le.

Lancez TextEdit ("Finder" → "Applications" → "TextEdit", double-cliquez). Allez dans "Format" → "Convertir au format Texte".
Coller le texte (attention, faîtes bien les deux étapes précédentes dans cet ordre, et non dans l'ordre inverse).

Enregistrez (sauvegardez) le document : nommez-le 'fdn-vpn.ovpn' (dans le premier cas ci-dessus) ou 'fdn-open.ovpn' (dans le second cas), placez le où vous le souhaitez, confirmez que vous souhaitez utiliser l'extension "ovpn".  

![macos_sauvegarde_fichier_configuration.png](./image/macos_sauvegarde_fichier_configuration.png)  

## Configurer tunnelblick

Ouvrez le fichier de configuration, `fdn-vpn.ovpn` ou `fdn-open.ovpn`, en double-cliquant dessus. Choisissez si vous voulez que la connexion vpn soit accessible à vous seulement, ou à tous les utilisateurs, et saisissez le mot de passe de l'administrateur si besoin est. Une fenêtre s'affiche :  

![macos_configuration_tunnelblick.png](./image/macos_configuration_tunnelblick.png)

Dans un premier temps, vous pouvez laisser toutes les options par défaut et cliquer sur `Connecter`. Vous est alors demandé votre nom d'utilisateur et votre mot de passe, dans le cas d'un compte vpn chez fdn : à vous de décider si vous voulez les enregistrer dans le Trousseau d'accès ou non. Vous gagnerez du temps mais perdrez en sécurité en enregistrant ces informations dans votre ordinateur.  

![macos_identifiants_tunnelblick.png](./image/macos_identifiants_tunnelblick.png)  

Vous pouvez vous rendre à [whatismyip](https://www.whatismyip.com/) avant et après avoir activé votre VPN, et vérifier que votre IP a changé.

# Echec de la connexion

Pour diagnostiquer une panne, cliquez sur l'icone de tunnelbick (par défaut entre "Spotlight" et l'heure), puis sur "Détails VPN...", et enfin sur l'onglet "message".  

![macos_tunnelblick_erreur_end_line.png](./image/macos_tunnelblick_erreur_end_line.png)  

# « bad end line »

Si vous obtenez un message *Cannot load CA certificate file INLINE (no entries were read): error:0906D066:PEM routines:PEM\_read\_bio:bad end line*  
C'est que l'enregistrement du fichier de configuration a échoué. Repétez les étapes décrites plus haut, en prenant bien garde de convertir votre document en texte brut //avant// de coller le fichier fourni par fdn à [config-fdn-vpn.md](config-fdn-vpn.md) ou [config-fdn-vpn-public.md](config-fdn-vpn-public.md)
