/!\ Attention : fichier obsolète, voir https://git.fdn.fr/fdn-public/wiki/-/blob/master/vpn/README.md /!\  



\<code \|fdn-loutre.ovpn\>

1.  Copier-coller à partir d\'ici, et modifiez la partie auth-user-pass
    si vous ne voulez pas avoir à taper un mot de passe à chaque fois
2.  8\<\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--

<!-- -->

1.  C\'est nous qui prenons l\'initiative de nous connecter au serveur.

client

1.  On route de l\'IP, on ne fait pas de l\'ethernet.

dev tun

1.  Il est préférable d\'utiliser udp, le résultat fonctionne mieux. Il
    est
2.  cependant notable que les restrictions d\'accès Internet laissent
    souvent
3.  plus facilement passer tcp. Essayez donc udp, et seulement s\'il ne
    fonctionne
4.  pas, essayez tcp.
5.  proto udp

proto tcp

1.  Certains réseaux ont en fait une MTU bien inférieure à 1450. Dire
    aux connexions
2.  TCP d\'être très conservatives, pour que ça marche plus ou moins
    partout.

mssfix 1300

1.  En UDP, on peut s\'assurer que ça passe de toutes façons en
    fragmentant au besoin
2.  quand ça dépasse.
3.  fragment 1300
4.  Idéalement, ça devrait être détecté tout seul, mais c\'est loin de
    toujours fonctionner\...
5.  mtu-disc yes

<!-- -->

1.  En udp, Prévenir le serveur quand on termine, permet de relancer
2.  immédiatement sans attendre que le serveur se rende compte de la
3.  déconnexion par timeout.
4.  explicit-exit-notify

<!-- -->

1.  L\'adresse du serveur.

remote vpn-loutre.fdn.fr 1194

1.  Éventuellement, on peut avoir besoin de passer par un proxy http,
    décommenter cette ligne en mettant l\'adresse et le port du proxy.
2.  http-proxy 192.0.2.1 8080

<!-- -->

1.  Pour windows: utiliser route.exe.

route-method exe

1.  Attendre un peu avant d\'ajouter les routes.

route-delay 2

1.  Ne pas utiliser un port local statique, on est client de toutes
    façons.

nobind

1.  Garder la clé en mémoire, pour ne pas avoir besoin de la relire lors
    d\'un
2.  redémarrage.

persist-key

1.  On peut éventuellement ne pas tuer l\'interface du tunnel lors d\'un
    redémarrage, mais cela pose problème si au redémarrage on change de
    serveur.
2.  persist-tun

<!-- -->

1.  Décommenter cette ligne pour faire passer tout le trafic via le VPN:

redirect-gateway def1

1.  On peut aussi vouloir plutôt router seulement quelques destinations,
    par
2.  exemple ici tout Gitoyen:
3.  route 80.67.160.0 255.255.224.0

<!-- -->

1.  Décommenter cette ligne pour activer IPv6

tun-ipv6

1.  et décommenter cette ligne pour faire passer tout le trafic IPv6 via
    le VPN:

route-ipv6 ::/1 route-ipv6 8000::/1

1.  Envoyer un login et un mot de passe. Pour éviter de taper à la main
    login
2.  et mot de passe, vous pouvez ajouter à droite de \"auth-user-pass\"
    le nom d\'un
3.  fichier contenant ces deux informations, une par ligne.

auth-user-pass

1.  Un minimum de debug, c\'est toujours bien.

verb 3

1.  Certificat permettant de vérifier que c\'est bien à FDN que
2.  l\'on se connecte et donc à qui on donne notre mot de passe.

<ca> \-\-\-\--BEGIN CERTIFICATE\-\-\-\--
MIIFvTCCA6WgAwIBAgIJAOK7h7j3sSxsMA0GCSqGSIb3DQEBBQUAMHUxCzAJBgNV
BAYTAkZSMQ8wDQYDVQQIDAZGcmFuY2UxDDAKBgNVBAoMA0ZETjEaMBgGA1UEAwwR
dnBuLWxvdXRyZS5mZG4uZnIxKzApBgkqhkiG9w0BCQEWHHNhbXVlbC50aGliYXVs
dEBlbnMtbHlvbi5vcmcwHhcNMTQwODI0MTQ0NjMxWhcNNDIwMTA5MTQ0NjMxWjB1
MQswCQYDVQQGEwJGUjEPMA0GA1UECAwGRnJhbmNlMQwwCgYDVQQKDANGRE4xGjAY
BgNVBAMMEXZwbi1sb3V0cmUuZmRuLmZyMSswKQYJKoZIhvcNAQkBFhxzYW11ZWwu
dGhpYmF1bHRAZW5zLWx5b24ub3JnMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIIC
CgKCAgEAqgx9rccV4qefPAIJU0x9OF2JkYJf5Mvdx1L0Gxo1Dm3O2cUpUR74Nw79
SnHofld7ZtUWOWGAWU+hc47YQo6C6+KEcL+10y4rKDD7rrq+HkcCP20wfVGDARSN
8D+8tND1mG4iX7+NuSGOMhcrGnTxzqCfx8t0oZFWZtbVHBvGH4QqmqjO9wpM+BGh
sYFh0pWXTHP652lA0alXB/M5QHmIVl+yHqP9fwgNpkndZOFrXtpMHHSDdvA67eLM
QLWoFYxUkY4/J1DZ6/Z4PnHz/zTD3EkgKnECICzqXJMak8gWrCgCNibJGyPuGSFs
9BHLgJ+zCzfemIvURcYMQAFQVswyrVl48gQiY1+N/wD/QnlOpArLXrtc+5ms7ljc
9qrGzMkf7uOCX5oH61qTBNyGwhB7vw/KRoXLdmYkNop4+zD+MW6kJy/8e9SEBHVW
mWsSnDuWUinDRmoXfTqMLwPWZqjk8t9HWhbxPwQFm4Y6TEQ4pTBaoCPMZTixZri5
tHzcuNwzLehCQwqmb0TV/UqfS3GP5PDZ3dGAIKO1JFNnXCL8qk3GcwAbcWP8gIY9
9w1Rx0j5vwfXRS58JBfpVxUMb9X5b+khvVHAd5l1CG04xT+Fi2YBOwXs5g7cijZj
TW7HZMRXin6piC33lkd6V39zu9VmLpeomYcyxxLBYsBIooEn8OUCAwEAAaNQME4w
HQYDVR0OBBYEFBPxbz3PUZVp1VEd8TY0Lhn0M+rsMB8GA1UdIwQYMBaAFBPxbz3P
UZVp1VEd8TY0Lhn0M+rsMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggIB
AJzLyo5ADhtNS8wZm9IQM4upDNh8UFsIxqVyrPLS3ncqIden2kz67BySfxZXaMBm
YHzbS95VFN1SN3XavD37+gx6l6GyQrHUHvmD060pRGFwd+DMrBVG8iLSNE7rT/Vm
Tpi0tOw788Zl6c2kY6iT5wKwmxWlQO7ByszwEIiSpBrrotkhqoqW1qhWvVoRSq5v
qI4uk15kOLxrrvLM3tt+8NeUsmoxPzxr16js1GUMaIQIvfm6AM/yTmwtRcMWBi3p
8RIxqQtBYhb+JZaqtyjs0MsYPehqEf7EFKR8nXFDIZgFeWUdXflzfC3wahw7iBBl
ErSWTyfr22q24UVzVcCFF8VdSLcBIbl6fRXnej1CCKjCSZcP2RgAJ97mKLBUlI6D
Rmji76OlYbOVwmaqav0qSsiJ6jE4vswkpKp24FUThalULJ7FpN6M3IEN+R/7L8WD
2in5jvOBMUvKVpfZLR9y7zhHAZtPjTAAFKfFeGKVoh06LTU1lFMBbabD5j1GVuA3
85pvO3ToJGABQiGiUJrr966O+F5icgd4m+r7BBgXIjCh67tVExbEXIqHkx7oRpHI
t0R6JSDXgLXKaU7wAii+qK3AKuctjummaUkTcucNwT4rFbGkSx3uGM3MVrVjDZjj
PmEC82gcBuZgg6MN8xDRS9Vdq6SOB7k6ojksYnc3mtLK \-\-\-\--END
CERTIFICATE\-\-\-\-- </ca>

1.  8\<\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\--
2.  Copier-coller jusqu\'ici.

~~~
