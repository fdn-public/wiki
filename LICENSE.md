# License
- Le fichier support/doc/howto_ftth.md est sous license cc-by-sa 3.0
  (http://creativecommons.org/licenses/by-sa/3.0/). Ça à été vérifié
  en demandeant aux personnes contributrices, et la personne qui à
  poussé les modifications est aussi d'accord.
- Un certain nombre de commits ont importés des données venant de
  l'ancien wiki:
  - df172b004abaa92a8ca2fa5e90257f60e8cc9e5e ("Ajout images ancien wiki")
  - 265776a9b0f7f67b2a18c166e69239c042a3003d ("Correction liens + ajout
    images ancien wiki")
  - d113ee8bf44f5678129f07d5ebdf224ef7d2d28b ("On bouge dans le wiki
    public les dossiers et fichiers qui étaient accessibles publiquement
    dans l'ancien wiki")
  et le fait que ça vienne ou pas de l'ancien wiki n'est pas forcément
  mentionné explicitement dans tous les messages de commit.

  Donc tout ce wiki (sauf mentions contraires) utilise la même license:
  cc-by-sa 3.0 (http://creativecommons.org/licenses/by-sa/3.0/).