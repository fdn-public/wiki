# Le wiki public de FDN

Bienvenue sur le wiki public de l'association French Data Network (FDN)

![Logo FDN](/support/faq/images/sticker_ubuntu_party_300px.webp "Logo FDN")

Vous y trouverez notamment : une présentation de FDN, la FAQ du support, de la documentation, des tutoriels et des informations pour les nouvelles personnes adhérentes à FDN, etc.

## Informations sur FDN

- [Présentation de FDN](https://www.fdn.fr/asso/) (lien externe)
- [Nos services](https://www.fdn.fr/services/) (lien externe)
- [Nos actions](https://www.fdn.fr/actions/) (lien externe)
- [Nos projets](https://www.fdn.fr/projets/) (lien externe)
- [Notre actualité](https://www.fdn.fr/category/actus/) (lien externe)
- [Comment participer ?](https://www.fdn.fr/participer/) (lien externe)
- [L'espace adhérent-e](https://vador.fdn.fr/adherents/) (lien externe)
- [Le wiki réservé aux membres de l'association](https://git.fdn.fr/fdn/wiki/) (lien externe)


## Aide et contact

- [La météo du réseau](https://status.fdn.fr/) (lien externe)
- [La FAQ du support de FDN](support/README.md)
- [En panne ? Je réalise mon auto-diagnostic](support/faq/pas_de_connexion.md)
- [Je viens d'adhérer : mes premiers repères](support/faq/nouvelle%20personne%20adhérente.md)
- [Où trouver mes identifiants de connexion ?](support/faq/login_adhacc-42.md) 
- [Nous contacter](https://www.fdn.fr/contact/) (lien externe)

## Documentation technique et tutoriels

- [Notre lexique](lexique.md)
- [Documentation sur la fibre (FTTH)](support/doc/howto_ftth.md)
- [Documentation sur les VPN](vpn/README.md)

## À propos du Wiki

- [Un wiki à FDN, pour quoi faire ?](https://www.fdn.fr/le-wiki-de-fdn/) (lien externe)
- [Comment se connecter au wiki réservé aux membres de l'association ?](support/faq/acces_wiki.md) 
- [Contribuer ou modifier une page du wiki](https://git.fdn.fr/fdn/wiki/-/blob/master/pages/accueil-communication/doc/tuto%20wiki.md) (lien externe)

## Amélioration

Vous pouvez nous aider à améliorer l'accessibilité du site en nous signalant les problèmes éventuels que vous rencontrez.  
Pour ce faire, envoyez un courriel à [l'équipe communication](mailto:communication@fdn.fr).